# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 4                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD

M = MOD('example_4_p2', 'example_4_mesh', 'E_t', p=2,
        overwrite=True, m_dir='./meshes', r_dir='./results')

# define frequency (11.4 Hz) and condcutivities
M.MP.update_model_parameters(omega=71.63, sigma_anom=[1e-1],
                             sigma_ground=1e-4, sigma_air=1e-8,
                             eps_for_coord_search=1e-6)

# let FEniCS set up the variational formulations
M.FE.build_var_form(s_type='loop_r',
                    start=[-5e2, -5e2, 0.],   # z_values are ignored if topo\
                    stop=[5e2, 5e2, 0.])      # is not *None*

# call solver and convert H-fields
M.solve_main_problem()

# load existing model only for interpolation purposes
M = MOD('example_4_p2', 'example_4_mesh', 'E_t', p=2, overwrite=False,
        load_existing=True, m_dir='./meshes', r_dir='./results',
        field_selection='')

M.IB.create_slice_meshes('z', dim=2e3, n_segs=100, z=-0.1,  # 10cm below surf.
                         slice_name='surface')
M.IB.create_slice_meshes('z', dim=2e3, n_segs=100, z=50.,
                         slice_name='surface_50m_above')
slicE_0 = 'surface_slice_z'
slicE_50 = 'surface_50m_above_slice_z'

# interpolate fields on the observation slice
quantities = ['H_t', 'E_t']
for q in quantities:
    M.IB.interpolate(q, slicE_0)
    M.IB.interpolate(q, slicE_50)
M.IB.synchronize()
