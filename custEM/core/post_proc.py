# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import json
from custEM.misc import mpi_print as mpp
from custEM.misc import max_mem
from custEM.misc import write_h5
from custEM.core import Solver


class PostProcessing:

    """
    PostProcessing class for computing and exporting electric and magnetic
    fields, called from MOD instance.

    class internal functions:
    -------------------------

    - convert_results()
        convert main solution to other field quantities, e.g., E to H

    - export_config_file()
        export model paramter file using JSON

    - export_E_fields()
        export total and/or secondary electric field data

    - export_H_fields()
        export total and/or secondary magnetic field data

    - export_A_fields()
        export total and/or secondary potential 'field' data

    - export_all_results()
        export all available EM-field and/or potential data

    - save_field()
        utility function for writing data on hard drive

    - write_h5()
        utility export function for **h5** data files
    """

    def __init__(self, FE, export_domains, fs_type):

        """
        Initializes instance for converting and exporting postprocessing
        quantities and saves the domains of the mesh to '... Domains.pvd'.

        Required arguments:
        -------------------

        - FS, type class
            FunctionSpaces instance

        - MP, type class
            ModelParameters instance

        - FE, type class
            FE-approach (e.g., **E_vector**) instance
        """

        self.FS = FE.FS
        self.MP = FE.MP
        self.FE = FE
        self.MP.n_tx = self.FE.n_tx
        self.add_tx_suffix = False
        self.full_name = self.MP.out_dir + '/' + self.MP.out_name
        self.fs_type = fs_type
        if df.MPI.rank(self.MP.mpi_cw) == 0:
            if not os.path.isdir(self.MP.out_dir):
                os.makedirs(self.MP.out_dir)
        else:
            pass

        mpp('-->  {:<22}'.format('export directory') + '  =  ' +
            self.MP.out_dir, pre_dash=False)
        mpp('-->  {:<22}'.format('export name') + '  =  ' + self.MP.out_name +
            '_*E/H*_*real/imag*_*xml/h5/pvd*', pre_dash=False)
        if export_domains:
            mpp('...  exporting "' + self.MP.out_name + '_Domains.pvd" '
                'for model validation  ...', pre_dash=False)
            df.File(self.full_name + "_Domains.pvd") << FE.FS.DOM.domain_func

        self.dx_0 = self.FS.DOM.dx_0
        self.Solver = Solver(self.FS, self.FE)

        self.H_t_r, self.H_t_i = [], []
        self.H_t_r_cg, self.H_t_i_cg = [], []
        self.E_t_r, self.E_t_i = [], []
        self.E_t_r_cg, self.E_t_i_cg = [], []
        self.A_t_r, self.A_t_i = [], []
        self.A_t_r_cg, self.A_t_i_cg = [], []
        self.Phi_r, self.Phi_i = [], []
        self.F_s_r, self.F_s_i = [], []
        self.Om_r, self.Om_i = [], []

    def convert_results(self, convert_to_H=True, convert_to_E=False, bcs=None,
                        export_cg=False, export_pvd=True,
                        export_nedelec=True, write_config=True):

        """
        automatically convert results from E-fields to H-fields or vice versa
        or from potentials A-Phi to E or H, depending on the utilized approach

        Keyword arguments:
        ------------------

        - convert_to_H = True, type bool
            calculate H-fields for E-field or potential approaches

        - convert_to_E = True, type bool
            calculate E-fields, if H-field approach was used

        - bcs = None, type str
            specify boundary conditions for conversion, default are
            *homogeneous Neumann* BC.

        - export_cg = True, type bool
            specify if fields in data format should be exported directly
            after conversion

        - export_pvd = True, type bool
            specify if fields are also exported in *.pvd* format for *Paraview*

        - export_nedelec = True, type bool
            set to **True** for exporting caluclated data on a NedelecSpace

        - write_config, type bool
            flag controling the export of a config file containing relevant
            model parameters using JSON
        """

        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)
        self.M = df.PETScMatrix()
        self.w1 = [df.PETScVector() for j in range(self.FE.n_tx)]
        self.w2 = [df.PETScVector() for j in range(self.FE.n_tx)]

        if self.MP.approach == 'DC':
            self.dc_post_proc()
            return

        if self.FE.n_tx > 1:
            self.add_tx_suffix = True

        if type(self.FS.U) is not list:
            self.FS.U = [self.FS.U]
            mpp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            mpp('Warning! Currently only processing first Tx in postproc!')
            mpp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

        if write_config:
            self.export_config_file()
            self.export_resource_file()

        m_r = df.TrialFunction(self.FS.V_cg)
        m_i = df.TrialFunction(self.FS.V_cg)
        n_r = df.TestFunction(self.FS.V_cg)
        n_i = df.TestFunction(self.FS.V_cg)
        om = self.MP.omega

        if '_t' in self.MP.approach:
            if 'E' in self.MP.approach:
                for ti in range(self.FE.n_tx):
                    E_t_r, E_t_i = self.FS.U[ti].split(True)
                    self.E_t_r.append(E_t_r)
                    self.E_t_i.append(E_t_i)
                    self.E_t_r_cg.append(df.interpolate(E_t_r, self.FS.V_cg))
                    self.E_t_i_cg.append(df.interpolate(E_t_i, self.FS.V_cg))

            elif 'H' in self.MP.approach:
                for ti in range(self.FE.n_tx):
                    H_t_r, H_t_i = self.FS.U[ti].split(True)
                    self.H_t_r.append(H_t_r)
                    self.H_t_i.append(H_t_i)
                    self.H_t_r_cg.append(df.interpolate(H_t_r, self.FS.V_cg))
                    self.H_t_i_cg.append(df.interpolate(H_t_i, self.FS.V_cg))

                # NEED TO IMPLEMENT correct sigma distribution SIGMA HERE! #
                mpp('Warning! Calculating E with H_t-field approach is not '
                    'implemented right now - setting E to ZERO function!')
                if convert_to_E:
                    for ti in range(self.FE.n_tx):
                        self.E_t_r.append(df.Function(self.FS.V))
                        self.E_t_i.append(df.Function(self.FS.V))
                        self.E_t_r_cg.append(df.Function(self.FS.V_cg))
                        self.E_t_i_cg.append(df.Function(self.FS.V_cg))
                    self.export_E_fields(export_pvd, export_cg, export_nedelec)
            else:
                ar = df.inner(m_r, n_r) * self.dx_0
                ai = df.inner(m_i, n_i) * self.dx_0
                X, Y = [], []
                M2 = df.PETScMatrix()
                w3 = [df.PETScVector() for j in range(self.FE.n_tx)]
                w4 = [df.PETScVector() for j in range(self.FE.n_tx)]

                for ti in range(self.FE.n_tx):
                    if 'An' in self.MP.approach:
                        self.A_t_r_cg.append(df.Function(self.FS.V_cg))
                        self.A_t_i_cg.append(df.Function(self.FS.V_cg))
                        (A_x_r, A_y_r, A_z_r, Phi_r, A_x_i, A_y_i,
                         A_z_i, Phi_i) = self.FS.U[ti].split(True)
                        df.assign(self.A_t_r_cg[ti].sub(0), A_x_r)
                        df.assign(self.A_t_i_cg[ti].sub(0), A_x_i)
                        df.assign(self.A_t_r_cg[ti].sub(1), A_y_r)
                        df.assign(self.A_t_i_cg[ti].sub(1), A_y_i)
                        df.assign(self.A_t_r_cg[ti].sub(2), A_z_r)
                        df.assign(self.A_t_i_cg[ti].sub(2), A_z_i)
                        self.Phi_r.append(Phi_r)
                        self.Phi_i.append(Phi_i)
                        A_t_r = df.interpolate(self.A_t_r_cg[ti], self.FS.V)
                        A_t_i = df.interpolate(self.A_t_i_cg[ti], self.FS.V)
                        self.A_t_r.append(A_t_r)
                        self.A_t_i.append(A_t_i)

                    elif 'Am' in self.MP.approach:
                        (A_t_r, A_t_i, Ph_r, Ph_i) = self.FS.U[ti].split(True)
                        self.A_t_r.append(A_t_r)
                        self.A_t_i.append(A_t_i)
                        self.Phi_r.append(Ph_r)
                        self.Phi_i.append(Ph_i)
                        self.A_t_r_cg.append(df.interpolate(self.A_t_r[ti],
                                                            self.FS.V_cg))
                        self.A_t_i_cg.append(df.interpolate(self.A_t_i[ti],
                                                            self.FS.V_cg))

                    RHS1 = -(df.inner(self.A_t_i_cg[ti],
                                      om * n_r) * self.dx_0 +
                             df.inner(df.grad(self.Phi_i[ti]),
                                      om * n_r) * self.dx_0)
                    RHS2 = (df.inner(self.A_t_r_cg[ti],
                                     om * n_i) * self.dx_0 +
                            df.inner(df.grad(self.Phi_r[ti]),
                                     om * n_i) * self.dx_0)

                    H1, b1 = df.assemble_system(ar, RHS1, A_tensor=M2,
                                                b_tensor=w3[ti])
                    H2, b2 = df.assemble_system(ai, RHS2, A_tensor=M2,
                                                b_tensor=w4[ti])
                    X.append(b1)
                    Y.append(b2)

                self.E_t_r_cg = self.Solver.solve_system_mumps(
                        H1, X, self.FS.V_cg, sym=True, mute=True)
                self.E_t_i_cg = self.Solver.solve_system_mumps(
                        H2, Y, self.FS.V_cg, sym=True, mute=True,
                        first_call=False)

                for ti in range(self.FE.n_tx):
                    self.E_t_r.append(df.interpolate(self.E_t_r_cg[ti],
                                                     self.FS.V))
                    self.E_t_i.append(df.interpolate(self.E_t_i_cg[ti],
                                                     self.FS.V))

        if '_s' in self.MP.approach:
            if self.FS.p != self.FS.PF.pf_p:
                mpp('Notice! Converting primary fields from p2 to p1 or vice '
                    'versa for conforming summation of vectors.')
                self.FS.PF.E_0_r_cg2, self.FS.PF.E_0_i_cg2 = [], []
                self.FS.PF.H_0_r_cg2, self.FS.PF.H_0_i_cg2 = [], []
                for ti in range(self.FE.n_tx):
                    self.FS.PF.E_0_i_cg2.append(df.interpolate(
                            self.FS.PF.E_0_i_cg[ti], self.FS.V_cg))
                    self.FS.PF.E_0_r_cg2.append(df.interpolate(
                            self.FS.PF.E_0_r_cg[ti], self.FS.V_cg))
                    self.FS.PF.H_0_i_cg2.append(df.interpolate(
                            self.FS.PF.H_0_i_cg[ti], self.FS.V_cg))
                    self.FS.PF.H_0_r_cg2.append(df.interpolate(
                            self.FS.PF.H_0_r_cg[ti], self.FS.V_cg))
                self.FS.PF.E_0_r_cg = self.FS.PF.E_0_r_cg2
                self.FS.PF.E_0_i_cg = self.FS.PF.E_0_i_cg2
                self.FS.PF.H_0_r_cg = self.FS.PF.H_0_r_cg2
                self.FS.PF.H_0_i_cg = self.FS.PF.H_0_i_cg2

        if '_s' in self.MP.approach and self.FS.anom_flag:

            self.H_s_r, self.H_s_i = [], []
            self.H_s_r_cg, self.H_s_i_cg = [], []
            self.E_s_r, self.E_s_i = [], []
            self.E_s_r_cg, self.E_s_i_cg = [], []
            self.A_s_r, self.A_s_i = [], []
            self.A_s_r_cg, self.A_s_i_cg = [], []
            self.F_s_r, self.F_s_i = [], []
            self.F_s_r_cg, self.F_s_i_cg = [], []

            if 'E' in self.MP.approach:
                for ti in range(self.FE.n_tx):
                    E_s_r, E_s_i = self.FS.U[ti].split(True)
                    self.E_s_r.append(E_s_r)
                    self.E_s_i.append(E_s_i)
                    self.E_s_r_cg.append(df.interpolate(self.E_s_r[ti],
                                                        self.FS.V_cg))
                    self.E_s_i_cg.append(df.interpolate(self.E_s_i[ti],
                                                        self.FS.V_cg))
                    self.E_t_r_cg.append(df.Function(self.FS.V_cg))
                    self.E_t_i_cg.append(df.Function(self.FS.V_cg))
                    self.E_t_r_cg[ti].vector()[:] = self.E_s_r_cg[ti].vector(
                            ).get_local() + self.FS.PF.E_0_r_cg[ti].vector(
                                    ).get_local()
                    self.E_t_i_cg[ti].vector()[:] = self.E_s_i_cg[ti].vector(
                            ).get_local() + self.FS.PF.E_0_i_cg[ti].vector(
                                    ).get_local()
                    E_0_r = df.interpolate(self.FS.PF.E_0_r_cg[ti], self.FS.V)
                    E_0_i = df.interpolate(self.FS.PF.E_0_i_cg[ti], self.FS.V)
                    self.E_t_r.append(df.Function(self.FS.V))
                    self.E_t_i.append(df.Function(self.FS.V))
                    self.E_t_r[ti].vector()[:] = E_0_r.vector().get_local() +\
                        E_s_r.vector().get_local()
                    self.E_t_i[ti].vector()[:] = E_0_i.vector().get_local() +\
                        E_s_i.vector().get_local()

            elif 'H' in self.MP.approach:
                for ti in range(self.FE.n_tx):
                    H_s_r, H_s_i = self.FS.U[ti].split(True)
                    self.H_s_r.append(H_s_r)
                    self.H_s_i.append(H_s_i)
                    self.H_s_r_cg.append(df.interpolate(self.H_s_r[ti],
                                                        self.FS.V_cg))
                    self.H_s_i_cg.append(df.interpolate(self.H_s_i[ti],
                                                        self.FS.V_cg))
                    self.H_t_r_cg.append(df.Function(self.FS.V_cg))
                    self.H_t_i_cg.append(df.Function(self.FS.V_cg))
                    self.H_t_r_cg[ti].vector()[:] = self.H_s_r_cg[ti].vector(
                            ).get_local() + self.FS.PF.H_0_r_cg[ti].vector(
                                    ).get_local()
                    self.H_t_i_cg[ti].vector()[:] = self.H_s_i_cg[ti].vector(
                            ).get_local() + self.FS.PF.H_0_i_cg[ti].vector(
                                    ).get_local()
                    H_0_r = df.interpolate(self.FS.PF.H_0_r_cg[ti], self.FS.V)
                    H_0_i = df.interpolate(self.FS.PF.H_0_i_cg[ti], self.FS.V)
                    self.H_t_r.append(df.Function(self.FS.V))
                    self.H_t_i.append(df.Function(self.FS.V))
                    self.H_t_r[ti].vector()[:] = H_0_r.vector().get_local() +\
                        H_s_r.vector().get_local()
                    self.H_t_i[ti].vector()[:] = H_0_i.vector().get_local() +\
                        H_s_i.vector().get_local()

                if convert_to_E:
                    self.convert_H_to_E()
                    self.export_E_fields(export_pvd, export_cg,
                                         export_nedelec)
                    mpp('...  finished.')

            else:
                ar = df.inner(m_r, n_r) * self.dx_0
                ai = df.inner(m_i, n_i) * self.dx_0
                X, Y = [], []
                M2 = df.PETScMatrix()
                w3 = [df.PETScVector() for j in range(self.FE.n_tx)]
                w4 = [df.PETScVector() for j in range(self.FE.n_tx)]

                for ti in range(self.FE.n_tx):
                    if 'An' in self.MP.approach:
                        self.A_s_r_cg.append(df.Function(self.FS.V_cg))
                        self.A_s_i_cg.append(df.Function(self.FS.V_cg))
                        (A_x_r, A_y_r, A_z_r, Phi_r, A_x_i, A_y_i,
                         A_z_i, Phi_i) = self.FS.U[ti].split(True)
                        df.assign(self.A_s_r_cg[ti].sub(0), A_x_r)
                        df.assign(self.A_s_i_cg[ti].sub(0), A_x_i)
                        df.assign(self.A_s_r_cg[ti].sub(1), A_y_r)
                        df.assign(self.A_s_i_cg[ti].sub(1), A_y_i)
                        df.assign(self.A_s_r_cg[ti].sub(2), A_z_r)
                        df.assign(self.A_s_i_cg[ti].sub(2), A_z_i)
                        self.Phi_r.append(Phi_r)
                        self.Phi_i.append(Phi_i)
                        A_s_r = df.interpolate(self.A_s_r_cg[ti], self.FS.V)
                        A_s_i = df.interpolate(self.A_s_i_cg[ti], self.FS.V)
                        self.A_s_r.append(A_s_r)
                        self.A_s_i.append(A_s_i)

                    elif 'Am' in self.MP.approach:
                        (A_s_r, A_s_i, Ph_r, Ph_i) = self.FS.U[ti].split(True)
                        self.A_s_r.append(A_s_r)
                        self.A_s_i.append(A_s_i)
                        self.Phi_r.append(Ph_r)
                        self.Phi_i.append(Ph_i)
                        self.A_s_r_cg.append(df.interpolate(self.A_s_r[ti],
                                                            self.FS.V_cg))
                        self.A_s_i_cg.append(df.interpolate(self.A_s_i[ti],
                                                            self.FS.V_cg))

                    elif 'Fm' in self.MP.approach:
                        (F_s_r, F_s_i, Om_r, Om_i) = self.FS.U[ti].split(True)
                        self.F_s_r.append(F_s_r)
                        self.F_s_i.append(F_s_i)
                        self.Om_r.append(Om_r)
                        self.Om_i.append(Om_i)
                        self.F_s_r_cg.append(df.interpolate(self.F_s_r[ti],
                                                            self.FS.V_cg))
                        self.F_s_i_cg.append(df.interpolate(self.F_s_i[ti],
                                                            self.FS.V_cg))

                    if 'A' in self.MP.approach:

                        RHS1 = -(df.inner(self.A_s_i_cg[ti], om * n_r) *
                                 self.dx_0 +
                                 df.inner(df.grad(self.Phi_i[ti]), om * n_r) *
                                 self.dx_0)
                        RHS2 = (df.inner(self.A_s_r_cg[ti], om * n_i) *
                                self.dx_0 +
                                df.inner(df.grad(self.Phi_r[ti]), om * n_i) *
                                self.dx_0)

                    elif 'Fm' in self.MP.approach:
                        RHS1 = (df.inner(self.F_s_r_cg[ti], n_r) *
                                self.dx_0 +
                                df.inner(df.grad(self.Om_r[ti]), n_r) *
                                self.dx_0)
                        RHS2 = (df.inner(self.F_s_i_cg[ti], n_i) *
                                self.dx_0 +
                                df.inner(df.grad(self.Om_i[ti]), n_i) *
                                self.dx_0)

                    H1, b1 = df.assemble_system(ar, RHS1, A_tensor=M2,
                                                b_tensor=w3[ti])
                    H2, b2 = df.assemble_system(ai, RHS2, A_tensor=M2,
                                                b_tensor=w4[ti])
                    X.append(b1)
                    Y.append(b2)

                if 'A' in self.MP.approach:
                    self.E_s_r_cg = self.Solver.solve_system_mumps(
                            H1, X, self.FS.V_cg, sym=True, mute=True)
                    self.E_s_i_cg = self.Solver.solve_system_mumps(
                            H2, Y, self.FS.V_cg, sym=True, mute=True,
                            first_call=False)

                    for ti in range(self.FE.n_tx):
                        self.E_t_r_cg.append(df.Function(self.FS.V_cg))
                        self.E_t_i_cg.append(df.Function(self.FS.V_cg))
                        self.A_t_r_cg.append(df.Function(self.FS.V_cg))
                        self.A_t_i_cg.append(df.Function(self.FS.V_cg))
                        self.E_t_r_cg[ti].vector()[:] = (
                            self.E_s_r_cg[ti].vector().get_local(
                            ) + self.FS.PF.E_0_r_cg[ti].vector().get_local())
                        self.E_t_i_cg[ti].vector()[:] = (
                            self.E_s_i_cg[ti].vector().get_local(
                            ) + self.FS.PF.E_0_i_cg[ti].vector().get_local())
                        self.A_t_r_cg[ti].vector()[:] = (
                            self.A_s_r_cg[ti].vector().get_local(
                            ) + self.FS.PF.E_0_i_cg[ti].vector().get_local() *
                            (1. / self.MP.omega))
                        self.A_t_i_cg[ti].vector()[:] = (
                            self.A_s_i_cg[ti].vector().get_local(
                            ) + self.FS.PF.E_0_r_cg[ti].vector().get_local() *
                            (-1. / self.MP.omega))

                        self.E_t_r.append(df.interpolate(self.E_t_r_cg[ti],
                                                         self.FS.V))
                        self.E_t_i.append(df.interpolate(self.E_t_i_cg[ti],
                                                         self.FS.V))
                        self.E_s_r.append(df.interpolate(self.E_s_r_cg[ti],
                                                         self.FS.V))
                        self.E_s_i.append(df.interpolate(self.E_s_i_cg[ti],
                                                         self.FS.V))
                elif 'Fm' in self.MP.approach:
                    self.H_s_r_cg = self.Solver.solve_system_mumps(
                            H1, X, self.FS.V_cg, sym=True, mute=True)
                    self.H_s_i_cg = self.Solver.solve_system_mumps(
                            H2, Y, self.FS.V_cg, sym=True, mute=True,
                            first_call=False)
                    for ti in range(self.FE.n_tx):
                        self.H_t_r_cg.append(df.Function(self.FS.V_cg))
                        self.H_t_i_cg.append(df.Function(self.FS.V_cg))
                        self.H_t_r_cg[ti].vector()[:] = (
                            self.H_s_r_cg[ti].vector().get_local(
                            ) + self.FS.PF.H_0_r_cg[ti].vector().get_local())
                        self.H_t_i_cg[ti].vector()[:] = (
                            self.H_s_i_cg[ti].vector().get_local(
                            ) + self.FS.PF.H_0_i_cg[ti].vector().get_local())

                        self.H_t_r.append(df.interpolate(self.H_t_r_cg[ti],
                                                         self.FS.V))
                        self.H_t_i.append(df.interpolate(self.H_t_i_cg[ti],
                                                         self.FS.V))
                        self.H_s_r.append(df.interpolate(self.H_s_r_cg[ti],
                                                         self.FS.V))
                        self.H_s_i.append(df.interpolate(self.H_s_i_cg[ti],
                                                         self.FS.V))
                    if convert_to_E:

                        dx = self.FS.DOM.dx
                        k_r = df.TrialFunction(self.FS.V)
                        k_i = df.TrialFunction(self.FS.V)
                        l_r = df.TestFunction(self.FS.V)
                        l_i = df.TestFunction(self.FS.V)
                        br = df.inner(k_r, l_r) * dx(0)
                        bi = df.inner(k_i, l_i) * dx(0)
                        Kr = df.inner(df.curl(self.F_s_r[0]) /
                                      self.MP.sigma[0], l_r) * dx(0)
                        Ki = df.inner(df.curl(self.F_s_i[0]) /
                                      self.MP.sigma[0], l_i) * dx(0)
                        for j in range(1, len(self.MP.sigma)):
                            br += df.inner(k_r, l_r) * dx(j)
                            bi += df.inner(k_i, l_i) * dx(j)
                            Kr += df.inner(df.inv(self.MP.sigma[j]) *
                                           df.curl(self.F_s_r[0]), l_r) * dx(j)
                            Ki += df.inner(df.inv(self.MP.sigma[j]) *
                                           df.curl(self.F_s_i[0]), l_i) * dx(j)
                        E1, b1 = df.assemble_system(br, Kr, A_tensor=self.M,
                                                    b_tensor=self.w1[0])
                        E2, b2 = df.assemble_system(bi, Ki, A_tensor=self.M,
                                                    b_tensor=self.w2[0])
                        E_s_r = self.Solver.solve_system_mumps(
                                E1, [b1], self.FS.V, sym=True, mute=True)
                        E_s_i = self.Solver.solve_system_mumps(
                                E2, [b2], self.FS.V, sym=True, mute=True,
                                first_call=False)
                        self.E_s_r.append(E_s_r[0])
                        self.E_s_i.append(E_s_i[0])
                        self.E_s_r_cg.append(df.interpolate(E_s_r[0],
                                                            self.FS.V_cg))
                        self.E_s_i_cg.append(df.interpolate(E_s_i[0],
                                                            self.FS.V_cg))
                        E_t_r_cg = df.Function(self.FS.V_cg)
                        E_t_i_cg = df.Function(self.FS.V_cg)
                        E_t_r = df.Function(self.FS.V)
                        E_t_i = df.Function(self.FS.V)
                        E_0_r = df.interpolate(self.FS.PF.E_0_r_cg[0],
                                               self.FS.V)
                        E_0_i = df.interpolate(self.FS.PF.E_0_i_cg[0],
                                               self.FS.V)
                        E_t_r_cg.vector()[:] = \
                            self.E_s_r_cg[0].vector().get_local() + \
                            self.FS.PF.E_0_r_cg[0].vector().get_local()
                        E_t_i_cg.vector()[:] = \
                            self.E_s_i_cg[0].vector().get_local() + \
                            self.FS.PF.E_0_i_cg[0].vector().get_local()
                        E_t_r.vector()[:] = \
                            E_s_r[0].vector().get_local() + \
                            E_0_r.vector().get_local()
                        E_t_i.vector()[:] = \
                            E_s_i[0].vector().get_local() + \
                            E_0_i.vector().get_local()
                        self.E_t_r_cg.append(E_t_r_cg)
                        self.E_t_i_cg.append(E_t_i_cg)
                        self.E_t_r.append(E_t_r)
                        self.E_t_i.append(E_t_i)
                        self.export_E_fields(export_pvd, export_cg,
                                             export_nedelec)
                mpp('...  finished.')

        elif ('_s' in self.MP.approach and self.FS.anom_flag is False):

            for ti in range(self.FE.n_tx):
                mpp('  -  No anomaly set! -->  Primary Fields exported  -  ',
                    post_dash=True)

                # # # Hacked # # #
                self.FS.PF.E_0_r_cg[ti].vector()[:] += 1e-32
                self.FS.PF.E_0_i_cg[ti].vector()[:] += 1e-32
                self.FS.PF.H_0_r_cg[ti].vector()[:] += 1e-32
                self.FS.PF.H_0_i_cg[ti].vector()[:] += 1e-32
                # # # Hacked # # #

                self.E_t_r_cg.append(self.FS.PF.E_0_r_cg[ti])
                self.E_t_i_cg.append(self.FS.PF.E_0_i_cg[ti])
                self.H_t_r_cg.append(self.FS.PF.H_0_r_cg[ti])
                self.H_t_i_cg.append(self.FS.PF.H_0_i_cg[ti])
            self.export_H_fields(export_pvd, True, export_nedelec)
            self.export_E_fields(export_pvd, True, export_nedelec)

        if 'H' in self.MP.approach or 'F' in self.MP.approach:
            self.export_H_fields(export_pvd, export_cg, export_nedelec)
        else:
            self.export_E_fields(export_pvd, export_cg, export_nedelec)

        if convert_to_H and 'H' not in self.MP.approach and\
           'F' not in self.MP.approach:

            mpp('...  deriving H-fields from E  ...', pre_dash=False)

            u1, v1 = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
            u2, v2 = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
            X, Y = [], []

            if '_t' in self.MP.approach:
                for ti in range(self.FE.n_tx):
                    if 'E' in self.MP.approach:
                        RHS1 = -df.inner(self.E_t_i[ti] / (om * mu),
                                         df.curl(v1)) * self.dx_0
                        RHS2 = df.inner(self.E_t_r[ti] / (om * mu),
                                        df.curl(v2)) * self.dx_0
                    elif 'A' in self.MP.approach:
                        RHS1 = -df.inner(df.curl(self.A_t_r[ti]) /
                                         mu, v1) * self.dx_0
                        RHS2 = -df.inner(df.curl(self.A_t_i[ti]) /
                                         mu, v2) * self.dx_0

                    # ID conditions for H field conversion, might be of
                    # interest at some point, but currently not neccessary

#                   if self.FS.bc_type == 'ZD':
#                       self.FE.FS.add_dirichlet_bc()
#                       mpp('  -  using zero Dirichlet conditions for H-field '
#                           'conversion  -  ')
#                   elif self.FS.bc_type == 'ID':
#                       self.FE.FS.add_dirichlet_bc(MP=self.MP, bc_H=True)
#                       mpp('  -  using inhomogeneous Dirichlet conditions '
#                           ' for H-field conversion  -  ')
#                   else:
#                       self.FE.FS.bc = None

                    H1, b1 = df.assemble_system(
                            df.inner(u1, v1) * self.dx_0, RHS1,
                            A_tensor=self.M, b_tensor=self.w1[ti])
                    H2, b2 = df.assemble_system(
                            df.inner(u2, v2) * self.dx_0, RHS2,
                            A_tensor=self.M, b_tensor=self.w2[ti])

                    X.append(b1)
                    Y.append(b2)

                self.H_t_r = self.Solver.solve_system_mumps(
                        H1, X, self.FS.V, sym=True, mute=True)
                self.H_t_i = self.Solver.solve_system_mumps(
                        H2, Y, self.FS.V, sym=True, mute=True,
                        first_call=False)
                for ti in range(self.FE.n_tx):
                    self.H_t_r_cg.append(df.interpolate(self.H_t_r[ti],
                                                        self.FS.V_cg))
                    self.H_t_i_cg.append(df.interpolate(self.H_t_i[ti],
                                                        self.FS.V_cg))

            if '_s' in self.MP.approach and self.FS.anom_flag is True:
                for ti in range(self.FE.n_tx):
                    if 'E' in self.MP.approach:
                        RHS1 = -df.inner(self.E_s_i[ti] /
                                         (om * mu), df.curl(v1)) * self.dx_0
                        RHS2 = df.inner(self.E_s_r[ti] /
                                        (om * mu), df.curl(v2)) * self.dx_0
                    elif 'A' in self.MP.approach:
                        RHS1 = -df.inner(df.curl(self.A_s_r[ti]) /
                                         mu, v1) * self.dx_0
                        RHS2 = -df.inner(df.curl(self.A_s_i[ti]) /
                                         mu, v2) * self.dx_0
                    H1, b1 = df.assemble_system(
                            df.inner(u1, v1) * self.dx_0,
                            RHS1, A_tensor=self.M, b_tensor=self.w1[ti])
                    H2, b2 = df.assemble_system(
                            df.inner(u2, v2) * self.dx_0,
                            RHS2, A_tensor=self.M, b_tensor=self.w2[ti])
                    X.append(b1)
                    Y.append(b2)

                self.H_s_r = self.Solver.solve_system_mumps(
                        H1, X, self.FS.V, sym=True, mute=True)
                self.H_s_i = self.Solver.solve_system_mumps(
                        H2, Y, self.FS.V, sym=True, mute=True,
                        first_call=False)
                for ti in range(self.FE.n_tx):
                    self.H_s_r_cg.append(df.interpolate(self.H_s_r[ti],
                                                        self.FS.V_cg))
                    self.H_s_i_cg.append(df.interpolate(self.H_s_i[ti],
                                                        self.FS.V_cg))

                    self.H_t_r_cg.append(df.Function(self.FS.V_cg))
                    self.H_t_i_cg.append(df.Function(self.FS.V_cg))
                    self.H_t_r_cg[ti].vector()[:] = self.H_s_r_cg[ti].vector(
                            ).get_local(
                            ) + self.FS.PF.H_0_r_cg[ti].vector().get_local()
                    self.H_t_i_cg[ti].vector()[:] = self.H_s_i_cg[ti].vector(
                            ).get_local(
                            ) + self.FS.PF.H_0_i_cg[ti].vector().get_local()
                    H_0_r = df.interpolate(self.FS.PF.H_0_r_cg[ti], self.FS.V)
                    H_0_i = df.interpolate(self.FS.PF.H_0_i_cg[ti], self.FS.V)
                    self.H_t_r.append(df.Function(self.FS.V))
                    self.H_t_i.append(df.Function(self.FS.V))
                    self.H_t_r[ti].vector()[:] = H_0_r.vector().get_local() +\
                        self.H_s_r[ti].vector().get_local()
                    self.H_t_i[ti].vector()[:] = H_0_i.vector().get_local() +\
                        self.H_s_i[ti].vector().get_local()

            self.export_H_fields(export_pvd, export_cg, export_nedelec)
            mpp('...  finished.', post_dash=True)

    def export_config_file(self):

        """
        write model parameters - mainly MP and FE instance dictionaries -
        to file in the specified export directory (*out_dir*) using JSON
        """

        if df.MPI.rank(self.MP.mpi_cw) == 0:
            A = self.MP.__dict__.copy()
            if A['tensor_flag']:
                del A['sigma']
                del A['sigma_anom']
                del A['sigma_air']
                del A['sigma_ground']
                del A['delta_sigma']
                try:
                    del A['path']
                except KeyError:
                    pass
            del A['mpi_cw']
            del A['mpi_cs']
            A['topo'] = str(A['topo'])
            A['bc'] = self.FE.__dict__['bc']
            A['pf_type'] = self.FE.__dict__['pf_type']
            A['s_type'] = self.FE.__dict__['s_type']
            A['closed_path'] = self.FE.__dict__['closed_path']
            A['pvd_flag'] = self.FE.__dict__['pvd_flag']
            A['origin'] = self.FE.__dict__['origin']
            A['r'] = self.FE.__dict__['r']
            A['start'] = self.FE.__dict__['start']
            A['stop'] = self.FE.__dict__['stop']
            A['length'] = self.FE.__dict__['length']
            A['azimuth'] = self.FE.__dict__['azimuth']
            A['min_length'] = self.FE.__dict__['min_length']
            A['n_segs'] = self.FE.__dict__['n_segs']

            with open(self.full_name + "_config.json", "w") as outfile:
                json.dump(A, outfile, indent=0)
        else:
            pass
        mpp('...  parameter file "*mod_name*_config.json" '
            'dumped to export directory.', pre_dash=False)

    def export_resource_file(self):

        """
        write consumed computational resources and times to file in the
        specified export directory (*out_dir*) using JSON
        """

        max_memory = max_mem(for_dump=True)
        if df.MPI.rank(self.MP.mpi_cw) == 0:
            outfile = open(self.full_name + '_resource.txt', 'w')
            outfile.write('#'*80 + '\n')
            outfile.write(' '*12 + 'Maximum RAM requirement [GB]:  -->  ' +
                          str(int(max_memory) + 1) + '\n')
            outfile.write('#'*80 + '\n')
            if '_t' in self.MP.approach:
                outfile.write(' '*12 + 'System assembly time [s]:  -->  ' +
                              str(int(self.FE.assembly_time)) + '\n')
                outfile.write('#'*80 + '\n')
                outfile.write(' '*12 + 'FE main system solution time  [s]:  --'
                              '>  ' + str(int(self.FS.solution_time)) + '\n')
            elif '_s' in self.MP.approach:
                outfile.write(' '*12 + 'Combined FE system assembly and '
                              'solution time [s]:  -->  ' +
                              str(int(self.FS.solution_time)) + '\n')
            outfile.write('#'*80)
            outfile.close()
        else:
            pass
        mpp('...  resource file "*mod_name*_resource.txt" '
            'dumped to export directory.', pre_dash=False)

    def export_E_fields(self, export_pvd, export_cg, export_nedelec):

        """
        export electric fields using specified data (*xml/h5*) and/or *pvd*
        format from a Lagrange VectorFunctionSpace

        Required arguments:
        -------------------

        - export_pvd, type bool
            specify if fields are also exported in *.pvd* format for *Paraview*

        - export_cg, type bool
            specify if fields in data format should be exported directly
            after conversion

        - export_nedelec = True, type bool
            set to **True** for exporting caluclated data on a NedelecSpace
        """

        mpp('...  exporting E-fields for Paraview  ...', pre_dash=False)

        try:
            self.E_t_r_cg
        except AttributeError:
            mpp('Warning! "E_t_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_E_t', self.E_t_r_cg, self.E_t_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.E_t_r
        except AttributeError:
            mpp('Warning! "E_t_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_E_t', self.E_t_r, self.E_t_i, False, False,
                            export_nedelec=export_nedelec)

        try:
            self.E_s_r_cg
        except AttributeError:
            if '_s' in self.FE.FS.approach:
                mpp('Warning! "E_s_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_E_s', self.E_s_r_cg, self.E_s_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.E_s_r
        except AttributeError:
            if '_s' in self.FE.FS.approach:
                mpp('Warning! "E_s_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_E_s', self.E_s_r, self.E_s_i, False, False,
                            export_nedelec=export_nedelec)

    def export_H_fields(self, export_pvd, export_cg, export_nedelec):

        """
        export magnetic fields using specified data (*xml/h5*) and/or *pvd*
        format from a Lagrange VectorFunctionSpace.

        Required arguments:
        -------------------

        - export_pvd, type bool
            specify if fields are also exported in *.pvd* format for *Paraview*

        - export_cg, type bool
            specify if fields in data format should be exported directly
            after conversion

        - export_nedelec = True, type bool
            set to **True** for exporting caluclated data on a NedelecSpace
        """

        mpp('...  exporting H-fields for Paraview ...', pre_dash=False)

        try:
            self.H_t_r_cg
        except AttributeError:
            mpp('Warning! "H_t_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_H_t', self.H_t_r_cg, self.H_t_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.H_t_r
        except AttributeError:
            mpp('Warning! "H_t_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_H_t', self.H_t_r, self.H_t_i, False, False,
                            export_nedelec=export_nedelec)

        try:
            self.H_s_r_cg
        except AttributeError:
            if '_s' in self.FE.FS.approach:
                mpp('Warning! "H_s_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_H_s', self.H_s_r_cg, self.H_s_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.H_s_r
        except AttributeError:
            if '_s' in self.FE.FS.approach:
                mpp('Warning! "H_s_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_H_s', self.H_s_r, self.H_s_i, False, False,
                            export_nedelec=export_nedelec)

    def export_A_fields(self, export_pvd, export_cg,
                        export_nedelec=True):

        """
        export potentials using specified data (*xml/h5*) and/or *pvd*
        format from a Lagrange VectorFunctionSpace

        Required arguments:
        -------------------

        - export_cg, type bool
            specify if fields in data format should be exported directly
            after conversion

        - export_pvd, type bool
            specify if fields are also exported in *.pvd* format for *Paraview*

         - export_nedelec = True, type bool
            set to **True** for exporting caluclated data on a NedelecSpace
        """

        mpp('...  exporting A-potentials  ...', pre_dash=False)

        try:
            self.A_t_r_cg
        except AttributeError:
            mpp('Warning! "A_t_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_A_t', self.A_t_r_cg, self.A_t_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.A_t_r
        except AttributeError:
            mpp('Warning! "A_t_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_A_t', self.A_t_r, self.A_t_i, False, False,
                            export_nedelec=export_nedelec)

        try:
            self.A_s_r_cg
        except AttributeError:
            mpp('Warning! "A_s_r_cg" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_A_s', self.A_s_r_cg, self.A_s_i_cg, export_pvd,
                            export_cg, export_nedelec=False)

        try:
            self.A_s_r
        except AttributeError:
            mpp('Warning! "A_s_r" not found in "PP". Continuing  ...')
            pass
        else:
            self.save_field('_A_s', self.A_s_r, self.A_s_i, False, False,
                            export_nedelec=export_nedelec)

#        Exporting scalar potentials would need to be fixed at some point
#        if users are interested in that.

#        try:
#            self.Phi_r
#        except AttributeError:
#            pass
#        else:
#            if export_pvd:
#                df.File(self.full_name + '_Phi_real.pvd') << self.Phi_r
#                df.File(self.full_name + '_Phi_imag.pvd') << self.Phi_i
#            if export_cg:
#                mpp('TO DO!, need to enable export of scalar Phi data ...')
#                # df.File(out_name + '_Phi_real.xml') << self.Phi_r
#                # df.File(out_name + '_Phi_imag.xml') << self.Phi_i
#                pass

    def export_all_results(self, export_cg=False, export_pvd=True,
                           quantities='EAH', export_nedelec=True):

        """
        export a selection of calculated electric, magnetic or potential fields
        using specified data (*xml/h5*) and/or *pvd* format

        Keyword arguments:
        ------------------

        - export_cg = False, type bool
            specify if fields in data format should be exported directly
            after conversion

        - export_pvd = True, type bool
            specify if fields are also exported in *.pvd* format for *Paraview*

        - quantities = None, type str
            if **export_all** is False, specify which data should be exported,
            use a combination of **E**, **H** and/or **A** combined in one
            string

        - export_nedelec = True, type bool
            set to **True** for exporting caluclated data on a NedelecSpace
        """

        mpp('...  exporting selecetion of results  ...', pre_dash=False)

        if 'E' in quantities:
            self.export_E_fields(export_pvd, export_cg, export_nedelec)
        if 'H' in quantities:
            self.export_H_fields(export_pvd, export_cg, export_nedelec)
        if 'A' in quantities:
            self.export_A_fields(export_pvd, export_cg, export_nedelec)

    def save_field(self, quant, q1, q2, export_pvd, export_cg,
                   export_nedelec):

        """
        Utility function to write data on hard drive
        """

        full_name = self.full_name
        for ti in range(len(q1)):
            if self.add_tx_suffix:
                full_name = self.full_name + '_tx_' + str(ti)
            if export_pvd:
                df.File(full_name + quant + '_real_cg.pvd') << q1[ti]
                df.File(full_name + quant + '_imag_cg.pvd') << q2[ti]
            if export_cg and self.MP.file_format == 'xml':
                df.File(full_name + quant + '_real_cg.xml') << q1[ti]
                df.File(full_name + quant + '_imag_cg.xml') << q2[ti]
            elif export_cg and self.MP.file_format == 'h5':
                write_h5(self.MP.mpi_cw, q1[ti],
                         full_name + quant + '_real_cg.h5')
                write_h5(self.MP.mpi_cw, q2[ti],
                         full_name + quant + '_imag_cg.h5')
            if export_nedelec:
                if self.MP.file_format == 'xml':
                    df.File(full_name + quant + '_real.xml') << q1[ti]
                    df.File(full_name + quant + '_imag.xml') << q2[ti]
                elif self.MP.file_format == 'h5':
                    write_h5(self.MP.mpi_cw, q1[ti],
                             full_name + quant + '_real.h5')
                    write_h5(self.MP.mpi_cw, q2[ti],
                             full_name + quant + '_imag.h5')

    def convert_H_to_E(self):

        dx = self.FS.DOM.dx
        k_r = df.TrialFunction(self.FS.V)
        k_i = df.TrialFunction(self.FS.V)
        l_r = df.TestFunction(self.FS.V)
        l_i = df.TestFunction(self.FS.V)
        br = df.inner(k_r, l_r) * dx(0)
        bi = df.inner(k_i, l_i) * dx(0)
        Kr = df.inner(df.curl(self.H_s_r[0]) / self.MP.sigma[0], l_r) * dx(0)
        Ki = df.inner(df.curl(self.H_s_i[0]) / self.MP.sigma[0], l_i) * dx(0)
        for j in range(1, len(self.MP.sigma)):
            br += df.inner(k_r, l_r) * dx(j)
            bi += df.inner(k_i, l_i) * dx(j)
            Kr += df.inner(df.inv(self.MP.sigma[j]) *
                           df.curl(self.H_s_r[0]), l_r)*dx(j)
            Ki += df.inner(df.inv(self.MP.sigma[j]) *
                           df.curl(self.H_s_i[0]), l_i)*dx(j)

        mpp('...  deriving E-fields from H  ...', pre_dash=False)

        H1, b1 = df.assemble_system(br, Kr, A_tensor=self.M,
                                    b_tensor=self.w1[0])
        H2, b2 = df.assemble_system(bi, Ki, A_tensor=self.M,
                                    b_tensor=self.w2[0])

        E_s_r = self.Solver.solve_system_mumps(H1, [b1], self.FS.V,
                                               sym=True, mute=True)
        E_s_i = self.Solver.solve_system_mumps(H2, [b2], self.FS.V,
                                               sym=True, mute=True,
                                               first_call=False)
        self.E_s_r.append(E_s_r[0])
        self.E_s_i.append(E_s_i[0])
        self.E_s_r_cg.append(df.interpolate(E_s_r[0],
                                            self.FS.V_cg))
        self.E_s_i_cg.append(df.interpolate(E_s_i[0],
                                            self.FS.V_cg))
        E_t_r_cg = df.Function(self.FS.V_cg)
        E_t_i_cg = df.Function(self.FS.V_cg)
        E_t_r = df.Function(self.FS.V)
        E_t_i = df.Function(self.FS.V)
        E_0_r = df.interpolate(self.FS.PF.E_0_r_cg[0], self.FS.V)
        E_0_i = df.interpolate(self.FS.PF.E_0_i_cg[0], self.FS.V)
        E_t_r_cg.vector()[:] = \
            self.E_s_r_cg[0].vector().get_local() + \
            self.FS.PF.E_0_r_cg[0].vector().get_local()
        E_t_i_cg.vector()[:] = \
            self.E_s_i_cg[0].vector().get_local() + \
            self.FS.PF.E_0_i_cg[0].vector().get_local()
        E_t_r.vector()[:] = \
            E_s_r[0].vector().get_local() + \
            E_0_r.vector().get_local()
        E_t_i.vector()[:] = \
            E_s_i[0].vector().get_local() + \
            E_0_i.vector().get_local()
        self.E_t_r_cg.append(E_t_r_cg)
        self.E_t_i_cg.append(E_t_i_cg)
        self.E_t_r.append(E_t_r)
        self.E_t_i.append(E_t_i)

    def dc_post_proc(self, export_cg=False, export_pvd=True,
                     export_nedelec=True, write_config=True):

        X, self.E = [], []
        u = df.TrialFunction(self.FE.FS.V_cg)
        v = df.TestFunction(self.FE.FS.V_cg)
        K = df.inner(u, v) * self.dx_0
        for ti in range(self.FE.n_tx):
            RHS = df.inner(-df.grad(self.FS.U[ti]), v) * self.dx_0
            H, b = df.assemble_system(K, RHS,
                                      A_tensor=self.M, b_tensor=self.w1[ti])
            X.append(b)

        mpp('Derivation of electric field from potential:')
        self.E_cg = self.Solver.solve_system_mumps(
                            H, X, self.FS.V_cg, sym=True, mute=True)
        for ti in range(self.FE.n_tx):
            self.E.append(df.interpolate(self.E_cg[ti], self.FE.FS.V))
        full_name = self.full_name
        for ti in range(len(self.E_cg)):
            if self.add_tx_suffix:
                full_name = self.full_name + '_tx_' + str(ti)
            if export_pvd:
                df.File(full_name + '_E_cg.pvd') << self.E_cg[ti]
            if export_cg and self.MP.file_format == 'xml':
                df.File(full_name + '_E_cg.xml') << self.E_cg[ti]
            elif export_cg and self.MP.file_format == 'h5':
                write_h5(self.MP.mpi_cw, self.E_cg[ti],
                         full_name + '_E_cg.h5')
            if export_nedelec:
                if self.MP.file_format == 'xml':
                    df.File(full_name + '_E.xml') << self.E[ti]
                elif self.MP.file_format == 'h5':
                    write_h5(self.MP.mpi_cw, self.E[ti],
                             full_name + '_E.h5')
