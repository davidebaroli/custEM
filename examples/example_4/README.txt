﻿################################################################################
# # # # #                          Example 4                           # # # # #
################################################################################

    - Start with generating the mesh by running the *mesh_example_4.py* script.
    
    - Please note that the computation with calling the *run_example_4.py*
      script requires < 256 GB RAM, if maximum 12 mpi-processes are used.
    
    - The *plot_example_4.py* script will generate identical plots as shown
      in Figure 10 in the corresponding publication (Rochlitz et al., 2018,
      under review) in the *plots* directory, named "example_4_XXX.pdf".

################################################################################