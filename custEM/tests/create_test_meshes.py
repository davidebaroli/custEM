# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc import block_print
from custEM.misc import enable_print
from mpi4py import MPI

"""
Create simple meshes for second test which checks computation with all
frequency-domain approached supported by custEM and if the *pyGIMLi* and TetGen
libraries are installed properly.
"""


def create_hed_mesh():

    M = BlankWorld(name='hed_halfspace_mesh',
                   m_dir='./meshes',
                   preserve_edges=True)

    M.build_surface(insert_lines=[mu.line_x(start=-0.5, stop=0.5, n_segs=5)])

    M.build_halfspace_mesh()

    M.call_tetgen(tet_param='-pq1.4aAQ', export_vtk=True)


def create_line_mesh():

    M = BlankWorld(name='line_dip_plate_mesh',
                   m_dir='./meshes',
                   preserve_edges=True)

    M.build_surface(insert_lines=[mu.line_x(start=-5e2, stop=5e2, n_segs=51)])

    M.build_layered_earth_mesh(2, [-500.])

    M.add_plate(1000., 1000., 200., [500.0, 100.0, -1000.], 45., 117.,
                cell_size=1e4)

    M.there_is_always_a_bigger_world(1e1, 1e1, 1e1)

    M.call_tetgen(tet_param='-pq1.4aAQ', export_vtk=True)


def create_loop_mesh():

    M = BlankWorld(name='loop_brick_mesh',
                   m_dir='./meshes',
                   preserve_edges=True)

    M.build_surface(insert_loops=[mu.loop_r(start=[-5e2, -5e2],
                                            stop=[5e2, 5e2], n_segs=84)])

    M.build_halfspace_mesh()

    M.add_brick(start=[-1000., -300., -800.],
                stop=[-1500.0, 700.0, -400.], cell_size=1e4)

    M.call_tetgen(tet_param='-pq1.4aAQ', export_vtk=True)


if __name__ == "__main__":

    block_print()
    parent = MPI.Comm.Get_parent()
    create_hed_mesh()
    create_line_mesh()
    create_loop_mesh()
    parent.Barrier()
    parent.Disconnect()
    enable_print()
