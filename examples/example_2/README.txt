﻿################################################################################
# # # # #                          Example 2                           # # # # #
################################################################################

    - Start with generating the mesh by running the *mesh_example_2.py* script.

    - Please note that the computation with calling the *run_example_2.py*
      script requires up tp 1 TB RAM. Only the p1 computations on the coarse
      mesh can be carried out with < 64 GB RAM.

    - The two *plot_example_2_xxxxx.py* scripts will generate identical plots
      as shown in Figures 4 & 5 in the corresponding publication
      (Rochlitz et al., 2018, under review) in the *plots* directory,
      named "10_Hz_p2.png" and "errors_portrait.png".

################################################################################