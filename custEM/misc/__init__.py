# -*- coding: utf-8 -*-
"""
misc
====

Submodules:

- **synthetic_definitions** for defining supporting python functions
- **misc** for general utility functions used in all submodules
- **pyhed_calculations** for calling **pyhed**

################################################################################
"""

try:
    from . misc import mpi_print
    from . misc import max_mem
    from . misc import dump_csr
    from . misc import run_serial
    from . misc import run_multiple_serial
    from . misc import check_if_model_exists
    from . misc import check_approach_and_file_format
    from . misc import read_paths
    from . misc import root_write
    from . misc import write_h5
    from . misc import read_h5
    from . misc import make_directories
    from . misc import resort_coordinates
    from . misc import Ground
    from . misc import Air
    from . misc import DirichletBoundary
    from . misc import get_coordinates
    from . misc import get_coordinates
    from . misc import block_print
    from . misc import enable_print
except ImportError as err:
    print('Warning, some utility functions could not be imported '
          'from ths "misc" module!\n see:\n')
    print(err)
    print('\n ... "proceeding anyway" ...' )

from . synthetic_definitions import *

# THE END
