# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import numpy as np
import os

"""
Auxiliary functions for meshgen_tools
"""


def pointset(pointset):

    """
    Return a given set of 3D points, either list or array, as numpy array.
    """
    return(np.array(pointset))


def line(start=[0., 0., 0.], stop=[0., 0., 0.], n_segs=100, z=0., topo=None,
         t_dir=None):

    """
    Return 3D coordinates of points on a line in arbitrary direction.

    Keyword arguments:
    ------------------

    - start, stop = [0., 0., 0.], type list or array
        start and stop points.

    - n_segs = 100, type int
        number of segments on the line.

    - z = 0., type float
        offset of the line in z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.

    Notice:
    -------

    - If the topography function is used in combination with a DEM, currently
      a raw DEM parameter file (**"DEM_name" + '_parameters.txt') needs to be
      provided, only containing the 4 values in 4 lines:

        **centering**
        **x_offset**
        **y_offset**
        **rotation(RAD)**.
    """

    nodes = np.zeros((int(n_segs) + 1, 3))
    nodes[:, 0] = np.linspace(start[0], stop[0], n_segs + 1)
    nodes[:, 1] = np.linspace(start[1], stop[1], n_segs + 1)
    nodes[:, 2] = np.linspace(start[2], stop[2], n_segs + 1)
    return(assign_topography(nodes, t_dir, topo,  z))


def line_x(start=-1e2, stop=1e2, n_segs=100, y=0., z=0., topo=None,
           t_dir=None):

    """
    Return 3D coordinates of points on a line in x-direction.

    Keyword arguments:
    ------------------

    - start, stop = +-1e2, type float
        start and stop values in x-direction.

    - n_segs = 100, type int
        number of segments on the line.

    - y, z = 0., type float
        offset of the line in y- and z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.

    Notice:
    -------

    - If the topography function is used in combination with a DEM, currently
      a raw DEM parameter file (**"DEM_name" + '_parameters.txt') needs to be
      provided, only containing the 4 values in 4 lines:

        **centering**
        **x_offset**
        **y_offset**
        **rotation(RAD)**.
    """

    nodes = np.zeros((int(n_segs) + 1, 3))
    nodes[:, 0] = np.linspace(start, stop, int(n_segs) + 1)
    nodes[:, 1] = y
    return(assign_topography(nodes, t_dir, topo, z))


def line_y(start=-1e2, stop=1e2, n_segs=100, x=0., z=0., topo=None,
           t_dir=None):

    """
    Return 3D coordinates of points on a line in y-direction.

    Keyword arguments:
    ------------------

    - start, stop = +-1e2, type float
        start and stop values in x-direction.

    - n_segs = 100, type int
        number of segments on the line.

    - x, z = 0., type float
        offset of the line in x- and z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.

    Notice:
    -------

    - If the topography function is used in combination with a DEM, currently
      a raw DEM parameter file (**"DEM_name" + '_parameters.txt') needs to be
      provided, only containing the 4 values in 4 lines:

        **centering**
        **x_offset**
        **y_offset**
        **rotation(RAD)**.
    """

    nodes = np.zeros((int(n_segs) + 1, 3))
    nodes[:, 0] = x
    nodes[:, 1] = np.linspace(start, stop, n_segs + 1)
    return(assign_topography(nodes, t_dir, topo,  z))


def line_z(start=-1e2, stop=1e2, n_segs=100, x=0., y=0.):

    """
    Return 3D coordinates of points on a line in z-direction.

    Keyword arguments:
    ------------------

    - start, stop = +-1e2, type float
        start and stop values in x-direction.

    - n_segs = 100, type int
        number of segments on the line-

    - x, y = 0., type float
        offset of the line in x- and y-direction.
    """

    nodes = np.zeros((int(n_segs) + 1, 3))
    nodes[:, 0] = x
    nodes[:, 1] = y
    nodes[:, 2] = np.linspace(start, stop, n_segs + 1)
    return(nodes)


def loop_r(start=[-1e2, -1e2], stop=[1e2, 1e2], n_segs=4, z=0.,
           topo=None, t_dir=None, rotation=None, nx=None, ny=None):

    """
    Return 3D coordinates of points on a rectangular loop.

    Keyword arguments:
    ------------------

    - start, stop = +-[1e2, 1e2], type list: [float, float]
        opposite corner coordinates of rectangular loop.

    - nx_segsm, ny_segs = 100, type int
        number of segments on the x- and y-directed edges of the loop

    - z = 0., type float
        offset of the loop in z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.
    """

    if nx is None and ny is None:
        peri_half = np.abs(start[0] - stop[0]) + np.abs(start[1] - stop[1])
        ratio1 = np.abs(start[0] - stop[0]) / peri_half
        ratio2 = np.abs(start[1] - stop[1]) / peri_half
        nx_segs = int(np.round((n_segs/2.) * ratio1))
        ny_segs = int(np.round((n_segs/2.) * ratio2))
        if nx_segs == 0:
            nx_segs = 1
        if ny_segs == 0:
            ny_segs = 1
    else:
        nx_segs, ny_segs, = nx, ny

    nodes = line_x(start[0], stop[0], n_segs=nx_segs, y=start[1],
                   topo=topo, t_dir=t_dir)[:-1, :]
    nodes = np.vstack((nodes, line_y(start[1], stop[1], n_segs=ny_segs,
                                     x=stop[0], topo=topo,
                                     t_dir=t_dir)[:-1, :]))
    nodes = np.vstack((nodes, line_x(stop[0], start[0], n_segs=nx_segs,
                                     y=stop[1], topo=topo,
                                     t_dir=t_dir)[:-1, :]))
    nodes = np.vstack((nodes, line_y(stop[1], start[1], n_segs=ny_segs,
                                     x=start[0], topo=topo,
                                     t_dir=t_dir)[:-1, :]))
    if rotation is not None:
        shft = [np.mean(nodes[:, 0]), np.mean(nodes[:, 1]), 0.]
        nodes = rotate_around_point(nodes, shft, [np.deg2rad(rotation)], ['z'])
    return(assign_topography(nodes, t_dir, topo, z))


def loop_c(origin=[0., 0., 0.], r=5e1, n_segs=100, z=0.,
           topo=None, t_dir=None):

    """
    Return 3D coordinates of points on a circular loop.

    Keyword arguments:
    ------------------

    - origin = [0., 0., 0.], type list of floats
        x-, y-, z- coordinates of the center of the loop.

    - r = 1e5, type float
        radius of the loop.

    - n_segs = 100, type int
        number of segments on the loop circumference.

    - z = 0., type float
        offset of the loop in z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.
    """

    nodes = create_circle(r, n_segs, z)
    for j in range(len(nodes)):
        nodes[j, :] += origin
    return(assign_topography(nodes, t_dir, topo, z))


def loop_e(origin=[0., 0., 0.], a=5e1, b=5e1, n_segs=100, z=0.,
           topo=None, t_dir=None):

    """
    Return 3D coordinates of points on a circular loop.

    Keyword arguments:
    ------------------

    - origin = [0., 0., 0.], type list of floats
        x-, y-, z- coordinates of the center of the loop.

    - r = 1e5, type float
        radius of the loop.

    - n_segs = 100, type int
        number of segments on the loop circumference.

    - z = 0., type float
        offset of the loop in z-direction.

    - topo = None, type str or python function
        DEM-topography file or synthetic topography (python) function.

    - t_dir = None, type str
        directory of topography (DEM) files.
    """

    nodes = create_ellipse(a, b, n_segs, z)
    for j in range(len(nodes)):
        nodes[j, :] += origin
    return(assign_topography(nodes, t_dir, topo, z))


def rotate(array, alpha, axis='z'):

    """
    Rotate given **array** about **alpha(RAD)** around given **axis**
    Default rotation axis is **z**,. alternatively **x** or **y**.
    """

    if axis == 'z':
        rotation_matrix = np.array(([np.cos(-alpha), -np.sin(-alpha), 0],
                                    [np.sin(-alpha), np.cos(-alpha), 0],
                                    [0, 0, 1]), dtype=np.float64)
    elif axis == 'x':
        rotation_matrix = np.array(([1, 0, 0],
                                    [0, np.cos(-alpha), -np.sin(-alpha)],
                                    [0, np.sin(-alpha), np.cos(-alpha)]),
                                   dtype=np.float64)
    elif axis == 'y':
        rotation_matrix = np.array(([np.cos(-alpha), 0, np.sin(-alpha)],
                                    [0, 1, 0],
                                    [-np.sin(-alpha), 0, np.cos(-alpha)]),
                                   dtype=np.float64)

    for i in range(array.shape[0]):
        array[i] = array[i].dot(rotation_matrix)
    return(array)


def translate(array, shift, back=False):

    """
    Translate given *array** about **shift**.
    """

    if not back:
        return(array - np.array(shift).reshape(1, 3))
    else:
        return(array + np.array(shift).reshape(1, 3))


def rotate_around_point(array, shift, angles, axes, pre_rotate=False):

    if len(angles) != len(axes):
        print('Error! Length of lists of *angles* and rotation *axes* must be '
              'the same!')
        raise SystemExit

    array = translate(array, shift)
    if pre_rotate:
        array = rotate(array, -angles[1], axes[1])
        array[:, 1] *= np.cos(angles[0])
    for jjj in range(len(angles)):
        array = rotate(array, angles[jjj], axes[jjj])
    return(translate(array, shift, back=True))


def create_circle(r, n, h):

    """
    Create a list of **n** (number of) 3D coordinates on the circumference of a
    circle with radius **r** and height **h**.
    """

    return(np.array([(np.cos(2 * np.pi / n * x)*r,
                     np.sin(2 * np.pi / n * x)*r,
                     h) for x in range(0, n)]))


def create_ellipse(a, b, n, h):

    """
    Create a list of **n** (number of) 3D coordinates on the circumference of
    an ellipsum with 'radii' **a** and **b**, and height **h**.
    """

    def ellipse(t, a, b):
        return a*np.cos(t), b*np.sin(t)

    create_ellipseints = [ellipse(t, a, b) for t in np.linspace(0, 2*np.pi, n)]
    x, y = [np.array(v) for v in list(zip(*create_ellipseints))]
    xy = np.hstack((x.reshape(n, 1), y.reshape(n, 1)))
    return(np.hstack((xy, np.ones((n, 1)) * h)))


def poly_area(corners):
    n = len(corners)
    area = 0.0
    for i in range(n):
        j = (i + 1) % n
        area += corners[i][0] * corners[j][1]
        area -= corners[j][0] * corners[i][1]
    area = abs(area) / 2.0
    return(area)


def poly_peri(corners):
    n = len(corners)
    peri = 0.0
    for i in range(n - 1):
        peri += np.sqrt((corners[i+1, 0] - corners[i, 0])**2 +
                        (corners[i+1, 1] - corners[i, 1])**2)
    peri += np.sqrt((corners[-1, 0] - corners[0, 0])**2 +
                    (corners[-1, 1] - corners[0, 1])**2)
    return(peri)


def find_edge_node_ids(frame, mesh, id_offset):

    """
    Internal utility function for meshgen tools to find the edge node ids of a
    horizontal 2D mesh for the extension of the mesh in z-direction.
    """

    ids = []
    frame = np.vstack((frame, frame[0]))
    for j in range(len(frame) - 1):
        tmp_idx, tmp_pos, dist = [], [], []
        for idx, pos in enumerate(mesh.positions().array()):
            if is_between_2D(frame[j, :], frame[j+1, :], pos):
                tmp_idx.append(idx)
                tmp_pos.append(pos[:2])
        tmp_pos = np.array(tmp_pos)
        for jj in range(len(tmp_pos)):
            dist.append(np.linalg.norm(frame[j, :] - tmp_pos[jj]))
        tmp_list = [sx for _, sx in sorted(zip(dist, tmp_idx))]
        ids.append(np.array(tmp_list) + id_offset)
    return(ids)


def find_closest(l1, l2):

    """
    Utility function for finding the closest entry in a list
    of 1D corrdinates in comparison to all entries of another list.
    """

    ll = []
    for k in range(len(l1)):
        ll.append(min(range(len(l2)), key=lambda i: abs(l2[i]-l1[k])))
    return(ll)


def closest_node(node, nodes):

    """
    Utility function for finding the closest node in a list
    of 2D/3D corrdinates.
    """

    nodes = np.asarray(nodes)
    dist_2 = np.sum((nodes - node)**2, axis=1)
    return(np.argmin(dist_2))


def find_on_frame(orig, coords):

    frame = []
    orig = np.vstack((orig, orig[0, :]))

    for j in range(len(orig)-1):
        tmp = []
        for k, coord in enumerate(coords):
            if is_between_2D(orig[j], orig[j+1], coord):
                if k not in frame:
                    tmp.extend([k])
        tmp_ordered = order_list(orig[j], coords[tmp], tmp)
        frame.extend(tmp_ordered)
    return(frame)


def order_list(ref, points, tmp):

    dist = np.sqrt((points[:, 0] - ref[0])**2 + (points[:, 1] - ref[1])**2)
    return(np.array(np.array(tmp)[dist.argsort()], dtype=int))


def is_between(a, b, c, tol=1e-6, vtol=1e2):

    """
    search if a Nedelec-dof is located on the edge between two points,
    needed for crooked sources defined as Path
    """

    if (a[0] - tol <= c[0] <= b[0] + tol or a[0] + tol >= c[0] >= b[0] - tol):
        if ((a[1] - tol <= c[1] <= b[1] + tol or
             a[1] + tol >= c[1] >= b[1] - tol)):
            if ((a[2] - tol <= c[2] <= b[2] + tol or
                 a[2] + tol >= c[2] >= b[2] - tol)):

                if np.isclose((c[1] - a[1]) * (b[0] - a[0]),
                              (c[0] - a[0]) * (b[1] - a[1]), tol):
                    return(True)
    return(False)


def is_between_2D(a, b, c, tol=1e-6, print_points=False):

    """
    search if a Nedelec-dof is located on the edge between two points,
    needed for crooked sources defined as Path
    """

    if print_points:
        print(a, b, c)
    if (a[0] - tol <= c[0] <= b[0] + tol or a[0] + tol >= c[0] >= b[0] - tol):
        if ((a[1] - tol <= c[1] <= b[1] + tol or
             a[1] + tol >= c[1] >= b[1] - tol)):
                if np.isclose((c[1] - a[1]) * (b[0] - a[0]),
                              (c[0] - a[0]) * (b[1] - a[1]), tol):
                    return True
    return False


def is_between_2Dvert(a, b, c, tol=1e-6, print_points=False):

    """
    search if a Nedelec-dof is located on the edge between two points,
    needed for crooked sources defined as Path
    """

    if print_points:
        print(a, b, c)
    if (a[0] - tol <= c[0] <= b[0] + tol or a[0] + tol >= c[0] >= b[0] - tol):
        if ((a[2] - tol <= c[2] <= b[2] + tol or
             a[2] + tol >= c[2] >= b[2] - tol)):
            if np.isclose(c[1], a[1], tol) and np.isclose(c[1], b[1], tol):
                if np.isclose((c[2] - a[2]) * (b[0] - a[0]),
                              (c[0] - a[0]) * (b[2] - a[2]), tol):
                    return True
    return False


def write_synth_topo_to_asc(t_dir, file_name, topo, spacing=25.,
                            x_min=-1e4, y_min=-1e4):

    """
    Write an array of synthetically generated topography data to an ".asc" file
    """
    with open(t_dir + '/' + file_name + '.asc', "w") as demfile:
        demfile.write('ncols         ' + str(topo.shape[1]) + '\n'
                      'nrows         ' + str(topo.shape[0]) + '\n'
                      'xllcorner     ' + str(x_min) + '\n'
                      'yllcorner     ' + str(y_min) + '\n'
                      'cellsize      ' + str(spacing) + '\n'
                      'NODATA_value  -9999\n')
    with open(t_dir + '/' + file_name + '.asc', "ab") as demfile:
        np.savetxt(demfile, topo, '%3.8f')


def write_synth_topo_to_xyz(t_dir, file_name, topo):

    """
    Write an array of synthetically generated topography data to an ".asc" file
    """

    with open(t_dir + '/' + file_name + '.xyz', "wb") as demfile:
        np.savetxt(demfile, topo)


def assign_topography(nodes, t_dir, topo, z=0.):

    """
    Utility function for assigning topography for lines or loops, etc.
    """

    if topo is None:
        nodes[:, 2] = z
    else:
        if type(topo) is not str:
            nodes[:, 2] = topo(nodes[:, 0], nodes[:, 1])
        else:
            if os.path.isfile(t_dir + '/' + topo + '_parameters.txt'):
                tmp = np.loadtxt(t_dir + '/' + topo + '_parameters.txt')
            else:
                tmp = [True, None, None, None]
            from custEM.meshgen.dem_interpolator import DEM
            dgm = DEM(t_dir + '/' + topo, centering=bool(tmp[0]),
                      easting_shift=tmp[1], northing_shift=tmp[2])
            nodes[:, 2] = dgm(nodes[:, 0], nodes[:, 1], rotation=tmp[3])
        nodes[:, 2] += z
    return(nodes)


def inside_poly(x, y, points):

    """
    Return True if a coordinate (x, y) is inside a polygon defined by
    a list of verticies [(x1, y1), (x2, x2), ... , (xN, yN)].

    Reference: http://www.ariel.com.au/a/python-point-int-poly.html
    """
    n = len(points)
    inside = False
    p1x, p1y = points[0]
    for i in range(1, n + 1):
        p2x, p2y = points[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y
    return(inside)


def refine_path(path, n_segs):

    """
    Refine each section of a 3D path into *n_segs* equal segments.
    """

    new_path = np.zeros(((np.int(len(path) - 1) * n_segs) + 1, 3))
    off = 0
    for j in range(len(path) - 1):
        refined = np.array(
                [np.linspace(path[j, 0], path[j+1, 0], n_segs + 1),
                 np.linspace(path[j, 1], path[j+1, 1], n_segs + 1),
                 np.linspace(path[j, 2], path[j+1, 2], n_segs + 1)])
        new_path[off:off + n_segs, :] = refined.T[:-1, :]
        off += n_segs
    new_path[-1, :] = path[-1, :]
    return new_path


def refine_rx(coords, r=5., rot=None):

    """
    Refine rx position by enclosing them with hexagons of radius *r*.
    The smaller *r*, the better the refinement. Note that each hexagon will be
    subdivided into 6 equilateral triangles, which will support optimum
    tetrahedra quality during the meshing process.
    """

    rx_refined = []
    for rec in coords:
        rx_refined.append(loop_c(origin=rec, r=r, z=rec[2], n_segs=3))
    if rot is not None:
        degs = np.arange(len(coords)) * rot
        for j in range(len(coords)):
            rx_refined[j] = rotate_around_point(
                    rx_refined[j], coords[j], [np.deg2rad(degs[j])], ['z'])
    return(rx_refined)


def get_unique_poly_ids(polys, max_poly_length):

        arr = np.zeros((len(polys), max_poly_length), dtype=int)
        for k, ele in enumerate(polys):
            arr[k, :len(ele)] = sorted(ele)
        _, ids = np.unique(arr, axis=0, return_index=True)
        return(ids)
