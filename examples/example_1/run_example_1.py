# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 1                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
import numpy as np

approaches = ['E_t', 'E_s']    # E_s computation == pyhed reference
omegas = [1e0 * 2. * np.pi,    # if no anomalies are incorporated in a mesh
          1e1 * 2. * np.pi,
          1e2 * 2. * np.pi,
          1e3 * 2. * np.pi]
freqs_str = ['1', '10', '100', '1000']

# ########################## run p1 computations ############################ #

p = 1
mesh = 'example_1_mesh'

for jj, omega in enumerate(omegas):   # four frequencies
    for approach in approaches:       # two approaches

        # Initialize MODel
        mod = 'example_1_' + freqs_str[jj]
        M = MOD(mod, mesh, approach, p=p, overwrite=True,
                m_dir='./meshes', r_dir='./results', fs_type='CG')

        # define frequency and condcutivities
        M.MP.update_model_parameters(omega=omega,
                                     sigma_anom=[],
                                     sigma_ground=[1e-2],
                                     procs_per_proc=1)

        # let FEniCS set up the variational formulations
        M.FE.build_var_form(pf_type='halfspace',
                            s_type='hed',
                            azimuth=90.)

        # call solver and convert H-fields
        M.solve_main_problem(convert=False)
        M.PP.convert_results(export_pvd=False, export_cg=True)

        # load existing model only for interpolation purposes
        # M = MOD(mod, mesh, approach, p=p, overwrite=False,
        #         load_existing=True, m_dir='./meshes',
        #         r_dir='./results')

        M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, n_segs=1000)
        M.IB.create_slice_meshes('z', dim=5e3, n_segs=100, slice_name='surf')
        linE = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_1000_line_x'

        # interpolate fields on the observation line
        quantities = ['H_t', 'E_t']
        for q in quantities:
            M.IB.interpolate(q, linE)
            M.IB.interpolate(q, 'surf_slice_z')
        M.IB.synchronize()
