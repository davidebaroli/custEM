# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import getopt
import sys
import numpy as np
from mpi4py import MPI

"""
This file is always called in serial from the root process of an mpi run,
otherwise a crash would occur due to a missing 'MPI-Parent' for synchronization
"""


class Path_mesh:

    """
    Mesh calculation class for generating straight or crooked (e.g.,
    topography), line meshes.

    Class internal functions:
    -------------------------

    - calc_line_meshes()
        generate the 1D mesh for interpolation
    """

    def __init__(self, para_dir, path_name):

        """
        Required arguments:
        -------------------

        - points, type array of shape (n, 3)
            points to interpolate on

        - out_name, None or type str
            could be specified manually, if None, a default name will be used

        Keyword arguments:
        ------------------

        - on_topo=False, type bool
            set to True if the Z-values of a 'x-line' or 'y-line'-mesh should
            be adjusted to a certain topography, the flag is set in the
            **Interpolator** instance of the **MOD** class
        """

        import json
        with open(para_dir + '/interp_dict_0.json', 'r') as thefile:
            self.__dict__.update(json.load(thefile))
        points = np.load(para_dir + '/points.npy')
        if path_name is None:
            self.out_name = 'NOT_DEFINED'
            print('Warning! No export name for path interpolation mesh set, '
                  'using *NOT_DEFINED* as dummy name, continuing ... ')
        else:
            self.out_name = path_name

        if os.path.isfile(str(self.m_dir) + '/paths/' +
                          str(self.out_name) + '_path.xml'):
            print('Desired "path_mesh" already exists, continuing ... ')
        else:
            print('1D "path_mesh" is calculated and '
                  'saved in global "mesh_dir"/paths ... ')

            slice_mesh = df.BoxMesh(df.Point(self.x0, -self.dim, -self.dim),
                                    df.Point(self.x1, self.dim, self.dim),
                                    len(points) - 1, 2, 2)

            boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')
            cf = df.MeshFunction('size_t', boundary_mesh,
                                 boundary_mesh.topology().dim(), 0)
            x1 = df.AutoSubDomain(lambda x: x[1] < -self.dim + self.tol)
            x1.mark(cf, 1)
            x_l = df.SubMesh(boundary_mesh, cf, 1)
            boundary_mesh2 = df.BoundaryMesh(x_l, 'exterior')
            cf2 = df.MeshFunction('size_t', boundary_mesh2,
                                  boundary_mesh2.topology().dim(), 0)
            x2 = df.AutoSubDomain(lambda x: x[2] < -self.dim + self.tol)
            x2.mark(cf2, 2)
            path = df.SubMesh(boundary_mesh2, cf2, 2)
            path.coordinates()[:, :] = points

            if self.on_topo:
                import custEM.misc.synthetic_definitions as sd
                try:
                    topo_func = getattr(sd, str(self.topo))
                    line_x.coordinates()[:, 2] = topo_func(
                        line_x.coordinates()[:, 0],
                        line_x.coordinates()[:, 1])
                except (AttributeError, NameError):
                    from custEM.meshgen.dem_interpolator import DEM
                    tmp = self.get_topo_parameters()
                    dem = DEM(self.t_dir + '/' + self.mesh_name +
                              '_surf.xyz',
                              centering=bool(tmp[1]),
                              easting_shift=None, northing_shift=None)

                    path.coordinates()[:, 2] = dem(
                            path.coordinates()[:, 0],
                            path.coordinates()[:, 1],
                            rotation=tmp[4])
            path.coordinates()[:, 2] += self.z

            df.File(str(self.m_dir) + '/paths/' + str(self.out_name) +
                    '_path.xml') << path

    def get_topo_parameters(self):

        tmp = np.genfromtxt(para_dir + '/' + self.mesh_name +
                            '_parameters.txt', dtype=None,
                            encoding=None)
        dummy = []
        for j, elem in enumerate(tmp):
            dummy.append(str(elem))

        tmp = dummy
        if tmp[1] == 'False':
            tmp[1] = False
        else:
            tmp[1] = True
        if tmp[2] == 'None':
            tmp[2] = None
        else:
            tmp[2] = float(tmp[2])
        if tmp[3] == 'None':
            tmp[3] = None
        else:
            tmp[3] = float(tmp[3])
        if tmp[4] == 'None':
            tmp[4] = None
        else:
            tmp[4] = float(tmp[4])
        return(tmp)


if __name__ == "__main__":

    """
    read the three arguments for the **Line_mesh_calc** class, call the latter
    and synchronize with the 'Parent process' afterwards
    """

    myopts, args = getopt.getopt(sys.argv[1:], "d:o:")
    for o, a in myopts:
        if o == '-d':
            para_dir = str(a)
        elif o == '-o':
            out_name = str(a)
        else:
            print("Usage: %s -d   parameter directory   -o     output name")

    if out_name == 'None':
        out_name = None
    parent = MPI.Comm.Get_parent()
    Path_mesh(para_dir, out_name)
    parent.Barrier()
    parent.Disconnect()
