# -*- coding: utf-8 -*-
"""
fem
===

Submodules:

- ** frequency_domain_approaches** for implemented frequency-domain approaches:
  (E-field, H-field, A-V-mixed, A-V-nodal, F-U-mixed)
- ** time_domain_approaches** for implemented time-domain approaches:
  (implicit Euler, inverse fourier-transform based, rational Arnoldi)
- **fem_base** for setting up the finite element kernel
- **fem_utils** for defining approach base class and related utility functions
- **primary_fields** for setting up primary fields
- **rhs_asembly** for right-hand-side assembly using total field approaches

################################################################################
"""

from . fem_base import *
from . fem_utils import *
from . primary_fields import *
from . rhs_assembly import *
from . frequency_domain_approaches import *
from . time_domain_approaches import *

# THE END
