# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
from custEM.misc.synthetic_definitions import example_2_loop_tx
import numpy as np


omegas = [1e0 * 2. * np.pi, 1e1 * 2. * np.pi,
          1e2 * 2. * np.pi, 1e3 * 2. * np.pi]

freqs_str = ['1', '10', '100', '1000']

# ########################## run p1 computations ############################ #

meshes = ['example_2_mesh_coarse',
          'example_2_mesh_inter',
          'example_2_mesh_fine']

for mesh in meshes:                        # three meshes
    for jj, omega in enumerate(omegas):    # four frequencies
        # Initialize MODel
        mod = 'example_2_p1_f_' + freqs_str[jj]
        M = MOD(mod, mesh, 'E_t', p=1, overwrite=True,
                m_dir='./meshes', r_dir='./results')

        # define frequency and condcutivities
        M.MP.update_model_parameters(omega=omega,
                                     sigma_anom=[],
                                     sigma_ground=[1e-3, 1e-2, 1e-4],
                                     sigma_air=[1e-7],
                                     procs_per_proc=1)

        # let FEniCS set up the variational formulations
        M.FE.build_var_form(pf_type='layered_earth',
                            s_type='path',
                            closed_path=True,
                            path=example_2_loop_tx())

        # call solver and convert H-fields
        M.solve_main_problem()

        # load existing model only for interpolation purposes
        # M = MOD(mod, mesh, 'E_t', p=1, overwrite=False,
        #         load_existing=True, m_dir='./meshes', r_dir='./results')

        if jj == 0:
            M.IB.create_slice_meshes('z', dim=2e3, n_segs=100,
                                     slice_name='surface')
        slicE = 'surface_slice_z'

        # interpolate fields on the observation slice
        quantities = ['H_t', 'E_t']
        for q in quantities:
            M.IB.interpolate(q, slicE)
        M.IB.synchronize()

# ########################## run p2 computations ############################ #

for jj, omega in enumerate(omegas):    # four frequencies
    # Initialize MODel

    mod = 'example_2_p2_f_' + freqs_str[jj]
    M = MOD(mod, 'example_2_mesh_coarse', 'E_t', p=1, overwrite=True,
            m_dir='./meshes', r_dir='./results')

    # define frequency and condcutivities
    M.MP.update_model_parameters(omega=omega,
                                 sigma_anom=[],
                                 sigma_ground=[1e-3, 1e-2, 1e-4],
                                 sigma_air=[1e-7],
                                 procs_per_proc=1)

    # let FEniCS set up the variational formulations
    M.FE.build_var_form(pf_type='layered_earth',
                        s_type='path',
                        closed_path=True,
                        path=example_2_loop_tx())

    # call solver and convert H-fields
    M.solve_main_problem()

    # load existing model only for interpolation purposes
    # M = MOD(mod, 'example_2_mesh_coarse', 'E_t', p=2, overwrite=False,
    #         load_existing=True, m_dir='./meshes', r_dir='./results')

    slicE = 'surface_slice_z'
    # interpolate fields on the observation slice
    quantities = ['H_t', 'E_t']
    for q in quantities:
        M.IB.interpolate(q, slicE)
    M.IB.synchronize()
