
# coding: utf-8

# # Visualization tutorial
# 
# This is the first version of the plot manual, introducing basic functions for
# importing interpolated data and depicting the latter in 1D, 2D and 3D.
# This manual is going to be extended to include even more useful commands for
# analyzing different models computed with custEM (mismatches, field variations)
# or ccompare ustEM models with external results for validation/cross-checking.
# For all keyword arguments, it is referred to the docs of the PLOT class.

# ### Import data on default meshes to plot

# In[ ]:


from custEM.post import make_plain_subfigure_box
from custEM.post import Plot
import matplotlib.pyplot as plt

# maybe set also other matplotlib parameters, e.g., rcParam
plt.ioff()    # temporarily disable on-the-fly plot representation to speed up

# plt.ion()   # move and uncomment these two commands after making the plots 
# plt.show()  # to use interactive plots and for showing results

lims = [2., 2., 2., 10., 10., 10.]  # limits [km] for "small" and "large"

# import default data
# #############################################################################


P = Plot('tutorial_mod',            # model name
         'tutorial_mesh',           # mesh name
         'E_t',                     # approach name
         r_dir='./results',         # results directory
         s_dir='./plots/default/',  # save directory for default plots
         fig_size=[8, 12])          # default figure size

P.load_default_line_data()   # import all data interpolated on the coordinate
                             # axes in the limits +- 2km (*small* line meshes)
P.load_default_line_data(interp_meshes='large')   # same as above for *large*
                                                  # lines meshes (+- 10 km)
    
# the default slice mesh data are only available if interpolation on these
# meshes was enabled in the *run_tutorial*

# P.load_default_slice_data()  # import all data interpolated on *small* slices
#                              # perpendicular to the coordinate axes, slices
#                              # have dimensions of 4 x 4 km
# P.load_default_slice_data(interp_meshes='large')  # same as above for *large*
#                                                   # slices (20 x 20 km dim.)

# print(P.line_data.keys())    # print all imported line data entries
# print(P.slice_data.keys())   # print all imported slice data entries


# ### Plot data interpolated on default meshes
# 
# There are lots of keyword arguments available to customize the plots.
# It is referred to the **Plot** class documentation

# In[ ]:


# plot default data
# #############################################################################

line1 = 'default_small_line_x'
line2 = 'default_small_line_y'
line3 = 'default_small_line_z'
line4 = 'default_large_line_x'
line5 = 'default_large_line_y'
line6 = 'default_large_line_z'
lines = [line1, line2, line3, line4, line5, line6]

for i, linE in enumerate(lines):
    print('plotting field data: ', linE)
    P.plot_line_data(mesh=linE, xlim=[-lims[i], lims[i]], title='tutorial')

# the default slice mesh data are only available if interpolation on these
# meshes was enabled in the *run_tutorial*

# slice1 = 'default_small_slice_x'
# slice2 = 'default_small_slice_y'
# slice3 = 'default_small_slice_z'
# slice4 = 'default_large_slice_x'
# slice5 = 'default_large_slice_y'
# slice6 = 'default_large_slice_z'
# slices = [slice1, slice2, slice3, slice4, slice5, slice6]

# for i, slicE in enumerate(slices):
#     print('plotting field data: ', slicE)
#     P.plot_slice_data(mesh=slicE, title='tutorial')


# ### Import data on custom interpolation meshes to plot
# 
# Any interpolation line or slice with a custom name can be important and
# the data visualized as described subsequently.

# In[ ]:


# Initialize a new instance with a new save directory to plot all of the
# data interpolated on custom lines and slices.
# #############################################################################

P = Plot('tutorial_mod', 'tutorial_mesh', 'E_t', r_dir='./results',
         s_dir='./plots/custom/', fig_size=[8, 12])

# import data on the three custom lines
lines = ['10_km_line_x']
dummy = [P.import_line_data(line, key='L1') for line in lines]

# import data on the three 'normal' slices
slices = ['axis_slice_x', 'axis_slice_y', 'axis_slice_z']
dummy = [P.import_slice_data(slicE, stride=5) for slicE in slices]

# import only H-fields on the slice 50 m above the surface topography with
# a stride of 40 for the quiver plots
# P.import_slice_data('above_topo_slice_z', EH='H',
#                     key_name='K2', coord_key='K2', stride=10)


# ### Plot data on custom interpolation meshes

# In[ ]:


P.plot_line_data(key='L1', title='custom_line')
P.plot_slice_data(mesh=slices[2], title='custom_slice')


# ### Make 3D plots for topography models
# 
# - Work in progress.

# In[ ]:


# Will be updated soon with a proper example!
# #############################################################################

# fig = plt.figure(figsize=(10, 7))
# P.add_3D_slice_plot(fig, 'K1_H_t', 'above_topo_slice_z', sp_pos=111,
#                     q_length=0.2, label=r'$\Re(\mathbf{|H|})$ $(V/ m)$',
#                     q_slicE='K2', q_name='K2_H_t', clim=[1e-8, 1e-4])
# plt.savefig('./plots/custom/H_r_3D_plot.png')
#
# fig = plt.figure(figsize=(10, 7))
# P.add_3D_slice_plot(fig, 'K1_H_t', 'above_topo_slice_z', sp_pos=111,
#                     q_length=0.2, label=r'$\Im(\mathbf{|H|})$ $(V/ m)$',
#                     q_slicE='K2', q_name='K2_H_t', clim=[1e-8, 1e-4],
#                     ri='imag')
# plt.savefig('./plots/custom/H_i_3D_plot.png')

