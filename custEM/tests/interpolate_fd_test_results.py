# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


from custEM.core import MOD


"""
Interpolate the results obtained from the *lvl_2_test*.
"""

n_max_new_procs = 0  # change to accelerate multithreading
# 1. test result, E_t approach, halfspace, no anomaly, HED source
M1 = MOD('fd_test_E_t', 'hed_halfspace_mesh', 'E_t', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t')
M1.IB.init_default_interpolation_meshes()
M1.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M1.IB.interpolate_default(target_type='lines', interp_meshes='small')
M1.IB.interpolate_default(target_type='lines', interp_meshes='large')
M1.IB.interpolate_default(target_type='slices', interp_meshes='small')
M1.IB.interpolate_default(target_type='slices', interp_meshes='large')
M1.IB.synchronize()

# 2. test results, E_s appraoch with pf E, halfspace, no anomaly, HED source

M2 = MOD('fd_test_E_s_pf_E', 'hed_halfspace_mesh', 'E_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t', fs_type='CG')
M2.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M2.IB.interpolate_default(target_type='lines', interp_meshes='small')
M2.IB.interpolate_default(target_type='lines', interp_meshes='large')
M2.IB.interpolate_default(target_type='slices', interp_meshes='small')
M2.IB.interpolate_default(target_type='slices', interp_meshes='large')
M2.IB.synchronize()

# 3. test results, E_s appraoch with pf H, halfspace, no anomaly, HED source

M3 = MOD('fd_test_E_s_pf_H', 'hed_halfspace_mesh', 'E_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t', fs_type='CG')
M3.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M3.IB.interpolate_default(target_type='lines', interp_meshes='small')
M3.IB.interpolate_default(target_type='lines', interp_meshes='large')
M3.IB.interpolate_default(target_type='slices', interp_meshes='small')
M3.IB.interpolate_default(target_type='slices', interp_meshes='large')
M3.IB.synchronize()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# 4. test result, Am_t approach, two-layered-earth, dipping plate anomaly,
#                                1 km dipole source
M4 = MOD('fd_test_Am_t', 'line_dip_plate_mesh', 'Am_t', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t')
M4.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M4.IB.interpolate_default(target_type='lines', interp_meshes='small')
M4.IB.interpolate_default(target_type='lines', interp_meshes='large')
M4.IB.interpolate_default(target_type='slices', interp_meshes='small')
M4.IB.interpolate_default(target_type='slices', interp_meshes='large')
M4.IB.synchronize()

# 5. test result, An_t approach, two-layered-earth, dipping plate anomaly,
#                                1 km dipole source
M5 = MOD('fd_test_An_t', 'line_dip_plate_mesh', 'An_t', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t')
M5.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M5.IB.interpolate_default(target_type='lines', interp_meshes='small')
M5.IB.interpolate_default(target_type='lines', interp_meshes='large')
M5.IB.interpolate_default(target_type='slices', interp_meshes='small')
M5.IB.interpolate_default(target_type='slices', interp_meshes='large')
M5.IB.synchronize()

# 6. test result, Am_s approach, two-layered-earth, dipping plate anomaly,
#                                1 km dipole source
M6 = MOD('fd_test_Am_s', 'line_dip_plate_mesh', 'Am_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t, E_s, H_s')
M6.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M6.IB.interpolate_default(target_type='lines', interp_meshes='small')
M6.IB.interpolate_default(target_type='lines', interp_meshes='large')
M6.IB.interpolate_default(target_type='slices', interp_meshes='small')
M6.IB.interpolate_default(target_type='slices', interp_meshes='large')
M6.IB.synchronize()

# 7. test result, Fm_s approach, two-layered-earth, dipping plate anomaly,
#                                1 km dipole source
M7 = MOD('fd_test_Fm_s', 'line_dip_plate_mesh', 'Fm_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='E_t, H_t, E_s, H_s')
M7.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M7.IB.interpolate_default(target_type='lines', interp_meshes='small')
M7.IB.interpolate_default(target_type='lines', interp_meshes='large')
M7.IB.interpolate_default(target_type='slices', interp_meshes='small')
M7.IB.interpolate_default(target_type='slices', interp_meshes='large')
M7.IB.synchronize()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# 8. test result, H_s approach, with pf E, halfspace, brick anomaly,
#                                                     1x1 km loop source
M8 = MOD('fd_test_H_s_pf_E', 'loop_brick_mesh', 'H_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='H_t, H_s, E_t, E_s')
M8.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M8.IB.interpolate_default(target_type='lines', interp_meshes='small')
M8.IB.interpolate_default(target_type='lines', interp_meshes='large')
M8.IB.interpolate_default(target_type='slices', interp_meshes='small')
M8.IB.interpolate_default(target_type='slices', interp_meshes='large')
M8.IB.synchronize()

# 9. test result, H_s approach, with pf H, halfspace, brick anomaly,
#                                                     1x1 km loop source
M9 = MOD('fd_test_H_s_pf_H', 'loop_brick_mesh', 'H_s', p=1,
         overwrite=False, m_dir='meshes', r_dir='fd_test_results',
         para_dir='meshes/para', load_existing=True,
         field_selection='H_t, H_s, E_t, E_s')
M9.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M9.IB.interpolate_default(target_type='lines', interp_meshes='small')
M9.IB.interpolate_default(target_type='lines', interp_meshes='large')
M9.IB.interpolate_default(target_type='slices', interp_meshes='small')
M9.IB.interpolate_default(target_type='slices', interp_meshes='large')
M9.IB.synchronize()

# 10. test result, An_s approach, halfspace, brick anomaly, 1x1 km loop source
M10 = MOD('fd_test_An_s', 'loop_brick_mesh', 'An_s', p=1,
          overwrite=False, m_dir='meshes', r_dir='fd_test_results',
          para_dir='meshes/para', load_existing=True,
          field_selection='E_t, H_t, E_s, H_s')
M10.IB.update_interpolation_parameters(max_new_procs_spawn=n_max_new_procs)
M10.IB.interpolate_default(target_type='lines', interp_meshes='small')
M10.IB.interpolate_default(target_type='lines', interp_meshes='large')
M10.IB.interpolate_default(target_type='slices', interp_meshes='small')
M10.IB.interpolate_default(target_type='slices', interp_meshes='large')
M10.IB.synchronize()
