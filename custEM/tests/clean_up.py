# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import shutil


try:
    shutil.rmtree('lvl_1_test_results')
except:
    pass

try:
    shutil.rmtree('lvl_2_test_results')
except:
    pass

try:
    shutil.rmtree('lvl_3_test_results')
except:
    pass

try:
    shutil.rmtree('meshes')
except:
    pass

try:
    shutil.rmtree('results')
except:
    pass

try:
    shutil.rmtree('lvl_2_test_plots')
except:
    pass

try:
    shutil.rmtree('field_empymod.vtk')
except:
    pass

try:
    shutil.rmtree('field_pyhed.vtk')
except:
    pass
