# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
import dolfin as df
from custEM.misc import make_directories
from custEM.misc import block_print
from custEM.misc import enable_print


block_print()

"""
First test which checks if custEM and FEniCS are properly installed with
all requirements via the conda-forge package.
"""

make_directories('base_test_results', 'meshes', 'E_t')

# %% initialize parameters for the 'lvl_1_test'

mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

# Uncomment the "df.set_log_level(X)" command to enable FEniCS (dolfin) prints.
# default dolfin log_level is 0, which means NO prints from dolfin
# Avilable log levels:
# ########################################################################### #
#     CRITICAL  = 50, errors that may lead to data corruption and suchlike
#     ERROR     = 40, things that go boom
#     WARNING   = 30, things that may go boom later
#     INFO      = 20, information of general interest
#     PROGRESS  = 16, what's happening (broadly)
#     TRACE     = 13, what's happening (in detail)
#     DBG       = 10  sundry
# ########################################################################### #

# df.set_log_level(20)       # set log level here

# %% Testing fullspace model on UnitCubeMesh with secondary field approach

M = MOD('test_fullspace_E_s', 'UnitCubeMesh', 'E_s', p=1,
        overwrite=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='xml')

M.MP.update_model_parameters(omega=1e2,
                             sigma_anom=[],
                             sigma_ground=1e-2,
                             sigma_air=1e-2)

M.FE.build_var_form(pf_type='fullspace', s_type='hed')
M.solve_main_problem(convert=True)


# %% Testing fullspace model on UnitCubeMesh with total field approach

M = MOD('test_fullspace_E_t', 'UnitCubeMesh', 'E_t', p=1,
        overwrite=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='xml', dg_interpolation=True)

M.MP.update_model_parameters(omega=1e2,
                             sigma_anom=[],
                             sigma_ground=1e-2,
                             sigma_air=1e-2)

M.FE.build_var_form(s_type='hed', length=2000.1)
M.solve_main_problem(convert=True)

# for debugging
# M.IB.create_slice_meshes('y', dim=1e4, slice_name='y')
# M.IB.interpolate('E_t', 'y_slice_y')
# M.IB.synchronize()
# print(df.MPI.rank(mpi_cw))

# df.File('E_i_fs_ned.pvd') << M.PP.E_t_i[0]
# df.File('E_r_fs_ned.pvd') << M.PP.E_t_r[0]
# df.File('H_i_fs_ned.pvd') << M.PP.H_t_i[0]
# df.File('H_r_fs_ned.pvd') << M.PP.H_t_r[0]

# %% Testing halfspace model on UnitCubeMesh with total field approach

M = MOD('test_halfspace_E_t', 'UnitCubeMesh', 'E_t', p=1,
        overwrite=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='xml')

M.MP.update_model_parameters(omega=1e2,
                             sigma_anom=[],
                             sigma_ground=1e-2,
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='hed', length=2000.1)
M.solve_main_problem(convert=True)

# for debugging
# df.File('E_i_hs_ned.pvd') << M.PP.E_t_i[0]
# df.File('E_r_hs_ned.pvd') << M.PP.E_t_r[0]

enable_print()

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
        print('      Basic test performed succesfully in mpi mode!!!')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
    else:
        pass

else:
    print('###############################################')
    print('###############################################')
    print('###############################################')
    print('      Basic test performed succesfully !!!')
    print('###############################################')
    print('###############################################')
    print('###############################################')
