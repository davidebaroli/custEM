.. _examplelabel:

########
Examples
########

.. include:: ../examples/README.rst

HED in half-space
=================

This example is used to validate both, the total field approach implementation
and the provided primary field solutions in the pyhed library. The solutions
calculated with custEM are compared to results by *empymod* and *DIPOLE1D*.

.. raw:: html
    :file: ./_static/example_1_1.html

|
    
.. figure:: ./_static/example_1_mesh.png
   :width: 80%
   :align: center
   :alt: Halfspace mesh with 10 km observation line.
   
   Halfspace mesh with 10 km observation line.

|
   
.. raw:: html
    :file: ./_static/example_1_2.html

|

.. image:: ./_static/example_1_et.png
   :width: 49%
   :alt: E_real on surface.
.. image:: ./_static/example_1_ht.png
   :width: 49%
   :alt: H_real on surface.

*E_real and H_real on surface.*

|
    
.. raw:: html
    :file: ./_static/example_1_3.html

|
    
.. figure:: ./_static/example_1_plot.png
   :width: 80%
   :align: center
   :alt: Results on 10 km observation lines perpendicular to HED Tx.
   
   Results on 10 km observation lines perpendicular to HED Tx.
   
Topography example
==========

Topography model example - work in progress.

Example #3
==========

Model study example or similar - work in progress.