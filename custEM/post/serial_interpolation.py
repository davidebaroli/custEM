# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 15:14:20 2016

@author: Rochlitz.R
"""

import dolfin as df
from custEM.core import MOD
import numpy as np
import sys
import os
import getopt
from mpi4py import MPI

"""
This file is always called in serial from the root process of an mpi run,
otherwise a crash would occur due to a missing 'MPI-Parent' for synchronization
"""


class SerialInterpolator:

    """
    Interpolator class for interpolation in serial, called from an mpi root
    process on the fly. this is neceassary since FEniCS does not allow for
    interpolation between different meshes in parallel so far.

    Class internal functions:
    -------------------------

    - interpolate()
        perform the desired interpolation using FEniCS functionalities
    """

    def __init__(self, para_dir, quantity, interpolation_mesh,
                 interp_mesh_dir, out_name, rank):

        """
        Required arguments:
        -------------------

        - para_dir, type str
            parameter directory including a temporary "interp_dict.json" file,
            same as the **para_dir** as set in the **MOD** instance

        - quantitiy, type str
            Quantitiy to be interpolated, either **E_t**, **E_s**,
            **H_t**, **H_s**, **A_t** or **A_s**

        - interpolation_mesh, type str
            name of the interpolation mesh

        - interp_mesh_dir, type str
            name of the interpolation mesh directory

        - out_name, None or type str
            could be specified manually, if None, a default name will be used
        """

        import json
        with open(para_dir + '/interp_dict_' + str(rank) + '.json', 'r') as tf:
            self.__dict__.update(json.load(tf))
        os.remove(para_dir + '/interp_dict_' + str(rank) + '.json')
        try:
            self.interp_mesh_dir = interp_mesh_dir
            self.M = MOD(str(self.mod_name), str(self.mesh_name),
                         str(self.approach), overwrite=False,
                         file_format=str(self.file_format),
                         load_existing=True, r_dir=str(self.r_dir),
                         para_dir=str(self.para_dir), mute=True,
                         m_dir=str(self.m_dir), field_selection=quantity,
                         p=self.p)
            self.M.I.find_quantity_in_fenics_instance(quantity)
        except:
            print("Fatal error ! Import failed! Aborting ...")
            raise SystemExit
        self.interpolate(interpolation_mesh, out_name)

    def interpolate(self, interp_mesh, out_name):

        """
        interpolate data between two meshes (usually from 3D to slice or line)

        Required arguments:
        -------------------

        - it is referred to the **__init__**
        """

        target_mesh = df.Mesh(str(self.interp_mesh_dir) + '/' +
                              str(interp_mesh) + '.xml')
        if self.dg_interpolation:
            V_target = df.VectorFunctionSpace(target_mesh, "DG", self.interp_p)
        else:
            V_target = df.VectorFunctionSpace(target_mesh, "CG", self.interp_p)

        try:
            data_interp1 = df.interpolate(self.M.I.q1, V_target)
            q1_data = np.hstack((data_interp1.vector().get_local(
                    ).reshape(-1, 3),
                                 V_target.tabulate_dof_coordinates(
                                 ).reshape(-1, 3)[0::3, :]))
            data_interp2 = df.interpolate(self.M.I.q2, V_target)
            q2_data = np.hstack((data_interp2.vector().get_local(
                    ).reshape(-1, 3),
                                 V_target.tabulate_dof_coordinates(
                                 ).reshape(-1, 3)[0::3, :]))
        except:
            print('Interpolation failed, trying project function')
            try:
                data_interp1 = df.interpolate(self.M.I.q1, V_target)
                q1_data = np.hstack((data_interp1.vector().get_local(
                        ).reshape(-1, 3),
                                     V_target.tabulate_dof_coordinates(
                                     ).reshape(-1, 3)[0::3, :]))
                data_interp2 = df.interpolate(self.M.I.q2, V_target)
                q2_data = np.hstack((data_interp2.vector().get_local(
                        ).reshape(-1, 3),
                                     V_target.tabulate_dof_coordinates(
                                     ).reshape(-1, 3)[0::3, :]))
            except:
                print('Project function failed as well!')
                print('continuing...')

        try:
            complex_data = q1_data + 1j * q2_data

            if self.interp_p != 2:
                out_name += '_p1_interpolated'

            df.File(str(self.interp_dir) + '/' +
                    out_name + '_real.pvd') << data_interp1
            df.File(str(self.interp_dir) + '/' + out_name +
                    '_imag.pvd') << data_interp2
            np.save(str(self.interp_dir) + '/' + out_name + '.npy',
                    complex_data)
        except:
            print(str(self.interp_dir) + '/' + out_name + '.npy is missing!')


if __name__ == "__main__":

    """
    read the five arguments for the **Interpolator** class, call the latter
    and synchronize with the 'Parent process' afterwards
    """

    myopts, args = getopt.getopt(sys.argv[1:], "d:q:i:j:o:r:")
    for oo, aa in myopts:
        if oo == '-d':
            para_dir = str(aa)
        elif oo == '-q':
            quantity = str(aa)
        elif oo == '-i':
            interp_mesh = str(aa)
        elif oo == '-j':
            interp_m_dir = str(aa)
        elif oo == '-o':
            out_name = str(aa)
        elif oo == '-r':
            rank = str(aa)
        else:
            print("Usage: %s   -d parameter dir  -q   interpolation quantity  "
                  " -i interpolation target mesh    -j interpolation mesh "
                  " directory      -o  export name       -r   rank id"
                  % sys.argv[0])

    parent = MPI.Comm.Get_parent()
    SerialInterpolator(para_dir, quantity, interp_mesh,
                       interp_m_dir, out_name, rank)
    parent.Barrier()
    parent.Disconnect()
