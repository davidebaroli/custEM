# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import numpy as np
import hashlib
import custEM as ce
from custEM.misc import mpi_print as mpp
from custEM.misc import run_serial as rs
from custEM.misc import write_h5
from custEM.misc import read_h5
from mpi4py import MPI
import time


class PrimaryField:

    """
    PrimaryField class for computing or loading existing PrimaryFields for
    'Secondary' field formulations, called by **MOD.FE** instance via the
    FunctionSpace instance function **add_Primary_Field()**.

    For documentation of the the ***comet** toolbox (**pyhed** module), it is
    referred to the documentation of this package.

    Class internal functions:
    -------------------------

    - functions are deprecated...
        The **PrimaryField** module will be simplified soon!
    """

    def __init__(self, FS, MP, calc=True, **pf_kwargs):

        """
        Initialization of PrimaryField parameters

        Required arguments:
        -------------------

        - FS, type class
            FunctionSpaces instance

        - MP, type class
            ModelParameters instance

        Keyword arguments:
        ------------------

        These parameters can be updated to custom values via setting the same
        keyword arguments with calling the function **build_var_form()** of
        the **MOD.FE** instance.

        - s_type = 'HED', type str
            infinite length horizontal dipole source, alternatives:
            **'Line'** (real finite length dipole source),
            **'Loop_C'** (circular loop source) and
            **'Loop_R'** (rectangular loop source)
            **'Path'** (arbitrary sources)

        - closed_path = False, type bool
            set True, if an ungrounded (loop) transmitter should be built by
            closing the path between the first and last coordinate of **Path**
            source

        - pf_type='Halfspace', type str
            primary field type, alternatives: **Fullspace**, ***LayerdEarth*
            or **Custom**

        - pvd_flag=False, type bool
            flag controlling if 'pvd' - files are exported (**'True'**) or not

        - pf_name=None, type str
            An alternative name for the primary fields of the model can be
            defined. It is not recommended to change default name conventions

        - pyhed_interp = False, type bool
            set True, if the alternative interpolation-based primary field
            calculation technique of the *pyhed*-module should be used

        - pf_EH_flag = 'E', tyoe str
            either **E** or **H** to specify, which type of primary fields
            should be used for the FE caluclations later on
        """

        self.FS = FS
        self.MP = MP
        self.s_type = 'hed'
        self.pf_type = pf_kwargs.pop('pf_type', 'halfspace')
        self.pvd_flag = True
        self.pf_name = None
        self.path = None
        self.closed_path = False
        self.pyhed_interp = False
        self.pf_EH_flag = 'E'
        self.ph_log_level = 0
        self.s_dir = self.MP.r_dir + '/primary_fields/' + self.pf_type + '/'

        self.origin = [0.0, 0.0, 0.0]
        self.r = 100.0
        self.start = [-100.0, 0.0, 0.0]
        self.stop = [100.0, 0.0, 0.0]
        self.dipole_z = 0.
        self.length = 1.
        self.azimuth = 0.
        self.min_length = None
        self.n_segs = 100
        self.pf_p = 1
        self.n_tx = 1
        for key in pf_kwargs:
            if key not in self.__dict__:
                if key not in ['bc', 'sigma_from_func',
                               'eps_from_func', 'quasi_static']:
                    mpp('!!! Warning: Unknown PF-MODEL parameter set:', key)

        self.__dict__.update(pf_kwargs)

        if self.n_tx > 1:
            mpp('Fatal Error! Multiple Tx / right-hand sides not implemented '
                'in primary_fields and approaches module yet! For urgent '
                'support of this feature, please contact the authors.')
            raise SystemExit

        # for temporary serial pyhed runs for PF computations on boundaries !

        self.thick = self.MP.thick
        self.sigma = []
        self.sigma = self.MP.sigma[1:1+self.MP.n_layers]

        self.omega = self.MP.omega
        self.J = self.MP.J
        self.n_procs = int(MPI.COMM_WORLD.Get_size() *
                           self.MP.procs_per_proc)

        if self.pf_type not in ('fullspace', 'halfspace', 'layered_earth',
                                'custom'):
            mpp('!!! Typing Error: Choose either "fullspace", "halfspace", '
                'layered_earth or "custom" as "pf_type" parameter')
            raise Exception(self.pf_type)
            raise SystemExit

    def export_primary_field(self, field, f_type='H'):

        """
        Saves externally calculated primary fields in dedicated files.
        """
        df.MPI.barrier(self.MP.mpi_cw)

        if np.shape(field)[0] == 3 and np.shape(field)[1] != 3:
            field = field.T
            # always (n, 3) to ensure .ravel() is working correctly

        # save location
        real_name = '{}_{}_0_real_cg'.format(self.pf_name, f_type)
        imag_name = '{}_{}_0_imag_cg'.format(self.pf_name, f_type)

        V_cg = df.VectorFunctionSpace(self.FS.mesh, 'CG', self.pf_p)
        df_imag = df.Function(V_cg)
        df_real = df.Function(V_cg)
        df_real.vector().set_local(field.real.ravel())
        df_real.vector().apply('insert')
        df_imag.vector().set_local(field.imag.ravel())
        df_imag.vector().apply('insert')

        mpp('...  saving primary "' + f_type + '" field in '
            '"primary_fields" directory  ...', pre_dash=False)

        if self.MP.file_format == 'h5':
            write_h5(self.MP.mpi_cw, df_real, self.s_dir + real_name + '.h5')
            write_h5(self.MP.mpi_cw, df_imag, self.s_dir + imag_name + '.h5')
        elif self.MP.file_format == 'xml':
            df.File(self.s_dir + real_name + '.xml') << df_real
            df.File(self.s_dir + imag_name + '.xml') << df_imag

        if self.pvd_flag:
            df.File(self.s_dir + real_name + '.pvd') << df_real
            df.File(self.s_dir + imag_name + '.pvd') << df_imag

        mpp('...  done.', pre_dash=False)

    def load_primary_fields(self, boundary_fields=False):

        if self.pf_name is None:
            self.pf_name = self.init_primary_field_name(boundary_fields)

        if self.pf_p != self.FS.p:
            mpp('Notice! using specified polynomial order: ' + str(self.pf_p) +
                ' for primary fields.')
        V_cg = df.VectorFunctionSpace(self.FS.mesh, 'CG', self.pf_p)

        if os.path.isfile(self.s_dir + self.pf_name + '_E_0_real_cg.' +
                          self.MP.file_format) is False:
            if self.pf_type is 'fullspace':
                if self.s_type is not 'hed':
                    mpp('Fatal Error!: Currently only "hed" supported as '
                        '"s_type" parameter for "fullspace" primary fields!')
                    raise SystemExit
                else:
                    self.HED_Fullspace_field(V_cg)

            elif self.pf_type is 'halfspace' or 'layered_earth':
                self.Primary_Halfspace_field(V_cg, boundary_fields)

            else:
                # here one can implement other custom primary fields
                pass

        mpp('...  loading primary "' + self.pf_type + '" fields  ...')
        target_str = self.s_dir + self.pf_name

        if self.MP.file_format == 'h5':
            E_0_r, E_0_i = df.Function(V_cg), df.Function(V_cg)
            H_0_r, H_0_i = df.Function(V_cg), df.Function(V_cg)
            read_h5(self.MP.mpi_cw, E_0_r, target_str + '_E_0_real_cg.h5')
            read_h5(self.MP.mpi_cw, E_0_i, target_str + '_E_0_imag_cg.h5')
            read_h5(self.MP.mpi_cw, H_0_r, target_str + '_H_0_real_cg.h5')
            read_h5(self.MP.mpi_cw, H_0_i, target_str + '_H_0_imag_cg.h5')

        elif self.MP.file_format == 'xml':
            E_0_r = df.Function(V_cg, target_str + '_E_0_real_cg.xml')
            E_0_i = df.Function(V_cg, target_str + '_E_0_imag_cg.xml')
            H_0_r = df.Function(V_cg, target_str + '_H_0_real_cg.xml')
            H_0_i = df.Function(V_cg, target_str + '_H_0_imag_cg.xml')

        if boundary_fields:
            self.E_0_r_cg_B, self.E_0_i_cg_B = [], []
            self.H_0_r_cg_B, self.H_0_i_cg_B = [], []
            self.E_0_r_cg_B.append(E_0_r)
            self.E_0_i_cg_B.append(E_0_i)
            self.H_0_r_cg_B.append(H_0_r)
            self.H_0_i_cg_B.append(H_0_i)
        else:
            self.E_0_r_cg, self.E_0_i_cg = [], []
            self.H_0_r_cg, self.H_0_i_cg = [], []
            self.E_0_r_cg.append(E_0_r)
            self.E_0_i_cg.append(E_0_i)
            self.H_0_r_cg.append(H_0_r)
            self.H_0_i_cg.append(H_0_i)

    def HED_Fullspace_field(self, V_cg):

        path = os.getcwd()
        E_0_r_cg = df.Function(V_cg)
        E_0_i_cg = df.Function(V_cg)
        H_0_r_cg = df.Function(V_cg)
        H_0_i_cg = df.Function(V_cg)

        xyz = (V_cg.tabulate_dof_coordinates().reshape(-1, 3)[0::3, :])
        mpp('...  calculating primary "' + self.pf_type + '" "' + self.s_type +
            '" field  ...')
        mpp('  -  ' + 'origin:' + '   -->  ', self.origin, pre_dash=False)

        Ids = self.MP.J * self.length
        omega = self.MP.omega
        kk = np.sqrt((self.MP.sigma_ground[0] * omega * self.MP.mu) / 2)
        k = kk - kk * 1j
        a = xyz.shape[0]

        E_x = np.zeros((a), dtype=complex)
        E_y = np.zeros((a), dtype=complex)
        E_z = np.zeros((a), dtype=complex)
        H_x = np.zeros((a), dtype=complex)
        H_y = np.zeros((a), dtype=complex)
        H_z = np.zeros((a), dtype=complex)

        for i in range(a):

            x, y, z = xyz[i, :]
            r = np.linalg.norm((x, y, z))
            if r < 1e-2:
                r = 1.0

            const_E = ((-Ids / (4 * np.pi * self.MP.sigma_ground[0] * r**3)) *
                       np.exp(-k * r * 1j))
            E_x[i] = (const_E * (x * x / r**2) *
                      (-k**2 * r**2 + 3 * k * r * 1j + 3) +
                      const_E * (k**2 * r**2 - k * r * 1j - 1))
            E_y[i] = (const_E * (x * y / r**2) *
                      (-k**2 * r**2 + 3 * k * r * 1j + 3))
            E_z[i] = (const_E * (x * z / r**2) *
                      (-k**2 * r**2 + 3 * k * r * 1j + 3))

            const_H = ((Ids / (4 * np.pi * r**2)) *
                       (1j * k * r + 1) * np.exp(-k * r * 1j))
            H_x[i] = const_H * 0.0
            H_y[i] = const_H * (-z / r)
            H_z[i] = const_H * (y / r)

            if self.azimuth == 90.:
                H_y[i] = const_H * 0.0
                H_x[i] = const_H * (-z / r)
                H_z[i] = const_H * (x / r)
                E_y[i] = (const_E * (y * y / r**2) *
                          (-k**2 * r**2 + 3 * k * r * 1j + 3) +
                          const_E * (k**2 * r**2 - k * r * 1j - 1))
                E_x[i] = (const_E * (y * x / r**2) *
                          (-k**2 * r**2 + 3 * k * r * 1j + 3))
                E_z[i] = (const_E * (y * z / r**2) *
                          (-k**2 * r**2 + 3 * k * r * 1j + 3))

        df.MPI.barrier(self.MP.mpi_cw)
        E_0_r_cg.vector().set_local((np.hstack((E_x.real[np.newaxis].T,
                                     np.hstack((E_y.real[np.newaxis].T,
                                                E_z.real[np.newaxis].T))
                                                )).ravel()))
        E_0_r_cg.vector().apply('insert')

        E_0_i_cg.vector().set_local((np.hstack((E_x.imag[np.newaxis].T,
                                     np.hstack((E_y.imag[np.newaxis].T,
                                                E_z.imag[np.newaxis].T))
                                                )).ravel()))
        E_0_i_cg.vector().apply('insert')

        H_0_r_cg.vector().set_local((np.hstack((H_x.real[np.newaxis].T,
                                     np.hstack((H_y.real[np.newaxis].T,
                                                H_z.real[np.newaxis].T))
                                                )).ravel()))
        H_0_r_cg.vector().apply('insert')

        H_0_i_cg.vector().set_local((np.hstack((H_x.imag[np.newaxis].T,
                                     np.hstack((H_y.imag[np.newaxis].T,
                                                H_z.imag[np.newaxis].T))
                                                )).ravel()))
        H_0_i_cg.vector().apply('insert')

        os.chdir(self.MP.r_dir + '/primary_fields/' + self.pf_type)
        mpp('...  done.')
        mpp('...  saving primary field in "primary_fields" directory  ...')

        if self.MP.file_format == 'h5':
            write_h5(self.MP.mpi_cw, E_0_r_cg,
                     self.pf_name + '_E_0_real_cg.h5')
            write_h5(self.MP.mpi_cw, E_0_i_cg,
                     self.pf_name + '_E_0_imag_cg.h5')
            write_h5(self.MP.mpi_cw, H_0_r_cg,
                     self.pf_name + '_H_0_real_cg.h5')
            write_h5(self.MP.mpi_cw, H_0_i_cg,
                     self.pf_name + '_H_0_imag_cg.h5')
        elif self.MP.file_format == 'xml':
            df.File(self.pf_name + '_E_0_real_cg.xml') << E_0_r_cg
            df.File(self.pf_name + '_E_0_imag_cg.xml') << E_0_i_cg
            df.File(self.pf_name + '_H_0_real_cg.xml') << H_0_r_cg
            df.File(self.pf_name + '_H_0_imag_cg.xml') << H_0_i_cg

        if self.pvd_flag:
            df.File(self.pf_name + '_E_0_real_cg.pvd') << E_0_r_cg
            df.File(self.pf_name + '_E_0_imag_cg.pvd') << E_0_i_cg
            df.File(self.pf_name + '_H_0_real_cg.pvd') << H_0_r_cg
            df.File(self.pf_name + '_H_0_imag_cg.pvd') << H_0_i_cg

        df.MPI.barrier(self.MP.mpi_cw)
        os.chdir(path)

    def Primary_Halfspace_field(self, V_cg, boundary_fields):

        if 'loop' in self.s_type:
            self.closed_path = True

        # use uneven number of dipoles to avoid singularities / artifacts in
        # on symmetry plane of dipole source
        if not self.closed_path and (self.n_segs % 2) == 0:
            self.n_segs += 1
            mpp('  -  raised "n_segs" by 1 to get an odd number of segments '
                'for avoiding singularities in symmetry plane  -  ')

        if boundary_fields:
            self.write_serial_calculation_parameters(os.getcwd())
            rs(os.path.dirname(ce.__file__) +
               '/fem/calc_primary_boundary_fields.py',
               ['-m'], [self.MP.mesh_name],
               ['-d'], [self.MP.r_dir],
               ['-t'], [self.pf_type],
               ['-f'], [self.MP.file_format])
            return

        from comet import pyhed as ph
        ph.log.setLevel(self.ph_log_level)

        t1 = time.time()
        xyz = (V_cg.tabulate_dof_coordinates().reshape(-1, 3)[0::3, :]).T

        if self.s_type is 'hed':
            source = ph.loop.buildDipole(self.origin, length=self.length,
                                         angle=np.deg2rad(self.azimuth))
            mpp('########## Primary "' + self.pf_type + '_' + self.s_type +
                '" field is being calculated ... ###########\nHED origin:')
            mpp('    Origin' + '   -->  ', self.origin, pre_dash=False)

        elif self.s_type is 'line':
            source = ph.loop.buildLine(self.start[:2], self.stop[:2],
                                       num_segs=self.n_segs,
                                       max_length=self.min_length)
            mpp('########## Primary "' + self.pf_type + '_' + self.s_type +
                '" field is being calculated ... ###########\n'
                'Line source parameters:')
            mpp('    Start_point' + '   -->  ', self.start, pre_dash=False)
            mpp('    Stop_point' + '   -->  ', self.stop, pre_dash=False)
            mpp('    No. of segments' + '   --> ', self.n_segs, pre_dash=False)

        elif self.s_type is 'loop_c':
            source = ph.loop.buildCircle(self.r, P=self.origin,
                                         num_segs=self.n_segs,
                                         max_length=self.min_length)
            mpp('########## Primary "' + self.pf_type + '_' + self.s_type +
                '" field is being calculated ... ###########\n'
                'Circular loop source parameters:')
            mpp('    Origin' + '   -->  ', self.origin, pre_dash=False)
            mpp('    Radius' + '   -->  ', self.r, pre_dash=False)
            mpp('    No. of segments' + '   --> ', self.n_segs, pre_dash=False)

        elif self.s_type is 'loop_r':

            if df.near(self.start[0], self.stop[0], eps=1e-4) or \
               df.near(self.start[1], self.stop[1], eps=1e-4):

                mpp('Fatal error: Given start and stop coordinates do not '
                    'match to a rectangular loop source: x or y start or stop '
                    'coordinates are identical')
                raise SystemExit

            S_1 = [self.start[0], self.start[1], 0.0]
            S_2 = [self.stop[0], self.start[1], 0.0]
            S_3 = [self.stop[0], self.stop[1], 0.0]
            S_4 = [self.start[0], self.stop[1], 0.0]

            source = ph.loop.buildRectangle(np.array((S_1, S_2, S_3, S_4)),
                                            num_segs=self.n_segs,
                                            max_length=self.min_length)
            mpp('########## Primary "' + self.pf_type + '_' + self.s_type +
                '" field is being calculated ... ###########\n'
                'Rectangular loop source parameters:')
            mpp('    Corner 1' + '   -->  ', S_1, pre_dash=False)
            mpp('    Corner 2' + '   -->  ', S_2, pre_dash=False)
            mpp('    Corner 3' + '   -->  ', S_3, pre_dash=False)
            mpp('    Corner 4' + '   -->  ', S_4, pre_dash=False)
            mpp('    No. of segments' + '   -->  ', self.n_segs/4,
                pre_dash=False)

        elif self.s_type is 'path':
            source = ph.loop.buildLoop(self.path, num_segs=self.n_segs,
                                       max_length=self.min_length,
                                       grounded=not self.closed_path)
            mpp('########## Primary "' + self.pf_type + '_' + self.s_type +
                '" field is being calculated ... ###########\n'
                'Source from path parameters:')
            mpp('    closed loop source' + '   -->  ',
                self.closed_path, pre_dash=False)

        else:
            mpp('!!! Fatal Error, supported  "s_type" parameters for '
                'Halfspace" primary fields are: "HED, "Line", "Loop_C"'
                ' or "Loop_R"')
            raise SystemExit

        source.setLoopMesh(xyz)
        source.config.rho = 1. / np.array(self.MP.sigma[1:1+self.MP.n_layers])
        source.config.d = self.MP.thick
        source.config.f = self.MP.omega / (2.0 * np.pi)
        source.config.current = self.MP.J

        # ################### calc E-Field ##################################

        source.config.ftype = 'E'
        E_field = source.calculate(interpolate=self.pyhed_interp,
                                   src_z=self.dipole_z,
                                   num_cpu=self.MP.procs_per_proc)
        self.export_primary_field(E_field, f_type='E')

        # ################### calc H-Field ##################################

        source.config.ftype = 'H'
        if self.dipole_z != 0.:
            mpp('Warning! Shifting Tx to depth is only considered for '
                'primary E fields, not H fields! Continuning  ...')
        H_field = source.calculate(interpolate=self.pyhed_interp,
                                   src_z=self.dipole_z,  # ignored !
                                   num_cpu=self.MP.procs_per_proc)
        self.export_primary_field(H_field, f_type='H')

        df.MPI.barrier(self.MP.mpi_cw)
        t2 = time.time() - t1
        mpp('Computation time (s) for primary fields [s]:  -->  ', t2,
            pre_dash=False)

    def init_primary_field_name(self, boundary_fields):

        name = (self.MP.mesh_name + '_' + self.pf_type + '_' + self.s_type +
                '_nL_' + str(self.MP.n_layers) + '_sg_' +
                '_format_' + self.MP.file_format +
                '_Om_' + str(self.MP.omega) + '_J_' + str(self.MP.J) + '_Sg_' +
                str(self.MP.sigma_ground[:self.MP.n_layers]) +
                '_n_segs_' + str(self.n_segs) + '_min_length' +
                str(self.min_length) + '_x0_' + str(self.origin[0]) +
                '_y0_' + str(self.origin[1]) + '_z0_' + str(self.origin[2]) +
                '_R_' + str(self.r) + '_sx0_' + str(self.start[0]) +
                '_sy0_' + str(self.start[1]) + '_sy0_' + str(self.start[2]) +
                '_sx1_' + str(self.stop[0]) + '_sz1_' + str(self.stop[1]) +
                '_sz1_' + str(self.stop[2]) + '_pf_p_' + str(self.pf_p) +
                str(self.path))

        if boundary_fields:
            name += '_BF'

        return(hashlib.sha256(name.encode('ascii')).hexdigest())

    def write_serial_calculation_parameters(self, path):

        import json
        os.chdir(self.MP.r_dir + '/primary_fields/' + self.pf_type)

        if self.path is not None:
            np.save('source_path.npy', self.path)
            self.path = 'import'

        A = self.__dict__.copy()
        del A['FS']
        del A['MP']
        json = json.dumps(A)
        f = open("pf_dict.json", "w")
        f.write(json)
        f.close()
        os.chdir(path)
