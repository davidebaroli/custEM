# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
from custEM.misc import mpi_print as mpp
from custEM.fem import ApproachBase
from custEM.fem import print_fem_parameters
from custEM.fem import check_sigma_dx_conformity
from custEM.fem import check_source
from custEM.fem import add_sigma_vals_to_list
from custEM.fem import check_sigma_vals
from custEM.fem import build_DG_func
from custEM.fem import build_DG_tensor_func


"""
Here, four different EM modeling approaches: "A_Phi_nodal",
A_Phi_mixed" and "E_vector", referring to (Badea, 2001), (Ansari, 2014)
and e.g. (Schwarzbach, 2009), respectively, are implemented. The fourth
approach is a "H_vector" formulation derived by the authors.
"""


class A_V_nodal(ApproachBase):

    """
    FE implementation of nodal A-Phi potential approach (Badea, 2001) or
    (Puzyref, 2013)
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters

        super(self.__class__, self).__init__()

        (self.v_1r, self.v_2r, self.v_3r, self.v_4r,
         self.v_1i, self.v_2i, self.v_3i, self.v_4i) = df.TestFunctions(FS.M)
        (self.u_1r, self.u_2r, self.u_3r, self.u_4r, self.u_1i,
         self.u_2i, self.u_3i, self.u_4i) = df.TrialFunctions(FS.M)

        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v_1r', 'v_1i', 'v_2r', 'v_2i', 'v_3r', 'v_3i', 'DOM',
                     'v_4r', 'v_4i', 'u_1r', 'u_1i', 'u_2r', 'u_2i',
                     'u_3r', 'u_3i', 'u_4r', 'u_4i', 'dx', 'FS', 'MP', 'mute',
                     'path']

        # temporary solution
        mpp('  -  Notice! *sigma_from_func* not implemented for this approch '
            'until now  -  Continuing  ...')
        self.sigma_from_func = False

    def build_var_form(self, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            self.sigma_from_func = False
            mpp('Issue! "sigma_from_func" option for anisotropic cond. not '
                'implemented yet!\nUsing "old" multiple domain technique ...')

        self.bc = 'ZD'
        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        self.LHS_form()
        if '_t' in self.MP.approach:
            self.RHS_form_Total()
            self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
        else:
            self.FS.anom_flag = check_sigma_vals(self.MP)
            self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.RHS_form_Secondary()

    def LHS_form(self):

        """
        left-hand side contributions for A-Phi nodal approach
        """

        C = df.Constant((self.MP.mu * self.MP.omega))
        c1 = add_sigma_vals_to_list(self.MP.sigma)
        dx = self.FS.DOM.dx

        self.L = ((1./C) * df.inner(df.grad(self.u_1r),
                                    df.grad(self.v_1r)) * dx(0) -
                  df.inner(c1[0] * self.u_1i, self.v_1r) * dx(0) -
                  df.inner(self.u_4i.dx(0), c1[0] * self.v_1r) * dx(0) -

                  df.inner(c1[0] * self.u_1r, self.v_1i) * dx(0) -
                  (1./C) * df.inner(df.grad(self.u_1i),
                                    df.grad(self.v_1i)) * dx(0) -
                  df.inner(self.u_4r.dx(0), c1[0] * self.v_1i) * dx(0) +

                  (1./C) * df.inner(df.grad(self.u_2r),
                                    df.grad(self.v_2r)) * dx(0) -
                  df.inner(c1[0] * self.u_2i, self.v_2r) * dx(0) -
                  df.inner(self.u_4i.dx(1), c1[0] * self.v_2r) * dx(0) -

                  df.inner(c1[0] * self.u_2r, self.v_2i) * dx(0) -
                  (1./C) * df.inner(df.grad(self.u_2i),
                                    df.grad(self.v_2i)) * dx(0) -
                  df.inner(self.u_4r.dx(1), c1[0] * self.v_2i) * dx(0) +

                  (1./C) * df.inner(df.grad(self.u_3r),
                                    df.grad(self.v_3r)) * dx(0) -
                  df.inner(c1[0] * self.u_3i, self.v_3r) * dx(0) -
                  df.inner(self.u_4i.dx(2), c1[0] * self.v_3r) * dx(0) -

                  df.inner(c1[0] * self.u_3r, self.v_3i) * dx(0) -
                  (1./C) * df.inner(df.grad(self.u_3i),
                                    df.grad(self.v_3i)) * dx(0) -
                  df.inner(self.u_4r.dx(2), c1[0] * self.v_3i) * dx(0) -

                  df.inner(c1[0] * self.u_1i, self.v_4r.dx(0)) * dx(0) -
                  df.inner(c1[0] * self.u_2i, self.v_4r.dx(1)) * dx(0) -
                  df.inner(c1[0] * self.u_3i, self.v_4r.dx(2)) * dx(0) -
                  df.inner(c1[0] * df.grad(self.u_4i),
                           df.grad(self.v_4r)) * dx(0) -
                  df.inner(c1[0] * self.u_1r, self.v_4i.dx(0)) * dx(0) -
                  df.inner(c1[0] * self.u_2r, self.v_4i.dx(1)) * dx(0) -
                  df.inner(c1[0] * self.u_3r, self.v_4i.dx(2)) * dx(0) -
                  df.inner(c1[0] * df.grad(self.u_4r),
                           df.grad(self.v_4i)) * dx(0))

        for j in range(1, self.FS.DOM.n_domains):

            if type(self.MP.sigma[j]) is float:
                self.L += ((1./C) * df.inner(df.grad(self.u_1r),
                                             df.grad(self.v_1r)) * dx(j) -
                           df.inner(c1[j] * self.u_1i, self.v_1r) * dx(j) -
                           df.inner(self.u_4i.dx(0), c1[j] * self.v_1r)*dx(j) -

                           df.inner(c1[j] * self.u_1r, self.v_1i) * dx(j) -
                           (1./C) * df.inner(df.grad(self.u_1i),
                                             df.grad(self.v_1i)) * dx(j) -
                           df.inner(self.u_4r.dx(0), c1[j] * self.v_1i)*dx(j) +

                           (1./C) * df.inner(df.grad(self.u_2r),
                                             df.grad(self.v_2r)) * dx(j) -
                           df.inner(c1[j] * self.u_2i, self.v_2r) * dx(j) -
                           df.inner(self.u_4i.dx(1), c1[j] * self.v_2r)*dx(j) -

                           df.inner(c1[j] * self.u_2r, self.v_2i) * dx(j) -
                           (1./C) * df.inner(df.grad(self.u_2i),
                                             df.grad(self.v_2i)) * dx(j) -
                           df.inner(self.u_4r.dx(1), c1[j] * self.v_2i)*dx(j) +

                           (1./C) * df.inner(df.grad(self.u_3r),
                                             df.grad(self.v_3r)) * dx(j) -
                           df.inner(c1[j] * self.u_3i, self.v_3r) * dx(j) -
                           df.inner(self.u_4i.dx(2), c1[j] * self.v_3r)*dx(j) -

                           df.inner(c1[j] * self.u_3r, self.v_3i) * dx(j) -
                           (1./C) * df.inner(df.grad(self.u_3i),
                                             df.grad(self.v_3i)) * dx(j) -
                           df.inner(self.u_4r.dx(2), c1[j] * self.v_3i)*dx(j) -

                           df.inner(c1[j] * self.u_1i, self.v_4r.dx(0))*dx(j) -
                           df.inner(c1[j] * self.u_2i, self.v_4r.dx(1))*dx(j) -
                           df.inner(c1[j] * self.u_3i, self.v_4r.dx(2))*dx(j) -
                           df.inner(c1[j] * df.grad(self.u_4i),
                                    df.grad(self.v_4r)) * dx(j) -
                           df.inner(c1[j] * self.u_1r, self.v_4i.dx(0))*dx(j) -
                           df.inner(c1[j] * self.u_2r, self.v_4i.dx(1))*dx(j) -
                           df.inner(c1[j] * self.u_3r, self.v_4i.dx(2))*dx(j) -
                           df.inner(c1[j] * df.grad(self.u_4r),
                                    df.grad(self.v_4i)) * dx(j))
            else:
                self.L += (+(1.0/C) * df.inner(df.grad(self.u_1r),
                                               df.grad(self.v_1r)) * dx(j) -
                           df.inner(c1[j][0][0] * self.u_1i, self.v_1r)*dx(j) -
                           df.inner(self.u_4i.dx(0),
                                    c1[j][0][0] * self.v_1r) * dx(j) -

                           df.inner(c1[j][0][0] * self.u_1r, self.v_1i)*dx(j) -
                           (1.0/C) * df.inner(df.grad(self.u_1i),
                                              df.grad(self.v_1i)) * dx(j) -
                           df.inner(self.u_4r.dx(0),
                                    c1[j][0][0] * self.v_1i) * dx(j) +

                           (1.0/C) * df.inner(df.grad(self.u_2r),
                                              df.grad(self.v_2r)) * dx(j) -
                           df.inner(c1[j][1][1] * self.u_2i, self.v_2r)*dx(j) -
                           df.inner(self.u_4i.dx(1),
                                    c1[j][1][1] * self.v_2r) * dx(j) -

                           df.inner(c1[j][1][1] * self.u_2r, self.v_2i)*dx(j) -
                           (1.0/C) * df.inner(df.grad(self.u_2i),
                                              df.grad(self.v_2i)) * dx(j) -
                           df.inner(self.u_4r.dx(1),
                                    c1[j][1][1] * self.v_2i) * dx(j) +

                           (1.0/C) * df.inner(df.grad(self.u_3r),
                                              df.grad(self.v_3r)) * dx(j) -
                           df.inner(c1[j][2][2] * self.u_3i, self.v_3r)*dx(j) -
                           df.inner(self.u_4i.dx(2),
                                    c1[j][2][2] * self.v_3r) * dx(j) -

                           df.inner(c1[j][2][2] * self.u_3r, self.v_3i)*dx(j) -
                           (1.0/C) * df.inner(df.grad(self.u_3i),
                                              df.grad(self.v_3i)) * dx(j) -
                           df.inner(self.u_4r.dx(2),
                                    c1[j][2][2] * self.v_3i) * dx(j) -

                           df.inner(c1[j][0][0] * self.u_1i, self.v_4r.dx(0)) *
                           dx(j) - df.inner(c1[j][1][1] * self.u_2i,
                                            self.v_4r.dx(1)) *
                           dx(j) - df.inner(c1[j][2][2] * self.u_3i,
                                            self.v_4r.dx(2)) *
                           dx(j) - df.inner(c1[j] * df.grad(self.u_4i),
                                            df.grad(self.v_4r)) * dx(j) -
                           df.inner(c1[j][0][0] * self.u_1r, self.v_4i.dx(0)) *
                           dx(j) - df.inner(c1[j][1][1] * self.u_2r,
                                            self.v_4i.dx(1)) *
                           dx(j) - df.inner(c1[j][2][2] * self.u_3r,
                                            self.v_4i.dx(2)) *
                           dx(j) - df.inner(c1[j] * df.grad(self.u_4r),
                                            df.grad(self.v_4i)) * dx(j))

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = (df.inner(df.Constant(("0.0")), self.v_1r) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_1i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2r) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_3r) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_3i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_4r) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_4i) * dx_0)

    def RHS_form_Secondary(self):

        """
        right-hand side contributions for secondary field formulation
        """

        f_0 = df.Constant(('0'))
        dx = self.FS.DOM.dx
        PF = self.FS.PF
        om1 = df.Constant((1./self.MP.omega))
        c2 = add_sigma_vals_to_list(self.MP.delta_sigma)
        self.R = (df.inner(f_0, self.v_1r) * dx(0) +
                  df.inner(f_0, self.v_1i) * dx(0) +
                  df.inner(f_0, self.v_2r) * dx(0) +
                  df.inner(f_0, self.v_2i) * dx(0) +
                  df.inner(f_0, self.v_3r) * dx(0) +
                  df.inner(f_0, self.v_3i) * dx(0) +
                  df.inner(f_0, self.v_4r) * dx(0) +
                  df.inner(f_0, self.v_4i) * dx(0))

        if self.pf_EH_flag == 'H':
            mpp("Notice! Using alternative primary magnetic fields. "
                "For 'A-Phi-nodal', this requires transformation from vector "
                "to scalar fields.")
            import custEM as ce
            Solver = ce.core.Solver(self.FS, self)
            M, w1, w2 = df.PETScMatrix(), df.PETScVector(), df.PETScVector()

            mpp('############################################################')
            mpp('Testing A-Phi nodal p2 SF approch with p1 H to E conversion!')
            mpp('############################################################')
            k_r, k_i = df.TrialFunction(self.FS.V), df.TrialFunction(self.FS.V)
            l_r, l_i = df.TestFunction(self.FS.V), df.TestFunction(self.FS.V)
            br = df.inner(k_r, l_r) * dx(0)
            bi = df.inner(k_i, l_i) * dx(0)
            H_0_r = df.interpolate(PF.H_0_r_cg[0], self.FS.V)
            H_0_i = df.interpolate(PF.H_0_i_cg[0], self.FS.V)
            Kr = df.inner(df.inv(self.MP.sigma_air[0]) * df.curl(H_0_r),
                          l_r) * dx(0)
            Ki = df.inner(df.inv(self.MP.sigma_air[0]) * df.curl(H_0_i),
                          l_i) * dx(0)

            for j in range(1, len(self.MP.sigma)):
                br += df.inner(k_r, l_r) * dx(j)
                bi += df.inner(k_i, l_i) * dx(j)
                Kr += df.inner(df.inv(self.MP.sigma_ground[j-1]) *
                               df.curl(H_0_r), l_r) * dx(j)
                Ki += df.inner(df.inv(self.MP.sigma_ground[j-1]) *
                               df.curl(H_0_i), l_i) * dx(j)

            H1, b1 = df.assemble_system(br, Kr, A_tensor=M, b_tensor=w1)
            H2, b2 = df.assemble_system(bi, Ki, A_tensor=M, b_tensor=w2)
            E_0_r = Solver.solve_system_mumps(H1, [b1], self.FS.V,
                                              sym=True, mute=True)
            E_0_r_cg = df.interpolate(E_0_r[0], self.FS.V_cg)
            E_0_i = Solver.solve_system_mumps(H2, [b2], self.FS.V,
                                              sym=True, mute=True,
                                              first_call=False)
            E_0_i_cg = df.interpolate(E_0_i[0], self.FS.V_cg)

            # singular hack for increased PF accuracy
            #            df.File('E_r.xml') << E_0_r_cg
            #            df.File('E_i.xml') << E_0_i_cg
            #            raise SystemExit
            #            mesh = df.Mesh('meshes/_xml/example_3_mesh_p2.xml')
            #            FS = df.VectorFunctionSpace(mesh, 'CG', 1)
            #            Er = df.Function(self.FS.V_cg, 'E_r.xml')
            #            Ei = df.Function(self.FS.V_cg, 'E_i.xml')
            #            Er2 = df.interpolate(Er, FS)
            #            Ei2 = df.interpolate(Ei, FS)
            #            df.File('E_r2.xml') << Er2
            #            df.File('E_i2.xml') << Ei2
            #            V_cg = df.VectorFunctionSpace(self.FS.mesh, 'CG', 1)
            #            E_0_r_cg = df.Function(V_cg, 'E_r2.xml')
            #            E_0_i_cg = df.Function(V_cg, 'E_i2.xml')

        elif self.pf_EH_flag == 'E':
            E_0_r_cg = PF.E_0_r_cg[0]
            E_0_i_cg = PF.E_0_i_cg[0]

        for j in range(1, self.FS.DOM.n_domains):
            if type(self.MP.delta_sigma[j]) is float and \
               abs(self.MP.delta_sigma[j]) < 1e-8:
                if j is not 1:
                    mpp('Notice! "delta_sigma" < 1e-8 is assumed to be 0.0')
                    self.R += (df.inner(f_0, self.v_1r) * dx(j) +
                               df.inner(f_0, self.v_1i) * dx(j) +
                               df.inner(f_0, self.v_2r) * dx(j) +
                               df.inner(f_0, self.v_2i) * dx(j) +
                               df.inner(f_0, self.v_3r) * dx(j) +
                               df.inner(f_0, self.v_3i) * dx(j) +
                               df.inner(f_0, self.v_4r) * dx(j) +
                               df.inner(f_0, self.v_4i) * dx(j))
            else:
                if self.pf_EH_flag in ['E', 'H']:
                    if type(self.MP.delta_sigma[j]) is float:
                        self.R += (-df.inner(om1 * c2[j] * E_0_r_cg.sub(0),
                                             self.v_1r) * dx(j) +
                                   df.inner(om1 * c2[j] * E_0_i_cg.sub(0),
                                            self.v_1i) * dx(j) -
                                   df.inner(om1 * c2[j] * E_0_r_cg.sub(1),
                                            self.v_2r) * dx(j) +
                                   df.inner(om1 * c2[j] * E_0_i_cg.sub(1),
                                            self.v_2i) * dx(j) -
                                   df.inner(om1 * c2[j] * E_0_r_cg.sub(2),
                                            self.v_3r) * dx(j) +
                                   df.inner(om1 * c2[j] * E_0_i_cg.sub(2),
                                            self.v_3i) * dx(j) -
                                   df.inner(om1 * c2[j] * E_0_r_cg,
                                            df.grad(self.v_4r)) * dx(j) +
                                   df.inner(om1 * c2[j] * E_0_i_cg,
                                            df.grad(self.v_4i)) * dx(j))
                    else:
                        #  manual implementation of anisotropic delta sigma
                        c2 = add_sigma_vals_to_list(self.MP.sigma)
                        c1 = add_sigma_vals_to_list(self.MP.sigma_ground)
                        self.R += (-df.inner(c2[j][0][0] * E_0_r_cg.sub(0),
                                             om1 * self.v_1r) * dx(j) +
                                   df.inner(c2[j][0][0] * E_0_i_cg.sub(0),
                                            om1 * self.v_1i) * dx(j) -
                                   df.inner(c2[j][1][1] * E_0_r_cg.sub(1),
                                            om1 * self.v_2r) * dx(j) +
                                   df.inner(c2[j][1][1] * E_0_i_cg.sub(1),
                                            om1 * self.v_2i) * dx(j) -
                                   df.inner(c2[j][2][2] * E_0_r_cg.sub(2),
                                            om1 * self.v_3r) * dx(j) +
                                   df.inner(c2[j][2][2] * E_0_i_cg.sub(2),
                                            om1 * self.v_3i) * dx(j) -
                                   df.inner(om1 * c2[j] * E_0_r_cg,
                                            df.grad(self.v_4r)) * dx(j) +
                                   df.inner(om1 * c2[j] * E_0_i_cg,
                                            df.grad(self.v_4i)) * dx(j))
                        self.R += -(-df.inner(c1[j-1][0][0] * E_0_r_cg.sub(0),
                                              om1 * self.v_1r) * dx(j) +
                                    df.inner(c1[j-1][0][0] * E_0_i_cg.sub(0),
                                             om1 * self.v_1i) * dx(j) -
                                    df.inner(c1[j-1][1][1] * E_0_r_cg.sub(1),
                                             om1 * self.v_2r) * dx(j) +
                                    df.inner(c1[j-1][1][1] * E_0_i_cg.sub(1),
                                             om1 * self.v_2i) * dx(j) -
                                    df.inner(c1[j-1][2][2] * E_0_r_cg.sub(2),
                                             om1 * self.v_3r) * dx(j) +
                                    df.inner(c1[j-1][2][2] * E_0_i_cg.sub(2),
                                             om1 * self.v_3i) * dx(j) -
                                    df.inner(om1 * c1[j-1] * E_0_r_cg,
                                             df.grad(self.v_4r)) * dx(j) +
                                    df.inner(om1 * c1[j-1] * E_0_i_cg,
                                             df.grad(self.v_4i)) * dx(j))
                else:
                    mpp('Error! "pf_EH_flag must be either "E" or "H"!')
                    raise SystemExit


class A_V_mixed(ApproachBase):

    """
    FE implementation of (Ansari, 2014) or modified (Schwarzbach, 2009)
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters

        super(self.__class__, self).__init__()

        (self.v_1r, self.v_1i, self.v_2r, self.v_2i) = df.TestFunctions(FS.M)
        (self.u_1r, self.u_1i, self.u_2r, self.u_2i) = df.TrialFunctions(FS.M)
        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v_1r', 'v_1i', 'v_2r', 'v_2i', 'u_1r', 'u_1i', 'DOM',
                     'u_2r', 'u_2i', 'dx', 'FS', 'MP', 'mute', 'path']

        mpp('  -  Notice! *sigma_from_func* not implemented for this approch '
            'until now  -  Continuing  ...')
        self.sigma_from_func = False

    def build_var_form(self, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            self.sigma_from_func = False
            mpp('Issue! "sigma_from_func" option for anisotropic cond. not '
                'implemented yet!\nUsing "old" multiple domain technique ...')

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)
        self.LHS_form()

        if '_t' in self.MP.approach:
            self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
            self.RHS_form_Total()
        else:
            self.FS.anom_flag = check_sigma_vals(self.MP)
            self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.RHS_form_Secondary()

    def LHS_form(self):

        """
        left-hand side contributions for A-Phi mixed approach
        """

        C = self.MP.mu * self.MP.omega
        dx = self.FS.DOM.dx
        c1 = add_sigma_vals_to_list(self.MP.sigma)

        # working formulation modified equivalent to potentials by Badea

        self.L = ((1./C) * df.inner(df.curl(self.u_1r),
                                    df.curl(self.v_1r)) * dx(0) -
                  df.inner(self.u_1i, c1[0] * self.v_1r) * dx(0) -
                  df.inner(df.grad(self.u_2i),
                           c1[0] * self.v_1r) * dx(0) -

                  df.inner(self.u_1r, c1[0] * self.v_1i) * dx(0) -
                  (1./C) * df.inner(df.curl(self.u_1i),
                                    df.curl(self.v_1i)) * dx(0) -
                  df.inner(df.grad(self.u_2r),
                           c1[0] * self.v_1i) * dx(0) -

                  df.inner(self.u_1i,
                           c1[0] * df.grad(self.v_2r)) * dx(0) +
                  df.inner(df.grad(self.u_2i), c1[0] *
                           df.grad(self.v_2r)) * dx(0) -

                  df.inner(self.u_1r,
                           c1[0] * df.grad(self.v_2i)) * dx(0) +
                  df.inner(df.grad(self.u_2r), c1[0] *
                           df.grad(self.v_2i)) * dx(0))

        for j in range(1, self.FS.DOM.n_domains):

            self.L += ((1./C) * df.inner(df.curl(self.u_1r),
                                         df.curl(self.v_1r)) * dx(j) -
                       df.inner(self.u_1i, c1[j] * self.v_1r) * dx(j) -
                       df.inner(df.grad(self.u_2i),
                                c1[j] * self.v_1r) * dx(j) -

                       df.inner(self.u_1r, c1[j] * self.v_1i) * dx(j) -
                       (1./C) * df.inner(df.curl(self.u_1i),
                                         df.curl(self.v_1i)) * dx(j) -
                       df.inner(df.grad(self.u_2r),
                                c1[j] * self.v_1i) * dx(j) -

                       df.inner(self.u_1i,
                                c1[j] * df.grad(self.v_2r)) * dx(j) +
                       df.inner(df.grad(self.u_2i), c1[j] *
                                df.grad(self.v_2r)) * dx(j) -

                       df.inner(self.u_1r,
                                c1[j] * df.grad(self.v_2i)) * dx(j) +
                       df.inner(df.grad(self.u_2r), c1[j] *
                                df.grad(self.v_2i)) * dx(j))

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = (df.inner(df.Constant(("0.0", "0.0", "0.0")),
                           self.v_1r) * dx_0 +
                  df.inner(df.Constant(("0.0", "0.0", "0.0")),
                           self.v_1i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2r) * dx_0)

    def RHS_form_Secondary(self):

        """
        right-hand side contributions for secondary field formulation
        """

        if self.pf_EH_flag == 'H':
            mpp("Notice! Using altern. primary magnetic fields.")
        f_0 = df.Constant(('0', '0', '0'))
        f_0s = df.Constant(('0'))
        dx = self.FS.DOM.dx
        PF = self.FS.PF
        c2 = add_sigma_vals_to_list(self.MP.delta_sigma)
        c3inv = add_sigma_vals_to_list(self.MP.sigma_ground, inverse=True)
        om1 = df.Constant((1./self.MP.omega))

        self.R = (df.inner(f_0, self.v_1r) * dx(0) +
                  df.inner(f_0, self.v_1i) * dx(0) +
                  df.inner(f_0s, self.v_2r) * dx(0) +
                  df.inner(f_0s, self.v_2i) * dx(0))

        for j in range(1, self.FS.DOM.n_domains):
            if type(self.MP.delta_sigma[j]) is float and \
               abs(self.MP.delta_sigma[j]) < 1e-8:
                if j is not 1:
                    mpp('Notice! "delta_sigma" < 1e-8 is assumed to be 0.0')
                    self.R += (df.inner(f_0, self.v_1r) * dx(j) +
                               df.inner(f_0, self.v_1i) * dx(j) +
                               df.inner(f_0s, self.v_2r) * dx(j) +
                               df.inner(f_0s, self.v_2i) * dx(j))

            else:
                if self.pf_EH_flag == 'E':
                    self.R += (
                        -df.inner(c2[j] * om1 * PF.E_0_r_cg[0],
                                  self.v_1r) * dx(j) +
                        df.inner(c2[j] * om1 * PF.E_0_i_cg[0],
                                 self.v_1i) * dx(j) -
                        df.inner(c2[j] * om1 * PF.E_0_r_cg[0],
                                 df.grad(self.v_2r)) * dx(j) +
                        df.inner(c2[j] * om1 * PF.E_0_i_cg[0],
                                 df.grad(self.v_2i)) * dx(j))
                elif self.pf_EH_flag == 'H':
                    self.R += (
                        -df.inner(c2[j] * c3inv[j-1] * df.curl(PF.H_0_r_cg[0]),
                                  om1 * self.v_1r) * dx(j) +
                        df.inner(c2[j] * c3inv[j-1] * df.curl(PF.H_0_i_cg[0]),
                                 om1 * self.v_1i) * dx(j) -
                        df.inner(c2[j] * c3inv[j-1] * df.curl(PF.H_0_r_cg[0]),
                                 om1 * df.grad(self.v_2r)) * dx(j) +
                        df.inner(c2[j] * c3inv[j-1] * df.curl(PF.H_0_i_cg[0]),
                                 om1 * df.grad(self.v_2i))*dx(j))
                else:
                    mpp('Error! "pf_EH_flag must be either "E" or "H"!')
                    raise SystemExit


class F_U_mixed(ApproachBase):

    """
    FE implementation of Tau-Omega approach on mixed elements
    (Mitsuhata, 2004), called F-U potential approach in custEM.
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters
        super(self.__class__, self).__init__()

        (self.v_1r, self.v_1i, self.v_2r, self.v_2i) = df.TestFunctions(FS.M)
        (self.u_1r, self.u_1i, self.u_2r, self.u_2i) = df.TrialFunctions(FS.M)
        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v_1r', 'v_1i', 'v_2r', 'v_2i', 'u_1r', 'u_1i', 'DOM',
                     'u_2r', 'u_2i', 'dx', 'FS', 'MP', 'mute', 'path']

        mpp('  -  Notice! *sigma_from_func* not implemented for this approch '
            'until now  -  Continuing  ...')
        self.sigma_from_func = False

    def build_var_form(self, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            self.sigma_from_func = False
            mpp('Issue! "sigma_from_func" option for anisotropic cond. not '
                'implemented yet!\nUsing "old" multiple domain technique ...')

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)
        self.LHS_form()

        if '_t' in self.MP.approach:
            self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
            self.RHS_form_Total()
        else:
            self.FS.anom_flag = check_sigma_vals(self.MP)
            self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.RHS_form_Secondary()

    def LHS_form(self):

        """
        left-hand side contributions for A-Phi mixed approach
        """

        dx = self.FS.DOM.dx
        c1inv = add_sigma_vals_to_list(self.MP.sigma, inverse=True)
        mu = df.Constant((self.MP.mu))
        om1 = df.Constant((1./self.MP.omega))

        self.L = (om1 * df.inner(c1inv[0] * df.curl(self.u_1r),
                                 df.curl(self.v_1r)) * dx(0) -
                  mu * df.inner(self.u_1i, self.v_1r) * dx(0) -
                  mu * df.inner(df.grad(self.u_2i), self.v_1r) * dx(0) -

                  mu * df.inner(self.u_1r, self.v_1i) * dx(0) -
                  om1 * df.inner(c1inv[0] * df.curl(self.u_1i),
                                 df.curl(self.v_1i)) * dx(0) -
                  mu * df.inner(df.grad(self.u_2r), self.v_1i) * dx(0) -

                  df.inner(mu * self.u_1i,
                           df.grad(self.v_2r)) * dx(0) +
                  df.inner(df.grad(self.u_2i), mu *
                           df.grad(self.v_2r)) * dx(0) -

                  df.inner(mu * self.u_1r,
                           df.grad(self.v_2i)) * dx(0) +
                  df.inner(df.grad(self.u_2r), mu *
                           df.grad(self.v_2i)) * dx(0))

        for j in range(1, self.FS.DOM.n_domains):

            self.L += (om1 * df.inner(c1inv[j] * df.curl(self.u_1r),
                                      df.curl(self.v_1r)) * dx(j) -
                       mu * df.inner(self.u_1i, self.v_1r) * dx(j) -
                       mu * df.inner(df.grad(self.u_2i), self.v_1r) * dx(j) -

                       mu * df.inner(self.u_1r, self.v_1i) * dx(j) -
                       om1 * df.inner(c1inv[j] * df.curl(self.u_1i),
                                      df.curl(self.v_1i)) * dx(j) -
                       mu * df.inner(df.grad(self.u_2r), self.v_1i) * dx(j) -

                       df.inner(mu * self.u_1i,
                                df.grad(self.v_2r)) * dx(j) +
                       df.inner(df.grad(self.u_2i), mu *
                                df.grad(self.v_2r)) * dx(j) -

                       df.inner(mu * self.u_1r,
                                df.grad(self.v_2i)) * dx(j) +
                       df.inner(df.grad(self.u_2r), mu *
                                df.grad(self.v_2i)) * dx(j))

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = (df.inner(df.Constant(("0.0", "0.0", "0.0")),
                           self.v_1r) * dx_0 +
                  df.inner(df.Constant(("0.0", "0.0", "0.0")),
                           self.v_1i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2i) * dx_0 +
                  df.inner(df.Constant(("0.0")), self.v_2r) * dx_0)

    def RHS_form_Secondary(self):

        """
        right-hand side contributions for secondary field formulation
        """

        if self.pf_EH_flag == 'H':
            mpp("Notice! Using altern. primary magnetic fields.")
        f_0 = df.Constant(('0', '0', '0'))
        f_0s = df.Constant(('0'))
        dx = self.FS.DOM.dx
        PF = self.FS.PF
        om1 = df.Constant((1./self.MP.omega))
        c1inv = add_sigma_vals_to_list(self.MP.sigma, inverse=True)
        c2 = add_sigma_vals_to_list(self.MP.delta_sigma)
        c3inv = add_sigma_vals_to_list(self.MP.sigma_ground, inverse=True)

        self.R = (df.inner(f_0, self.v_1r) * dx(0) +
                  df.inner(f_0, self.v_1i) * dx(0) +
                  df.inner(f_0s, self.v_2r) * dx(0) -
                  df.inner(f_0s, self.v_2i) * dx(0))

        for j in range(1, self.FS.DOM.n_domains):
            if type(self.MP.delta_sigma[j]) is float and \
               abs(self.MP.delta_sigma[j]) < 1e-8:
                if j is not 1:
                    mpp('Notice! "delta_sigma" < 1e-8 is assumed to be 0.0')
                    self.R += (df.inner(f_0, self.v_1r) * dx(j) +
                               df.inner(f_0, self.v_1i) * dx(j) +
                               df.inner(f_0s, self.v_2r) * dx(j) +
                               df.inner(f_0s, self.v_2i) * dx(j))

            else:
                if self.pf_EH_flag == 'E':
                    self.R += (df.inner(
                                    c1inv[j] * c2[j] * PF.E_0_r_cg[0],
                                    om1 * df.curl(self.v_1r)) * dx(j) -
                               df.inner(
                                    c1inv[j] * c2[j] * PF.E_0_i_cg[0],
                                    om1 * df.curl(self.v_1i)) * dx(j) +
                               df.inner(f_0s, self.v_2r) * dx(j) +
                               df.inner(f_0s, self.v_2i) * dx(j))

                elif self.pf_EH_flag == 'H':
                    self.R += (df.inner(om1 * (c3inv[j-1] - c1inv[j]) *
                                        df.curl(PF.H_0_r_cg[0]),
                                        df.curl(self.v_1r)) * dx(j) -
                               df.inner(om1 * (c3inv[j-1] - c1inv[j]) *
                                        df.curl(PF.H_0_i_cg[0]),
                                        df.curl(self.v_1i)) * dx(j) +
                               df.inner(f_0s, self.v_2r) * dx(j) +
                               df.inner(f_0s, self.v_2i) * dx(j))
                else:
                    mpp('Error! "pf_EH_flag must be either "E" or "H"!')
                    raise SystemExit


class F_U_nodal(ApproachBase):

    """
    FE implementation of Tau-Omega approach on nodal elements, might be
    considered in future.
    """

    def __init__(self, FS, MP):

        """
        DUMMY
        """

        pass


class E_vector(ApproachBase):

    """
    FE implementation of E-field approach (Grayver, 2014, Schwarzbach 2009)
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters
        super(self.__class__, self).__init__()

        (self.v_r, self.v_i) = df.TestFunctions(FS.M)
        (self.u_r, self.u_i) = df.TrialFunctions(FS.M)

        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v_r', 'v_i', 'u_r', 'u_i', 'DOM',
                     'dx', 'FS', 'MP', 'mute', 'path']

    def build_var_form(self, check_sigma_conformity=True, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            if '_s' in self.MP.approach:
                self.sigma_from_func = False
                mpp('Issue! "sigma_from_func" option for anisotropic cond. '
                    'only implemented for total E-field approach yet!\n'
                    'Using "old" multiple domain technique ...')

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)
        if self.quasi_static:
            # DEBUGGING
            # self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.LHS_form()
        else:
            self.LHS_form_full()

        if '_t' in self.MP.approach:
            self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
            self.RHS_form_Total()
        else:
            self.FS.anom_flag = check_sigma_vals(self.MP)
            self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.RHS_form_Secondary()

    def LHS_form(self):

        """
        left-hand side contributions for E-field approach
        """
        dx = self.FS.DOM.dx
        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            if self.MP.tensor_flag:
                dg_space = df.TensorFunctionSpace(self.FS.mesh, "DG", 0)
                sigma_func = build_DG_tensor_func(
                        dg_space, self.FS.DOM.domain_func, self.MP.sigma)
            else:
                sigma_func = build_DG_func(self.FS.S_DG,
                                           self.FS.DOM.domain_func,
                                           self.MP.sigma)
            self.sigma_func = sigma_func

            self.L = ((1./mu) * df.inner(df.curl(self.u_r),
                                         df.curl(self.v_r)) * dx_0 -
                      om * df.inner(sigma_func * self.u_i, self.v_r) * dx_0 -
                      om * df.inner(sigma_func * self.u_r, self.v_i) * dx_0 -
                      (1./mu) * df.inner(df.curl(self.u_i),
                                         df.curl(self.v_i)) * dx_0)

        else:
            c1 = add_sigma_vals_to_list(self.MP.sigma)
            self.L = ((1./mu) * df.inner(df.curl(self.u_r),
                                         df.curl(self.v_r)) * dx(0) -
                      om * df.inner(c1[0] * self.u_i, self.v_r) * dx(0) -
                      om * df.inner(c1[0] * self.u_r, self.v_i) * dx(0) -
                      (1./mu) * df.inner(df.curl(self.u_i),
                                         df.curl(self.v_i)) * dx(0))

            for j in range(1, self.FS.DOM.n_domains):
                self.L += ((1./mu) * df.inner(df.curl(self.u_r),
                                              df.curl(self.v_r)) * dx(j) -
                           om * df.inner(c1[j] * self.u_i, self.v_r) * dx(j) -
                           om * df.inner(c1[j] * self.u_r, self.v_i) * dx(j) -
                           (1./mu) * df.inner(df.curl(self.u_i),
                                              df.curl(self.v_i)) * dx(j))

    def LHS_form_full(self):

        """
        left-hand side contributions for E-field approach
        """
        dx = self.FS.DOM.dx
        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            sigma_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma)
            eps_func = build_DG_func(self.FS.S_DG,
                                     self.FS.DOM.domain_func,
                                     self.MP.eps)

            self.L = ((1./mu) * df.inner(df.curl(self.u_r),
                                         df.curl(self.v_r)) * dx_0 -
                      eps_func * om * om * df.inner(self.u_r,
                                                    self.v_r) * dx_0 -
                      om * df.inner(sigma_func * self.u_i, self.v_r) * dx_0 -
                      om * df.inner(sigma_func * self.u_r, self.v_i) * dx_0 -
                      (1./mu) * df.inner(df.curl(self.u_i),
                                         df.curl(self.v_i)) * dx_0 +
                      eps_func * om * om * df.inner(self.u_i, self.v_i) * dx_0)

        else:
            eps = [df.Constant(elem) for elem in self.MP.eps]
            c1 = add_sigma_vals_to_list(self.MP.sigma)
            self.L = ((1./mu) * df.inner(df.curl(self.u_r),
                                         df.curl(self.v_r)) * dx(0) -
                      (eps[0] * om * om) * df.inner(self.u_r, self.v_r)*dx(0) -
                      om * df.inner(c1[0] * self.u_i, self.v_r) * dx(0) -
                      om * df.inner(c1[0] * self.u_r, self.v_i) * dx(0) -
                      (1./mu) * df.inner(df.curl(self.u_i),
                                         df.curl(self.v_i)) * dx(0) +
                      (eps[0] * om * om) * df.inner(self.u_i, self.v_i)*dx(0))
            for j in range(1, self.FS.DOM.n_domains):
                self.L += ((1./mu) * df.inner(
                        df.curl(self.u_r), df.curl(self.v_r)) * dx(j) -
                        (eps[j] * om * om) * df.inner(self.u_r,
                                                      self.v_r) * dx(j) -
                        om * df.inner(c1[j] * self.u_i, self.v_r) * dx(j) -
                        om * df.inner(c1[j] * self.u_r, self.v_i) * dx(j) -
                        (1./mu) * df.inner(
                        df.curl(self.u_i), df.curl(self.v_i)) * dx(j) +
                        (eps[j] * om * om) * df.inner(self.u_i,
                                                      self.v_i) * dx(j))

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = (df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v_r)*dx_0 +
                  df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v_i)*dx_0)

    def RHS_form_Secondary(self):

        """
        right-hand side contributions for secondary field formulation
        """

        if self.pf_EH_flag == 'H':
            mpp("Notice! Using altern. primary magnetic fields.")

        om = df.Constant(self.MP.omega)
        PF = self.FS.PF

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            d_sig_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.delta_sigma)
            if self.pf_EH_flag == 'E':
                self.R = (om * df.inner(d_sig_func * PF.E_0_i_cg[0],
                                        self.v_r) * dx_0 +
                          om * df.inner(d_sig_func * PF.E_0_r_cg[0],
                                        self.v_i) * dx_0)
            elif self.pf_EH_flag == 'H':
                invsig_g_func = build_DG_func(
                        self.FS.S_DG, self.FS.DOM.domain_func,
                        self.MP.sigma_air + self.MP.sigma_ground, inverse=True)
                self.R = (om * df.inner(d_sig_func * invsig_g_func * df.curl(
                                        PF.H_0_i_cg[0]), self.v_r) * dx_0 +
                          om * df.inner(d_sig_func * invsig_g_func * df.curl(
                                        PF.H_0_r_cg[0]), self.v_i) * dx_0)

        else:
            f_0 = df.Constant(('0', '0', '0'))
            dx = self.FS.DOM.dx
            c2 = add_sigma_vals_to_list(self.MP.delta_sigma)
            c3inv = add_sigma_vals_to_list(self.MP.sigma_ground, inverse=True)

            self.R = (df.inner(f_0, self.v_r) * dx(0) +
                      df.inner(f_0, self.v_i) * dx(0))

            for j in range(1, self.FS.DOM.n_domains):
                if type(self.MP.delta_sigma[j]) is float and \
                   abs(self.MP.delta_sigma[j]) < 1e-8:
                    if j is not 1:
                        mpp('Notice! "delta_sigma" < 1e-8'
                            ' is assumed to be 0.0')
                        self.R += (df.inner(f_0, self.v_r) * dx(j) +
                                   df.inner(f_0, self.v_i) * dx(j))
                else:
                    if self.pf_EH_flag == 'E':
                        self.R += (om * df.inner(c2[j] * PF.E_0_i_cg[0],
                                                 self.v_r) * dx(j) +
                                   om * df.inner(c2[j] * PF.E_0_r_cg[0],
                                                 self.v_i) * dx(j))
                    elif self.pf_EH_flag == 'H':
                        self.R += (om * df.inner(c2[j] * c3inv[j-1] * df.curl(
                                            PF.H_0_i_cg[0]), self.v_r)*dx(j) +
                                   om * df.inner(c2[j] * c3inv[j-1] * df.curl(
                                            PF.H_0_r_cg[0]), self.v_i) * dx(j))
                    else:
                        mpp('Error! "pf_EH_flag must be either "E" or "H"!')
                        raise SystemExit


class H_vector(ApproachBase):

    """
    FE implementation of H-fild approach, not working yet!
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters
        super(self.__class__, self).__init__()

        (self.v_r, self.v_i) = df.TestFunctions(FS.M)
        (self.u_r, self.u_i) = df.TrialFunctions(FS.M)

        self.pf_EH_flag = 'E'
        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v_r', 'v_i', 'u_r', 'u_i', 'DOM',
                     'dx', 'FS', 'MP', 'mute', 'path']

    def build_var_form(self, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            self.sigma_from_func = False
            mpp('Issue! "sigma_from_func" option for anisotropic cond. not '
                'implemented yet!\nUsing "old" multiple domain technique ...')

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)

        if self.quasi_static:
            self.LHS_form()
        else:
            self.LHS_form_full()
            if '_s' in self.MP.approach:
                mpp('*full* not implemented on RHS yet for SF approach')
                raise SystemExit

        if '_t' in self.MP.approach:
            mpp('Fatal Error! Total H-field approach not working yet!\n'
                'Aborting ...')
            raise SystemExit
            self.RHS_form_Total()
            self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
        else:
            self.FS.anom_flag = check_sigma_vals(self.MP)
            self.FS.add_primary_fields(self.MP, **fem_kwargs)
            self.RHS_form_Secondary()

    def LHS_form(self):

        """
        left-hand side contributions for H-field approach
        """

        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            inv_s_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma, inverse=True)
            s_func = build_DG_func(self.FS.S_DG,
                                   self.FS.DOM.domain_func,
                                   self.MP.sigma)

            self.inv_s_func = inv_s_func
            self.s_func = s_func
            self.L = (df.inner(df.curl(self.u_r),
                               df.curl(inv_s_func * self.v_r)) * dx_0 -
                      mu * om * df.inner(self.u_i, self.v_r) * dx_0 -
                      mu * om * df.inner(self.u_r, self.v_i) * dx_0 -
                      df.inner(df.curl(self.u_i),
                               df.curl(inv_s_func * self.v_i)) * dx_0)
        else:
            dx = self.FS.DOM.dx
            c1inv = add_sigma_vals_to_list(self.MP.sigma, inverse=True)
            self.L = (df.inner(c1inv[0] * df.curl(self.u_r),
                               df.curl(self.v_r)) * dx(0) -
                      mu * om * df.inner(self.u_i, self.v_r) * dx(0) -
                      mu * om * df.inner(self.u_r, self.v_i) * dx(0) -
                      df.inner(c1inv[0] * df.curl(self.u_i),
                               df.curl(self.v_i)) * dx(0))

            for j in range(1, self.FS.DOM.n_domains):
                self.L += (df.inner(c1inv[j] * df.curl(self.u_r),
                                    df.curl(self.v_r)) * dx(j) -
                           mu * om * df.inner(self.u_i, self.v_r) * dx(j) -
                           mu * om * df.inner(self.u_r, self.v_i) * dx(j) -
                           df.inner(c1inv[j] * df.curl(self.u_i),
                                    df.curl(self.v_i)) * dx(j))

    def LHS_form_total(self):

        """
        left-hand side contributions for H-field approach
        """

        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            s_func = build_DG_func(self.FS.S_DG,
                                   self.FS.DOM.domain_func,
                                   self.MP.sigma)

            self.L = (df.inner(df.curl(self.u_r),
                               df.curl(self.v_r)) * dx_0 -
                      mu * om * s_func * df.inner(self.u_i, self.v_r) * dx_0 -
                      mu * om * s_func * df.inner(self.u_r, self.v_i) * dx_0 -
                      df.inner(df.curl(self.u_i),
                               df.curl(self.v_i)) * dx_0)

    def LHS_form_full(self):

        """
        left-hand side contributions for full H-field approach
        """

        mpp('Need to correct eps implementation for H field approach!')
        raise SystemExit

        om = df.Constant(self.MP.omega)
        mu = df.Constant(self.MP.mu)
        eps = df.Constant(self.MP.eps)
        s_func = build_DG_func(self.FS.S_DG,
                               self.FS.DOM.domain_func,
                               self.MP.sigma)
        self.s_func = s_func
        fact1 = (om * eps) / (om * om * eps * eps + s_func * s_func)
        fact2 = s_func / (om * om * eps * eps + s_func * s_func)
        self.fact1 = fact1
        self.fact2 = fact2

        mpp('Warning! Full magenetic field approach is still experimental and '
            'coorect resuls are not guaranteed! Continuing ...',
            post_dash=True)

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            inv_s_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma, inverse=True)
            self.inv_s_func = inv_s_func
            self.L = (df.inner(df.curl(self.u_r),
                               df.curl(fact2 * self.v_r)) * dx_0 +
                      df.inner(df.curl(self.u_i), df.curl(fact1 * self.v_r))
                      * dx_0 -
                      mu * om * df.inner(self.u_i, self.v_r) * dx_0 +
                      df.inner(df.curl(self.u_r), df.curl(fact1 * self.v_i))
                      * dx_0 -
                      mu * om * df.inner(self.u_r, self.v_i) * dx_0 -
                      df.inner(df.curl(self.u_i),
                               df.curl(fact2 * self.v_i)) * dx_0)

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = (df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v_r)*dx_0 +
                  df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v_i)*dx_0)

    def RHS_form_Secondary(self):

        """
        right-hand side contributions for secondary field formulation
        """

        if self.pf_EH_flag == 'E':
            mpp("Notice! Using alternative primary electric fields.")
        f_0 = df.Constant(('0', '0', '0'))
        PF = self.FS.PF

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            inv_s_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma, inverse=True)
            if self.pf_EH_flag == 'H':
                invsig_g_func = build_DG_func(
                        self.FS.S_DG, self.FS.DOM.domain_func,
                        self.MP.sigma_air + self.MP.sigma_ground, inverse=True)
                self.R = (df.inner((invsig_g_func - inv_s_func) *
                                   PF.H_0_r_cg[0], df.curl(self.v_r)) * dx_0 +
                          -df.inner((invsig_g_func - inv_s_func) *
                                    PF.H_0_i_cg[0], df.curl(self.v_i)) * dx_0)
            elif self.pf_EH_flag == 'E':
                sig_g_func = build_DG_func(
                        self.FS.S_DG, self.FS.DOM.domain_func,
                        self.MP.sigma_air + self.MP.sigma_ground)
                self.R = (df.inner((1. - inv_s_func * sig_g_func) *
                                   PF.E_0_r_cg[0], df.curl(self.v_r)) * dx_0 +
                          -df.inner((1. - inv_s_func * sig_g_func) *
                                    PF.E_0_i_cg[0], df.curl(self.v_i)) * dx_0)
        else:
            dx = self.FS.DOM.dx
            c1inv = add_sigma_vals_to_list(self.MP.sigma, inverse=True)
            c2 = add_sigma_vals_to_list(self.MP.delta_sigma)
            c3inv = add_sigma_vals_to_list(self.MP.sigma_ground, inverse=True)

            self.R = (df.inner(f_0, self.v_r) * dx(0) +
                      df.inner(f_0, self.v_i) * dx(0))

            for j in range(1, self.FS.DOM.n_domains):
                if type(self.MP.delta_sigma[j]) is float and \
                   abs(self.MP.delta_sigma[j]) < 1e-8:
                    if j is not 1:
                        mpp('Notice! "delta_sigma" < 1e-8is assumed to be 0.0')
                        self.R += (df.inner(f_0, self.v_r) * dx(j) +
                                   df.inner(f_0, self.v_i) * dx(j))
                else:
                    if self.pf_EH_flag == 'H':
                        self.R += (df.inner((c3inv[j-1] - c1inv[j]) *
                                            df.curl(PF.H_0_r_cg[0]),
                                            df.curl(self.v_r)) * dx(j) +
                                   -df.inner((c3inv[j-1] - c1inv[j]) *
                                             df.curl(PF.H_0_i_cg[0]),
                                             df.curl(self.v_i)) * dx(j))
                    elif self.pf_EH_flag == 'E':
                        self.R += (df.inner(
                                        (c1inv[j]) * c2[j] * PF.E_0_r_cg[0],
                                        df.curl(self.v_r)) * dx(j) +
                                   -df.inner(
                                         (c1inv[j]) * c2[j] * PF.E_0_i_cg[0],
                                         df.curl(self.v_i)) * dx(j))
                    else:
                        mpp('Error! "pf_EH_flag must be either "E" or "H"!')
                        raise SystemExit
