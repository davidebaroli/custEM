custEM.serial package
=====================

Submodules
----------

custEM.serial.calc\_line\_meshes module
---------------------------------------

.. automodule:: custEM.serial.calc_line_meshes
    :members:
    :undoc-members:
    :show-inheritance:

custEM.serial.calc\_primary\_boundary\_fields module
----------------------------------------------------

.. automodule:: custEM.serial.calc_primary_boundary_fields
    :members:
    :undoc-members:
    :show-inheritance:

custEM.serial.calc\_primary\_fields module
------------------------------------------

.. automodule:: custEM.serial.calc_primary_fields
    :members:
    :undoc-members:
    :show-inheritance:

custEM.serial.calc\_slice\_meshes module
----------------------------------------

.. automodule:: custEM.serial.calc_slice_meshes
    :members:
    :undoc-members:
    :show-inheritance:

custEM.serial.interpolate\_in\_serial module
--------------------------------------------

.. automodule:: custEM.serial.interpolate_in_serial
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: custEM.serial
    :members:
    :undoc-members:
    :show-inheritance:
