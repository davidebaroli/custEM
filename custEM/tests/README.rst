################################################################################
# # # #  #                      First steps notes                      # # # # #
################################################################################
    Preliminary Note: These are tests, the accuracy of the calculated fields of
    the *lvl_1* and *lvl_2* tests on coarse meshes is just good enough to
    review that the fields match the expected behavior! If python3 is not
    default, it might be necessary to exchange *python* commands with *python3*.

################################################################################

    1. test_basics.py: 
    ==================

    - Execute via your Python editor or in the command prompt via:
	
        --> python lvl_1_test.py
		
    - Alternatively, try multiprocessing and execute in the command prompt via:

        --> mpirun -n 4 python lvl_1_test.py

    This test checks if FEniCS and custEM are installed correctly and all paths
    are set correctly. Note: As long as "pyGIMLi" is not installed, only the
    *lvl_1* test is supported.

################################################################################

    2. test_fd_approaches.py:
    =========================

    - Preferably run this test with multiprocessing:

        --> mpirun -n 4 python lvl_2_test.py

    - Even if only 1 core is used, the mpirun syntax is necessary at the moment.
      ("mpirun -n 1 --bind-to none python lvl_2_test.py" instead of 
      (python lvl_2_test.py)
     
    This test checks if "pyGIMLi" is installed properly, mesh generation
    with "TetGen" is supported and if the "pyhed" module works correctly.
    Furhtermore, functionylity of all frequency-domain (fd) approaches is
    tested.

    - Interpolation of the results can be conducted with using the following
      script, even if only 1 core is used, the mpirun syntax is necessary at
      the moment.

        --> (mpirun -n 25) python interpolate_fd_test_results.py
    
    Note: the maximum number of simultaneous interpolations is 48. If your
	      computer architecture allows for, maximum speed can be achieved by
          either using 49 processes (48 without root) for the mpirun call or
          by changing the *n_max_new_procs* parameters in line 25 in the
          *interpolate_fd_test_reults.py* script to a corresponding value.
          The latter appraoch will spawn *n_max_new_procs* additional processes
          aside from the number of processes for the *mpirun* call.
	
    - Default plots of the obtained results on lines and slices parallel to the
      coordinate axes can be generated using the following script (in serial).

        --> plot_fd_test_results.py
        
################################################################################

    3. test_topo_model.py:
    ============================

    - A run in serial would need hours of computation time for this test since
      systems with > 1 M dofs needs to be solved.
    - Needs to be run with multiprocessing (or hours of computation time):

        --> mpirun -n 32 --bind-to socket python test_topo_model.py	

    The "bind-to socket" flag might accelerate the solution process if multiple
    sockets are available on your machine. This test checks if TetGen and all
    utility modules of custEM are working: 'meshgen_tools', interp_tools' and
    'plot_tools'. It calculates the first serious model with p2 in the second
    half of the test. This requires approximately 250 GB RAM with 12 procs.
    The number of processes can be adjusted after the '-n' flag in the mpirun
    call. Using 32 parallel processes will reduce the computation time by
    ~30-50%, but increase the memory requirements by ~20%. The overall results
    or data interpolated on lines and slice can be viewed in Paraview or
    visualized with the *plot_tools*.
    
################################################################################