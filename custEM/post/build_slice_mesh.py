# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import sys
import getopt
import numpy as np
from mpi4py import MPI

"""
This file is always called in serial from the root process of an mpi run,
otherwise a crash would occur due to a missing 'MPI-Parent' for synchronization
"""


class Slice_mesh_calc:

    """
    Mesh calculation class for generating straight or crooked (e.g.,
    topography), slice meshes.

    Class internal functions:
    -------------------------

    - calc_slice_meshes()
        generate the 2D mesh for interpolation
    """

    def __init__(self, para_dir, axes, out_name):

        """
        Required arguments:
        -------------------

        - para_dir, type str
            parameter directory including a temporary "interp_dict.json" file,
            same as the **para_dir** as set in the **MOD** instance

        - axes, type str
            a permutation or combination of 'xyz', for which
            slice-normal-direction a 'slice'-mesh should be generated

        - out_name, None or type str
            could be specified manually, if None, a default name will be used
        """

        import json
        with open(para_dir + '/interp_dict_0.json', 'r') as thefile:
            self.__dict__.update(json.load(thefile))
        if type(self.dim) is float and \
           np.isclose(np.abs(self.x0), np.abs(self.x1)) and \
           np.isclose(np.abs(self.y0), np.abs(self.y1)) and \
           np.isclose(np.abs(self.z0), np.abs(self.z1)):
            self.dims = [-self.dim, -self.dim, -self.dim, self.dim, self.dim,
                         self.dim]
            if type(self.n_segs) is int:
                self.n_segs = [self.n_segs, self.n_segs]
            else:
                print('Error! Invalid combination of scalar "dim" and "n_segs"'
                      ' values. Consider degfining start and stop coordinates '
                      'in two directions and two "n_segs" values for each '
                      'direction in a list')
                raise SystemExit
        elif type(self.dim) is list and len(self.dim) == 3:
            self.dims = [-self.dim[0], -self.dim[1], -self.dim[2],
                         self.dim[0], self.dim[1], self.dim[2]]
        else:
            self.dims = [self.x0, self.y0, self.z0, self.x1, self.y1, self.z1]
            if type(self.n_segs) is int:
                print('Error! For not centered slices with, e.g., x0 != -x1, '
                      'use two "n_segs" values for each direction in a list')
                raise SystemExit

        if ''.join(sorted(axes)) in ['x', 'y', 'z', 'xy', 'xz', 'yz', 'xyz']:
            self.build_slice_meshes(axes, out_name, para_dir, self.on_topo)
        else:
            print('Warning!!!, automated custom line mesh generation not '
                  'implemented in custEM yet!')

    def build_slice_meshes(self, axes, out_name, para_dir, on_topo=False):

        """
        generate the 2D mesh for interpolation

        Required arguments:
        -------------------

        - it is referred to the **__init__**

        Keyword arguments:
        ------------------

        - on_topo=False, type bool
            set to True if the Z-values of a 'z-slice'-mesh should be adjusted
            to a certain topography, the flag is set in the **Interpolator**
            instance of the **MOD** class
        """

        if 'x' in axes:
            if out_name is None:
                out_name = 'x_' + str(self.x) + '_dim_' + \
                           str(self.dims[0]) + '_n_' + str(self.n_segs)

            if os.path.isfile(str(self.m_dir) + '/slices/' + str(out_name) +
                              '_slice_x.xml'):
                print('Desired "x_slice_mesh" already exists, continuing ... ')
            else:
                print('2D "x_slice_mesh" is calculated and '
                      'saved in global "mesh_dir"/slices ... ')

                slice_mesh = df.BoxMesh(
                        df.Point(self.dims[0], self.dims[1], self.dims[2]),
                        df.Point(self.dims[3], self.dims[4], self.dims[5]),
                        2, self.n_segs[0], self.n_segs[1])
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')

                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                xs = df.AutoSubDomain(lambda x: x[0] < self.dims[0] + self.tol)
                xs.mark(cf, 1)
                slice_x = df.SubMesh(boundary_mesh, cf, 1)
                slice_x.coordinates()[:, 0] += -self.dims[0] + self.x
                df.File(str(self.m_dir) + '/slices/' + str(out_name) +
                        '_slice_x.xml') << slice_x

        if 'y' in axes:
            if out_name is None:
                out_name = 'y_' + str(self.y) + '_dim_' + \
                           str(self.dim) + '_n_' + str(self.n_segs)

            if os.path.isfile(str(self.m_dir) + '/slices/' + str(out_name) +
                              '_slice_y.xml'):
                print('Desired "y_slice_mesh" already exists, continuing ... ')
            else:
                print('2D "y_slice_mesh" is calculated and '
                      'saved in global "mesh_dir"/slices ... ')

                slice_mesh = df.BoxMesh(
                        df.Point(self.dims[0], self.dims[1], self.dims[2]),
                        df.Point(self.dims[3], self.dims[4], self.dims[5]),
                        self.n_segs[0], 2, self.n_segs[1])
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')

                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                ys = df.AutoSubDomain(lambda x: x[1] < self.dims[1] + self.tol)
                ys.mark(cf, 1)
                slice_y = df.SubMesh(boundary_mesh, cf, 1)
                slice_y.coordinates()[:, 1] += -self.dims[1] + self.y
                df.File(str(self.m_dir) + '/slices/' + str(out_name) +
                        '_slice_y.xml') << slice_y

        if 'z' in axes:
            if out_name is None:
                out_name = 'z_' + str(self.z) + '_dim_' + \
                           str(self.dim) + '_n_' + str(self.n_segs)
                if on_topo:
                    out_name += '_on_topo'

            if os.path.isfile(str(self.m_dir) + '/slices/' + str(out_name) +
                              '_slice_z.xml'):
                print('Desired "z_slice_mesh" already exists, continuing ... ')
            else:
                print('2D "z_slice_mesh" is calculated and '
                      'saved in global "mesh_dir"/slices ... ')

                slice_mesh = df.BoxMesh(
                        df.Point(self.dims[0], self.dims[1], self.dims[2]),
                        df.Point(self.dims[3], self.dims[4], self.dims[5]),
                        self.n_segs[0], self.n_segs[1], 2)
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')
                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                zs = df.AutoSubDomain(lambda x: x[2] <
                                      self.dims[2] + self.tol)
                zs.mark(cf, 1)
                slice_z = df.SubMesh(boundary_mesh, cf, 1)

                if on_topo:
                    import custEM.misc.synthetic_definitions as sd
                    if os.path.isfile(self.t_dir + '/' +
                                      self.mesh_name + '_surf.xyz'):
                        from custEM.meshgen.dem_interpolator import DEM
                        tp = self.get_topo_parameters()
                        dem = DEM(self.t_dir + '/' +
                                  self.mesh_name + '_surf.xyz',
                                  centering=bool(tp[1]),
                                  easting_shift=None, northing_shift=None)
                        slice_z.coordinates()[:, 2] = dem(
                                slice_z.coordinates()[:, 0],
                                slice_z.coordinates()[:, 1], rotation=tp[4])
                    else:
                        topo_func = getattr(sd, str(self.topo))
                        slice_z.coordinates()[:, 2] = topo_func(
                            slice_z.coordinates()[:, 0],
                            slice_z.coordinates()[:, 1])
                    if self.on_water_surface:
                        slice_z.coordinates()[:, 2][slice_z.coordinates(
                                )[:, 2] < 0.] = 0.
                    slice_z.coordinates()[:, 2] += self.z
                else:
                    slice_z.coordinates()[:, 2] += -self.dims[2] + self.z
                df.File(str(self.m_dir) + '/slices/' + str(out_name) +
                        '_slice_z.xml') << slice_z

    def get_topo_parameters(self):

        tmp = np.genfromtxt(para_dir + '/' + self.mesh_name +
                            '_parameters.txt', dtype=None,
                            encoding=None)
        dummy = []
        for j, elem in enumerate(tmp):
            dummy.append(str(elem))

        tmp = dummy
        if tmp[1] == 'False':
            tmp[1] = False
        else:
            tmp[1] = True
        if tmp[2] == 'None':
            tmp[2] = None
        else:
            tmp[2] = float(tmp[2])
        if tmp[3] == 'None':
            tmp[3] = None
        else:
            tmp[3] = float(tmp[3])
        if tmp[4] == 'None':
            tmp[4] = None
        else:
            tmp[4] = float(tmp[4])
        return(tmp)


if __name__ == "__main__":

    """
    read the three arguments for the **Slice_mesh_calc** class, call the latter
    and synchronize with the 'Parent process' afterwards
    """

    myopts, args = getopt.getopt(sys.argv[1:], "d:a:o:")
    for o, a in myopts:
        if o == '-d':
            para_dir = str(a)
        elif o == '-a':
            axes = str(a)
        elif o == '-o':
            out_name = str(a)
        else:
            print("Usage: %s -d   parameter directory     -a   axes     "
                  "-o     output name"
                  % sys.argv[0])

    if out_name == 'None':
        out_name = None
    parent = MPI.Comm.Get_parent()
    Slice_mesh_calc(para_dir, axes, out_name)
    parent.Barrier()
    parent.Disconnect()
