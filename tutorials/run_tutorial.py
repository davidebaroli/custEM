
# coding: utf-8

# # Run tutorial
#
# This tutorial is a guide to compute finite-element solutions for
# CSEM setups with custEM. In the current state, we considered a small
# model which can be run without MPI in serial. It is similar to example '#'1
# in the examples directory. In addition, a plate anomaly was incorporated
# in the model and the transmitter is a rectangular loop now. Please note that
# the documentation of custEM is still in development as is this tutorial.

# ### Generate the mesh for computation

# In[ ]:


# First, we create the underlying mesh, for details see the meshgen tutorial
# #############################################################################

import dolfin as df
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.meshgen_utils import line_x, loop_r

mpi_cw = df.MPI.comm_world

if df.MPI.rank(mpi_cw) == 0:
        # create world
        O = BlankWorld(name='tutorial_mesh', m_dir='./meshes',
                       preserve_edges=True)

        # add observation line: 10 km x-directed observation line
        O.build_surface(
            insert_lines=[line_x(start=-5e3, stop=5e3, n_segs=500)],
            insert_loops=[loop_r(start=[-5e2, -5e2], stop=[5e2, 5e2],
                                 n_segs=40)])

        # build halfspace mesh and extend 2D surface mesh to 3D world
        O.build_layered_earth_mesh(n_layers=3,
                                   layer_depths=[-300., -1000.])

        # Add plate anomaly
        O.add_plate(500., 500., 100.,           # length, width, height
                    [100., 0., -700.],          # origin (center) of plate
                    45., 117.,                  # dip, dip azimuth
                    cell_size=1e4)              # max. cell volume constraint

        # Append boundary mesh
        O.there_is_always_a_bigger_world(1e2, 1e2, 1e2)

        # call TetGen
        O.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)
else:
    pass


# ### Initialize FE model

# In[ ]:


from custEM.core import MOD
from custEM.misc import max_mem
import shutil
import os

# clean up existing meshes, this can be uncommented if the mesh should not
# be converted again on every run. However, notice that a new mesh with the
# identical name is not automatically updated as the mesh conversion routine is
# only called if an *xml* or *h5* mesh with the specified name does not exist

# if os.path.isdir('./meshes/_h5'):
#     shutil.rmtree('./meshes/_h5')
# if os.path.isdir('./meshes/_xml'):
#     shutil.rmtree('./meshes/_xml')

# initialize model
# #############################################################################

approach = 'E_t'   # either 'E_t', 'E_s', 'H_s', 'Am_t', 'Am_s', 'An_s'
M = MOD('tutorial_mod',       # mod name (required)
        'tutorial_mesh',      # mesh name (required)
        approach,             # approach name (required)
        p=1,                  # polynomial order
        overwrite=True,       # overwrite an existing mesh with the same name)
        r_dir='./results',    # custom results directory, default:-> "path.txt"
        m_dir='./meshes')     # custom mesh directory, default:-> "path.txt"
#       load_existing=False,  # load existing results, requires overwrite=False
#       file_format='h5',     # default, alternatively 'xml', automatically
                              # set if parallel HDF5 format is not supported
#       test_mode=False,      # Set True if a test run of the current approach
                              # should be conducted on a coarse UnitCubeMesh
#       mute=False,           # suppress not so relevant prints
#       para_dir='...',       # custom mesh parameter directory if required
#       out_dir='...',        # custom output directory if required
#       out_name='...')       # custom output name if required


# ### Set model parameters
#
# The conductivity distribution needs to be set in the order layers or
# anoamlies were added to the mesh and for now, it must be specified manually
# in which layer anomalies are located for secondary field calculations.
#
# - Setup: three-layered earth with topography and 1 anomaly in the 2nd layer
# - this requires a list of 3 entries for sigma_ground
# - this requires a list of 1 entry for sigma_anom
# - this requires a list of 1 entry for anomaly_layer_markers
#
# Aside from that, please notice:
#
# - Frequencies are specified in terms of the angular frequency omega!
# - sigma_air is 1e-8 by default and only relevant for total field approaches
#   we found that a contrast of 4 orders of magnitude is totally sufficient to
#   obtain accurate results, even 3 order of magnitude are usually fine. If the
#   contrast is too high, this might lead to artifacts and increases comp. time.
#

# In[ ]:


# update phyiscal model parameters
# #############################################################################

M.MP.update_model_parameters(
        omega=1e1,                        # angular frequency
        J=1,                              # source current density
        sigma_anom=[1e0],                 # conductivities for plate and cuboid
        anomaly_layer_markers=[2],        # both anomalies are in the 2nd layer
        sigma_ground=[1e-3, 1e-2, 1e-4],  # layer conductivities (top to bot.)
        sigma_air=1e-8,                   # airspace conductivity
#        procs_per_proc=1,                # use 'proc_per_proc' cores for
                                          # faster primary field calc. (SF)
#        eps_for_coord_search=1e-6,       # might be changed to find source
                                          # dofs for total field approach (TF)
#        mu=4 * np.pi * 1e-7,             # constant magnetic permeability
#        eps=8.85 * 1e-12,                # electric permittivity, currently
        )                                 # not considered, so *irrelevant*


# ### Update FE parameters and transmitter setup
#
# If the model includes topography, z-values for specifying the transmitter
# positions (e.g., *start* and *stop* keyword arguments) are ignored and the
# transmitter is automatically shifted in z-direction to follow the surface.
# Some of the paramters are only required for secondary (SF) or total field (TF)
# approaches.

# In[ ]:


# update FE keyword arguments
# #############################################################################

M.FE.build_var_form(
        pf_type='layered_earth',  # "fullspace", "layered_earth", "custom" (SF)
#       pf_name=None,             # specify custom, primary field name (SF)
#       pvd_flag=False,           # set 'True' if primary fields (PF) should be
#                                 # saved in 'pvd' format for Paraview (SF)
        s_type='loop_r',          # alternatives' "hed, loop_c, loop_r, path"
        start=[-5e2, -5e2, 0.],   # start coordinate of line sources or first
                                  # corner coordinate for loop_r transmitters
        stop=[5e2, 5e2, 0.],      # stop coordinate of Line sources or opposite
                                  # corner coordinate for Loop_R transmitters
#       origin=[0., 0., 0],       # Origin for hed or loop_c transmitters (Tx)
#       r=1e2,                    # radius for loop_c sources
#       procs_per_proc=1,         # same as for MP, can be overwritten (SF)
#       pf_EH_flag='E',           # Alternatively 'H', both types of PF
                                  # can be used for all approaches (SF)
#       azimuth=0.,               # hed azimuth (DEG) (SF)
#       length=1.                      # hed length
#       min_length                # min. length of segments to build Tx (SF)
#       n_segs                    # max. number of segments to divide a finite
                                  # length Tx, overwrites min_length
#       bc                        # boundary condition (BC): default Neumann
                                  # alternative: 'ZD' or equiv. 'ZeroDirichlet'
                                  # alternative: 'ID' for inhomogeneous
                                  # Dirichlet conditions for TF approaches#
        )


# ### Solve FE problem
#
# The default option uses the symmetric MUMPS solver and should be fine for
# most applications. It includes automated conversion from A to E/H- or from
# E- to -H fields and exports the solutions as *pvd* file and *xml* data file
# in the specified results directory (**r_dir**).

# In[ ]:


# solve
# #############################################################################

M.solve_main_problem()        # to disable automated conversion anc export,
                              # set *convert=False*
max_mem()                     # print maximum RAM requirements


# In[ ]:


# furhter options for setting up the solver and post-processing
# #############################################################################

# If required, one could manually adjust the following parameters:

# M.solve_main_problem(
#       convert=True,         # Set to False to NOT convert main FE results
#       sym=True,             # Symmetric system to solve or not
#       solver='MUMPS'        # Alternatives are not working properly, but
#                             # allowed arguments are 'iter' and 'default'.
#       method,               # Krylov sub-space method for iterative solvers
#       pc,                   # Preconditioner for iterative solvers
#       bc)                   # define/overwrite boundary conditions (BC)
#                             # (can be set in FE instance before)

# If "convert=False" was set in the solution function, the following functions
# can be called manually:

# M.PP.convert_results(
#       convert_to_H=True,    # Convert E/A to H for all appr. despite 'H_s'
#       convert_to_E=False,   # Convert H to E for 'H_s' approach
#       bcs=None,             # Specify BC for conversion (no reason to change)
#       export_xml_or_h5=True,# Export E and H field data as 'xml' or 'h5' file
#       export_pvd=True,      # Export E and H field data as 'pvd' for Paraview
#       write_config=True)    # write config file for visualization and record

# M.PP.export_all_results(export_pvd, export_xml_or_h5)
# M.PP.export_E_cg_fields(export_pvd, export_xml_or_h5)
# M.PP.export_H_cg_fields(export_pvd, export_xml_or_h5)
# M.PP.export_A_cg_fields(export_pvd, export_xml_or_h5)
# M.PP.export_config_file()


# ### Interpolation
#
#
# # !!! NOTICE !!!
#
# **The following interpolation parts are not working with jupyter notebooks
# right now as the interpolation tools were designed for multiprocessing and
# some issues occure with MPI communication and jupyter which need to solved!**
#
# If interpolation is conducted directly after the FE computation and field
# conversion, fields don't need to be imported again to initialize an
# "interpolation" MOD instance. If separately conducted, FE results from a
# previously calculated model can be imported using the following command:

# In[ ]:


# load existing model
# #############################################################################

# from custEM.core import MOD
# approach = 'E_t'   # either 'E_t', 'E_s', 'H_s', 'Am_t', 'Am_s', 'An_s'
# M = MOD('tutorial_mod', 'tutorial_mesh', approach, overwrite=False,
#         load_existing=True, r_dir='results', m_dir='meshes')


# #### Create interpolation meshes
#
# Create default (lines and slices of 2x2 or 10x10 km dimensions following the
# coordinate axes) interpolation meshes or custom ones. Note that all meshes
# are stored and need to be generated only once. Users can define additional
# sets of default interpolation meshes in the "interp_tools" and tag them.

# In[ ]:


M.IB.init_default_interpolation_meshes()


# One could generate custom line or slice interpolation meshes, which are
# automatically named on the given parameters but might also get custom tags.
# Interpolation parameters can be either updated via
# M.IB.update_interpolation_parameters(...) or by directly passing them to the
# *create_line/slice_mesh(...)* functions as keyword arguments. Note that for
# all custom names, the suffix 'line'- or 'slice'- '_x', '_y','_z' will be
# added for coordinate axis specification for the **plot_tools**.
# Available parameters are described in the commented section below:

# In[ ]:


# create custom interpolation meshes
# #############################################################################

# M.IB.update_interpolation_parameters(
#       x0=-1e4, x1=1e4,      # start and stop value for x-directed lines
#       y0=-1e4, y1=1e4,      # start and stop value for y-directed lines
#       z0=-1e4, z1=1e4,      # start and stop value for z-directed lines
#       x=0., y=., z=0.,      # offset of lines or slices in non line- or
#                             # non-orthogonal slice direction
#       n_segs=400,           # number of segments(edges, or faces*2) in line-
#                             # or along the two slice-directions
#       dim=1e4,              # dimensions of slice meshes, the origin of the
#                             # latter is currently limited to (0., 0., 0.)
#       tol=1e-2,             # tolerance to cut slice or line meshes from 3D
#                             # UnitCubeMesh
#       on_topo=True/False    # set automatically to adjust horizontal line or
#                             # slice meshes to follow the topography. In that
#                             # case, the offset z is relative to topography
#                             # Users might set this flag to False for straight
#                             # horizontal interp. meshes for topo models

# M.IB.create_line_meshes(
#         'xy',               # specify line-direction, a combination of 'xyz'
#                             # so max. 3 orthogonal lines generated at once
#         line_name='topo')   # Set custom line names

M.IB.create_line_meshes(
        'x',                        # x-line
#        on_topo=False,             # straight line though topography
        line_name='10_km',          # custom line name,
        x0=-5e3, x1=5e3,            # start, stop of the x-line-mesh
#       y=0., z=0.                  # set offset in y-dir: 50m and
        )                           # z_dir: 300m depth

M.IB.create_slice_meshes(
        'xyz',                      # create slices in x-, y-, z-dir
        slice_name='axis',          # set custom slice name
        dim=5e3,                    # slices have 10x10km dimensions
        n_segs=100)                 # set grid density

# M.IB.create_slice_meshes
#         'z',                      # z-slice
#         slice_name='above_topo',  # set custom slice name
#         z=50)                     # z-slice with 50m offset above topo


# Run Interpolation
#
# Interpolation on the default sets or prev. created lines and slices is
# conducted as described subsequently. For secondary fields approaches,
# alternative quantities are 'E_s' and 'H_s'. Enabling prints is possible
# by setting the keyword argument *mute=False* in the interpolate function.
#
# Interpolate E and H fields on all default interpolation meshes using multi-
# threading. Note, it might be useful to adjust the number of maximum parallel
# threads to adjust interpolation time via setting the commented flag above or
# via directly setting this keyword argument to the 'interpolate_default()'
# function(s). The 'sync=False' flag specifies that all 4 default interpolation
# steps should be conducted at once, as far as the number of tasks < max_procs.
# (default: MPI-synchronization after each call of interpolate_default() )

# In[ ]:


# run interpolation on default intepolation-mesh sets
# #############################################################################

# M.IB.update_interpolation_parameters(max_procs=*YOUR_NO_PROCS*)

M.IB.interpolate_default(target_type='lines', interp_meshes='small')
M.IB.interpolate_default(target_type='lines', interp_meshes='large')
# M.IB.interpolate_default(target_type='slices', interp_meshes='small')
# M.IB.interpolate_default(target_type='slices', interp_meshes='large')
M.IB.synchronize()             # synchronize multi-threading, used here to
                              # clear the counter for busy subprocesses

for L in ['10_km_line_x']:
    for Q in ['E_t', 'H_t']:                # Interpolate E and H fields
        M.IB.interpolate(Q, L)               # on all generated line meshes

for S in ['axis_slice_x', 'axis_slice_y', 'axis_slice_z']:
    for Q in ['E_t', 'H_t']:                # Interpolate E and H fields
        M.IB.interpolate(Q, S)               # on "axis" slice meshes

# M.IB.interpolate('H_t',                    # interpolate H on a slice with
#                 'above_topo_slice_z')     # 50m offset to the surface
M.IB.synchronize()                           # synchronize multi-threading

# Note: Synchronization of multi-threading can be conducted at any time


