# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
from custEM.misc import mpi_print as mpp


class PreProcessing:

    """
    PreProcessing class called from MOD instance

    class internal functions:
    -------------------------

    - import_all_results()
        import results of an existing model - all quantities

    - import_selected_results()
        import results of an existing model - selected quantities

    - load()
        basic import function for data files

    - read_h5()
        utility import function for **h5** data files
    """

    def __init__(self, FS, MP, load, field_selection='all',
                 self_mode=False, fs_type=None):

        """
        initalize instance for importing already calculated solutions

        Required arguments:
        -------------------

        - FS, type class
            FunctionSpaces instance

        - MP, type class
            ModelParameters instance

        Keyword arguments:
        ------------------

        - field_selection = 'all', type str
            flag controling which quantities are tried to be imported, possible
            choices are a combination of **E_t**, **E_s**, **H_t**, **H_s**,
            **A_t** or **A_s** appended in ONE! string.
        """

        self.FS = FS
        self.MP = MP
        self.self_mode = self_mode
        self.fs_type = fs_type
        self.import_files = True
        if type(load) is bool:
            self.import_files = False
        self.full_name = MP.out_dir + '/' + MP.out_name
        n_tx = [0]
        for root, dirs, files in os.walk(MP.out_dir):
            for f_name in files:
                if MP.out_name in f_name and \
                   f_name.endswith(self.MP.file_format):
                    n_tx.extend([sum(int(s) for s in f_name[
                            f_name.find('tx'):-1] if s.isdigit())])
        self.MP.n_tx = max(n_tx) + 1

        if self.MP.n_tx == 1:
            pass
        elif self.MP.n_tx != 1 and type(load) is bool:
            self.full_name += '_tx_0'
        else:
            self.full_name += '_tx_' + str(load)

        if field_selection == 'all':
            MP.mute = True
            self.import_all_results(MP.file_format, MP.mute)
            MP.mute = False
        else:
            self.import_selected_results(
                    field_selection, MP.file_format, MP.mute)

    def import_all_results(self, file_format, mute):

        """
        load all existing quantities from already calculated solutions

        Required arguments:
        -------------------

        - file_format, type str
            either 'h5' or 'xml', set within model paramers in **MOD** instance

        - mute, type bool
            controls print or no print of import message
        """

        if not mute:
            mpp('Importing results from "' + self.full_name + '" ...')

        if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
            self.E_t_r_cg = self.load('E_t_real_cg', file_format, 1, mute=mute)
            self.E_t_i_cg = self.load('E_t_imag_cg', file_format, 1, mute=mute)
            self.E_s_r_cg = self.load('E_s_real_cg', file_format, 1, mute=mute)
            self.E_s_i_cg = self.load('E_s_imag_cg', file_format, 1, mute=mute)

            self.H_t_r_cg = self.load('H_t_real_cg', file_format, 1, mute=mute)
            self.H_t_i_cg = self.load('H_t_imag_cg', file_format, 1, mute=mute)
            self.H_s_r_cg = self.load('H_s_real_cg', file_format, 1, mute=mute)
            self.H_s_i_cg = self.load('H_s_imag_cg', file_format, 1, mute=mute)

            self.A_t_r_cg = self.load('A_t_real_cg', file_format, 1, mute=mute)
            self.A_t_i_cg = self.load('A_t_imag_cg', file_format, 1, mute=mute)
            self.A_s_r_cg = self.load('A_s_real_cg', file_format, 1, mute=mute)
            self.A_s_i_cg = self.load('A_s_imag_cg', file_format, 1, mute=mute)
        if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
            self.E_t_r = self.load('E_t_real', file_format, 2, mute=mute)
            self.E_t_i = self.load('E_t_imag', file_format, 2, mute=mute)
            self.E_s_r = self.load('E_s_real', file_format, 2, mute=mute)
            self.E_s_i = self.load('E_s_imag', file_format, 2, mute=mute)

            self.H_t_r = self.load('H_t_real', file_format, 2, mute=mute)
            self.H_t_i = self.load('H_t_imag', file_format, 2, mute=mute)
            self.H_s_r = self.load('H_s_real', file_format, 2, mute=mute)
            self.H_s_i = self.load('H_s_imag', file_format, 2, mute=mute)

            self.A_t_r = self.load('A_t_real', file_format, 2, mute=mute)
            self.A_t_i = self.load('A_t_imag', file_format, 2, mute=mute)
            self.A_s_r = self.load('A_s_real', file_format, 2, mute=mute)
            self.A_s_i = self.load('A_s_imag', file_format, 2, mute=mute)

    def import_selected_results(self, selection, file_format, mute):

        """
        load selected quantities from already calculated solutions

        Required arguments:
        -------------------

        - selection, type str
            string that must contain a combination of of **E_t**, **E_s**,
            **H_t**, **H_s**, **A_t** or **A_s** appended in ONE! string.

        - file_format, type str
            either 'h5' or 'xml', set within model paramers in **MOD** instance

        - mute, type bool
            controls print or no print of import message
        """

        if not mute:
            mpp('Importing selected fields: ' + str(selection) +
                ', from "' + self.full_name + '" ...')

        if 'E_t' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.E_t_r_cg = self.load('E_t_real_cg', file_format, 1, mute)
                self.E_t_i_cg = self.load('E_t_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.E_t_r = self.load('E_t_real', file_format, 2, mute)
                self.E_t_i = self.load('E_t_imag', file_format, 2, mute)

        if 'E_s' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.E_s_r_cg = self.load('E_s_real_cg', file_format, 1, mute)
                self.E_s_i_cg = self.load('E_s_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.E_s_r = self.load('E_s_real', file_format, 2, mute)
                self.E_s_i = self.load('E_s_imag', file_format, 2, mute)

        if 'H_t' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.H_t_r_cg = self.load('H_t_real_cg', file_format, 1, mute)
                self.H_t_i_cg = self.load('H_t_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.H_t_r = self.load('H_t_real', file_format, 2, mute)
                self.H_t_i = self.load('H_t_imag', file_format, 2, mute)

        if 'H_s' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.H_s_r_cg = self.load('H_s_real_cg', file_format, 1, mute)
                self.H_s_i_cg = self.load('H_s_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.H_s_r = self.load('H_s_real', file_format, 2, mute)
                self.H_s_i = self.load('H_s_imag', file_format, 2, mute)

        if 'A_t' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.A_t_r_cg = self.load('A_t_real_cg', file_format, 1, mute)
                self.A_t_i_cg = self.load('A_t_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.A_t_r = self.load('A_t_real', file_format, 2, mute)
                self.A_t_i = self.load('A_t_imag', file_format, 2, mute)

        if 'A_s' in selection:
            if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                self.A_s_r_cg = self.load('A_s_real_cg', file_format, 1, mute)
                self.A_s_i_cg = self.load('A_s_imag_cg', file_format, 1, mute)
            if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                self.A_s_r = self.load('A_s_real', file_format, 2, mute)
                self.A_s_i = self.load('A_s_imag', file_format, 2, mute)

    def load(self, quantity, file_format, switch, mute=False):

        """
        Utility function to read data files in 'h5' format

        Required arguments:
        -------------------

        - quantitiy, type str
            quantitiy, e.g., **E_t_real_cg**, which should be imported

        - file_format, type str


        Keyword arguments:
        ------------------

        - mute, type bool
            controls print or no print of import warning messages
        """

        if file_format == 'h5':
            if os.path.isfile(self.full_name + '_' + quantity + '.h5'):
                if not self.import_files:
                    return(None)
                return(self.read_h5(self.full_name + '_' + quantity + '.h5',
                                    switch))
            else:
                if not mute:
                    mpp('Warning, model "' + self.full_name + '_' + quantity +
                        '.h5' + '_..." could not be found! Continuing ...')

        elif file_format == 'xml':
            if os.path.isfile(self.full_name + '_' + quantity + '.xml'):
                if not self.import_files:
                    return(None)
                if self.fs_type is 'None' or 'cg' in self.fs_type.lower():
                    return(df.Function(self.FS.V_cg, self.full_name + '_' +
                                       quantity + '.xml'))
                if self.fs_type is 'None' or 'ned' in self.fs_type.lower():
                    return(df.Function(self.FS.V, self.full_name + '_' +
                                       quantity + '.xml'))
            else:
                if not mute:
                    mpp('Warning, model "' + self.full_name + '_' + quantity +
                        '.xml' + '_..." could not be found! Continuing ...')

    def read_h5(self, f_name, switch):

        """
        Utility function to read data files in 'h5' format

        Required arguments:
        -------------------

        - fname, type str
            file name to import from, containing also the export path

        - file_format, type str
            either 'h5' or 'xml', set within model paramers in **MOD** instance

        Keyword arguments:
        ------------------

        - typE = 'CG', type str
            import data function space type, either **CG** or **Nedelec**
        """

        if switch == 1:
            data_function = df.Function(self.FS.V_cg)
        elif switch == 2:
            data_function = df.Function(self.FS.V)

        if self.self_mode:
            f = df.HDF5File(self.MP.mpi_cs, f_name, "r")
            f.read(data_function, "/data")
            f.close()
        else:
            f = df.HDF5File(self.MP.mpi_cw, f_name, "r")
            f.read(data_function, "/data")
            f.close()
        return(data_function)
