# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import time
from custEM.misc import mpi_print as mpp
from custEM.misc import max_mem


class Solver:

    """
    Solver class called internally from MOD instance

    class internal functions:
    -------------------------

    - solve_default()
        use default FEniCS solver, no support of symmetry

    - solve_mumps()
        use MUMPS solver, no memory limit!, symmetry support!

    - solve_iter()
        various iterative solution methods which may be implemented in
        future

    - solve_default()
        use default FEniCS solver, no support of symmetry

    - solve_mumps()
        use MUMPS solver, no memory limit!, symmetry support!

    - solve_iter()
        various iterative solution methods which may be implemented in
        future

    BoundaryConditions:
    -------------------

    - default choice of boundary conditions are implicit Neumann 'bc'

    - for zero Dirichlet bc, change bc argument in the 'solve' functions
      or **during assembly for all total field** approaches!
    """

    def __init__(self, FS, FE, mumps_debug=False):

        """
        Initializes Solver instance for assembling and solution of FE systems

        Required arguments:
        -------------------

        - FS, type class
            FunctionSpaces instance
        """

        self.FS = FS
        self.FE = FE
        df.PETScOptions.set("mat_mumps_icntl_14", 60)
        df.PETScOptions.set("mat_mumps_icntl_10", 1)
        df.PETScOptions.set("mat_mumps_icntl_28", 2)
        df.PETScOptions.set("mat_mumps_cntl_1", 1e-8)
        #        df.PETScOptions.set("mat_mumps_cntl_7", 1e-10)
        #        df.PETScOptions.set("mat_mumps_icntl_35", 1)  BLR test
        # choose SCOTCH explicitly, not necessary as icntl 28 forces SCOTCH
        # df.PETScOptions.set("mat_mumps_icntl_29", 1)
        if mumps_debug:
            df.PETScOptions.set("mat_mumps_icntl_4", 3)

    def solve_var_form_default(self, L, R, U, bc, mute=False):

        """
        use default petsc_lu solver to solve a LinearVariationProblem.

        Required arguments:
        -------------------

        - L = left-hand-side, type UFL form
            not assembled! UFL form of the left-hand-side of the system of
            equations


        - R = right-hand-side, type UFL form
            not assembled! UFL form of the right-hand-side of the system of
            equations


        - U = Solution space, type dolfin FunctionSpace
            mixed solution FunctionSpace


        - bc = boundary conditions, type str
            so far **'ZeroDirichlet'** or short **'ZD'** bc
            and implicit Neumann bc by using **None** are supported
        """

        t_0 = time.time()

        if not mute:
            mpp('Default petsc LU solver initialized:')
        mpp('...  solving system  ...', pre_dash=False)
        if bc is None:
            df.solve(L == R, U)

        elif bc is 'ZeroDirichlet' or 'ZD':

            self.FS.add_dirichlet_bc()
            df.solve(L == R, U, bcs=self.FS.bc)

        else:
            mpp('Typing Error, Choose either None (defualt) or "ZD" or '
                '"ZeroDirichlet" as boundary condition. Further bcs may be '
                'available in future')

        self.FS.solution_time = time.time() - t_0
        if not mute:
            mpp('Solution time for FE system including assembly [s]:  ' +
                str(self.FS.solution_time), post_dash=True)

    def solve_system_default(self, A, b, mute=False):

        """
        Use default petsc_lu solver to solve an assembled system of equations.

        Required arguments:
        -------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix


        - b = right-hand-side vector, type PETScVector
            assembled RHS vector
        """

        t_0 = time.time()

        if not mute:
            mpp('Default petsc LU solver initialized:')
        mpp('...  solving system  ...', pre_dash=False)

        df.solve(A, self.FS.U.vector(), b.vector())

        self.FS.solution_time = time.time() - t_0
        if not mute:
            mpp('Solution time for main system of equations [s]:  ' +
                str(self.FS.solution_time), post_dash=True)

    def solve_system_mumps(self, A, b, FS=None, sym=False,
                           out_of_core=False, mute=False, first_call=True):

        """
        Use MUMPS solver to solve an assembled linear system of equations.

        Required arguments:
        -------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix


        - b = right-hand-side vector, type PETScVector
            assembled RHS vector

        Keyword arguments:
        ------------------

        - sym = False, type int or bool
            set to **1** for positive definite symmetric matrix
            set to **2** or **True** for general structurally symmetric matrix

        - out_of_core = False, type bool
            set to **True** if MUMPS should be allowed to use disc space for
            the solution process if the memory is completely exploited
        """

        second_call = True
        if FS is None:
            self.U = [df.Function(self.FS.M) for ti in range(self.FE.n_tx)]
        else:
            self.U = [df.Function(FS) for ti in range(self.FE.n_tx)]
        for ti in range(self.FE.n_tx):
            if not first_call:
                if second_call:
                    mpp('...  solving additional right-hand sides  ...',
                        pre_dash=False)
                    t2 = time.time()
                mute = True
                second_call = False
            if hasattr(b[ti], 'array') or hasattr(b[ti], 'vec'):
                to_solve = b[ti]
            else:
                to_solve = b[ti].vector()
            self.call_mumps(A, to_solve, self.U[ti].vector(), sym, mute,
                            out_of_core, first_call)
            first_call = False
        if self.FE.n_tx > 1:
            mpp('  -  solution time for ' + str(self.FE.n_tx - 1) +
                ' additional right-hand sides [s]:  --> '
                ' ' + str(int(time.time() - t2)), pre_dash=False)
        if FS is None:
            self.FS.U = self.U
        else:
            return(self.U)

    def call_mumps(self, A, b, u, sym, mute, out_of_core, first_call=True):

        """
        See definitions of *solve_system_mumps()* or *solve_var_form_mumps()*
        methods
        """

        t_0 = time.time()
        if first_call:
            if not mute:
                mpp('Solution of FE system of equations:')
            if out_of_core:
                df.PETScOptions.set("mat_mumps_icntl_22", 1)
                mpp("OUT_OF_CORE option applied! Advice: don't do this!")

            from petsc4py import PETSc
            if sym is 1:
                A.mat().setOption(PETSc.Mat.Option.SPD, True)
                if not mute:
                    mpp('  -  positive definite symmetric MUMPS solver '
                        'initialized  -  ', pre_dash=False)
            if sym is 2 or sym:
                A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
                if not mute:
                    mpp('  -  general symmetric MUMPS solver initialized  -  ',
                        pre_dash=False)
            else:
                A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, False)
                A.mat().setOption(PETSc.Mat.Option.SPD, False)
                if not mute:
                    mpp('  -  unsymmetric MUMPS LU solver initialized  -  ',
                        pre_dash=False)

            self.solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
            mpp('...  solving system  ...', pre_dash=False)
        self.solver.solve(u, b)
        self.FS.solution_time = time.time() - t_0
        if not mute:
            mpp('  -  solution time for main FE system [s]:  -->  ' +
                str(int(self.FS.solution_time)), pre_dash=False)
            max_mem()

    def solve_var_form_iter(self,  L, R, U, bc, sym=False,
                            method='gmres', pc='petsc_amg', mute=False,
                            abs_tol=1e-6, rel_tol=1e-6, maxiter=2000):

        """
        Use Krylov solver to solve a LinearVariationProblem.

        Required arguments:
        -------------------

        - L = left-hand-side, type UFL form
            not assembled! UFL form of the left-hand-side of the system of
            equations


        - R = right-hand-side, type UFL form
            not assembled! UFL form of the right-hand-side of the system of
            equations

        - U = Solution space, type dolfin FunctionSpace
            mixed solution FunctionSpace


        - bc = boundary conditions, type str
            so far **'ZeroDirichlet'** or short **'ZD'** bc
            and implicit Neumann bc by using **None** are supported

        Keyword arguments:
        ------------------

        - sym = False, type bool
            set to **True** if a symmetric system of equations is solved

        - method = 'gmres', type str
            iterative solution method

        - pc = 'petsc_amg', type str
            prconditioning type

        - abs_tol = 1e-6, type float
            absolute tolereance

        - rel_tol = 1e-6, type float
            relative tolerance

        - maxiter = 2000, type int
            maximum number of iterations
        """

        start = time.time()

        mpp('Using Krylov solver    -->   "Not working proberly, '
            ' ... in development!')

        if bc is None:
            problem = df.LinearVariationalProblem(L, R, U)

        elif bc is 'ZeroDirichlet' or 'ZD':

            self.FS.add_dirichlet_bc()
            problem = df.LinearVariationalProblem(L, R, U, self.FS.bc)

        else:
            mpp('Typing Error, Choose either None (defualt) or "ZD" or '
                '"ZeroDirichlet" as boundary condition. Further bcs may be '
                'available in future')

        solver = df.LinearVariationalSolver(problem)
        # solver.parameters['symmetric'] = sym
        solver.parameters['linear_solver'] = method
        solver.parameters['preconditioner'] = pc
        solver.parameters['krylov_solver']['monitor_convergence'] = True
        solver.parameters['krylov_solver']["absolute_tolerance"] = abs_tol
        solver.parameters['krylov_solver']["relative_tolerance"] = rel_tol
        solver.parameters['krylov_solver']["maximum_iterations"] = maxiter
        # solver.parameters['krylov_solver']['nonzero_initial_guess'] = True
        mpp('...  solving system  ...', post_dash=True)
        solver.solve()

        stop = time.time() - start
        if not mute:
            mpp('solution time for FE system including assembly:  ' +
                str(stop), post_dash=True)

    def solve_system_iter(self, A, b, sym=False,
                          method='gmres', pc='petsc_amg', mute=False,
                          abs_tol=1e-6, rel_tol=1e-6, maxiter=2000):

        """
        Use Krylov solver to solve an assembled linear system of equations.

        Required arguments:
        -------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix


        - b = right-hand-side vector, type PETScVector
            assembled RHS vector

        Keyword arguments:
        ------------------

        - sym = True, type bool
            set to **False** if an unsymmetric system of equations is solved

        - method = 'gmres', type str
            iterative solution method

        - pc = 'petsc_amg', type str
            prconditioning type

        - abs_tol = 1e-6, type float
            absolute tolereance

        - rel_tol = 1e-6, type float
            relative tolerance

        - maxiter = 2000, type int
            maximum number of iterations
        """

        mpp('Using Krylov solver    -->   "Not working proberly, '
            ' ... in development!')

        t_0 = time.time()

#        prm = df.parameters.krylov_solver  # short form
#        prm.absolute_tolerance = abs_tol
#        prm.relative_tolerance = rel_tol
#        prm.maximum_iterations = maxiter
#        prm.monitor_convergence = True

        # Set PETSc solve type (conjugate gradient) and preconditioner
        # (algebraic multigrid)
        df.PETScOptions().set("ksp_type", method)
        df.PETScOptions().set("pc_type", pc)
#        df.PETScOptions.set("ksp_knoll")
        df.PETScOptions().set("pc_hypre_type", "boomeramg")
#        df.PETScOptions().set("pc_hypre_boomeramg_agg_nl", 10)
#        df.PETScOptions().set("pc_hypre_boomeramg_max_levels", 500)
#        df.PETScOptions().set("pc_hypre_boomeramg_relax_weight_all", 0.7)

#        df.PETScOptions.set("pc_hypre_euclid_levels", 3)
#        df.PETScOptions().set("ksp_gmres_restart", 50)
#        df.PETScOptions().set("ksp_monitor_true_residual")
        df.PETScOptions().set("ksp_max_it", maxiter)
        df.PETScOptions().set("ksp_rtol", rel_tol)
        df.PETScOptions().set("ksp_atol", abs_tol)
#        # Since we have a singular problem, use SVD solver on the multigrid
#        # 'coarse grid'
#        PETScOptions.set("mg_coarse_ksp_type", "preonly")
#        PETScOptions.set("mg_coarse_pc_type", "svd")
#
#        # Set the solver tolerance
#        PETScOptions.set("ksp_rtol", 1.0e-8)
#
#        # Print PETSc solver configuration
#        df.PETScOptions.set("ksp_view")
        df.PETScOptions.set("ksp_monitor")

        # Create Krylov solver and set operator
        solver = df.PETScKrylovSolver()
        solver.set_operator(A)

        # Set PETSc options on the solver
        solver.set_from_options()
        mpp('...  solving system  ...', post_dash=True)
        # Solve
        solver.solve(self.FS.U.vector(), b.vector())

#        solver = df.KrylovSolver(method, pc)
#

#        solver.solve(A, self.FS.U.vector(), b.vector())

        stop = time.time() - t_0
        if not mute:
            mpp('Solution time for main system of equations:  ' + str(stop),
                post_dash=True)
