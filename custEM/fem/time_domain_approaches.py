# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
import custEM as ce
from custEM.misc import mpi_print as mpp
from custEM.misc import write_h5, read_h5
from custEM.misc import root_write as rw
from custEM.core import MOD
try:
    from custEM.misc import DirichletBoundary
except:
    print('DirchletBoundary not imported, continuing')
from custEM.misc import run_multiple_serial as rs_multiple
from custEM.fem import ApproachBase
from custEM.fem import print_fem_parameters
from custEM.fem import check_sigma_dx_conformity
from custEM.fem import check_source
from custEM.fem import add_sigma_vals_to_list
from custEM.fem import RHS_assembler
from custEM.fem import build_DG_func
from petsc4py import PETSc
import scipy.linalg as la
import time
import os


class E_implicit_euler(ApproachBase):

    """
    FE implementation of implicit euler time-stepping method, e.g.,
    (Um, 2010), with either first or second order discretization in time
    and possible incorporation of displacement currents.

    class internal functions:
    -------------------------

    - time_stepping()
        runs the complete time-stepping solution process as well as H-field
        conversion and export methods, FE modeling and time stepping related
        keyword arguments can be passed to this method.

    - build_var_form()
        set up variational formulation and assemble mass and curl-curl matrices
        as well right-hand side (source) vector

    - init_initial_condition()
        initialize initial condition fpr time-stepping approach

    - first_order_IE()
        perform first-order implicit Euler time-stepping approach

    - second_order_IE()
        perform second-order implicit Euler time-stepping approach

    - first_order_IE_full()
        perform first-order implicit Euler time-stepping approach including
        displacement currents

    - convert_to_H()
        method to calculate the magentic field H from E on-the-fly at specified
        time steps for export

    - init_times()
        initalize internally used time vector for the time stepping

    - write_data()
        method for saving results at specified times to file

    - gaussian()
        define gaussian source pulse for impulse response modeling

    - calc_dc_field()
        calculate static electric field for obtaining the DC level

    - interpolate_all_td_result()
        load stored data from parallel computation and interpolate fields
        at all specified times
    """

    def __init__(self, FS, MP):

        """
        - v, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters

        super(self.__class__, self).__init__()

        self.v = df.TestFunction(FS.V)
        self.u = df.TrialFunction(FS.V)
        self.be_order = 1
        self.log_t_max = 0
        self.log_t_min = -6
        self.n_log_steps = 13
        self.n_lin_steps = 10
        self.export_pvd = False
        self.dc_flag = False
        self.var_form_only = False
        self.n_on = 11
        self.pulse_amp = 0.3
        self.times = None
        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v', 'DOM', 'u', 'dx', 'dx_0', 'FS', 'MP', 'mute', 'path']
        self.dx_0 = self.FS.DOM.dx_0
        self.source_type = 'j'
        self.tol = 1e-2

    def time_stepping(self, convert_to_H=True, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        self.target_times = np.logspace(
                self.log_t_min, self.log_t_max,
                int((self.log_t_max - self.log_t_min) * 10 + 1))
        self.n_times = len(self.target_times)

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)

        self.build_var_form()
        if self.be_order == 1 and self.quasi_static:
            u_0 = self.init_initial_condition(None)
            self.first_order_IE(u_0, convert_to_H)
        elif self.be_order == 2:
            u_0, u_1 = self.init_initial_condition(None, order=2)
            self.second_order_IE(u_0, u_1, convert_to_H)
        elif self.be_order == 1 and not self.quasi_static:
            u_0, u_1 = self.init_initial_condition(None, order=2)
            self.first_order_IE_full(u_0, u_1, convert_to_H)
        else:
            mpp('Fatal Error!, Only order *1* or *2* for implicit Euler method'
                ' are supported. Aborting ...')
            raise SystemExit
        mpp('...  Finished!', post_dash=True)

    def build_var_form(self):

        """
        weak form for E-field tiem stepping approach
        """

        mpp('...  assembling system matrix  ...')
        t0 = time.time()

        self.mu1 = df.Constant(1./self.MP.mu)
        if self.bc is not None:
            boundary = DirichletBoundary()
            bc = df.DirichletBC(self.FS.V, df.Constant((0, 0, 0)), boundary)
        else:
            bc = None

        if self.sigma_from_func:
            d_sig_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma)
            C_form = df.inner(self.mu1 * df.curl(self.u),
                              df.curl(self.v)) * self.dx_0
            B_form = d_sig_func * df.inner(self.u, self.v) * self.dx_0
            if not self.quasi_static:
                d_eps_func = build_DG_func(self.FS.S_DG,
                                           self.FS.DOM.domain_func,
                                           np.ones(self.FS.DOM.n_domains) *
                                           self.MP.eps_0)
                A_form = d_eps_func * df.inner(self.u, self.v) * self.dx_0
        else:
            dx = self.FS.DOM.dx
            sigma = add_sigma_vals_to_list(self.MP.sigma)
            C_form = df.inner(self.mu1 * df.curl(self.u),
                              df.curl(self.v)) * dx(0)
            B_form = sigma[0] * df.inner(self.u, self.v) * dx(0)
            if not self.quasi_static:
                A_form = self.MP.eps_0 * \
                         df.inner(self.u, self.v) * self.dx(0)

            for j in range(1, self.FS.DOM.n_domains):
                C_form += df.inner(self.mu1 * df.curl(self.u),
                                   df.curl(self.v)) * dx(j)
                B_form += sigma[j] * df.inner(self.u, self.v) * dx(j)
                if not self.quasi_static:
                    A_form = self.MP.eps_0 * \
                             df.inner(self.u, self.v) * self.dx(j)
        self.A, self.B, self.C = df.PETScMatrix(), df.PETScMatrix(), \
            df.PETScMatrix()
        a, b, c = df.PETScVector(), df.PETScVector(), df.PETScVector()
        RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v) * self.dx_0
        df.assemble_system(B_form, RHS, bc, A_tensor=self.B, b_tensor=b)
        df.assemble_system(C_form, RHS, bc, A_tensor=self.C, b_tensor=c)
        if not self.quasi_static:
            df.assemble_system(A_form, RHS, bc, A_tensor=self.A, b_tensor=a)
        Assembler = RHS_assembler(self, self.bc)
        Assembler.assemble()
        self.J = Assembler.b

        self.assembly_time = time.time() - t0
        mpp('  -  time elapsed [s]:  -->  ', self.assembly_time,
            pre_dash=False)

#        tmp = df.Function(self.FS.V)
#        tmp.vector()[:] = self.J
#        df.File('J.pvd') << tmp
#        bla = build_DG_func(self.FS.S_DG,
#                                       self.FS.DOM.domain_func,
#                                       self.MP.sigma, inverse=True)
#        RHS = df.inner(bla * tmp, self.v) * self.dx_0
#        u = df.Function(self.FS.V)
#        B = df.PETScMatrix()
#        b = df.PETScVector()
#        D, ab  = df.assemble_system(df.inner(self.u, self.v) * self.dx_0,
#                                    RHS,
#                                    A_tensor=B, b_tensor=b)
#        solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), D, "mumps")
#        solver.solve(u.vector(), ab)
#        df.File('u0.pvd') << u
#        raise SystemExit


#        if 'loop_r' in self.s_type:
#            return
#        if type(self.closed_path) is not list:
#            self.closed_path = [self.closed_path]
#        if not self.closed_path[0] or self.s_type.lower() in ['line', 'hed']:
#            mpp('...  importing DC level for inital condition  ...')
#            if self.MP.file_format in 'h5':
#                from custEM.misc import read_h5
#                E_dc = df.Function(self.FS.V)
#                try:
#                    read_h5(self.MP.mpi_cw, E_dc,
#                           self.MP.out_dir + '/' + self.MP.out_name + '_E.h5')
#                except (RuntimeError, FileNotFoundError):
#                    mpp('...  calculating DC level for inital condition  ...')
#                    self.calc_dc_field()
#                    read_h5(self.MP.mpi_cw, E_dc,
#                           self.MP.out_dir + '/' + self.MP.out_name + '_E.h5')
#            self.E_dc = E_dc
#            self.dc_flag = True

    def init_initial_condition(self, ini_con, order=1):

        if ini_con is None:
            if order == 1:
                return(df.Function(self.FS.V))
            else:
                return(df.Function(self.FS.V), df.Function(self.FS.V))

    def first_order_IE(self, u_0, convert_to_H):

        outfileE = df.File(self.MP.out_dir + '/E_' + self.MP.mod_name + '.pvd')
        outfileH = df.File(self.MP.out_dir + '/H_' + self.MP.mod_name + '.pvd')
        counter, export_counter, export_times = 0, 0, np.empty(0)
        times, self.export_flags = self.init_times()
        dt = np.diff(times, axis=0).ravel()
        mpp('...  Starting times stepping  ...')
        mpp('  -  Overall number of time steps:  -->  ' + str(len(dt)))

        uu, vv = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
        LHS = df.inner(uu, vv) * self.dx_0
        RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), vv) * self.dx_0
        AA, bb = df.PETScMatrix(), df.PETScVector()
        A, b = df.assemble_system(LHS, RHS, A_tensor=AA, b_tensor=bb)
        A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
        mpp('  -  system matrix size:  -->  ' + str(A.mat().getSize()[
                0]), pre_dash=False)
        self.H_solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
        gauss = self.gaussian(np.linspace(0., 10**(self.log_t_min-1), 101),
                              0.5 * 10**(self.log_t_min-1),
                              self.pulse_amp * 10**(self.log_t_min-1))

        if self.var_form_only:
            return

        t0 = time.time()
        while counter < len(dt):
            t0 = time.time()
            mpp('starting step: %d, t =%8.3e, dt = %8.3e' % (
                    counter, times[counter + 1], dt[counter]))
            u_1 = df.Function(self.FS.V)
            if self.source_type == 'j':
                if counter < self.n_on - 6:
                    b = self.B*u_0.vector()
                    mpp('off', counter)
                elif counter < self.n_on - 1:
                    b = self.B*u_0.vector() - dt[counter] * self.J
                    mpp('on', counter)
                else:
                    b = self.B*u_0.vector()
                    mpp('off', counter)
            elif self.source_type == 'djdt':
                if counter in self.n_on_djdt:
                    b = self.B*u_0.vector() -\
                        dt[counter] * self.J * gauss[counter]
                    mpp('on', counter)
                else:
                    b = self.B*u_0.vector()
                    mpp('off', counter)
            else:
                mpp('Fatal Error! *source_type* must be either "j" or "djdt."')
                raise SystemExit
            if not np.isclose(dt[counter], dt[counter - 1],
                              atol=dt[counter]*1e-2):
                D = self.B + dt[counter]*self.C
                solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), D, "mumps")
            solver.solve(u_1.vector(), b)

            # Update previous solution and save E-field
            if self.export_flags[counter + 1]:
                if convert_to_H:
                    H = self.convert_to_H(u_1, vv)
                    if self.export_pvd:
                        u_cg = df.interpolate(u_1, self.FS.V_cg)
                        u_cg.rename('E_field', 'label')
                        H_cg = df.interpolate(H, self.FS.V_cg)
                        H_cg.rename('H_field', 'label')
                        outfileE << (u_cg, times[counter + 1])
                        outfileH << (H_cg, times[counter + 1])
                else:
                    H = df.Function(self.FS.V)
                    H_cg = df.Function(self.FS.V_cg)
                self.write_data(export_counter, u_1, H)
                export_times = np.append(export_times, times[counter + 1])
                export_counter += 1
            counter += 1
            u_0.assign(u_1)
            mpp('TIME passed:  ', time.time() - t0)
        np.savetxt(self.MP.out_dir + '/' + self.MP.mod_name + '_times.txt',
                   np.array(export_times))

        self.FS.solution_time = time.time() - t0
        mpp('Overall solution time for time stepping (s):  ' +
            str(self.FS.solution_time))

#    def first_order_IE_full(self, u_0, u_1, convert_to_H):
#
#        OutfileE = df.File(self.MP.out_dir + '/E_' + self.MP.mod_name +'.pvd')
#        OutfileH = df.File(self.MP.out_dir + '/H_' + self.MP.mod_name +'.pvd')
#        counter, export_counter, export_times = 0, 0, np.empty(0)
#        times, self.export_flags = self.init_times()
#        dt = np.diff(times, axis=0).ravel()
#        mpp('Overall number of time steps:  -->  ' + str(len(dt)))
#
#        uu, vv = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
#        LHS = df.inner(uu, vv) * self.dx_0
#        RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), vv) * self.dx_0
#        AA, bb = df.PETScMatrix(), df.PETScVector()
#        A, b = df.assemble_system(LHS, RHS, A_tensor=AA, b_tensor=bb)
#        A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
#        mpp('  -  system matrix size:  -->  ' + str(A.mat().getSize()[
#                0]), pre_dash=False)
#        self.H_solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
#        gauss = self.gaussian(np.linspace(0., 10**self.log_t_min, 101),
#                              0.5 * 10**self.log_t_min,
#                              0.1 * 10**self.log_t_min)
#
#        while counter < len(dt):
#            t0 = time.time()
#            mpp('starting step: %d, t =%8.1e, dt = %8.1e' % (
#                    counter, times[counter + 1], dt[counter]))
#            u_2 = df.Function(self.FS.V)
#            if self.source_type == 'j':
#                if counter < self.n_on - 1:
#                    b = ((2./dt[counter]) * self.A + self.B) * u_1.vector() -\
#                        (1./dt[counter]) * self.A * u_0.vector() +\
#                        dt[counter] * self.J
#                    mpp('on', counter)
#                else:
#                    b = ((2./dt[counter]) * self.A + self.B) * u_1.vector() -\
#                        (1./dt[counter]) * self.A * u_0.vector()
#                    mpp('off', counter)
#            elif self.source_type == 'djdt':
#                if counter < self.n_on - 1:
#                    b = ((2./dt[counter]) * self.A + self.B) * u_1.vector() -\
#                        (1./dt[counter]) * self.A * u_0.vector() -\
#                        dt[counter] * self.J * gauss[counter]
#                    mpp('on', counter)
#                else:
#                    b = ((2./dt[counter]) * self.A + self.B) * u_1.vector() -\
#                        (1./dt[counter]) * self.A * u_0.vector()
#                    mpp('off', counter)
#            else:
#               mpp('Fatal Error! *source_type* must be either "j" or "djdt."')
#                raise SystemExit
#            if not np.isclose(dt[counter], dt[counter - 1],
#                              atol=dt[counter]*1e-2):
#                D = (1./dt[counter]) * self.A + self.B + dt[counter] * self.C
#                solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), D, "mumps")
#            solver.solve(u_2.vector(), b)
#
#            # Update previous solution and save E-field
#            if self.export_flags[counter + 1]:
#                H = self.convert_to_H(u_2, vv)
#                if self.export_pvd:
#                    u_cg = df.interpolate(u_2, self.FS.V_cg)
#                    u_cg.rename('E_field', 'label')
#                    H_cg = df.interpolate(H, self.FS.V_cg)
#                    H_cg.rename('H_field', 'label')
#                    OutfileE << (u_cg, times[counter + 1])
#                    OutfileH << (H_cg, times[counter + 1])
#                self.write_data(export_counter, u_1, H)
#                export_times = np.append(export_times, times[counter + 1])
#                export_counter += 1
#            counter += 1
#            u_0.assign(u_1)
#            u_1.assign(u_2)
#            mpp('TIME passed:  ', time.time() - t0)
#        np.savetxt(self.MP.out_dir + '/' + self.MP.mod_name + '_times.txt',
#                   np.array(export_times))
#        mpp('...  Finished!')

    def second_order_IE(self, u_0, u_1, convert_to_H):

        u_0 = df.Function(self.FS.V)
        OutfileE = df.File(self.MP.out_dir + '/E_' + self.MP.mod_name + '.pvd')
        OutfileH = df.File(self.MP.out_dir + '/H_' + self.MP.mod_name + '.pvd')

        counter, export_counter, export_times = 0, 0, np.empty(0)
        times, self.export_flags = self.init_times()
        dt = np.diff(times, axis=0).ravel()
        mpp('Overall number of time steps:  -->  ' + str(len(dt)))

        uu, vv = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
        LHS = df.inner(uu, vv) * self.dx_0
        RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), vv) * self.dx_0
        AA, bb = df.PETScMatrix(), df.PETScVector()
        A, b = df.assemble_system(LHS, RHS, A_tensor=AA, b_tensor=bb)
        A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
        mpp('  -  system matrix size:  -->  ' + str(A.mat().getSize()[
                0]), pre_dash=False)
        self.H_solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
        gauss = self.gaussian(np.linspace(0., 10**self.log_t_min, 101),
                              0.5 * 10**self.log_t_min,
                              self.pulse_amp * 10**self.log_t_min)

        if self.var_form_only:
            return

        u_1 = df.Function(self.FS.V)
        t0 = time.time()

        # time stepping after step 1
        while counter < len(dt):
            t0 = time.time()
            mpp('starting step: %d, t =%8.3e, dt = %8.3e' % (
                    counter, times[counter + 1], dt[counter]))
            u_2 = df.Function(self.FS.V)

            if self.source_type == 'j':
                if counter < self.n_on - 2:
                    b = self.B*(4.*u_1.vector() - u_0.vector()) -\
                        2*dt[counter]*self.J
                    mpp('on', counter)
                else:
                    b = self.B*(4.*u_1.vector() - u_0.vector())
                    mpp('off', counter)

            elif self.source_type == 'djdt':
                if counter in self.n_on_djdt:
                    b = self.B*(4.*u_1.vector() - u_0.vector()) -\
                        2*dt[counter]*self.J * gauss[counter]
                    mpp('on', counter)
                else:
                    b = self.B*(4.*u_1.vector() - u_0.vector())
                    mpp('off', counter)

            else:
                mpp('Fatal Error! *source_type* must be either "j" or "djdt."')
                raise SystemExit

            if not np.isclose(dt[counter], dt[counter - 1],
                              atol=dt[counter]*1e-2):
                D = 3.*self.B + 2.*dt[counter]*self.C
                solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), D, "mumps")
            solver.solve(u_2.vector(), b)

            # Update previous solution and save E-field
            if self.export_flags[counter + 1]:
                if convert_to_H:
                    H = self.convert_to_H(u_2, vv)
                    if self.export_pvd:
                        u_cg = df.interpolate(u_2, self.FS.V_cg)
                        u_cg.rename('E_field', 'label')
                        H_cg = df.interpolate(H, self.FS.V_cg)
                        H_cg.rename('H_field', 'label')
                        OutfileE << (u_cg, times[counter + 1])
                        OutfileH << (H_cg, times[counter + 1])
                else:
                    H = df.Function(self.FS.V)
                    H_cg = df.Function(self.FS.V_cg)
                self.write_data(export_counter, u_1, H)
                export_times = np.append(export_times, times[counter + 1])
                export_counter += 1
            counter += 1
            u_0.assign(u_1)
            u_1.assign(u_2)
            mpp('TIME passed:  ', time.time() - t0)
        np.savetxt(self.MP.out_dir + '/' + self.MP.mod_name + '_times.txt',
                   np.array(export_times))

        self.FS.solution_time = time.time() - t0
        mpp('Solution time for main system of equations [s]:  ' +
            str(self.FS.solution_time), post_dash=True)
        mpp('...  Finished!')

    def convert_to_H(self, u, vv):

        H = df.Function(self.FS.V)
        mpp('...  deriving H-fields from E  ...', pre_dash=False)
        RHS = df.inner(self.mu1 * df.curl(u), vv) * self.dx_0
        b = df.assemble(RHS)
        self.H_solver.solve(H.vector(), b)
        return(H)

    def init_times(self):

        if self.source_type == 'j':
            all_times = np.linspace(-10., 0., self.n_on)
            all_times = np.append(
                    all_times, np.linspace(10**(self.log_t_min - 2),
                                           10**self.log_t_min, 101)[:-1])

        if self.source_type == 'djdt':
            all_times = np.linspace(0., 10**(self.log_t_min-1),
                                    101) - 10**(self.log_t_min-1)
            all_times = np.append(
                    all_times, np.linspace(10**(self.log_t_min - 1),
                                           10**self.log_t_min, 101)[:-1])
            self.n_on_djdt = np.arange(100, dtype=int)
            if self.be_order == 2:
                self.n_on_djdt = np.arange(99, dtype=int)

        if self.times is not None:
            all_times = self.times
        else:
            if self.n_lin_steps == 0:
                all_times = np.append(all_times, np.logspace(
                        self.log_t_min, self.log_t_max, self.n_log_steps))
            else:
                log_times = np.logspace(self.log_t_min, self.log_t_max,
                                        self.n_log_steps)
                for jj in range(len(log_times) - 1):
                    all_times = np.append(all_times, np.linspace(
                        log_times[jj], log_times[jj+1],
                        self.n_lin_steps + 1)[:-1])

                all_times = np.append(all_times, log_times[-1])

        export_flags = np.array([False for ll in range(len(all_times))])
        for tt in self.target_times:
            export_flags[np.abs(all_times - tt).argmin()] = True
        if len(export_flags[export_flags]) != len(self.target_times):
            mpp('Fatal error! Not all target times can be exported with this '
                'mix of linear and logarithmic time steps! Choose more time'
                'steps or decrease the number of target times! Aborting  ...')
        return(all_times.reshape(-1, 1), export_flags)

    def write_data(self, counter, E, H):

        out_E = self.MP.out_dir + '/E_' + self.MP.mod_name + '.' + \
            self.MP.file_format
        out_H = self.MP.out_dir + '/H_' + self.MP.mod_name + '.' + \
            self.MP.file_format
        if self.MP.file_format in 'xml':
            mpp('Warning! writing xml output of time domain data currently '
                'not supported! Writing every step to the same file  ...!')
            df.File(out_E + '.' + self.MP.file_format) << E
            df.File(out_H + '.' + self.MP.file_format) << H
        elif self.MP.file_format in 'h5':
            if counter == 0:
                self.E_file = write_h5(self.MP.mpi_cw, E, out_E,
                                       counter=counter, close=False)
                self.H_file = write_h5(self.MP.mpi_cw, H, out_H,
                                       counter=counter, close=False)
            elif counter == len(self.target_times) - 1:
                write_h5(self.MP.mpi_cw, E, out_E,
                         counter=counter, new=False, f=self.E_file, close=True)
                write_h5(self.MP.mpi_cw, H, out_H,
                         counter=counter, new=False, f=self.H_file, close=True)
            else:
                write_h5(self.MP.mpi_cw, E, out_E, f=self.E_file,
                         new=False, counter=counter, close=False)
                write_h5(self.MP.mpi_cw, H, out_H, f=self.H_file,
                         new=False, counter=counter, close=False)

    def gaussian(self, x, x0, sigma):
        return (np.exp(-(x - x0)**2 / sigma**2)) / (sigma * np.sqrt(np.pi))

    def calc_dc_field(self):

        M = MOD(self.MP.mod_name, self.MP.mesh_name, 'DC', overwrite=True,
                p=2, r_dir=self.MP.r_dir, m_dir=self.MP.m_dir)
        M.MP.update_model_parameters(
                sigma_ground=self.MP.sigma_ground[:len(self.MP.sigma_anom)],
                sigma_anom=self.MP.sigma_anom,
                sigma_air=self.MP.sigma_air,
                anomaly_layer_markers=self.MP.anomaly_layer_markers)
        M.FE.build_var_form(s_type=self.s_type,
                            path=self.path,
                            bc='ZD',
                            closed_path=self.closed_path,
                            start=self.start,
                            stop=self.stop,
                            origin=self.origin,
                            length=self.length,
                            azimuth=self.azimuth,
                            r=self.r)
        M.PP.full_name = './results/E_TD/' + self.MP.mesh_name + '_results/' +\
                         self.MP.mod_name
        M.solve_main_problem(export_pvd=False, export_cg=False)

    def interpolate_all_td_results(self, interp_mesh, mute=True, interp_p=None,
                                   max_new_procs_spawn=None, use_root=False,
                                   fs_type=None, dg_interp=False,
                                   export_suffix=None, interp_dir=None):

        """
        Customized interpolation function.

        Required arguments:
        -------------------

        - interp_mesh, type str
            name of the mesh to interpolate on.

        Keyword arguments:
        ------------------

        - mute = False, type bool
            set True, if info-interpolation messages should not be printed.

        - interp_p = 2, type int
            polyonimal order of interpolation mesh *FunctionSpaces*

        - max_new_procs_spawn = None, type int
            specify how many additional processes should be spawned in
            addition to the number of procs already used in the mpirun call,
            usually it is not necessary to spawn additional processes for
            significant acceleration of all interpolation tasks.
            Notice! This might lead to random synchronization issues.

        - use_root = False, type bool
            Use also the root process for serial interpolation tasks. Setting
            this flag to **True** has no effect if **max_new_procs_spawn**
            is not **0**!

        - export_suffix = None, type str
            specify a custom export name of the interpolated data if desired.

        - interp_dir, type str
            specify, if a custom interpolation directory is desired, otherwise,
            the defualt interpolation directory is located in the corresponding
            results directory

        - dg:interpolation = None, type bool
            set True or False to overwrite *self.dg_interpolation* for
            manually enabling discontinuous or continuous interpolation

        Example:
        --------

        >>> ### to do
        """

        if export_suffix is None:
            export_suffix = '_on_' + interp_mesh

        self.rank = 0
        self.interp_p = 1
        self.max_procs = df.MPI.size(self.MP.mpi_cw)
        self.interp_dir = (self.MP.r_dir + '/' + self.MP.approach + '/' +
                           self.MP.mesh_name +
                           '_results/' + self.MP.mod_name + '_interpolated')
        if interp_dir is not None:
            self.interp_dir = interp_dir
        if interp_p is None:
            interp_p = self.interp_p
        if max_new_procs_spawn is None:
            max_new_procs_spawn = 0
        if fs_type is None:
            fs_type = 'ned'

        if not mute:
            mpp('Interpolation export directory is: "' + self.interp_dir[:-1] +
                ', "files named as: "' + export_suffix + '"', post_dash=True,
                barrier=False)

        if df.MPI.rank(self.MP.mpi_cw) == 0:
            if not os.path.isdir(self.interp_dir):
                os.mkdir(self.interp_dir)
        else:
            pass

        if interp_mesh.find('line') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/lines'
        elif interp_mesh.find('slice') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/slices'
        elif interp_mesh.find('path') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/paths'
        else:
            mpp('MeshName Error: Interpolation target mesh name must '
                'end either with "line", "slice" or "path" so far!',
                post_dash=True)
            raise SystemExit

        if os.path.isfile(self.interp_mesh_dir + '/' +
                          interp_mesh + '.xml') is False:
            mpp('Warning! Interpolation mesh "' + str(interp_mesh) + '.xml" '
                'could not be found. Continuing ...')
            return

        mpp('...  interpolating E and H on ' + '"' + interp_mesh +
            '"  ...', pre_dash=False, barrier=False)
        df.MPI.barrier(df.MPI.comm_world)

        for j in range(self.n_times):
            if max_new_procs_spawn != 0:
                if len(self.icomms) >= max_new_procs_spawn:
                    mpp('Waiting for simultaneously running interpolation '
                        'tasks to be finished! If might be considered to '
                        'increase the value of "max_new_procs_spawn".')
                    self.synchronize()
                    self.rank = 0

            self.rank += 1
            if self.max_procs == 1 and max_new_procs_spawn == 0:
                use_root = True
            if use_root and max_new_procs_spawn == 0:
                self.rank -= 1

            if df.MPI.rank(self.MP.mpi_cw) == self.rank:
                print('Myproc: ', df.MPI.rank(self.MP.mpi_cw),
                      ' starts interp!')
                target_mesh = df.Mesh(self.MP.mpi_cs, self.interp_mesh_dir +
                                      '/' + interp_mesh + '.xml')
                V_target = df.VectorFunctionSpace(target_mesh, "CG",
                                                  self.interp_p)
                mesh = df.Mesh(self.MP.mpi_cs)
                hdf5 = df.HDF5File(self.MP.mpi_cs, self.MP.m_dir + '/_' +
                                   self.MP.file_format + '/' +
                                   self.MP.mesh_name +
                                   '.' + self.MP.file_format, "r")
                hdf5.read(mesh, '/mesh', False)
                V = df.FunctionSpace(mesh, 'N1curl', self.p)
                E, H = df.Function(V), df.Function(V)
                read_h5(self.MP.mpi_cs, E, self.MP.out_dir + '/E_' +
                        self.MP.mod_name + '.' + self.MP.file_format, j)
                read_h5(self.MP.mpi_cs, H, self.MP.out_dir + '/H_' +
                        self.MP.mod_name + '.' + self.MP.file_format, j)

                d_interp1 = df.interpolate(E, V_target)
                d_interp2 = df.interpolate(H, V_target)
                data1 = np.hstack((d_interp1.vector().get_local(
                         ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                         ).reshape(-1, 3)[0::3, :]))
                data2 = np.hstack((d_interp2.vector().get_local(
                         ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                         ).reshape(-1, 3)[0::3, :]))

                np.save(self.interp_dir + '/E_' + self.MP.mod_name +
                        '_on_' + interp_mesh +
                        '_' + str(j) + '.npy', data1)
                np.save(self.interp_dir + '/H_' + self.MP.mod_name +
                        '_on_' + interp_mesh +
                        '_' + str(j) + '.npy', data2)
                df.File(self.MP.mpi_cs, self.interp_dir + '/E_' +
                        self.MP.mod_name + '_on_' + interp_mesh +
                        '_' + str(j) + '.pvd') << d_interp1
                df.File(self.MP.mpi_cs, self.interp_dir + '/H_' +
                        self.MP.mod_name + '_on_' + interp_mesh +
                        '_' + str(j) + '.pvd') << d_interp2
            else:
                pass
            if self.rank == self.max_procs - 1 and max_new_procs_spawn == 0:
                self.rank = 0
                mpp('Waiting for all mpi processes to finish serial '
                    'interpolation tasks...\nNotice: For a greater amount of '
                    'interpolation tasks it might be considered to change the '
                    '"max_new_procs_spawn" flag. '
                    'NOTE! Using additional processes for interpolation '
                    'with **max_new_procs_spawn!=0** is not supported '
                    'right now! Continuing  ...')
            elif self.rank >= self.max_procs - 1 and max_new_procs_spawn != 0:
                spawn_rank = self.rank - self.max_procs + 1
                self.write_serial_calculation_parameters(rank=spawn_rank)
                if df.MPI.rank(self.MP.mpi_cw) == 0:
                    print('Spawning additional process to accelerate all '
                          'interpolation tasks in multithreading mode ...')
                self.icomms.append(
                        rs_multiple(os.path.dirname(ce.__file__) +
                                    '/post/serial_interpolation.py',
                                    ['-d'], [self.MP.para_dir],
                                    ['-q'], ['E_t', 'H_t'],
                                    ['-i'], [interp_mesh],
                                    ['-j'], [self.interp_mesh_dir],
                                    ['-o'], [export_suffix],
                                    ['-r'], [str(spawn_rank)],
                                    ['-c'], [str(j)]))


class E_fourier_transform_based(ApproachBase):

    def __init__(self, FS, MP):

        super(self.__class__, self).__init__()

        self.FS = FS
        self.MP = MP
        self.log_t_max = 0
        self.log_t_min = -6
        self.omegas = None
        self.times = np.logspace(-6, 0, 61)
        self.fd_mod_name = self.MP.mod_name

    def calc_frequency_solutions(self, init_only=False, **fem_kwargs):

        self.__dict__.update(fem_kwargs)
        self.n_times = len(self.times)
        if self.omegas is None:
            self.initalize_frequencies()

        if init_only:
            return

        for jj, omega in enumerate(self.omegas):
            M = MOD(self.fd_mod_name + '_' + str(omega), self.MP.mesh_name,
                    'E_t', p=self.FS.p, overwrite=True,
                    m_dir=self.MP.m_dir, r_dir=self.MP.r_dir)

            # define frequency and condcutivities
            M.MP.update_model_parameters(
                omega=omega,
                sigma_anom=self.MP.sigma_anom,
                sigma_ground=self.MP.sigma_ground[:len(self.MP.sigma_anom)],
                sigma_air=self.MP.sigma_air,
                procs_per_proc=1)

            # let FEniCS set up the variational formulations
            M.FE.build_var_form(pf_type='halfspace',
                                s_type=self.s_type,
                                start=self.start,
                                stop=self.stop,
                                closed_path=self.closed_path,
                                n_segs=self.n_segs)

            # call solver and convert H-fields
            M.solve_main_problem()

    def interpolate_frequency_solutions(self, interp_mesh):

        for omega in self.omegas[118:120]:

            M = MOD(self.fd_mod_name + '_' + str(omega), self.MP.mesh_name,
                    'E_t', p=self.FS.p, overwrite=False, load_existing=True,
                    m_dir=self.MP.m_dir, r_dir=self.MP.r_dir)
            linE = 'rec_pos_00_path'

            # interpolate fields on the observation line
            quantities = ['H_t', 'E_t']
            for q in quantities:
                M.IB.interpolate(q, linE)
            M.IB.synchronize()

    def reorder_results(self, interp_mesh, quantities='EH'):

        """
        Reorder frequency-domain data station-wise by importing all
        results and storing them in one file per station, containing the
        values for all chosen frequencies
        """

        EH = []
        if 'E' in quantities:
            EH.append('E_t')
        if 'H' in quantities:
            EH.append('H_t')

        print('HACK')
        self.fd_mod_name = 'TEST_p2'
        out_path = (self.MP.r_dir + '/E_FT/' + self.MP.mesh_name +
                    '_results/' + self.MP.mod_name + '_interpolated')
        if not os.path.isdir(out_path):
            rw(os.mkdir(out_path))
        for quantity in EH:
            tmp_name = (self.MP.r_dir + '/' + 'E_t' + '/' +
                        self.MP.mesh_name + '_results/' + self.fd_mod_name +
                        '_' + str(self.omegas[0]) + '_interpolated/' +
                        quantity + '_on_' + interp_mesh)
            n_rec_pos = len(np.load(tmp_name + '.npy'))
            coords = np.load(tmp_name + '.npy')[:, 3:].real
            all_frequencies = np.zeros((len(self.omegas), n_rec_pos, 6))
            if df.MPI.rank(self.MP.mpi_cw) == 0:
                for jj, omega in enumerate(self.omegas):
                    path = (self.MP.r_dir + '/' + 'E_t' + '/' +
                            self.MP.mesh_name + '_results/' +
                            self.fd_mod_name + '_' + str(omega) +
                            '_interpolated/')
                    import_name = (path + quantity + '_on_' + interp_mesh)
                    try:
                        tmp = np.load(import_name + '.npy')
                    except:
                        print(import_name + 'is missing')
                    all_frequencies[jj, :, :] = np.ascontiguousarray(
                                                     tmp[:, :3]).view('float')

                np.savetxt(out_path + '/' + 'omegas.dat', self.omegas)
                for kk, coord in enumerate(coords):
                    np.savetxt(out_path + '/' + quantity + '_at_' +
                               str(coord[0]) + '_' + str(coord[1]) + '_' +
                               str(coord[2]) + '.dat', np.concatenate((
                                   all_frequencies[:, kk, :])).reshape(-1, 6))

    def fht_hanstein_80(self, modus, fd_data):

        fht_coeff = [5.001828e-11, 8.000316e-11, 1.256402e-10, 2.009589e-10,
                     3.155941e-10, 5.047858e-10, 7.927364e-10, 1.267965e-09,
                     1.991264e-09, 3.184983e-09, 5.001829e-09, 8.000316e-09,
                     1.256403e-08, 2.009589e-08, 3.155941e-08, 5.047858e-08,
                     7.927363e-08, 1.267965e-07, 1.991263e-07, 3.184982e-07,
                     5.001826e-07, 8.000310e-07, 1.256401e-06, 2.009585e-06,
                     3.155931e-06, 5.047835e-06, 7.927307e-06, 1.267950e-05,
                     1.991228e-05, 3.184892e-05, 5.001599e-05, 7.999741e-05,
                     1.256258e-04, 2.009226e-04, 3.155029e-04, 5.045568e-04,
                     7.921612e-04, 1.266520e-03, 1.987636e-03, 3.175872e-03,
                     4.978954e-03, 7.942905e-03, 1.242000e-02, 1.973483e-02,
                     3.065536e-02, 4.821916e-02, 7.364350e-02, 1.128325e-01,
                     1.647498e-01, 2.348622e-01, 3.004906e-01, 3.372419e-01,
                     2.337323e-01, -8.647107e-02, -6.612999e-01, -8.142974e-01,
                     3.308580e-01, 1.402432e-00, -1.565116e-00, 8.359842e-01,
                     -3.059907e-01, 9.125030e-02, -2.463153e-02, 6.369876e-03,
                     -1.618934e-03, 4.085877e-04, -1.028279e-04, 2.584884e-05,
                     -6.494904e-06, 1.631643e-06, -4.098700e-07, 1.029566e-07,
                     -2.586174e-08, 6.496194e-09, -1.631772e-09, 4.098828e-10,
                     -1.029579e-10, 2.586184e-11, -6.496186e-12, 1.631761e-12]

        nc = len(fht_coeff)
        nc_mt = nc + self.n_times - 1
        td_data = np.zeros(self.n_times)
        dc_level = np.mean(fd_data[-3:])

        for nn in range(nc_mt):
            omega = self.omegas[nn]
            if modus == 1:                          # STEP ON
                fd_data[nn] = fd_data[nn] / complex(0., omega)
            elif modus == -1:                       # STEP OFF
                fd_data[nn] = (dc_level - fd_data[nn]) / complex(0., omega)
            else:
                pass                                # impulse response
            ita = np.max([1, nn + 1 - nc + 1])
            ite = np.min([self.n_times, nn])
            for it in range(ita-1, ite):            # Original FORTRAN version:
                # "DO 20 IT=ita,ite" which was run once for ita=ite=1.
                # "range(ita, ite)" is not stepped into when ita=ite
                td_data[it] = td_data[it] - (
                        fd_data[nn] * fht_coeff[nc - nn + it - 1])

        # Post Processing
        for it in range(self.n_times):
            aux = 1./self.times[it] * (np.sqrt(2./np.pi))     # * SQRT (2/pi)
            td_data[it] = td_data[it] * aux

        return(td_data)

    def initalize_frequencies(self):

        t0 = 10.**self.log_t_min       # First time channel
        tmax = 10.**self.log_t_max     # Last time channel
        nc = 80                        # Hahnstein filters
        self.q = 10. ** 0.1            # Factor for increasing the time channel
        t_shift = 1.653801536E+03      # Factor for shift of frequency

        nt = 1
        tmp = t0
        while True:
            tmp = tmp * self.q
            nt = nt + 1
            if(tmp > tmax):
                break

        nc_mt = nc + nt - 1
        omega = t_shift/t0
        self.omegas = []
        for iteration in range(nc_mt):
            omega = omega/self.q
            self.omegas.append(omega)


class E_rational_arnoldi(ApproachBase):

    """
    FE implementation of rational arnoldi (krylov subspace) method after
    Börner (2015).

    class internal functions:
    -------------------------

    - rational_arnoldi()
        runs the complete rational-arnoldi solution process as well as H-field
        conversion and export methods, FE modeling and time stepping related
        keyword arguments can be passed to this method.

    - build_var_form()
        set up variational formulation and assemble mass and curl-curl matrices
        as well right-hand side (source) vector

    - get_poles_from_table()
        define poles used in rational Arnoldi method

    - build_rational_arnoldi_basis()
        build Krylov subspace basis

    - expm_ra()
        calculate exponential

    - project_solution()
        calculate solution on subspace and project it back

    - export_fields()
        obtain magnetic fields, if specified, and save results to file

    - write_data()
        method for saving E and H at a single point in time to file

    - interpolate_all_td_result()
        load stored data from parallel computation and interpolate fields
        at all specified times
    """

    def __init__(self, FS, MP):

        """
        - v, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters

        super(self.__class__, self).__init__()

        self.v = df.TestFunction(FS.V)
        self.u = df.TrialFunction(FS.V)
        self.log_t_max = 0
        self.log_t_min = -6
        self.t_0 = 0.
        self.export_pvd = False
        self.convert_to_H = True
        self.export_cg = False
        self.export_nedelec = True
        self.times = np.logspace(-6, 0, 61)
        self.n_mult = 6
        self.n_poles = 3
        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v', 'DOM', 'u', 'dx', 'dx_0', 'FS', 'MP', 'mute', 'path']
        self.dx_0 = self.FS.DOM.dx_0

    def rational_arnoldi(self, var_form_only=False, **fem_kwargs):

        self.__dict__.update(fem_kwargs)
        self.n_times = len(self.times)
        self.n = int(12 * (self.n_mult) / self.n_poles)
        self.m = int(self.n_poles * self.n + 1)

        if var_form_only:
            return

        self.build_var_form()
        self.get_poles_from_table()
        self.xi = np.append(np.tile(self.poles[self.n_mult - 1, :], self.n),
                            np.array([np.inf]))
        self.xi_index = np.tile(np.arange(self.n_poles, dtype=int), self.n)
        self.xi_index = np.append(self.xi_index, self.n_poles + 1)

        mpp('...  building subspace basis ...')
        self.build_krylov_subspace_basis()

        mpp('...  projecting solution  ...')
        self.project_solution()

        mpp('...  exporting solution  ...')
        self.export_fields()

        mpp('...  Finished!', post_dash=True)

    def build_var_form(self):

        t0 = time.time()
        self.mu1 = df.Constant(1./self.MP.mu)
        if self.bc is not None:
            boundary = DirichletBoundary()
            bc = df.DirichletBC(self.FS.V, df.Constant((0, 0, 0)), boundary)
        else:
            bc = None

        if self.sigma_from_func:
            d_sig_func = build_DG_func(self.FS.S_DG,
                                       self.FS.DOM.domain_func,
                                       self.MP.sigma)
            C_form = df.inner(self.mu1 * df.curl(self.u),
                              df.curl(self.v)) * self.dx_0
            B_form = d_sig_func * df.inner(self.u, self.v) * self.dx_0
        else:
            dx = self.FS.DOM.dx
            sigma = add_sigma_vals_to_list(self.MP.sigma)
            C_form = df.inner(self.mu1 * df.curl(self.u),
                              df.curl(self.v)) * dx(0)
            B_form = sigma[0] * df.inner(self.u, self.v) * dx(0)
            for j in range(1, self.FS.DOM.n_domains):
                C_form += df.inner(self.mu1 * df.curl(self.u),
                                   df.curl(self.v)) * dx(j)
                B_form += sigma[j] * df.inner(self.u, self.v) * dx(j)

        self.B, self.C = df.PETScMatrix(), df.PETScMatrix()
        b, c = df.PETScVector(), df.PETScVector()
        RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), self.v) * self.dx_0
        df.assemble_system(B_form, RHS, bc, A_tensor=self.B, b_tensor=b)
        df.assemble_system(C_form, RHS, bc, A_tensor=self.C, b_tensor=c)

        Assembler = RHS_assembler(self, self.bc)
        Assembler.assemble()
        self.assembly_time = time.time() - t0
        mpp('  -  time elapsed (s):  -->  ', self.assembly_time,
            pre_dash=False)

        # calculate inital condition
        mpp('...  calculating initial condition  ...', pre_dash=False)
        bb = df.Function(self.FS.V)
        solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), self.B, "mumps")
        solver.solve(bb.vector(), Assembler.b)
        self.b = bb.vector()

    def get_poles_from_table(self):

        """
        Define *poles* for ration arnoldi method.
        """

        if self.n_poles == 1:
            self.poles = -np.array([[5.66e4],
                                    [1.13e5],
                                    [1.57e5],
                                    [2.14e5],
                                    [2.69e5],
                                    [3.14e5]])
        elif self.n_poles == 2:
            self.poles = -np.array([[1.26e4, 7.88e5],
                                    [2.52e4, 2.56e6],
                                    [3.32e4, 3.88e6],
                                    [4.59e4, 5.00e6],
                                    [5.40e4, 6.30e6],
                                    [6.35e4, 7.58e6]])
        elif self.n_poles == 3:
            self.poles = -np.array([[7e3, 1.70e5, 9.99e5],
                                    [8.36e3, 2.41e5, 5.23e6],
                                    [1.27e4, 3.76e5, 6.95e6],
                                    [2.06e4, 4.23e5, 1.27e7],
                                    [2.60e4, 5.34e5, 1.38e7],
                                    [2.60e4, 6.73e5, 1.96e7]])
        else:
            mpp('Fatal Error! Only "1, 2, 3" are valid values for "n_poles" '
                'right now. Aborting!')
            raise SystemExit

    def build_krylov_subspace_basis(self):

        """
        buildRCfast(C, M, b, xi, xiindex, m, inner_product)
        returns V, H, HH.
        """

        t0 = time.time()
        self.V = [df.Function(self.FS.V) for j in range(self.m + 1)]
        for j in range(self.m + 1):
            self.V[j] = self.V[j].vector()
        w = df.Function(self.FS.V)
        w = w.vector()
        H = np.zeros((self.m + 1, self.m))
        self.V[0] = self.b * 1./np.sqrt(self.b.inner(self.B*self.b))

        for j in range(self.m):
            if self.xi_index[j] == 0:
                if j < self.n_poles:
                    D1 = df.PETScMatrix(self.B.mat() - self.C.mat() *
                                        (1. / self.xi[j]))
                    solver1 = df.PETScLUSolver(D1, "mumps")
                solver1.solve(w, self.C * self.V[j])
            elif self.xi_index[j] == 1:
                if j < self.n_poles:
                    D2 = df.PETScMatrix(self.B.mat() - self.C.mat() *
                                        (1. / self.xi[j]))
                    solver2 = df.PETScLUSolver(D2, "mumps")
                solver2.solve(w, self.C * self.V[j])
            elif self.xi_index[j] == 2:
                if j < self.n_poles:
                    D3 = df.PETScMatrix(self.B.mat() - self.C.mat() *
                                        (1. / self.xi[j]))
                    solver3 = df.PETScLUSolver(D3, "mumps")
                solver3.solve(w, self.C * self.V[j])
            elif self.xi_index[j] == 3:
                if j < self.n_poles:
                    D4 = df.PETScMatrix(self.B.mat() - self.C.mat() *
                                        (1. / self.xi[j]))
                    solver4 = df.PETScLUSolver(D4, "mumps")
                solver4.solve(w, self.C * self.V[j])

            H[:j, j] = 0.
            for reo in range(2):
                for k in range(j+1):
                    h = self.V[k].inner(self.B*w)
                    H[k, j] += h
                    w = w - self.V[k] * h
            H[j + 1, j] = np.sqrt(w.inner(self.B*w))
            w *= 1./H[j + 1, j]
            self.V[j+1][:] = w
            mpp('...  ' + str(j) + ' done  ...', pre_dash=False)
        self.FS.solution_time = time.time() - t0
        mpp('  -  build time for krylov subspace basis (s):  ' +
            str(self.FS.solution_time))

    def expm_ra(self, A, E):

        """
        calculate matrix exponential with eigenvalues and eigenvectors
        """

        DD, VV = la.eig(A, E)
        DD = np.diag(DD.real)
        return(la.solve(VV.T, np.dot(VV, np.diag(np.exp(np.diag(DD)))).T))

    def project_solution(self):

        prod = [self.C * self.V[j] for j in range(self.m)]
        Mm = np.eye(self.m)
        Km = np.zeros((self.m, self.m))
        for i in range(self.m):
            for j in range(self.m):
                Km[i, j] = self.V[i].inner(prod[j])

        bBb = self.b.inner(self.B*self.b)
        proj = np.sqrt(bBb) * np.eye(self.m, 1)
        Um = np.zeros((self.m, self.n_times))
        if df.MPI.rank(self.MP.mpi_cw) == 0:
            for k in range(self.n_times):
                dt = self.times[k] - self.t_0
                Um[:, k] = np.dot(self.expm_ra(-dt * Km, Mm), proj).ravel()
        else:
            pass
        Um = self.MP.mpi_cw.bcast(Um, root=0)

        self.E = [df.Function(self.FS.V) for k in range(self.n_times)]
        for j in range(self.m):
            for k in range(self.n_times):
                self.E[k].vector()[:] += self.V[j].get_local() * Um[j, k]

    def export_fields(self):

        self.E_cg, self.H_cg = None, None
        if self.export_cg:
            ('Warning! CG field export is not implemented yet. Skipping  ...')
            # self.E_cg = [df.Function(self.FS.V) for k in range(self.n_times)]
            # for k in range(self.n_times):
            #     self.E_cg[k] = df.interpolate(self.E[k], self.FS.V)

        self.H = None
        if self.convert_to_H:
            mpp('...  deriving H-fields from E  ...', pre_dash=False)

            uu, vv = df.TrialFunction(self.FS.V), df.TestFunction(self.FS.V)
            LHS = df.inner(uu, vv) * self.dx_0
            RHS = df.inner(df.Constant(("0.0", "0.0", "0.0")), vv) * self.dx_0
            A, b = df.PETScMatrix(), df.PETScVector()
            df.assemble_system(LHS, RHS, self.bc, A_tensor=A, b_tensor=b)
            A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)

            H_solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
            self.H = [df.Function(self.FS.V) for k in range(self.n_times)]

            for k in range(self.n_times):
                RHS = df.inner(self.mu1 * df.curl(self.E[k]), vv) * self.dx_0
                b = df.assemble(RHS)
                H_solver.solve(self.H[k].vector(), b)
                mpp('...  converting E at time ' + str(k) + '  ...',
                    pre_dash=False)

        if self.export_nedelec:
            self.write_data()

        if self.export_pvd:
            outfile_E = df.File(self.MP.out_dir + '/E_' +
                                self.MP.mod_name + '.pvd')

            if self.E_cg is None:
                self.E_cg = []
                for k in range(self.n_times):
                    self.E_cg.append(df.interpolate(self.E[k], self.FS.V_cg))

            for k in range(self.n_times):
                self.E_cg[k].rename('E_field', 'label')
                outfile_E << (self.E_cg[k], self.times[k])

            if self.H is not None:
                outfile_H = df.File(self.MP.out_dir + '/H_' +
                                    self.MP.mod_name + '.pvd')
                if self.H_cg is None:
                    self.H_cg = []
                    for k in range(self.n_times):
                        self.H_cg.append(df.interpolate(self.H[k],
                                                        self.FS.V_cg))
                for k in range(self.n_times):
                    self.H_cg[k].rename('H_field', 'label')
                    outfile_H << (self.H_cg[k], self.times[k])

    def write_data(self):

        out_E = self.MP.out_dir + '/E_' + self.MP.mod_name + '.' + \
            self.MP.file_format
        if self.H is not None:
            out_H = self.MP.out_dir + '/H_' + self.MP.mod_name + '.' + \
                self.MP.file_format

        if self.MP.file_format in 'xml':
            mpp('Warning! writing xml output of time domain data currently '
                'not supported! Only last time step exported!  ...')
            df.File(out_E + '.' + self.MP.file_format) << self.E[-1]
            if self.H is not None:
                df.File(out_H + '.' + self.MP.file_format) << self.H[-1]

        elif self.MP.file_format in 'h5':
            self.E_file = write_h5(self.MP.mpi_cw, self.E[0], out_E,
                                   counter=0, close=False)
            if self.H is not None:
                self.H_file = write_h5(self.MP.mpi_cw, self.H[0], out_H,
                                       counter=0, close=False)

            for k in range(1, self.n_times - 1):
                write_h5(self.MP.mpi_cw, self.E[k], out_E,
                         counter=k, new=False, f=self.E_file, close=False)
                if self.H is not None:
                    write_h5(self.MP.mpi_cw, self.H[k], out_H,
                             counter=k, new=False, f=self.H_file, close=False)

            write_h5(self.MP.mpi_cw, self.E[k], out_E, f=self.E_file,
                     new=False, counter=self.n_times - 1, close=True)
            if self.H is not None:
                write_h5(self.MP.mpi_cw, self.H[k], out_H, f=self.H_file,
                         new=False, counter=self.n_times - 1, close=True)

    def interpolate_all_td_results(self, interp_mesh, mute=True, interp_p=None,
                                   max_new_procs_spawn=None, use_root=False,
                                   fs_type=None, dg_interp=False,
                                   export_suffix=None, interp_dir=None):

        """
        Customized interpolation function.

        Required arguments:
        -------------------

        - interp_mesh, type str
            name of the mesh to interpolate on.

        Keyword arguments:
        ------------------

        - mute = False, type bool
            set True, if info-interpolation messages should not be printed.

        - interp_p = 2, type int
            polyonimal order of interpolation mesh *FunctionSpaces*

        - max_new_procs_spawn = None, type int
            specify how many additional processes should be spawned in
            addition to the number of procs already used in the mpirun call,
            usually it is not necessary to spawn additional processes for
            significant acceleration of all interpolation tasks.
            Notice! This might lead to random synchronization issues.

        - use_root = False, type bool
            Use also the root process for serial interpolation tasks. Setting
            this flag to **True** has no effect if **max_new_procs_spawn**
            is not **0**!

        - export_suffix = None, type str
            specify a custom export name of the interpolated data if desired.

        - interp_dir, type str
            specify, if a custom interpolation directory is desired, otherwise,
            the defualt interpolation directory is located in the corresponding
            results directory

        - dg:interpolation = None, type bool
            set True or False to overwrite *self.dg_interpolation* for
            manually enabling discontinuous or continuous interpolation

        Example:
        --------

        >>> ### to do
        """

        if export_suffix is None:
            export_suffix = '_on_' + interp_mesh

        self.rank = 0
        self.interp_p = 1
        self.max_procs = df.MPI.size(self.MP.mpi_cw)
        self.interp_dir = (self.MP.r_dir + '/' + self.MP.approach + '/' +
                           self.MP.mesh_name +
                           '_results/' + self.MP.mod_name + '_interpolated')
        if interp_dir is not None:
            self.interp_dir = interp_dir
        if interp_p is None:
            interp_p = self.interp_p
        if max_new_procs_spawn is None:
            max_new_procs_spawn = 0
        if fs_type is None:
            fs_type = 'ned'

        if not mute:
            mpp('Interpolation export directory is: "' + self.interp_dir[:-1] +
                ', "files named as: "' + export_suffix + '"', post_dash=True,
                barrier=False)

        if df.MPI.rank(self.MP.mpi_cw) == 0:
            if not os.path.isdir(self.interp_dir):
                os.mkdir(self.interp_dir)
        else:
            pass

        if interp_mesh.find('line') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/lines'
        elif interp_mesh.find('slice') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/slices'
        elif interp_mesh.find('path') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/paths'
        else:
            mpp('MeshName Error: Interpolation target mesh name must '
                'end either with "line", "slice" or "path" so far!',
                post_dash=True)
            raise SystemExit

        if os.path.isfile(self.interp_mesh_dir + '/' +
                          interp_mesh + '.xml') is False:
            mpp('Warning! Interpolation mesh "' + str(interp_mesh) + '.xml" '
                'could not be found. Continuing ...')
            return

        mpp('...  interpolating E and H on ' + '"' + interp_mesh +
            '"  ...', pre_dash=False, barrier=False)
        df.MPI.barrier(df.MPI.comm_world)

        for j in range(self.n_times):
            if max_new_procs_spawn != 0:
                if len(self.icomms) >= max_new_procs_spawn:
                    mpp('Waiting for simultaneously running interpolation '
                        'tasks to be finished! If might be considered to '
                        'increase the value of "max_new_procs_spawn".')
                    self.synchronize()
                    self.rank = 0

            self.rank += 1
            if self.max_procs == 1 and max_new_procs_spawn == 0:
                use_root = True
            if use_root and max_new_procs_spawn == 0:
                self.rank -= 1

            if df.MPI.rank(self.MP.mpi_cw) == self.rank:
                print('Myproc: ', df.MPI.rank(self.MP.mpi_cw),
                      ' starts interp!')
                target_mesh = df.Mesh(self.MP.mpi_cs, self.interp_mesh_dir +
                                      '/' + interp_mesh + '.xml')
                V_target = df.VectorFunctionSpace(target_mesh, "CG",
                                                  self.interp_p)
                mesh = df.Mesh(self.MP.mpi_cs)
                hdf5 = df.HDF5File(self.MP.mpi_cs, self.MP.m_dir + '/_' +
                                   self.MP.file_format + '/' +
                                   self.MP.mesh_name +
                                   '.' + self.MP.file_format, "r")
                hdf5.read(mesh, '/mesh', False)
                V = df.FunctionSpace(mesh, 'N1curl', self.p)
                E, H = df.Function(V), df.Function(V)
                read_h5(self.MP.mpi_cs, E, self.MP.out_dir + '/E_' +
                        self.MP.mod_name + '.' + self.MP.file_format, j)
                read_h5(self.MP.mpi_cs, H, self.MP.out_dir + '/H_' +
                        self.MP.mod_name + '.' + self.MP.file_format, j)

                d_interp1 = df.interpolate(E, V_target)
                d_interp2 = df.interpolate(H, V_target)
                data1 = np.hstack((d_interp1.vector().get_local(
                         ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                         ).reshape(-1, 3)[0::3, :]))
                data2 = np.hstack((d_interp2.vector().get_local(
                         ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                         ).reshape(-1, 3)[0::3, :]))

                np.save(self.interp_dir + '/E_' + self.MP.mod_name +
                        '_on_' + interp_mesh +
                        '_' + str(j) + '.npy', data1)
                np.save(self.interp_dir + '/H_' + self.MP.mod_name +
                        '_on_' + interp_mesh +
                        '_' + str(j) + '.npy', data2)
                df.File(self.MP.mpi_cs, self.interp_dir + '/E_' +
                        self.MP.mod_name + '_on_' + interp_mesh +
                        '_' + str(j) + '.pvd') << d_interp1
                df.File(self.MP.mpi_cs, self.interp_dir + '/H_' +
                        self.MP.mod_name + '_on_' + interp_mesh +
                        '_' + str(j) + '.pvd') << d_interp2
            else:
                pass
            if self.rank == self.max_procs - 1 and max_new_procs_spawn == 0:
                self.rank = 0
                mpp('Waiting for all mpi processes to finish serial '
                    'interpolation tasks...\nNotice: For a greater amount of '
                    'interpolation tasks it might be considered to change the '
                    '"max_new_procs_spawn" flag. '
                    'NOTE! Using additional processes for interpolation '
                    'with **max_new_procs_spawn!=0** is not supported '
                    'right now! Continuing  ...')
            elif self.rank >= self.max_procs - 1 and max_new_procs_spawn != 0:
                spawn_rank = self.rank - self.max_procs + 1
                self.write_serial_calculation_parameters(rank=spawn_rank)
                if df.MPI.rank(self.MP.mpi_cw) == 0:
                    print('Spawning additional process to accelerate all '
                          'interpolation tasks in multithreading mode ...')
                self.icomms.append(
                        rs_multiple(os.path.dirname(ce.__file__) +
                                    '/post/serial_interpolation.py',
                                    ['-d'], [self.MP.para_dir],
                                    ['-q'], ['E_t', 'H_t'],
                                    ['-i'], [interp_mesh],
                                    ['-j'], [self.interp_mesh_dir],
                                    ['-o'], [export_suffix],
                                    ['-r'], [str(spawn_rank)],
                                    ['-c'], [str(j)]))
