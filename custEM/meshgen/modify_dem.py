# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import numpy as np
import pygimli as pg
import os
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import valley
from custEM.misc.synthetic_definitions import cone_bathy
from custEM.misc.synthetic_definitions import double_cone_bathy


def loadASC(ascfile):

    """
    Load ASC (DEM matrix with location header) file.

    Required arguments:
    -------------------

    - all arguments are documented in the class-**__init__**.
    """

    with open(ascfile) as fid:
        header = {}
        sp = []
        nheader = 0
        while len(sp) < 3:
            sp = fid.readline().split()
            if len(sp) < 3:
                header[sp[0]] = float(sp[1])
                nheader += 1

    z = np.flipud(np.genfromtxt(ascfile, skip_header=nheader))
    if 'NODATA_value' in header:
        z[z == header['NODATA_value']] = -1.

    dx = header.pop('cellsize', 1.0)  # just a guess
    x = np.arange(header['ncols']) * dx + header['xllcorner']
    y = np.arange(header['nrows']) * dx + header['yllcorner']

    print('DEM original extent (x min, x max, y min, y max):')
    print(np.min(x), np.max(x), np.min(y), np.max(y))

    return(x, y, z)


def find_n_missing(x, y, x_extent, y_extent, cellsize):

    a = np.arange(x_extent[0], x_extent[1] + 1., cellsize)
    b = np.arange(y_extent[0], y_extent[1] + 1., cellsize)

    x_off, y_off = [0, 0], [0, 0]
    for j, aa in enumerate(a):
        if np.min(x) == aa:
            x_off[0] = j
        if np.max(x) == aa:
            x_off[1] = len(a) - j - 1
    for j, bb in enumerate(b):
        if np.min(y) == bb:
            y_off[0] = j
        if np.max(y) == bb:
            y_off[1] = len(b) - j - 1
    return(x_off, y_off)


# load asc

x, y, z = loadASC('../../../modeling/etang_de_thau/data/etang_de_thau.asc')

min_depth = -5.

for j in range(z.shape[0]):
    for k in range(z.shape[1]):
#        if z[j, k] < 0. and z[j, k] > -1.:
        if z[j, k] < 0.:
            z[j, k] = min_depth
        elif z[j, k] >= 0. and z[j, k] < 1.:
            z[j, k] = 1.


cellsize = 5.
x_extent = [747997.5 - 3000., 747997.5 + 17000.]
y_extent = [6254997.5 - 5500., 6254997.5 + 14500.]

x_off, y_off = find_n_missing(x, y, x_extent, y_extent, cellsize)
z = np.vstack((np.zeros((y_off[0], len(x))), z))
z = np.vstack((z, np.zeros((y_off[1], len(x)))))
z = np.hstack((np.zeros((len(y) + y_off[0] + y_off[1], x_off[0])), z))
z = np.hstack((z, np.zeros((len(y) + y_off[0] + y_off[1], x_off[1]))))

z2 = np.zeros(z.shape)
z2[:, :] = z[:, :]

dist = np.linspace(-1e4, 1e4, 4001)
#for j in range(1, z.shape[0] - 1):
#    for k in range(1, z.shape[1] - 1):
#        counter = 0
#        if z[j, k] < 1e-2:
#            if z[j - 1, k - 1] > 1e-2:
#                counter += 1
#            if z[j - 1, k] > 1e-2:
#                counter += 1
#            if z[j - 1, k + 1] > 1e-2:
#                counter += 1
#            if z[j, k - 1] > 1e-2:
#                counter += 1
#            if z[j, k + 1] > 1e-2:
#                counter += 1
#            if z[j + 1, k - 1] > 1e-2:
#                counter += 1
#            if z[j + 1, k] > 1e-2:
#                counter += 1
#            if z[j + 1, k + 1] > 1e-2:
#                counter += 1
#            if counter >= 4:
#                z2[j, k] = 0.1
#        else:
#            if z[j - 1, k - 1] < 1e-2:
#                counter += 1
#            if z[j - 1, k] < 1e-2:
#                counter += 1
#            if z[j - 1, k + 1] < 1e-2:
#                counter += 1
#            if z[j, k - 1] < 1e-2:
#                counter += 1
#            if z[j, k + 1] < 1e-2:
#                counter += 1
#            if z[j + 1, k - 1] < 1e-2:
#                counter += 1
#            if z[j + 1, k] < 1e-2:
#                counter += 1
#            if z[j + 1, k + 1] < 1e-2:
#                counter += 1
#            if counter >= 4:
#                z2[j, k] = -1.
#
#print('first step done')

#z[:, :] = z2[:, :]

nn = 25
for j in range(nn, z2.shape[0] - nn):
    for k in range(nn, z2.shape[1] - nn):
        if z[j, k] < 1e-2:
            if len(z[j-nn:j+nn+1, k-nn:k+nn+1][
                    z[j-nn:j+nn+1, k-nn:k+nn+1] < 0.]) < int((2*nn)**2/(2.)):
                z2[j, k] = 1.
        if z[j, k] > -1.5:
            if len(z[j-nn:j+nn+1, k-nn:k+nn+1][
                    z[j-nn:j+nn+1, k-nn:k+nn+1] > 0.]) < int((2*nn)**2/(2.)):
                z2[j, k] = min_depth
    print(j)

print('second step done')

print(np.min(z2), np.max(z2))


mu.write_synth_topo_to_asc('../../../modeling/etang_de_thau/data',
                           'etang_de_thau_w5m',
                           np.flipud(z2[100:-100, 100:-100]), spacing=5.,
                           x_min=747997.5 + 500., y_min=6254997.5 + 500.)


print('...  asc written  ...')

# write xyz for testing purposes

#a = np.arange(x_extent[0], x_extent[1] + 1., cellsize)
#b = np.arange(y_extent[0], y_extent[1] + 1., cellsize)
#c = z.reshape(-1, 1)
#
#A = np.zeros((len(z)**2, 3))
#dummy = 0
#for j in range(len(a)):
#    for k in range(len(b)):
#        A[dummy, 0] = a[j]
#        A[dummy, 1] = b[k]
#        A[dummy, 2] = c[dummy]
#        dummy += 1
#
#mu.write_synth_topo_to_xyz('../../../modeling/etang_de_thau',
#                           'etang_de_thau_finaL_edit', A)
