# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 15:14:20 2016

@author: Anonymous
"""

import os
from custEM.wrap.p223f.p223f_wrapper import Leroi
import numpy as np
import subprocess as sp
from custEM.post import make_subfigure_box
from custEM.post import Plot
import matplotlib.pyplot as plt


"""
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
# # # # #                         deprecated !!!                      # # # # #
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
# # # # #    visualization script for references obtained with p223f  # # # # #
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #

Executing script for P223f wrapper, might be reconsidered in future
"""

if __name__ == '__main__':
    plt.ioff()
    omega = 1e3 * np.pi * 2
    omega = '%08.2f' % omega
    axis = 'y'
    cc = 1
    ax_suffix2 = '_SS'
    ax_suffix = '_S'

    # special_line = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_2000_line_'
    # special_line2 = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_2000_line_'
    special_line = 'y0_-5000.0_y1_5000.0_x_0.0_z_0.0_n_2000_line_'
    special_line2 = 'y0_-5000.0_y1_5000.0_x_0.0_z_0.0_n_2000_line_'
    # special_line = 'default_small_line_'
    E_flg = True
    H_flg = True
    EH_flg = 'EH'
    pp1 = '1'
    pp2 = '2'

    mesh1 = '3_layer_Cloop_box'
    mesh2 = mesh1
    mesh3 = '3_layer_line_large'
    mod1 = 'O_' + omega + '_p' + pp1
    mod2 = 'O_' + omega + '_p' + pp1
    mod3 = 'O_' + omega + '_p' + pp1

    P = Plot(mod=mod1,
             mesh=mesh1,
             t_or_s='Total')

    P.load_default_slice_data(interp_meshes='small')
    P.import_slice_data('default_small_slice_z', mod=mod2, t_or_s='Secondary',
                        suffix=ax_suffix)

    P.plot_slice_data(slice_normal='z', fs=12, E=E_flg, H=False,
                      clim=[1e-8, 1e-2], label='Total')
    plt.savefig(mod1 + '_E_total.png', transparent=True, bbox_inches='tight')
#    P.plot_slice_data(mod=mod2, slice_normal='z', fs=12, E=E_flg, H=False,
#                      clim=[1e-8, 1e-2], label='Secondary', suffix=ax_suffix)
#    plt.savefig(mod1 + '_E_secondary.pdf')
#    P.plot_slice_data(slice_normal='z', fs=12, E=False, H=H_flg,
#                      clim=[1e-6, 1e-2], label='Total')
#    plt.savefig(mod1 + '_H_total.pdf')
#    P.plot_slice_data(mod=mod2, slice_normal='z', fs=12, E=False, H=H_flg,
#                      clim=[1e-6, 1e-2], label='Secondary', suffix=ax_suffix)
#    plt.savefig(mod1 + '_H_secondary.pdf')

#    P.plot_slice_errors(
#            mod1 + '_E_t_on_default_small_slice_z', clim=[1e0, 1e2],
#            data2=mod2 + '_E_t_on_default_small_slice_z' + ax_suffix)
#    plt.savefig(mod1 + '_E_errors.png')
#    P.plot_slice_errors(
#            mod1 + '_H_t_on_default_small_slice_z', clim=[1e0, 1e2],
#            data2=mod2 + '_H_t_on_default_small_slice_z' + ax_suffix)
#    plt.savefig(mod1 + '_H_errors.png')

    plt.ion()
    plt.show()

#############################################################################

# res = [1e2]
# thick = []
# cond = [00.]
# plates = [[2, 165., 0., -310., 500., 500., 135., 135., 0.]]
#
# I = Leroi(P.r_coords, P.t_coords, res, thick, cond, plates)
# I.update_modeling_parameters(FD=True,
#                              components='XYZ',
#                              source='Loop',
#                              f_fact=1.,
#                              cell_size=50.)
# I.write_leroi_input_file()
#
# sp.call('Leroi.exe')
# I.read_leroi_output_file()
#
# ax = make_subfigure_box(var='H')

# f = 1
# ax[0, 0].plot(I.r_coords[:, cc], np.abs(I.inphases[:, f, 0]), 'g')
# ax[0, 1].plot(I.r_coords[:, cc], np.abs(I.quadratures[:, f, 0]), 'g')
# ax[1, 0].plot(I.r_coords[:, cc], np.abs(I.inphases[:, f, 1]), 'b')
# ax[1, 1].plot(I.r_coords[:, cc], np.abs(I.quadratures[:, f, 1]), 'b')
# ax[2, 0].plot(I.r_coords[:, cc], np.abs(I.inphases[:, f, 2]), 'r')
# ax[2, 1].plot(I.r_coords[:, cc], np.abs(I.quadratures[:, f, 2]), 'r')

# os.chdir('../../results/E_vector/Secondary/halfspace_loop_plate_45_results/'
#         'p2_o_' + str(omega) + '_s_10.0_interpolated')
# Real = np.load('H_t_r_on_default_small_line_x.npy')[:, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_default_small_line_x.npy')[:, :3]  * 4 * np.pi * 1e2

# Real = np.load('H_t_r_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#       :, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#          :, :3] * 4 * np.pi * 1e2
#
# Real = np.load('H_t_r_on_x0_-2000_x1_2000_y_800_z_0.0_ne_400_line_x.npy')[
#       :, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_x0_-2000_x1_2000_y_800_z_0.0_ne_400_line_x.npy')[
#       	:, :3] * 4 * np.pi * 1e2

# nn = 801
# ax[0, 0].plot(np.linspace(-2e3, 2e3, nn), np.abs(Real[:, 0]), 'y')
# ax[0, 1].plot(np.linspace(-2e3, 2e3, nn), np.abs(Imag[:, 0]), 'y')
# ax[1, 0].plot(np.linspace(-2e3, 2e3, nn), np.abs(Real[:, 1]), 'c')
# ax[1, 1].plot(np.linspace(-2e3, 2e3, nn), np.abs(Imag[:, 1]), 'c')
# ax[2, 0].plot(np.linspace(-2e3, 2e3, nn), np.abs(Real[:, 2]), 'm')
# ax[2, 1].plot(np.linspace(-2e3, 2e3, nn), np.abs(Imag[:, 2]), 'm')

##########################################################

# os.chdir('../../../Secondary/halfspace_loop_plate_45_results/'
#          'p1_interpolated')
# Real = np.load('H_t_r_on_default_small_line_x.npy')[:, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_default_small_line_x.npy')[:, :3]  * 4 * np.pi * 1e2
#
# Real = np.load('H_t_r_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#         :, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#        :, :3] * 4 * np.pi * 1e2
#
# Real = np.load('H_t_r_on_x0_-2000_x1_2000_y_800_z_0.0_ne_400_line_x.npy')[
#        :, :3]
# Imag = np.load('H_t_i_on_x0_-2000_x1_2000_y_800_z_0.0_ne_400_line_x.npy')[
#        :, :3]
#
# ax[0, 0].plot(I.r_coords[:, coord_comp], np.abs(Real[:, 0]), 'k')
# ax[0, 1].plot(I.r_coords[:, coord_comp], np.abs(Imag[:, 0]), 'k')
# ax[1, 0].plot(I.r_coords[:, coord_comp], np.abs(Real[:, 1]), 'k')
# ax[1, 1].plot(I.r_coords[:, coord_comp], np.abs(Imag[:, 1]), 'k')
# ax[2, 0].plot(I.r_coords[:, coord_comp], np.abs(Real[:, 2]), 'k')
# ax[2, 1].plot(I.r_coords[:, coord_comp], np.abs(Imag[:, 2]), 'k')
#
# ########################################################################### #

# os.chdir('../../../Total/halfspace_loop_plate_45_results/'
#          'p2_o_1e1_s_1e0_interpolated')
# Real = np.load('H_t_r_on_default_small_line_x.npy')[:, :3] * 4 * np.pi * 1e2
# Imag = np.load('H_t_i_on_default_small_line_x.npy')[:, :3] * 4 * np.pi * 1e2

# Real = np.load('H_t_r_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#    :, :3]
# Imag = np.load('H_t_i_on_x0_-2000_x1_2000_y_-200_z_0.0_ne_400_line_x.npy')[
#    :, :3]

# ax[0, 0].plot(np.linspace(-2e3, 2e3, 801), np.abs(Real[:, 0]), 'k:')
# ax[0, 1].plot(np.linspace(-2e3, 2e3, 801), np.abs(Imag[:, 0]), 'k:')
# ax[1, 0].plot(np.linspace(-2e3, 2e3, 801), np.abs(Real[:, 1]), 'k:')
# ax[1, 1].plot(np.linspace(-2e3, 2e3, 801), np.abs(Imag[:, 1]), 'k:')
# ax[2, 0].plot(np.linspace(-2e3, 2e3, 801), np.abs(Real[:, 2]), 'k:')
# ax[2, 1].plot(np.linspace(-2e3, 2e3, 801), np.abs(Imag[:, 2]), 'k:')

# diff_r = I.inphases[:, f, :] - Real
# diff_i = I.quadratures[:, f, :] - Imag
#
# f, ax3 = plt.subplots(1, 1, sharex=True, sharey=False)
# ax3.plot(diff_r[:,2])
# ax3.plot(diff_i[:,2])
# ax3.set_xscale('log')
