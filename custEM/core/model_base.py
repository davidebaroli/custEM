# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import custEM as ce
from custEM.misc import mpi_print as mpp
from custEM.misc import check_approach_and_file_format
import numpy as np
import sys


class MOD:

    """
    Main class for 3D CSEM modeling using FEniCS. After an instance of this
    mainclass is created, everything will be stored within its environment.
    Instances within the mainclass 'MOD' are:

        - FS: :class:`custEM.fem.fem_base.FunctionSpaces`
        - FS.PF: :class:`custEM.fem.primary_fields.PrimaryField`
        - DOM: :class:`custEM.fem.fem_base.Domains`
        - MP: :class:`custEM.fem.fem_base.ModelParameters`
        - FE (depending on approach one of the following):
            * :class:`custEM.fem.approaches.A_Phi_nodal`
            * :class:`custEM.fem.approaches.A_Phi_mixed`
            * :class:`custEM.fem.approaches.E_vector`
            * :class:`custEM.fem.approaches.H_vector`
        - Solver: :class:`custEM.core.solvers.Solver`
        - PP (post-processing or pre-processing, respectively):
            * :class:`custEM.core.post_proc.PostProcessing`
            * :class:`custEM.core.pre_proc.PreProcessing`

    Class internal functions:
    -------------------------

    - solve_main_problem()
        function to solve a weak formulation, defined in the **FE**
        instance, either by manually assembling (call *RHS_assembler**) for
        total field calculations or by using the automated FEniCS way for
        secondary field calculations. Note that iterative solvers are
        theoretically considered but not working properly yet!

    - init_main_parameters()
        set a few dolfin parameters: so far, no need for customization

    - init_default_MOD_parameters()
        initialize default parameters which can be overwritten by
        specifying keyword arguments when calling the **MOD** instance

    - init_instances()
        initalize all required 'sub'-classes of the **MOD** class

    Example:
    --------

    >>> first_custEM_MOD = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
    >>>                        test_mode=True)

    """

    def __init__(self, mod_name, mesh_name, approach,
                 log_level=50, **main_kwargs):

        """
        initializes Mesh, Functionspaces, ModelParameters,
        FE kernel, Solvers, Post- or Pre-Processing kernel.

        Required arguments:
        -------------------

        - mod_name, type str
            name of the model

        - mesh_name, type str
            name of input mesh without suffix

        - approach, type str
            either **E_t**, **H_t**, **Am_t**, **An_t** or
            **E_s**, **H_s**, **Am_s**, **An_s**. Note that **H_t** and
            **An_t** are theoretically considered but not working yet.

        Keyword arguments:
        ------------------

        - p = 1, type int
            polynomial order, alternatively change to order **2** or **3**

        - file_format = 'h5', type str
            h5 for parallel mesh read-in support, alternatively change value
            to **'xml'** for dolfin-xml format

        - test_mode = False, type bool
            set **True** for test mode with a 6 * (20 x 20 x 20) cells
            UnitCubeMesh (dimensions of 20 x 20 x 20 km). If **True**, a
            specified **mesh** is always ignored

        - overwrite = False, type bool
            set **True** to overwrite a model with existing name

        - load_exisiting = False, type bool
            set **True** to load results form an existing model. Readable
            data are stored in the "PP" (preprocessing) instance

        - file_format = None, type str
            file format - either **h5** or **xml** - of meshes and data,
            will be set automatically but can also be set manually,
            if **None**, parallel-supporting **h5** is chosen as default,
            if *h5* support is not available, **xml** is chosen alternatively

        - field_selection = 'all', type str
            specify a number of selected quantities to speed up and avoid
            unnecessary imports. The string can contain "E_t", "E_s", "H_t",
            "H_s", "A_t" and "A_s" to specify the desired quantities

        - fs_type = 'None', type str
            set to **Ned/ned** or **CG/cg** to restrict import of functions
            on either Nedelec or CG Funcionspaces (insofar they exist)

        - dg_interpolation = False, type bool
            set **True** if fields should be interpolation from Nedelec-Spaces
            on discontinuous Lagrange-VectorFunctionSpaces during
            post-processing/interpolation

        - mute = False, type bool
            set to **True**, if unimportant prints should be muted

        - mpi_cw, mpi_cs, type MPI communicators
            MPI comm. attributes for internal usage, FEniCS version dependent

        - r_dir = '../results', type str
            results directory, can be changed here or as general default
            variable **'your_out_dir'** in the file *misc/paths.txt*

        - m_dir = '../meshes', type str
            mesh directory, same possibilites as for **r_dir**

        - para_dir = m_dir + '/para', type str
            directory of mesh parameter files

        - out_dir = r_dir + '/' + approach, type str
            redefine to **your_output_directory** for custom output directory

        - out_name = 'E' or 'H' or 'An' or 'Am' + '_' + mod_name, type str
            redefine to **your_output_name** for custom output name

        Example:
        --------

        >>> first_custEM_MOD = MOD('Test_1', 'halfspace_mesh', 'An_s', p=2)

        """

        self.mod_name = mod_name
        self.mesh_name = mesh_name
        self.approach = approach
        self.init_main_parameters(log_level)
        self.init_default_model_parameters()

        for key in main_kwargs:
            if key not in self.__dict__:
                mpp('Warning! Unknown keyword argument for MOD set:', key)
                mpp("... Let's stop before something unexpected happens ...",
                    pre_dash=False)
                raise SystemExit
        self.__dict__.update(main_kwargs)
        if self.test_mode is not False:
            self.out_name = 'TEST_MODE_' + self.out_name
            mpp('Notice: Prefix "TEST_MODE_" added to the given outname which'
                ' was given as: ' + self.out_name + ' !')

        self.out_dir = (self.r_dir + '/' + self.approach +
                        '/' + self.mesh_name + '_results')

        if 'para_dir' not in main_kwargs:
            self.para_dir = self.m_dir + '/para'

        if df.MPI.size(self.mpi_cw) > 1:
            if df.MPI.rank(self.mpi_cw) == 0:
                ce.misc.make_directories(self.r_dir, self.m_dir, self.approach)
                ce.misc.check_if_model_exists(self.mod_name, self.mod_name,
                                              self.out_dir, self.out_name,
                                              self.overwrite,
                                              self.load_existing, self.mute)
                print('...  initializing instances  ...')
        else:
            ce.misc.make_directories(self.r_dir, self.m_dir, self.approach)
            ce.misc.check_if_model_exists(self.mod_name, self.mod_name,
                                          self.out_dir, self.out_name,
                                          self.overwrite,
                                          self.load_existing, self.mute)
            print('...  initializing instances  ...')
        self.init_instances()

    def solve_main_problem(self, solver='MUMPS', bc=None, sym=True,
                           out_of_core=False, convert=True,
                           method='tfqmr', pc='ilu', **kwargs):

        """
        assembles and solves linear systems of euqations

        Keyword arguments:
        ------------------

        - solver = 'MUMPS', type int
            choose direct solver MUMPS, no alternatives so far

        - bc = None, type str
            default boundary conditions (BC) are implicit Neumann bc.
            Alternatively, **'ZeroDirichlet'** or short **'ZD'** can be choosen

        - sym = 1, 2 or True, type int or bool
            so far, all implemented systems of equations are symmetric,
            this parameter may be set to **False** in future FE formulations
            1 stands for positive definite symmetric matrices.
            2 stands for gerneral structurally symmetric matrices.

        - out_of_core = False, type bool
            set **True** to enable *'out_of_core'* option of direct solver
            MUMPS if main memory is exceeded. WARNING! may be REALY slow

        - convert = True, type bool
            automatically converts solution function *U* to real and
            imarginary parts of electric and magnetic fields and saves 'xml'
            and 'pvd' files. If set to **False**, functions *convert_results*
            and *export_all_results* in the postprocessing instance *PP* can
            be called manually with custom conversion and export parameters

        Example:
        --------

        example for **solve_main_problem** function with custom usage
        of *convert_results* and *export_all_results*

        >>> MOD.solve_main_problem(bc='ZD', convert=False)
        >>> MOD.PP.convert_results(export=False)
        >>> MOD.PP.export_all_results(only_pvd=False)

        """

        if bc is None:
            bc = self.FE.bc

        if '_t' in self.approach or self.approach == 'DC':
            Assembler = ce.fem.RHS_assembler(self.FE, bc)
            Assembler.assemble()

            if solver == 'MUMPS':
                self.Solver.solve_system_mumps(Assembler.A, Assembler.b,
                                               sym=sym,
                                               out_of_core=out_of_core)

            # elif solver == 'default':
            #     self.Solver.solve_system_default(Assembler.A, Assembler.b)
            # elif solver == 'iter':
            #     self.Solver.solve_system_iter(Assembler.A, Assembler.b,
            #                                   method=method, pc=pc)

            else:
                mpp('Fatal error! For performance and implementation reasons, '
                    'only direct solver MUMPS is supported currently!')
                raise SystemExit
        else:
            AA, bb = df.PETScMatrix(), df.PETScVector()
            if bc is None:
                A, b = df.assemble_system(self.FE.L, self.FE.R,
                                          A_tensor=AA, b_tensor=bb)
            elif bc is 'ZeroDirichlet' or 'ZD':
                self.FS.add_dirichlet_bc()
                A, b = df.assemble_system(self.FE.L, self.FE.R, self.FS.bc,
                                          A_tensor=AA, b_tensor=bb)
            else:
                mpp('FatalError, Choose either None (defualt) or "ZD" or '
                    '"ZeroDirichlet" as boundary condition. Further bcs may '
                    'be available in future (e.g. ID conditions).')

            mpp('  -  system matrix size:  -->  ' + str(A.mat().getSize()[0]),
                pre_dash=False)

            if self.FS.anom_flag is False:
                mpp('Warning: No anomalies set for secondary field approach. '
                    'FE solution is skipped')
                self.FS.solution_time = 0.
                self.FS.U = [self.FS.U]
            else:
                if solver == 'MUMPS':
                    self.Solver.solve_system_mumps(A, [b], sym=sym,
                                                   out_of_core=out_of_core)

                #  elif solver == 'iter':
                #    self.Solver.solve_var_form_iter(self.FE.L, self.FE.R,
                #                                    self.FS.U, bc=bc, sym=sym,
                #                                    method=method, pc=pc)
                #  elif solver == 'default':
                #    self.Solver.solve_var_form_default(self.FE.L, self.FE.R,
                #                                       self.FS.U, bc=bc)

                else:
                    mpp('Fatal error! For performance and implementation '
                        'reasons, only MUMPS solver is supported currently!')
                    raise SystemExit

        if convert is True:
            self.PP.convert_results(**kwargs)

    def init_main_parameters(self, log_level):

        """
        Set some dolfin parameters. Might contain more dolfin parameters with
        custom choices in future.
        """

        df.parameters['form_compiler']['optimize'] = True
        df.parameters['form_compiler']['cpp_optimize'] = True

        import logging
        logging.getLogger('FFC').setLevel(log_level)
        logging.getLogger('UFL').setLevel(log_level)
        df.set_log_level(log_level)
        # CRITICAL  = 50, errors that may lead to data corruption and suchlike
        # ERROR     = 40, things that go boom
        # WARNING   = 30, things that may go boom later
        # INFO      = 20, information of general interest
        # PROGRESS  = 16, what's happening (broadly)
        # TRACE     = 13, what's happening (in detail)
        # DBG       = 10  sundry
        df.parameters["allow_extrapolation"] = True
        np.set_printoptions(edgeitems=5,
                            threshold=11)
        sys.stdout.flush()

    def init_default_model_parameters(self):

        """
        Initalizes default parameters of the MOD class which will be updated
        if keyword arguments are set when calling the MOD instance.
        """

        self.r_dir, self.m_dir = ce.misc.read_paths(
                os.path.dirname(ce.__file__) + '/misc/paths.txt')

        self.mpi_cw = df.MPI.comm_world
        self.mpi_cs = df.MPI.comm_self
        self.out_name = self.mod_name
        self.test_mode = False
        self.file_format = None
        self.ned_kind = '1st'
        self.p = 1
        self.overwrite = False
        self.load_existing = False
        self.field_selection = 'all'
        self.fs_type = 'None'
        self.mute = False
        self.self_mode = False
        self.para_dir = self.m_dir + '/para'
        self.export_domains = True
        self.dg_interpolation = False
        self.mumps_debug = False

    def init_instances(self):

        """
        Initalizes instances within MOD (**FS, MP, PP, FE, Solver, I**),
        for more information read main documentation of the submodules,
        respectively.
        """

        if 'DC' in self.approach.upper() and self.approach != 'DC':
            self.approch = 'DC'

        self.file_format = check_approach_and_file_format(
                self.approach, self.file_format,
                os.path.dirname(ce.__file__) + '/misc/')

        self.MP = ce.fem.ModelParameters([self.mod_name, self.mesh_name,
                                          self.approach,
                                          self.file_format, self.m_dir,
                                          self.r_dir, self.para_dir,
                                          self.out_dir, self.out_name,
                                          self.mute, self.mpi_cw,
                                          self.mpi_cs], self.test_mode)
        self.FS = ce.fem.FunctionSpaces(self.MP, self.p, self.test_mode,
                                        self.self_mode, self.ned_kind,
                                        self.dg_interpolation,
                                        self.load_existing)

        if self.load_existing:
            self.PP = ce.core.PreProcessing(
                    self.FS, self.MP, self.load_existing,
                    field_selection=self.field_selection,
                    self_mode=self.self_mode, fs_type=self.fs_type)
        else:
            # DC field FE modeling
            if self.approach == 'DC':
                self.FE = ce.fem.fem_utils.DC(self.FS, self.MP)
                self.Solver = ce.core.Solver(self.FS, self.FE,
                                             mumps_debug=self.mumps_debug)
            # Time domain FE approaches
            elif self.approach in ['E_IE', 'E_RA', 'E_FT']:
                td = ce.fem.time_domain_approaches
                if self.approach == 'E_IE':
                    self.FE = td.E_implicit_euler(self.FS, self.MP)
                elif self.approach == 'E_RA':
                    self.FE = td.E_rational_arnoldi(self.FS, self.MP)
                elif self.approach == 'E_FT':
                    self.FE = td.E_fourier_transform_based(self.FS, self.MP)

            # Frequency domain FE approaches
            else:
                fd = ce.fem.frequency_domain_approaches
                if 'E' in self.approach:
                    self.FE = fd.E_vector(self.FS, self.MP)
                elif 'H' in self.approach:
                    self.FE = fd.H_vector(self.FS, self.MP)
                elif 'Am' in self.approach:
                    self.FE = fd.A_V_mixed(self.FS, self.MP)
                elif 'An' in self.approach:
                    self.FE = fd.A_V_nodal(self.FS, self.MP)
                elif'Fm' in self.approach:
                    self.FE = fd.F_U_mixed(self.FS, self.MP)
                elif'Fn' in self.approach:  # dummy, not implemented yet
                    self.FE = fd.F_U_nodal(self.FS, self.MP)

                self.Solver = ce.core.Solver(self.FS, self.FE,
                                             mumps_debug=self.mumps_debug)
            self.PP = ce.core.PostProcessing(self.FE, self.export_domains,
                                             self.fs_type)
        self.IB = ce.post.InterpolationBase(self.PP, self.dg_interpolation)
