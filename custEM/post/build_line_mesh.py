# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import sys
import numpy as np
import getopt
from mpi4py import MPI

"""
This file is always called in serial from the root process of an mpi run,
otherwise a crash would occur due to a missing 'MPI-Parent' for synchronization
"""


class Line_mesh:

    """
    Mesh calculation class for generating straight or crooked (e.g.,
    topography), line meshes.

    Class internal functions:
    -------------------------

    - calc_line_meshes()
        generate the 1D mesh for interpolation
    """

    def __init__(self, para_dir, axes, out_name):

        """
        Required arguments:
        -------------------

        - para_dir, type str
            parameter directory including a temporary "interp_dict.json" file,
            same as the **para_dir** as set in the **MOD** instance

        - axes, type str
            a permutation or combination of 'xyz', for which
            line-direction a 'line'-mesh should be generated

        - out_name, None or type str
            could be specified manually, if None, a default name will be used
        """

        import json
        with open(para_dir + '/interp_dict_0.json', 'r') as thefile:
            self.__dict__.update(json.load(thefile))
        if ''.join(sorted(axes)) in ['x', 'y', 'z', 'xy', 'xz', 'yz', 'xyz']:
            self.build_line_meshes(axes, out_name, para_dir, self.on_topo)
        else:
            print('Warning!!!, automated custom line mesh generation not '
                  'implemented in custEM yet!')

    def build_line_meshes(self, axes, out_name, para_dir, on_topo=False):

        """
        generate the 2D mesh for interpolation

        Required arguments:
        -------------------

        - it is referred to the **__init__**

        Keyword arguments:
        ------------------

        - on_topo=False, type bool
            set to True if the Z-values of a 'x-line' or 'y-line'-mesh should
            be adjusted to a certain topography, the flag is set in the
            **Interpolator** instance of the **MOD** class
        """

        if 'x' in axes:
            if out_name is None:
                out_name = 'x0_' + str(self.x0) + '_x1_' + str(self.x1) +\
                           '_y_' + str(self.y) + '_z_' + str(self.z) + \
                           '_n_' + str(self.n_segs)
                if on_topo:
                    out_name += '_on_topo'

            if os.path.isfile(str(self.m_dir) + '/lines/' + str(out_name) +
                              '_line_x.xml'):
                print('Desired "x_line_mesh" already exists, continuing ... ')
            else:
                print('1D "x_line_mesh" is calculated and '
                      'saved in global "mesh_dir"/lines ... ')

                slice_mesh = df.BoxMesh(df.Point(self.x0,
                                                 -1e4, -1e4),
                                        df.Point(self.x1, 1e4,
                                                 1e4), self.n_segs, 2, 2)
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')

                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                x1 = df.AutoSubDomain(lambda x: x[1] < -1e4 + self.tol)
                x1.mark(cf, 1)
                x_l = df.SubMesh(boundary_mesh, cf, 1)
                x_l.coordinates()[:, 1] += 1e4 + self.y

                boundary_mesh2 = df.BoundaryMesh(x_l, 'exterior')
                cf2 = df.MeshFunction('size_t', boundary_mesh2,
                                      boundary_mesh2.topology().dim(), 0)
                x2 = df.AutoSubDomain(lambda x: x[2] < -1e4 + self.tol)
                x2.mark(cf2, 2)
                line_x = df.SubMesh(boundary_mesh2, cf2, 2)
                line_x.coordinates()[:, 2] += 1e4

                if on_topo:
                    import custEM.misc.synthetic_definitions as sd
                    try:
                        topo_func = getattr(sd, str(self.topo))
                        line_x.coordinates()[:, 2] = topo_func(
                            line_x.coordinates()[:, 0],
                            line_x.coordinates()[:, 1])
                    except (AttributeError, NameError):
                        from custEM.meshgen.dem_interpolator import DEM
                        tmp = self.get_topo_parameters()
                        dem = DEM(self.t_dir + '/' + self.mesh_name +
                                  '_surf.xyz',
                                  centering=bool(tmp[1]),
                                  easting_shift=None, northing_shift=None)
                        line_x.coordinates()[:, 2] = dem(
                                line_x.coordinates()[:, 0],
                                line_x.coordinates()[:, 1], rotation=tmp[4])
                if self.on_water_surface:
                    line_x.coordinates()[:, 2][line_x.coordinates(
                            )[:, 2] < 0.] = 0.
                line_x.coordinates()[:, 2] += self.z
                df.File(str(self.m_dir) + '/lines/' + str(out_name) +
                        '_line_x.xml') << line_x

        if 'y' in axes:
            if out_name is None:
                out_name = 'y0_' + str(self.y0) + '_y1_' + str(self.y1) +\
                           '_x_' + str(self.x) + '_z_' + str(self.z) + \
                           '_n_' + str(self.n_segs)
                if on_topo:
                    out_name += '_on_topo'

            if os.path.isfile(str(self.m_dir) + '/lines/' + str(out_name) +
                              '_line_y.xml'):
                print('Desired "y_line_mesh" already exists, continuing ... ')
            else:
                print('1D "y_line_mesh" is calculated and '
                      'saved in global "mesh_dir"/lines ... ')

                slice_mesh = df.BoxMesh(df.Point(-1e4, self.y0,
                                                 -1e4),
                                        df.Point(1e4, self.y1,
                                                 1e4), 2, self.n_segs, 2)
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')

                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                y1 = df.AutoSubDomain(lambda x: x[0] < -1e4 + self.tol)
                y1.mark(cf, 1)
                y_l = df.SubMesh(boundary_mesh, cf, 1)
                y_l.coordinates()[:, 2] += 1e4 + self.x

                boundary_mesh2 = df.BoundaryMesh(y_l, 'exterior')
                cf2 = df.MeshFunction('size_t', boundary_mesh2,
                                      boundary_mesh2.topology().dim(), 0)
                y2 = df.AutoSubDomain(lambda x: x[0] < -1e4 + self.tol)
                y2.mark(cf2, 2)
                line_y = df.SubMesh(boundary_mesh2, cf2, 2)
                line_y.coordinates()[:, 0] += 1e4

                if on_topo:
                    import custEM.misc.synthetic_definitions as sd
                    try:
                        topo_func = getattr(sd, str(self.topo))
                        line_y.coordinates()[:, 2] = topo_func(
                            line_y.coordinates()[:, 0],
                            line_y.coordinates()[:, 1])
                    except (AttributeError, NameError):
                        from custEM.meshgen.dem_interpolator import DEM
                        tmp = self.get_topo_parameters()
                        dem = DEM(self.t_dir + '/' + self.mesh_name +
                                  '_surf.xyz',
                                  centering=bool(tmp[1]),
                                  easting_shift=None, northing_shift=None)
                        line_y.coordinates()[:, 2] = dem(
                                line_y.coordinates()[:, 0],
                                line_y.coordinates()[:, 1], rotation=tmp[4])
                if self.on_water_surface:
                    line_y.coordinates()[:, 2][line_y.coordinates(
                            )[:, 2] < 0.] = 0.
                line_y.coordinates()[:, 2] += self.z
                df.File(str(self.m_dir) + '/lines/' + str(out_name) +
                        '_line_y.xml') << line_y

        if 'z' in axes:
            if out_name is None:
                out_name = 'z0_' + str(self.z0) + '_z1_' + str(self.z1) +\
                           '_x_' + str(self.x) + '_y_' + str(self.y) + \
                           '_n_' + str(self.n_segs)

            if os.path.isfile(str(self.m_dir) + '/lines/' + str(out_name) +
                              '_line_z.xml'):
                print('Desired "z_line_mesh" already exists, continuing ... ')
            else:
                print('1D "z_line_mesh" is calculated and '
                      'saved in global "mesh_dir"/lines ... ')

                slice_mesh = df.BoxMesh(df.Point(-1e4, -1e4,
                                                 self.z0),
                                        df.Point(1e4, 1e4,
                                                 self.z1), 2, 2, self.n_segs)
                boundary_mesh = df.BoundaryMesh(slice_mesh, 'exterior')

                cf = df.MeshFunction('size_t', boundary_mesh,
                                     boundary_mesh.topology().dim(), 0)
                x1 = df.AutoSubDomain(lambda x: x[0] < -1e4 + self.tol)
                x1.mark(cf, 1)
                z_l = df.SubMesh(boundary_mesh, cf, 1)
                z_l.coordinates()[:, 0] += 1e4 + self.x
                boundary_mesh2 = df.BoundaryMesh(z_l, 'exterior')
                cf2 = df.MeshFunction('size_t', boundary_mesh2,
                                      boundary_mesh2.topology().dim(), 0)
                x2 = df.AutoSubDomain(lambda x: x[1] < -1e4 + self.tol)
                x2.mark(cf2, 2)
                line_z = df.SubMesh(boundary_mesh2, cf2, 2)
                line_z.coordinates()[:, 1] += 1e4 + self.y
                df.File(str(self.m_dir) + '/lines/' + str(out_name) +
                        '_line_z.xml') << line_z

    def get_topo_parameters(self):

        tmp = np.genfromtxt(para_dir + '/' + self.mesh_name +
                            '_parameters.txt', dtype=None,
                            encoding=None)
        dummy = []
        for j, elem in enumerate(tmp):
            dummy.append(str(elem))

        tmp = dummy
        if tmp[1] == 'False':
            tmp[1] = False
        else:
            tmp[1] = True
        if tmp[2] == 'None':
            tmp[2] = None
        else:
            tmp[2] = float(tmp[2])
        if tmp[3] == 'None':
            tmp[3] = None
        else:
            tmp[3] = float(tmp[3])
        if tmp[4] == 'None':
            tmp[4] = None
        else:
            tmp[4] = float(tmp[4])
        return(tmp)


if __name__ == "__main__":

    """
    read the three arguments for the **Line_mesh_calc** class, call the latter
    and synchronize with the 'Parent process' afterwards
    """

    myopts, args = getopt.getopt(sys.argv[1:], "d:a:o:")
    for o, a in myopts:
        if o == '-d':
            para_dir = str(a)
        elif o == '-a':
            axes = str(a)
        elif o == '-o':
            out_name = str(a)
        else:
            print("Usage: %s -d   parameter directory     -a   axes     "
                  "-o     output name"
                  % sys.argv[0])

    if out_name == 'None':
        out_name = None
    parent = MPI.Comm.Get_parent()
    Line_mesh(para_dir, axes, out_name)
    parent.Barrier()
    parent.Disconnect()
