# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import numpy as np


class PHC:

    def __init__(self, config_file):

        if type(config_file) is str:
            import json
            with open(config_file, 'r') as thefile:
                self.__dict__.update(json.load(thefile))

    def calc_reference(self, coords, EH_flag, max_procs=4):

        self.path = np.array([[0., -0.5, 0.],
                              [0., 0.5, 0.]])
        print('... calculating ' + self.s_type +
              ' source reference solution ...')
        from comet import pyhed as ph
        if self.s_type == 'hed':
            source = ph.loop.buildDipole(self.origin, length=self.length,
                                         angle=np.deg2rad(self.azimuth))
        elif self.s_type == 'line':
            source = ph.loop.buildLine(self.start, self.stop,
                                       num_segs=self.n_segs)
        elif self.s_type == 'loop_c':
            source = ph.loop.buildCircle(self.R, P=self.origin,
                                         num_segs=self.n_segs)
        elif self.s_type == 'loop_r':
            S_1 = [self.start[0], self.start[1], 0.0]
            S_2 = [self.stop[0], self.start[1], 0.0]
            S_3 = [self.stop[0], self.stop[1], 0.0]
            S_4 = [self.start[0], self.stop[1], 0.0]
            source = ph.loop.buildRectangle(np.array((S_1, S_2, S_3, S_4)),
                                            num_segs=self.n_segs)
        elif self.s_type == 'path':
            source = ph.loop.buildLoop(np.array(self.path),
                                       num_segs=self.n_segs,
                                       grounded=not self.closed_path)
        source.setLoopMesh(coords)
        source.config.rho = 1. / np.array(self.sigma[1:1+self.n_layers])
        source.config.d = self.thick
        source.config.f = self.omega / (2.0 * np.pi)
        source.config.current = self.J[0]
        source.config.ftype = EH_flag
        data = source.calculate(interpolate=False, maxCPUCount=max_procs)
        return(data.T)
