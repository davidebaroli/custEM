# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                     visualization script                    # # # # #
# ########################################################################### #

import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import Plot
from matplotlib import colors as cl

# define matplotlib parameters and model, mesh and key names
plt.ioff()
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 7, 9
clrs = cl.CSS4_COLORS

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

linE1 = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_200_line_x'
linE2 = 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_200_line_x'

# ####################  specify which data to be plotted  ################### #

EH = 'H'
mesh1 = 'example_3_mesh_p1_loop'
mesh2 = 'example_3_mesh_p2_loop'
mod1 = 'example_3_p1_anisoE'
mod2 = 'example_3_p2_anisoE'
mod3 = 'example_3_p1_anisoH'
mod4 = 'example_3_p2_anisoH'
linE = linE2
new2 = True

t1, t2, t3 = 'T1', 'T2', 'T3'
s1, s2, s3, s4, s5 = 'S1', 'S2', 'S3', 'S4', 'S5'
t11, t22, t33 = 'T11', 'T22', 'T33'
s11, s22, s33, s44, s55 = 'S11', 'S22', 'S33', 'S44', 'S55'

# ############################## import data ################################ #

P = Plot(mod=mod1, mesh=mesh1, approach='E_t', fig_size=(8, 6),
         r_dir='./results', s_dir='./plots')
for p in range(1, 3):
    for suf in ['H']:
        for suf2 in ['', '_loop']:
            if suf2 == '':
                cl = colors[0]
            else:
                cl = colors[3]

            mesh = 'example_3_mesh_p' + str(p) + suf2
            mod = 'example_3_p' + str(p) + '_aniso' + suf

            P.import_line_data(linE, mesh=mesh, mod=mod, approach='E_t', key=t11)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='Am_t', key=t22)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='An_t', key=t33)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='E_s', key=s11)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='H_s', key=s22)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='Am_s', key=s33)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='An_s', key=s44)
            P.import_line_data(linE, mesh=mesh, mod=mod, approach='Tm_s', key=s55)

#            P.plot_line_data(key=t11, EH=EH, label='E_t_p1', lw=1., ls='-',
#                             new=new2, color=cl, ylim=[1e-10, 1e-2])
#            P.plot_line_data(key=t22, EH=EH, new=False,
#                             lw=0.5, ls=':', color=cl)
#
#            P.plot_line_data(key=t33, EH=EH, new=False, label='An_t_p1',
#                             lw=0.5, ls=':', color=cl)
#
#            if suf2 == '_loop' and p == 1:
#                P.plot_line_data(key=t33, EH=EH, new=False, label='$\mathbf{A}-V^n_t$ p1',
#                             lw=0.3, ls='-', color=colors[2])
#            elif suf2 == '_loop' and p == 2:
#                P.plot_line_data(key=t33, EH=EH, new=False, label='$\mathbf{A}-V^n_t$ p2',
#                             lw=0.3, ls='-', color=colors[8])
#            else:
#                P.plot_line_data(key=t33, EH=EH, new=False, label='An_t_p1',
#                                 lw=0.5, ls=':', color=cl)
#
#            P.plot_line_data(key=s11, EH=EH, new=False,
#                             lw=0.5, ls=':', color=cl)
#            P.plot_line_data(key=s22, EH=EH, new=False,
#                             lw=0.5, ls=':', color=cl)
#            P.plot_line_data(key=s33, EH=EH, new=False,
#                             lw=0.5, ls=':', color=cl)
#            if suf2 == '_loop' and suf == 'H' and p == 2:
#                P.plot_line_data(key=s44, EH=EH, new=False, label='$\mathbf{A}-V^n_s$ p2, PF: $\mathbf{H}$',
#                                 lw=0.4, ls='-', color=colors[1])
#            else:
#                pass
#                P.plot_line_data(key=s44, EH=EH, new=False, label='An_s_p1',
#                                 lw=0.5, ls=':', color=cl)
#            P.plot_line_data(key=s55, EH=EH, new=False,
#                             lw=0.5, ls=':', color=cl)
#            plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
#                                wspace=0.1, hspace=0.14)
#            new2 = False

#plt.legend().remove()
##plt.savefig('./plots/' + EH + '_tf_p1.pdf', bbox_inches='tight', pad_inches=0)
#plt.savefig('./plots/' + 'H2' + '_all.pdf', bbox_inches='tight', pad_inches=0)

# ######################### plot secondary fields ########################### #

# # # # # # # # # # # # # #           p1          # # # # # # # # # # # # # # #

            lw2 = 0.5
            lw3 = 0.5
            ls2 = '-'
            ls3 = '-'
            if suf2 == '':
                cl1 = colors[0]
                cl2 = colors[0]
                cl3 = colors[0]
            else:
                cl1 = colors[3]
                cl2 = colors[3]
                cl3 = colors[3]
                lw2 = 1.

            if suf == 'H' and suf2 == '_loop' and p == 1:
                cl1 = colors[8]
                cl2 = colors[2]
                cl3 = colors[5]
                ls2 = ':'
            elif suf == 'H' and suf2 == '_loop' and p == 2:
                cl3 = colors[1]
                lw3 = 0.5
                ls3 = ':'

            P.plot_line_data(key=s11, EH=EH,
                             ylim=[1e-10, 1e-2],
                             new=new2, lw=lw2, ls=ls2, color=cl1, sf=True)
            P.plot_line_data(key=s22, EH=EH, new=False, label=r'$\mathbf{A}-V^n_s$ p2',
                             lw=lw2, ls=ls2, color=cl2, sf=True)
            P.plot_line_data(key=s33, EH=EH, new=False, label=r'$\mathbf{E}$_s \& $\mathbf{A}-V^m_s$ p1',
                             lw=lw2, ls=ls2, color=cl1, sf=True)
            P.plot_line_data(key=s44, EH=EH, new=False, label=r'$\mathbf{A}-V^n_s$ p1',
                             lw=lw3, ls=ls3, color=cl3, sf=True)
            P.plot_line_data(key=s55, EH=EH, new=False, label=r'$\mathbf{H}_s$ \& $\mathbf{F}-U^m_s$ p1',
                             lw=lw2, ls=ls2, color=cl2, sf=True)
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
                                wspace=0.1, hspace=0.14)
            new2 = False

#plt.legend().remove()
plt.savefig('./plots/sf_H2_all.pdf', transparent=True, bbox_inches='tight')

# # # # # # # # # # # # # #           p2          # # # # # # # # # # # # # # #

#plt.savefig('./plots/sf_p2.pdf', transparent=True, bbox_inches='tight')


plt.ion()   # uncomment to enable showing the plot
plt.show()  # uncomment to enable showing the plot
