.. _imphist:

**summary of main features before 09.08.2018**

* documentation added:

    - documentation of main page, installation notes, etc. added
    - 3 test scripts provided (*tests* directory)
    - 4 examples provided for reproducibility (*examples* directory)
    - 3 tutorials for meshing, FME computation and visualization available
    - automatically updated online documentation on readthedocs.io

* six *approaches* with arbitrary anisotropic conductivities supported for
  finite element modeling of CSEM problems in frequency domain:
  
    - Total and Secondary E-field: *E_t* and *E_s*
    - Secondary H-field: *H_s*
    - Total and Secondary A-Phi on mixed elements: *Am_t* and *Am_s*
    - Total and Secondary A-Phi on nodal elements: *An_s*
      (only xyz-directed anisotropy supported until now, extension possible)
    - automated post-processing
      
* meshgen tools robustly implemented for land-based or airborne CSEM setups:

    - full-space, half-space or layered-earth tetrahedral meshes
    - topography can be applied to any horizontal surface or interface
    - transmitters or observation lines on surface shifted automatically to
      follow topography if required
    - domain markers applied automatically and imported for computation from
      **MOD** instance.
    - arbitrary anomalies within layers or reaching the surface, several
      regularly shaped bodies pre-defined
    - enclosing tetrahedral mesh to increase domain size and minimize boundary
      artifacts

* interpolation on lines or slices:

    - lines or slices parallel to coordinate axes
    - crooked "horizontal" lines or slices following topography
    - multi-threading applied to speed up interpolation
    
* visualization of line or slice data:

    - automated line or slice plots of all electric or magnetic field components
    - misfit calculations and visualization
    - flexible generation of plots with patches combining various data 