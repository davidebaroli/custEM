# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import example_2_loop_tx
from custEM.meshgen import meshgen_utils as mu

## #########################       coarse mesh        ######################## #

# create world
M = BlankWorld(name='example_2_mesh_coarse',
               m_dir='./meshes',
               inner_area='box',                    # add observation area A
               inner_area_size=[2.1e3, 2.1e3],      # of 4.2 x 4.2 km size
               inner_area_cell_size=1000.,
               preserve_edges=True)

# add transmitter: crooked loop transmitter, defined in *synthetic_definitions*
M.build_surface(insert_paths=[example_2_loop_tx()], closed_path=True)

# build layered earth mesh and extend 2D surface mesh to 3D world
M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])

# extend the computational domain (reduce boundary artifacts for low freqs.)
M.there_is_always_a_bigger_world(x_fact=1e2, y_fact=1e2, z_fact=1e2)

# call TetGen
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)

# #########################    intermediate mesh     ######################## #

M = BlankWorld(name='example_2_mesh_inter',
               m_dir='./meshes',
               inner_area='box',
               inner_area_size=[2.1e3, 2.1e3],
               inner_area_cell_size=100.,
               preserve_edges=True)

M.build_surface(insert_paths=[example_2_loop_tx()], closed_path=True)
M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])
M.there_is_always_a_bigger_world(x_fact=1e2, y_fact=1e2, z_fact=1e2)
M.call_tetgen(tet_param='-pq1.3aA', export_vtk=True)

# #########################        fine mesh         ######################## #

M = BlankWorld(name='example_2_mesh_fine',
               m_dir='./meshes',
               inner_area='box',
               inner_area_size=[2.1e3, 2.1e3],
               inner_area_cell_size=50.,
               preserve_edges=True)

M.build_surface(insert_paths=[example_2_loop_tx()], closed_path=True)

M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])
M.there_is_always_a_bigger_world(x_fact=1e2, y_fact=1e2, z_fact=1e2)

M.call_tetgen(tet_param='-pq1.2aA', export_vtk=True)

# #########################         new mesh         ######################## #
#
#import numpy as np
#grid = np.zeros((121, 3))
#a = 0
#for x in np.linspace(-1.8e3, 1.8e3, 11):
#    for y in np.linspace(-1.8e3, 1.8e3, 11):
#        grid[a, 0] = x + np.random.rand() * 100.
#        grid[a, 1] = y + np.random.rand() * 100.
#        a += 1
#
#refined_rx = mu.refine_rx(grid, 10., 30.)
#del refined_rx[60]
#
## create world
#M = BlankWorld(name='example_2_mesh_p1',
#               m_dir='./meshes',
#               preserve_edges=True)
#
## add transmitter: crooked loop transmitter, defined in *synthetic_definitions*
#M.build_surface(insert_loops=[example_2_loop_tx()],
#                insert_paths=refined_rx)
#
## build layered earth mesh and extend 2D surface mesh to 3D world
#M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])
#
## extend the computational domain (reduce boundary artifacts for low freqs.)
#M.there_is_always_a_bigger_world(x_fact=1e1, y_fact=1e1, z_fact=1e1)
#
## call TetGen
#M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)
