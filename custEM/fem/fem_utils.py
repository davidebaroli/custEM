# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
from custEM.misc import mpi_print as mpp

"""
Definiton of base class initializing fem-realted parameters and defining
fem-related utility functions.
"""


class ApproachBase(object):

    """
    Initialize FE variables, which are needed for all approaches
    """

    def __init__(self):

        self.bc = None
        self.pf_type = 'halfspace'
        self.pf_name = None
        self.s_type = 'line'
        self.path = None
        self.closed_path = False
        self.pvd_flag = False
        self.pf_EH_flag = 'E'

        self.origin = [0.0, 0.0, 0.0]
        self.r = 100.0
        self.start = [-100, 0.0, 0.0]
        self.stop = [100.0, 0.0, 0.0]
        self.length = 1.
        self.azimuth = 0.
        self.min_length = None
        self.n_segs = 100
        self.sigma_from_func = True
        self.eps_from_func = False
        self.quasi_static = True
        self.n_tx = 1


class DC(ApproachBase):

    """
    FE implementation of E-field approach (Grayver, 2014, Schwarzbach 2009)
    """

    def __init__(self, FS, MP):

        """
        - v_j, type dolfin TestFunction
            test functions for real-valued coupled system of equations

        - u_j, type dolfin TrialFunction
            trial functions for real-valued coupled system of equations

        - mute, type list of str
            mute irrelevant variables during on-the-fly screen print
        """

        # Initialize trial and test functions and pipe FE parameters
        super(self.__class__, self).__init__()

        self.v = df.TestFunction(FS.S)
        self.u = df.TrialFunction(FS.S)

        self.FS = FS
        self.MP = MP
        self.p = self.FS.p
        self.mute = ['v', 'u', 'DOM',
                     'dx', 'FS', 'MP', 'mute', 'path']

    def build_var_form(self, **fem_kwargs):

        """
        print FE parameters and initialize variational formulations for
        assembly
        """

        self.__dict__.update(fem_kwargs)
        if self.MP.tensor_flag and self.sigma_from_func:
            if '_s' in self.MP.approach:
                self.sigma_from_func = False
                mpp('Issue! "sigma_from_func" option for anisotropic cond. '
                    'only implemented for total E-field approach yet!\n'
                    'Using "old" multiple domain technique ...')

        print_fem_parameters(fem_kwargs, self.__dict__)
        check_sigma_dx_conformity(self.FS.DOM.n_domains, len(self.MP.sigma))
        check_source(self.s_type, self.path)
        self.LHS_form()
        self.FS.add_primary_fields(self.MP, calc=False, **fem_kwargs)
        self.RHS_form_Total()

    def LHS_form(self):

        """
        left-hand side contributions for E-field approach
        """
        dx = self.FS.DOM.dx

        if self.sigma_from_func:
            dx_0 = self.FS.DOM.dx_0
            if self.MP.tensor_flag:
                dg_space = df.TensorFunctionSpace(self.FS.mesh, "DG", 0)
                sigma_func = build_DG_tensor_func(
                        dg_space, self.FS.DOM.domain_func, self.MP.sigma)
            else:
                sigma_func = build_DG_func(self.FS.S_DG,
                                           self.FS.DOM.domain_func,
                                           self.MP.sigma)

            self.sigma_func = sigma_func
            self.L = df.inner(df.grad(self.u),
                              sigma_func * df.grad(self.v)) * dx_0

        else:
            c1 = add_sigma_vals_to_list(self.MP.sigma)
            self.L = df.inner(df.grad(self.u),
                              c1[0] * df.grad(self.v)) * dx(0)

            for j in range(1, self.FS.DOM.n_domains):
                self.L += df.inner(df.grad(self.u),
                                   c1[j] * df.grad(self.v)) * dx(j)

    def RHS_form_Total(self):

        dx_0 = self.FS.DOM.dx_0
        self.R = df.inner(df.Constant(("0.0")), self.v) * dx_0


def print_fem_parameters(fem_kwargs, dictionary):

    for key in fem_kwargs:
        if key not in dictionary:
            mpp('Warning! Unknown FE parameter set:', key)

    mpp('FE parameters update:')
    for k, v in sorted(dictionary.items()):
        if k not in dictionary['mute']:
            mpp('-->  {:<22}'.format(str(k)) + '  =  ', v, pre_dash=False)


def check_sigma_dx_conformity(number_of_domains, len_sigma):

    """
    evaulate, if number of domains matches number of given sigma values
    """

    if number_of_domains is 1 or number_of_domains is 2:
        mpp('Notice! Only background domains 0 and 1 are assembled! '
            '"Fullspace" or "Halfspace" model?')

    if len_sigma != number_of_domains:
        mpp('len_sigma of proc 0: ', len_sigma)
        mpp('n_doms of proc 0: ', number_of_domains)
        mpp('Fatal error! The numbers of sigma values '
            'and given domains must be the same! \n'
            'Process is terminated')
        raise SystemExit


def check_sigma_vals(MP):

    """
    evaulate, if anomalies are included or halfspace or fullspace model is
    chosen
    """

    if not all(type(x) == float for x in MP.delta_sigma[2:]):
        return(True)
    else:
        if len(MP.delta_sigma) is 2 or (
                all(x < 1e-8 for x in MP.delta_sigma[2:]) and
                all(x > -1e-8 for x in MP.delta_sigma[2:])):
            return(False)
        else:
            return(True)


def add_sigma_vals_to_list(vals, inverse=False):

    s_list = []
    for j in range(len(vals)):
            if isinstance(vals[j], float):
                if inverse:
                    s_list.append(1. / df.Constant((vals[j])))
                else:
                    s_list.append(df.Constant((vals[j])))
            else:
                if inverse:
                    s_list.append(df.inv(vals[j]))
                else:
                    s_list.append(vals[j])
    return(s_list)


def build_DG_func(dg_space, domain_func, vals, inverse=False):

    dg_func = df.Function(dg_space)
    np_vals = np.array(vals)
    if inverse:
        dg_func.vector().set_local(1./np_vals[domain_func.array()])
    else:
        dg_func.vector().set_local(np_vals[domain_func.array()])
    dg_func.vector().apply('insert')
    return(dg_func)


def build_DG_tensor_func(dg_space, domain_func, vals, inverse=False):

    dg_func = df.Function(dg_space)
    domain_tensor_func = np.repeat(domain_func.array(), 9) * 9
    domain_tensor_func[1::9] += 1
    domain_tensor_func[2::9] += 2
    domain_tensor_func[3::9] += 3
    domain_tensor_func[4::9] += 4
    domain_tensor_func[5::9] += 5
    domain_tensor_func[6::9] += 6
    domain_tensor_func[7::9] += 7
    domain_tensor_func[8::9] += 8

    rearranged = []
    for val in vals:
        if type(val) is float:
            rearranged.extend(np.array([val, 0., 0.,
                                        0., val, 0.,
                                        0., 0., val]))
        elif len(val) == 3:
            rearranged.extend(np.array([val[0], 0., 0.,
                                        0., val[1], 0.,
                                        0., 0., val[2]]))
        elif len(val) == 6:
            rearranged.extend(np.array([val[0], val[1], val[2],
                                        val[1], val[3], val[4],
                                        val[2], val[4], val[5]]))
        else:
            mpp('Fatal Error! Anisotropy must be either specified by 3 main '
                'diagonal values or 6 (upper triangle) values of the tensor!')
            raise SystemExit

    if inverse:
        mpp('Inverse Tensor DG Space needs to be implemented ..., Aborting!')
        raise SystemExit
    else:
        dg_func.vector().set_local(np.array(rearranged)[domain_tensor_func])
    dg_func.vector().apply('insert')
    return(dg_func)


def check_source(s_type, path):

    """
    evaulate, which source type is chosen
    """

    if s_type == 'hed':
        return
    elif s_type == 'vmd':
        return
    elif s_type == 'line':
        return
    elif s_type == 'loop_c':
        return
    elif s_type == 'loop_r':
        return
    elif s_type == 'path':
        if path is None:
            mpp('Fatal Error! No pointset ("path" variable) for "s_type" '
                '--> ' + s_type + ' defined!')
            raise SystemExit
    else:
        mpp('Typing Error! Unknown "s_type" --> ' + s_type + '!')
        raise SystemExit
