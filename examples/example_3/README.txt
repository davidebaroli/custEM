﻿################################################################################
# # # # #                          Example 3                           # # # # #
################################################################################

    - Note, this example was reworked since the publication of Rochlitz et al. 
      in 2018 to benefit from recent improvements.

    - Start with generating the mesh by running the *mesh_example_3_line.py* or
      *mesh_example_3_loop.py* script, dependent on the desired source type.
    
    - Please note that the computation by calling either *run_example_3_line.py* 
      or *run_example_3_loop.py* requires up to 500 GB RAM, but significantly  
      less if only a few processes are used or the mesh complexity is reduced
      by coarsening the receiver refinement, anomaly cell sizes or lower
      mesh quality.
    
    - The *plot_example_3_"line/loop".py* scripts will generate similar plots 
      as shown in Figures 8 & 9 in the corresponding publication Rochlitz et al.
      in 2018 in the *plots* directory, named "tf.pdf" and "sf.pdf".

################################################################################