# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
import numpy as np
import dolfin as df

approaches = ['E_t', 'E_s', 'H_s', 'Am_t', 'Am_s', 'An_t', 'An_s', 'Fm_s']
omega = 1e1 * 2. * np.pi

# define anisotropic conductivity tensor for the brick anomaly:
# old syntax for anisotropy, deprecated!
aniso_cond = df.as_matrix(((0.1, 0., 0.), (0., 0.1, 0.), (0., 0., 1.)))
iso_cond = 0.2

# new syntax for (in this case VTI) anisotropy in development:
# aniso_cond = [0.1, 0.1, 1.]

# ########################## run p1 computations ############################ #

pf_EH = 'H'
linE1 = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_200_line_x'
linE2 = 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_200_line_x'

p = 1
mod = 'example_3_p1_aniso' + pf_EH
mesh = 'example_3_mesh_p1_line'

for approach in approaches:       # all approaches

    # Initialize MODel
    M = MOD(mod, mesh, approach, p=p, overwrite=True,
            m_dir='./meshes', r_dir='./results')

    # define frequency and conductivities
    M.MP.update_model_parameters(omega=omega,
                                 sigma_anom=[2e-1, aniso_cond],
                                 sigma_ground=[1e-3],
                                 procs_per_proc=1)

    # 1000m dipole path in following x-axis
    M.FE.build_var_form(pf_type='halfspace',
                        s_type='line',
                        start=[0., -5e2, 0.],
                        stop=[0., 5e2, 0.],
                        n_segs=101,
                        pf_EH_flag=pf_EH,
                        sigma_from_func=False)

    # Call solver and convert H-fields
    M.solve_main_problem(convert_to_E=True, sym=True)

    # load existing model only for interpolation purposes
    M = MOD(mod, mesh, approach, p=p, overwrite=False, load_existing=True,
            m_dir='./meshes', r_dir='./results')

    # create regular 5 km inteprolation line in x-dir at surface
    M.IB.create_line_meshes('x',  x0=-5e3, x1=5e3, n_segs=200)
    M.IB.create_line_meshes('x',  x0=-5e3, x1=5e3, z=50., n_segs=200)

    # interpolate fields on the observation line
    if '_s' in approach:   # if secondary field formulation used
        quantities = ['H_t', 'E_t', 'H_s', 'E_s']
    else:                  # if total field formulation used
        quantities = ['H_t', 'E_t']
    for q in quantities:
        M.IB.interpolate(q, linE1)
        M.IB.interpolate(q, linE2)
    M.IB.synchronize()

# ########################## run p2 computations ############################ #

p = 2
mod = 'example_3_p2_aniso' + pf_EH
mesh = 'example_3_mesh_p2_line'

for approach in approaches:

    # Initialize MODel
    M = MOD(mod, mesh, approach, p=p, overwrite=True,
            m_dir='./meshes', r_dir='./results')

    # define frequency and conductivities
    M.MP.update_model_parameters(omega=omega,
                                 sigma_anom=[2e-1, iso_cond],
                                 sigma_ground=[1e-3],
                                 procs_per_proc=3)

    # 1000 m crooked dipole path in following x-axis
    M.FE.build_var_form(pf_type='halfspace',
                        s_type='line',
                        start=[0., -5e2, 0.],
                        stop=[0., 5e2, 0.],
                        n_segs=51,
                        pf_EH_flag=pf_EH,
                        sigma_from_func=False)

    # Call solver and convert H-fields
    M.solve_main_problem(convert_to_E=True)

    # load existing model only for interpolation purposes
    M = MOD(mod, mesh, approach, p=p, overwrite=False, load_existing=True,
            m_dir='./meshes', r_dir='./results')

    # interpolate fields on the observation line
    if '_s' in approach:   # if secondary field formulation used
        quantities = ['H_t', 'E_t', 'H_s', 'E_s']
    else:                  # if total field formulation used
        quantities = ['H_t', 'E_t']

    for q in quantities:
        M.IB.interpolate(q, linE1)
        M.IB.interpolate(q, linE2)
    M.IB.synchronize()
