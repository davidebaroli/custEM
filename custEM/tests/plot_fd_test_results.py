# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import os
from custEM.post import Plot
import matplotlib.pyplot as plt


"""
Make some plots stored in the 'lvl_2_test_plots' directory.

Note: 1./2. result, 3./4. as well as 5./6. should be similar since the same
      model was computed by two approaches each
"""

if not os.path.isdir('lvl_2_test_plots/line_plots'):
    os.makedirs('lvl_2_test_plots/line_plots')
if not os.path.isdir('lvl_2_test_plots/slice_plots'):
    os.makedirs('lvl_2_test_plots/slice_plots')

plt.ioff()   # speed things up
E_flg = '_E_t'
H_flg = '_H_t'
EH_flg = 'H'    # change to E for E-field plots
pp1 = '1'
pp2 = '2'
fs = 10
Elim = [1e-9, 1e-3]
Hlim = [1e-6, 1e-2]
errlim = [1e-1, 1e1]
comps = ['x', 'y', 'z', 'mag']

mod1 = 'Lvl_2_test_HED_E_t'
mod2 = 'Lvl_2_test_HED_E_s'
mod3 = 'Lvl_2_test_Line_Am_t'
mod4 = 'Lvl_2_test_Line_Am_s'
mod5 = 'Lvl_2_test_Loop_H_s'
mod6 = 'Lvl_2_test_Loop_An_s'

mesh1 = 'HED_halfspace_mesh'
mesh2 = 'Line_dip_plate_mesh'
mesh3 = 'Loop_brick_mesh'

line1 = 'default_small_line_x'
line2 = 'default_small_line_y'
line3 = 'default_small_line_z'
line4 = 'default_large_line_x'
line5 = 'default_large_line_y'
line6 = 'default_large_line_z'
lines = [line1, line2, line3, line4, line5, line6]

slice1 = 'default_small_slice_x'
slice2 = 'default_small_slice_y'
slice3 = 'default_small_slice_z'
slice4 = 'default_large_slice_x'
slice5 = 'default_large_slice_y'
slice6 = 'default_large_slice_z'
slices = [slice1, slice2, slice3, slice4, slice5, slice6]

lims = [2., 2., 2., 10., 10., 10.]

###############################################################################
# # #                      make default line plots                        # # #
###############################################################################

P1 = Plot(mod=mod1, mesh=mesh1, approach='E_t', fig_size=[8, 12],
          r_dir='lvl_2_test_results', s_dir='lvl_2_test_plots/line_plots')

for k in ['small', 'large']:
    P1.load_default_line_data(interp_meshes=k)
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod2, sf=False, approach='E_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod3, mesh=mesh2, sf=False, approach='Am_t')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod4, mesh=mesh2, sf=False, approach='Am_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod5, mesh=mesh3, sf=True, approach='H_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod6, mesh=mesh3, sf=True, approach='An_s')

for i, line in enumerate(lines):
    print('plotting field data: ', line)
    P1.plot_line_data(mod=mod1, mesh=line, xlim=[-lims[i], lims[i]],
                      save=False)
    P1.plot_line_data(mod=mod2, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, title=mod1 + '_and_' + mod2)
    P1.plot_line_data(mod=mod3, mesh=line, xlim=[-lims[i], lims[i]],
                      save=False)
    P1.plot_line_data(mod=mod4, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, title=mod3 + '_and_' + mod4)
    P1.plot_line_data(mod=mod5, mesh=line, xlim=[-lims[i], lims[i]],
                      save=False)
    P1.plot_line_data(mod=mod6, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, title=mod5 + '_and_' + mod6)
    P1.plot_line_data(mod=mod5, mesh=line, xlim=[-lims[i], lims[i]],
                      save=False, sf=True)
    P1.plot_line_data(mod=mod6, mesh=line, xlim=[-lims[i], lims[i]], sf=True,
                      new=False, title=mod5 + '_and_' + mod6 + '_sec_fields')
    plt.close('all')

for i, line in enumerate(lines):
    print('plotting mismatches: ', line)
    P1.plot_line_errors(mod=mod1, mesh=line, xlim=[-lims[i], lims[i]],
                        mod2=mod2, title=mod1 + '_and_' + mod2 + '_mismatch')
    P1.plot_line_errors(mod=mod3, mesh=line, xlim=[-lims[i], lims[i]],
                        mod2=mod4, title=mod3 + '_and_' + mod4 + '_mismatch')
    P1.plot_line_errors(mod=mod5, mesh=line, xlim=[-lims[i], lims[i]],
                        mod2=mod6, title=mod5 + '_and_' + mod6 + '_mismatch')
    P1.plot_line_errors(mod=mod5, mesh=line, xlim=[-lims[i], lims[i]], sf=True,
                        mod2=mod6,
                        title=mod5 + '_and_' + mod6 + '_sec_fields_mismatch')
    plt.close('all')

###############################################################################
# # #                     make default slice plots                        # # #
###############################################################################


P2 = Plot(mod=mod1, mesh=mesh1, approach='E_t', fig_size=[8, 12],
          r_dir='lvl_2_test_results', s_dir='lvl_2_test_plots/slice_plots')

for k in ['small', 'large']:
    P2.load_default_slice_data(interp_meshes=k)
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod2, sf=False, approach='E_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod3, mesh=mesh2, sf=False, approach='Am_t')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod4, mesh=mesh2, sf=False, approach='Am_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod5, mesh=mesh3, sf=True, approach='H_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod6, mesh=mesh3, sf=True, approach='An_s')

for i, slicE in enumerate(slices):
    print('plotting field data: ', slicE)
    P2.plot_slice_data(mod=mod1, mesh=slicE, title=mod1)
    P2.plot_slice_data(mod=mod2, mesh=slicE, title=mod2)
    P2.plot_slice_data(mod=mod3, mesh=slicE, title=mod3)
    P2.plot_slice_data(mod=mod4, mesh=slicE, title=mod4)
    P2.plot_slice_data(mod=mod5, mesh=slicE, title=mod5)
    P2.plot_slice_data(mod=mod6, mesh=slicE, title=mod6)
    P2.plot_slice_data(mod=mod5, mesh=slicE, sf=True,
                       title=mod5 + '_sec_fields')
    P2.plot_slice_data(mod=mod6, mesh=slicE, sf=True,
                       title=mod6 + '_sec_fields')
    plt.close('all')

for i, slicE in enumerate(slices):
    print('plotting mismatches: ', slicE)
    P2.plot_slice_errors(mod=mod1, mod2=mod2, mesh=slicE,
                         title=mod1 + '_and_' + mod2 + '_mismatch')
    P2.plot_slice_errors(mod=mod3, mod2=mod4, mesh=slicE,
                         title=mod3 + '_and_' + mod4 + '_mismatch')
    P2.plot_slice_errors(mod=mod5, mod2=mod6, mesh=slicE,
                         title=mod5 + '_and_' + mod6 + '_mismatch')
    P2.plot_slice_errors(mod=mod5, mod2=mod6, mesh=slicE, sf=True,
                         title=mod5 + '_and_' + mod6 + '_sec_fields_mismatch')
    plt.close('all')
