
.. sphinx-inclusion-marker

** !!! Roadmap to custEM v.1.0 within the next months !!! **

* finish implementation of time domain approaches
* remove a few known bugs
* implemenet parallel interpolation
* implement plot tools for time-domain approaches
* tutorials for time-domain modeling and others
* update documentation, add important comments in the code, not only doc-strings

**custEM** is an open-source toolbox for customizable 3D finite-element modeling 
of controlled-source electromagnetic data. The toolbox is based on the finite-
element library FEniCS, see https://fenicsproject.org/ . Different 
total and secondary electric or magnetic field as well as gauged-potential
approaches are implemented. In addition, **custEM** contains a mesh generation
submodule for the straightforward generation of tetrahedral meshes supporting
a large number of marine, land-based, airborne or mixed model scenarios.
Interpolation and visualization tools simplify the post-processing workflow.

Mesh generation runs on any laptop or desktop PC. Also "toy" examples can be
tested on such machines. Because of using direct solvers, we recommend to get
access to a cluster with >= 32 cores/threads and >=256 GB RAM for calculating
sufficiently accurate models for more complex 3D setups. Anyway, already
reasonable solutions for "simple" 3D modeling studies often require only first
order polynomials and could be run on a desktop PC or laptop with 32-64 GB.

**Please note:** custEM is under continuous development. Usually, updates
for debugging, adding documentation or new features are committed frequently.
We plan to implement further improvements and resolve the last remaining
major issues for **custEM**: version **1.0**, in the next months.

For any kind of question about custEM, do not hesitate to contact us:

        raphael.rochlitz@leibniz-liag.de

################################################################################

Getting started
===============

To guide you through setting up the toolbox and making third party dependencies
available, please follow the installation notes. After successful installation,
please try to run the three test files provided in the corresponding directory.
In addition to these initial tests for the overall functionality of **custEM**,
we refer to the provided examples. They are identical to the ones presented in
the publication (Rochlitz et al., 2018). Further guides to develop solution
strategies for user-specific modeling problems are available in the tutorials
directory.

**Please note:** Even though described in other instructions separately, we want
to point out that for implementation reasons, currently the *mpirun* syntax
needs to be used for serial FEM computations as well. For instance, (instead of
*python run_example_1.py*) use:

    --> mpirun -n 1 python run_example_1.py
    
Anyway, mesh generation or visualization scripts always have to be called in
serial with the usual command line syntax or using a python editor, for
instance:

    --> python mesh_example_1.py    
    --> python plot_example_1.py
    
**Please note:** Usually, airspace conductivities are set to 1e-10 or
1e-12 Ohmm. For most CSEM setups, we found that 3-4 orders of magnitude contrast
between airspace and subsurface conductivities are totally sufficient to obtain
accurate solutions. Higher contrasts result in worse conditioning of the 
system matrix and slightly longer computation times but do not increase
the solution accuracy significantly (the effect is << 1 % of relative errors).

################################################################################

Installation
============

For basic instructions, it is referred to the :ref:`Installation <installlabel>`
file for more information.

Further notes:

* It is recommended to add all the *export* commands to your *bashrc* file.
  
* If custEM was installed via conda, it is recommended to add the '-u' flag
  after 'python' or 'python3' in the commmand promt calls to force all prints
  to appear in time and not delayed, e.g.:
  
      --> mpirun -n 12 python -u run_script.py 
  
* In order to use the provided *jupyter notebook* tutorials, jupyter needs to
  be installed **after** all other steps listed below:
  
      --> pip install jupyter

* Computation times might be speed up with reducing the number of mpirun
  processes (e.g., 8 instead of 32) and enabling OpenMP parallelization during
  the solution of the system of equations via MUMPS with adjusting the flag
  *OMP_NUM_THREAD*, e.g., *export OMP_NUM_THERADS=4* instead of 1.


################################################################################

Latest implementation progress / change notes
=============================================

**19.11.2019** - custEM version 0.99

* advanced mesh tools with node and edge markers
* pyhed fixed, identical sign conversion
* pyhed incorporated as third-party conda package now, not in wrap directory
* time stepping approach optimized
* rational krylov approach added
* inverse fourier transformation based approach added
* documentation updated
* few syntax changes

**12.06.2019** - custEM version 0.98

* conda installer added, installation simplified significantly
* custEM tests during conda package build enabled
* support got FEniCS version older than 2018 disabled
* documentation updated

**18.04.2019** and 20.05.2019 - custEM version 0.97

* Alternate tranformation factors for PF E-field tested with help of empymod 
* compatibility with FEniCS 2019.1
* potential approaches now correctly implemented with identical structure
  (based on E=iw*A + iw*grad*V)
  (based on H=iw*F + iw*grad*U)
* corresponding post_processing adapated 
* F-U (Tau-Omega) approach added
* bugfixes in meshgen and bathy tools 

**13.03.2019** - custEM version 0.96

* lot's of bugs fixed (overall compatibility with FEniCS 2018.1)
* multiple RHS supported for total field approaches and framwork for 
  adding this feature for secondary field approaches already implemented
* test, example and tutorial files updated to be again compatible with
  a this new version of custEM
* All provided scripts were tested also with previous versions based on
  FEniCS 2017.2, but it is heavily recommended to use this newest version with
  FEnICS 2018 to benefit from the increased performance!

**09.01.2019** - custEM version 0.95 

* version 0.94 included
* custEM now compatible with FEniCS 2018.1
* Significant performance boost due to parallel HDF5 support for FEniCS conda
  package and new MUMPS version, no matrix size restrictions anymore!
* Performance boost due to debugged symmetric MUMPS solver
  (previous custEM versions were bugged and called the general LU solver)
* Full Maxwell equations with displacement currents for E-field approach
  available, in progress for H-field approach
* H-field approach debugged
* Time-domain time-stepping approach added (not fully supported yet)
* Bathymetry/Topography meshing tools improved
* Irregular topography data supported

**28.08.2018** - custEM version 0.93

* bug for primary E-fields of line sources fixed!
* alternative and superior way of incorporating transmitters in meshes added
* examples 2 reworked
* first steps for meshing costal areas with topography, bathymetry initialized

**28.08.2018** - custEM version 0.92

* ReadtheDocs bug fixed: API for modules added!
* restructured repository (custEM modules now subdirectory of repository)
* examples added to table of contents

**23.08.2018** - custEM version 0.91

* fixed sorting-bug workaround for primary-field calculations!
* removed minor bugs in meshgen submodule
* added topography meshing tutorial to the tutorials directory 
* enabled edge preservation by using not only *.poly* but also *.edge* files for
  TetGen which is especially an advantage for implementing all kind of CSEM
  transmitters. As a consequence, TetGen does not need to be called with the 
  '-M' flag anymore for edge preservation, which saves up to 30% of dofs!

Previous implementation achievements are listed in the 
:ref:`Implementation history <imphist>` section for documentation purposes.

Upcoming features / improvements / bug removals
===============================================

The points listed here are minor improvements which are definitely planned to 
be implemented in the near future. If major achievements, for instance, support
for time-domain modeling, are ready to be implemented, there will be a clearly
visible announcement.

* implement support for non-vacuum magnetic permeabilities and if required,
  anisotropic permeabilities can be considered if required by users
* mixed bathymetry/topography support for coastal environments
* flexible interpolation grids and not only straight or crooked (topography)
  lines and faces with a regular node spacing
* embedded interpolation-mesh generation during mpirun computations
* continue visualization tutorial and add more examples

################################################################################

Documentation
=============

**Please note:** The python API documentation in the section *modules* is not 
available on ReadtheDocs due to issues with automated API-documentations.
Anyway, the complete API documentation is available as "hmtl_doc" zip-file in
the *docs* directory of the custEM repository and can be accessed via opening
the "index.html" file in the zip directory with your favorite browser.

The following content was generated automatically. The documentation of modules
and classes already provides a good overview about the specific usage of the 
corresponding functionalities in custEM. However, an overhaul of a few parts is
necessary, for instance:

* write proper headers of all python files
* add a few doc-strings that do not exactly match the *rst* syntax
* add link between submodules and classes
* correct specifications, e.g., module, class, instance, function or method
    
This is going to be conducted step by step when resolving final implementation
issues which we plan to resolve for the **custEM**: version **1.0**.

* Description of the main model class **MOD**: :class:`custEM.core.model.MOD`
* List of all :ref:`Submodules <modlabel>`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search` for contents

################################################################################

Tutorials
=========

Tutorials are available in the *tutorials* directory in the **custEM**
repository as *jupyter notebook*, *python* script, and in *html* format.
The *html* documentation of the :ref:`Tutorials <tutoriallabel>` can be
also accessed in the corresponding section. Please note that the tutorials 
are still under development and in particular, the usage of *jupyter notebooks*
is not straightforward in combination with multiprocessing. We are about to fix
related issues, but for now, secondary field computations and interpolation
are not possible if the *run_tutorial* is started from the *jupyter notebook*.
For running this tutorial, please use the provided *run_tutorial.py* python
script and call it from the command line with the *mpirun* syntax, e.g.:

    --> mpirun -n x python run_tutorial.py

with x, the number of cores.

Examples
========

A selection of CSEM modeling examples can be found :ref:`here <examplelabel>`.

################################################################################

License
=======

Copyright 2016-2018 by R. Rochlitz

The **custEM** toolbox is licensed under the GNU LESSER GENERAL PUBLIC LICENSE
(LGPL), just as **FEniCS**. The terms of the LGPL are listed in the 
:ref:`License <licenselabel>` file.

################################################################################

Authors
=======

Contributing authors and contacts are listed in the
:ref:`Authors <authorlabel>` file.

################################################################################

Citation
========

Rochlitz, R., Skibbe, N. and Günther, T., (2018), 
*custEM: customizable finite element simulation of complex controlled-source
electromagnetic data*,
GEOPHYSICS Software and Algorithms.

Next to us, don't forget to give credits to the authors of FEniCS, pyGIMLi or
TetGen if submodules based on these developments are used for your purposes.
