# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from __future__ import print_function
import numpy as np
import dolfin as df
import os
from mpi4py import MPI
import sys

"""
Miscellaneous functions used in FEniCS_EM, functions are self-explaining ...
"""


def max_mem(total=True, for_dump=False):

    """
    Support function that prints the maximum memory requirement up to that
    point.

    Keyword arguments:
    ------------------

    - total=True, type bool
        Flag if memory consumption should be printed in total or per process
    """

    import resource
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    if total:
        all_mem = np.sum(np.array(MPI.COMM_WORLD.gather(mem, root=0),
                                  dtype=float))*1e-6
        if for_dump:
            return(all_mem)
        else:
            if MPI.COMM_WORLD.Get_rank() == 0:
                print('  -  maximum memory usage [GB]:  -->  ', all_mem)
            else:
                pass
    else:
        if for_dump:
            return(mem)
        else:
            print('MEM', mem)


def dump_csr(fname, values, column_indices, row_pointers, size):

    """
    Support function that dumps a PETSCMatrix as a sparse csr-matrix to
    the hard disc.

    Required arguments:
    -------------------

    - fname, type str
        output name of the matrix without the suffix **'.csr'**

    - values, type array
        output values of matrix elements

    - column_indices, type array
        column indices

    - row_pointers, type array
        row_pointers

    - size, type int
        size (length) of the square matrix
    """

    ofile = fname + ".csr"
    fhdl = open(ofile, 'w')

    fhdl.write(str(size))
    fhdl.write('\n')
    fhdl.write(str(column_indices.shape[0]))
    fhdl.write('\n')

    for i in row_pointers:
        fhdl.write(str(i+1))
        fhdl.write('\n')

    for j in column_indices:
        fhdl.write(str(j+1))
        fhdl.write('\n')

    for m in values:
        fhdl.write(str(m))
        fhdl.write('\n')

    fhdl.close()
    print('Written: {}'.format(ofile))


def mpi_print(string, val=None, pre_dash=True, post_dash=False, barrier=True):

    """
    Support function that prints only from root process in mpi mode

    Required arguments:
    -------------------

    - string, type str
        string that should be printed

    Keyword arguments:
    ------------------

    - val=None, various types
        a value that should be printed after the "message"

    - pre_dash=True, type bool
        Flag that controls if a dashed line shoul be printed before the
        "message"

    - post_dash=False, type bool
        Flag that controls if a dashed line shoul be printed before the
        "message"
    """

    if barrier:
        df.MPI.barrier(df.MPI.comm_world)
    if df.MPI.rank(df.MPI.comm_world) == 0:
        if pre_dash is not True:
            pass
        else:
            print('=' * 80)
        if val is None:
            print(string)
        else:
            print(string, val)
        if post_dash is False:
            pass
        else:
            print('=' * 80)
    if barrier:
        df.MPI.barrier(df.MPI.comm_world)


def root_write(writing_function):

    """
    Support function that dumps data only from root process in mpi mode

    Required arguments:
    -------------------

    - writing_function, type python function
        a function for writing e.g. data or json files
    """

    if MPI.COMM_WORLD.Get_rank() == 0:
        writing_function()
        # yield
    else:
        pass


def run_serial(script_name, *serial_args):

    """
    Support function that calls a script in serial from root process in
    mpi mode

    Required arguments:
    -------------------

    - script_name, type str
        name of the python script that should be called in serial

    - *serial_args, type python list
        list of input console arguments for the python script
    """

    mpi_print('#############################################', pre_dash=False)
    mpi_print('Computation in serial started  ...', pre_dash=False)
    str_list = [script_name]
    for arg in serial_args:
        str_list.extend(arg)
    if MPI.COMM_WORLD.Get_rank() == 0:
        ic = MPI.COMM_SELF.Spawn(sys.executable, args=str_list, maxprocs=1)
        ic.Barrier()
        ic.Disconnect()
    else:
        ic = None
    mpi_print('...  time to continue in parallel.', pre_dash=False)
    mpi_print('#############################################', pre_dash=False)


def run_multiple_serial(script_name, *serial_args):

    """
    Support function which is nearly identical to **run_serial**.
    The Only difference is that a communicator object **ic** is returned
    that can be used for multi-threading of several serial runs.

    A synchronisation with all communicators can be foreced from the root
    process in mpi mode by collecting all communicator objects in a list and
    by using the "Barrier" and "Disconnect" mpi4py commands before continuing.

    Required arguments:
    -------------------

    - script_name, type str
        name of the python script that should be called in serial

    - *serial_args, type python list
        list of input console arguments for the python script
    """

    string_list = [script_name]
    for arg in serial_args:
        string_list.extend(arg)
    if MPI.COMM_WORLD.Get_rank() == 0:
        ic = MPI.COMM_SELF.Spawn(sys.executable, args=string_list, maxprocs=1)
    else:
        ic = None
    return(ic)


def check_approach_and_file_format(approach, file_format, path):

    if 'dc' in approach.lower():
        if file_format is None:
            try:
                df.HDF5File(df.MPI.comm_world, path + 'dummy_hdf5.h5', 'r')
                return('h5')
            except RuntimeError:
                mpi_print("  -  using 'xml' file format since parallel 'h5' "
                          "is not supported  -")
            return('xml')
        else:
            return(file_format)

    if '_t' not in approach and \
       '_s' not in approach and \
       '_IE' not in approach and \
       '_RA' not in approach and \
       '_FT' not in approach:
        mpi_print('Fatal Error! Use approach with "_t" or "_s" '
                  'subscript for the "approach" argument for either "total" '
                  'or "secondary" field formulation in frequency-domain '
                  'modeling, respectively!\n'
                  'Alternatively, "E_IE", "E_FT" or "E_RA" are valid '
                  'arguments for time-domain simulations.')
        raise SystemExit

    if 'E' not in approach:
        if 'H' not in approach:
            if 'Am' not in approach:
                if 'An' not in approach:
                    if 'Fm' not in approach:
                        mpi_print('Fatal Error! Specify correct approach: '
                                  'Either "E", "H", "Am", "An" or "Fm"!')
                        raise SystemExit

    if file_format is None:
        try:
            df.HDF5File(df.MPI.comm_world, path + 'dummy_hdf5.h5', 'r')
            return('h5')
        except RuntimeError:
            mpi_print("  -  using 'xml' file format since parallel 'h5' "
                      "is not supported  -")
        return('xml')
    else:
        return(file_format)


def check_if_model_exists(mod_name, mesh_name, out_dir, out_name,
                          overwrite, loadexisting, mute):

    """
    Support function that checks if a finite element model already exists which
    might either be overwritten or the calculation may be aborted...

    Required arguments:
    -------------------

    it is referred to the **'model'** class and **'MOD'** instance description
    """

    print('=' * 80)
    if os.path.isfile(out_dir + '/' +
                      out_name + '_Domains.pvd') is True:
        if overwrite is False:
            if loadexisting is False:
                print('Fatal Error! The specified model:\n' +
                      'mod_name = ' + mod_name + '  and\n' +
                      'mesh_name = ' + mesh_name + '  already exists.')
                print('Consider overwriting existing models by setting the '
                      'parameter "overwrite" to "True"!')
                raise SystemExit
            else:
                if not mute:
                    print('...  load existing model results  ...')
        elif overwrite is True and loadexisting is True:
            print('Fatal Error! Overwriting an existing model with setting '
                  'the parameter "overwrite" to "True" and loading the '
                  'existing results of this model at the same time with '
                  'setting "load_existing" to "True" at the same time makes '
                  'no sense in the programmers mind.')
            raise SystemExit
        else:
            print('Notice! Parameter "overwrite" is "True" '
                  'Do you realy want to overwrite\nthe existing model? '
                  'If not, you probably have enough time to abort this '
                  'run.')
    else:
        if overwrite is False and loadexisting is True:
            print('Fatal Error! The specified model could not be found. '
                  'Considering checking the "mod" name specified for the '
                  'import or calculating the desired model at all.')
            raise SystemExit


def read_paths(file_name):

    """
    Support function that import the **'r_dir'** and **'m_dir'**
    attributes for the **'MOD'** instance from file.

    Note!, these attributes are always overwritten if the keyword arguments
    **'r_dir'** or **'m_dir'** are set manually when initializing the **'MOD'**
    instance

    Required arguments:
    -------------------

    file_name, type str
        file name of the file which contains the paths
    """

    with open(file_name) as f:
        for line in f:
            a = ([k for k in range(len(line)) if line.startswith("'", k) or
                  line.startswith('"', k)])
            r = line.find('results_dir')
            if r is not -1:
                results_dir = line[a[0] + 1:a[1]]
            m = line.find('mesh_dir')
            if m is not -1:
                mesh_dir = line[a[0] + 1:a[1]]
    return (results_dir, mesh_dir)


def write_h5(mpi_cw, var, fname, counter=None, new=True, close=True, f=None):

    """
    Utility function to write data files in 'h5' format

    Required arguments:
    -------------------

    - var, type dolfin function
        Data which should be exported, e.g., the *E*-field solution
        function **E_t_r_cg**

    - fname, type str
        destination file name, containing also the export path
    """

    if new:
        f = df.HDF5File(mpi_cw, fname, "w")
    if counter is None:
        f.write(var, "/data")
    else:
        f.write(var, "fun", counter)
    if close:
        f.close()
    else:
        if new:
            return(f)


def read_h5(mpi_comm_world, var, fname, counter=None):

    f = df.HDF5File(mpi_comm_world, fname, "r")
    if counter is None:
        f.read(var, "/data")
    else:
        f.read(var, "fun/vector_%d" % counter)
    f.close()


def make_directories(r_dir, m_dir, approach, para_dir=None, m_dir_only=False):

    """
    Support function that initializes the output directorys

    Required arguments:
    -------------------

    r_dir, type str
        main results directory
    m_dir, type str
        main mesh directory

    Keyword arguments:
    ------------------

    para_dir=None, type str
        If **None**, the paradir is by defualt: 'm_dir/_para'. Else, the
        parameter directory will be initializied according to the updated path
    """

    if MPI.COMM_WORLD.Get_rank() == 0:
        if not m_dir_only:
            if not os.path.isdir(r_dir + '/' + approach):
                os.makedirs(r_dir + '/' + approach)
            if not os.path.isdir(r_dir + '/primary_fields/custom'):
                os.makedirs(r_dir + '/primary_fields/custom')
            if not os.path.isdir(r_dir + '/primary_fields/fullspace'):
                os.makedirs(r_dir + '/primary_fields/fullspace')
            if not os.path.isdir(r_dir + '/primary_fields/halfspace'):
                os.makedirs(r_dir + '/primary_fields/halfspace')
            if not os.path.isdir(r_dir + '/primary_fields/layered_earth'):
                os.makedirs(r_dir + '/primary_fields/layered_earth')

        if not os.path.isdir(m_dir + '/_h5'):
            os.makedirs(m_dir + '/_h5')
        if not os.path.isdir(m_dir + '/_xml'):
            os.makedirs(m_dir + '/_xml')
        if not os.path.isdir(m_dir + '/_mesh'):
            os.makedirs(m_dir + '/_mesh')
        if not os.path.isdir(m_dir + '/lines'):
            os.makedirs(m_dir + '/lines')
        if not os.path.isdir(m_dir + '/paths'):
            os.makedirs(m_dir + '/paths')
        if not os.path.isdir(m_dir + '/slices'):
            os.makedirs(m_dir + '/slices')
        if para_dir is None:
            if not os.path.isdir(m_dir + '/para'):
                os.makedirs(m_dir + '/para')
    else:
        pass


def resort_coordinates(c1, c2):

    """
    Find out how to resort two arrays with overall identical row entries but
    different order.

    Required arguments:
    -------------------

    - c1 and c2, type numpy_arrays
        two data arrays of shape (n, 3)

    Output:
    -------

    - c1_sort and c2_sort, type numpy_arrays
        index arrays which can be used to resort data from input1 to input2
        and vice versa.
    """

    def indexAndSorting(c_):
        posview = c_.ravel().view(
            np.dtype((np.void, c_.dtype.itemsize*c_.shape[1])))
        _, unique_idx = np.unique(posview, return_index=True)
        return unique_idx, unique_idx.argsort()

    c1_idx, c1_sort = indexAndSorting(c1)
    c2_idx, c2_sort = indexAndSorting(c2)
    return c1_idx[c2_sort], c2_idx[c1_sort]


def get_coordinates(FS):
    """
    Return coordinates (V_cg) of given function space as numpy array.

    Required arguments:
    -------------------

    - FS, type class
        FunctionSpaces instance
    """

    xyz = FS.V_cg.tabulate_dof_coordinates().reshape(-1, 3)[0::3, :]
    return xyz


def block_print():
    sys.stdout = open(os.devnull, 'w')


def enable_print():
    sys.stdout = sys.__stdout__


"""
Utility FEniCS definitions - required, if TEST_MODE is used
"""

try:
    class Air(df.SubDomain):
        def inside(self, x, on_boundary):
            return x[2] > - 1e-2

    class Ground(df.SubDomain):
        def inside(self, x, on_boundary):
            return x[2] < 1e-2
    class DirichletBoundary(df.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary
except:
    pass
