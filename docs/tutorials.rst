.. _tutoriallabel:

#########
Tutorials
#########

.. include:: ../tutorials/README.rst

Mesh generation
===============

.. raw:: html
    :file: ../tutorials/meshgen_tutorial.html
 
.. raw:: html
    :file: ../tutorials/topo_mesh_tutorial.html
    
FEM computation
===============

.. raw:: html
    :file: ../tutorials/run_tutorial.html
    
Visualization
=============

.. raw:: html
    :file: ../tutorials/plot_tutorial.html