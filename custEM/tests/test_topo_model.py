# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
from custEM.core import MOD
from custEM.misc import max_mem
from custEM.misc import run_serial
from custEM.misc import block_print
from custEM.misc import enable_print


block_print()

"""
Third test which checks if TetGen is available and if **meshgen_tools** are
working. The first run is with p1, the second one with p2. The latter requires
parallel computation and > 200 GB RAM since the problem has > 1M dofs.

Uncomment the **df.set_log_level(df.DEBUG)** command to see what FEniCS prints.
"""


mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

if df.MPI.rank(df.MPI.comm_world) == 0:
    if os.path.isfile('meshes/_h5/meshgen_sample_mesh.h5'):
        os.remove('meshes/_h5/meshgen_sample_mesh.h5')
else:
    pass

# %% Create Mesh

run_serial('create_topo_mesh.py')

# %% Computing Loop_R layered earth model with total field approach & p1 poly

M = MOD('p1', 'sample_topo_mesh', 'E_t', p=1,
        overwrite=True, m_dir='meshes', r_dir='topo_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e2,
                             sigma_anom=[1e0],
                             sigma_ground=[1e-2, 1e-1, 1e-3],
                             anomaly_layer_markers=[0])

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.])

M.solve_main_problem(convert=True)

# %% Initialize default interpoaltion meshes and interpolate p1 result on them

M = MOD('p1', 'sample_topo_mesh', 'E_t', p=1,
        m_dir='meshes', r_dir='topo_test_results', mute=True,
        para_dir='meshes/para', load_existing=True, overwrite=False)

M.IB.init_default_interpolation_meshes()
M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, z=0.)
M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, z=50.)
M.IB.create_slice_meshes('z', z=0., dim=5e3, n_segs=100)
M.IB.create_slice_meshes('z', z=50., dim=5e3, n_segs=100)

M.IB.interpolate_default(target_type='lines', interp_meshes='small')
M.IB.interpolate_default(target_type='lines', interp_meshes='large')
M.IB.interpolate_default(target_type='slices', interp_meshes='small')
M.IB.interpolate_default(target_type='slices', interp_meshes='large')

M.IB.interpolate('E_t',
                 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_100_on_topo_line_x')
M.IB.interpolate('H_t',
                 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_100_on_topo_line_x')
M.IB.interpolate('E_t', 'z_0.0_dim_5000.0_n_100_on_topo_slice_z')
M.IB.interpolate('H_t', 'z_50.0_dim_5000.0_n_100_on_topo_slice_z')
M.IB.synchronize()
max_mem()

if mpi_cw.Get_rank() == 0:
    print('\n')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
    print('  Topo test performed succesfully for p1 polynomials !!!')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
else:
    pass

# %% Computing Loop_R layered earth model with total field approach & p2 poly

M = MOD('p2', 'sample_topo_mesh', 'E_t', p=2,
        overwrite=True, m_dir='meshes', r_dir='topo_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e2,
                             sigma_anom=[1e0],
                             sigma_ground=[1e-2, 1e-1, 1e-3],
                             anomaly_layer_markers=[0],
                             eps_for_coord_search=1e-8)

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.])

M.solve_main_problem(convert=True)

# %% Initialize custom interpolation meshes with topography
#    and interpolate p2 result on them

# loading the results is not necessary if interpolation is performed on the fly
# the results were imported for the p1 test to check if pre-processing works

# M = MOD('p2', 'sample_topo_mesh', 'E_t', p=2,
#         m_dir='meshes', r_dir='topo_test_results',
#         para_dir='meshes/para', load_existing=True, overwrite=False)
M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, z=0.)
M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, z=50.)
M.IB.create_slice_meshes('z', z=0., dim=5e3, n_segs=100)
M.IB.create_slice_meshes('z', z=50., dim=5e3, n_segs=100)
M.IB.interpolate('E_t',
                 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_100_on_topo_line_x')
M.IB.interpolate('H_t',
                 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_100_on_topo_line_x')
M.IB.interpolate('E_t', 'z_0.0_dim_5000.0_n_100_on_topo_slice_z')
M.IB.interpolate('H_t', 'z_50.0_dim_5000.0_n_100_on_topo_slice_z')
M.IB.synchronize()

enable_print()

if mpi_cw.Get_rank() == 0:
    print('\n')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
    print('  Topo test performed succesfully for p2 polynomials !!!')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
else:
    pass
