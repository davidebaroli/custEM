# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import numpy as np
import custEM as ce
from custEM.fem import primary_fields as pf
from custEM.misc import mpi_print as mpp
from custEM.meshgen import mesh2xml2h5
from mpi4py import MPI


class FunctionSpaces:

    """
    FunctionSpace class called from MOD instance.

    class internal functions:
    -------------------------

    - build_old_syntax_spaces()
        deprecated function for FEniCS version < 2016.2 syntax

    - add_primary_fields()
        initialize PrimaryField instance, calculate or import prim. fields

    - add_dirichlet_bc()
        initalize zero-dirichlet boundary conditions on the model boundary

    Description of initialization order and FunctionSpaces:
    -------------------------------------------------------

    1. import mesh (see *init_mesh* documentation)

    2. build FEnICS FunctionSpaces dependent on the mesh, polynomial
       order **p** and the chosen approach

       - S = Nodal (LaGrange) FunctionSpace
           order p and dimension 1
       - V = Nedelec Space
           order p and dimension 3
       - V_cg = Nodal (LaGrange) VectorFunctionSpace
           order p and dimension 3
       - W = mixed Nedelec Space
           order p and dimension 6 for coupled real and imaginary parts
       - W_cg = mixed (LaGrange) VectorFunctionSpace
           order p and dimension 6 for coupled real and imaginary parts
       - M = mixed solution FunctionSpace
           order p and dimension 6 or 8 (dependent on appraoch)

    3. initalize empty solution Function **U** on mixed solution Space

    4. if the 'Secondary' field formulation is used, a PrimaryField
       instance **PF** is added to the FunctionSpace class later on by
       calling **add_Primary_Field()** from the *FE* instance
    """

    def __init__(self, MP, p, test_mode, self_mode, ned_kind, dg_interp,
                 load_existing):

        """
        Initializes FunctionsSpaces dependent on the chosen approach.

        Required arguments:
        -------------------

        - MP, type class
            ModelParameters instance

        - p, type int
            poynomial order

        - test_mode, type bool
            test_mode parameter to choose test_mode
        """

        (self.mesh, self.DOM) = ce.fem.init_mesh(test_mode, MP, self_mode)

        self.approach = MP.approach
        self.anom_flag = True
        self.p = p
        self.test_mode = test_mode

        if ned_kind == '1st':
            self.V = df.FunctionSpace(self.mesh, 'N1curl', p)
        elif ned_kind == '2nd':
            mpp('  -  Notice! Using second kind Nedelec element type  -  ')
            self.V = df.FunctionSpace(self.mesh, 'N2curl', p)
        elif ned_kind == 'div':  # Not in usage right now, just for tests
            mpp('  -  Notice! Using div conforming Nedelec elements  -  ')
            self.V = df.FunctionSpace(self.mesh, 'N1div', p)
        else:
            mpp('Typing error! Nedelec kind must be "1st" or "2nd" '
                '(see FEniCS book for definitions). Aborting  ...')
            raise SystemExit

        self.V_cg = df.VectorFunctionSpace(self.mesh, 'CG', p)
        self.V_dg = df.VectorFunctionSpace(self.mesh, 'DG', p)
        if dg_interp:
            # # # hacked!
            mpp('Warning! Need fix of workaround for discontinuous '
                'interpolation in *fem_base* module to avoid '
                'ambiguities in the code!')
            self.V_cg = df.VectorFunctionSpace(self.mesh, 'CG', p)
            # # # hacked!
            self.V_dg = df.VectorFunctionSpace(self.mesh, 'DG', p)

        if not load_existing:
            self.S = df.FunctionSpace(self.mesh, 'CG', p)
            self.S_DG = df.FunctionSpace(self.mesh, 'DG', 0)
            if ned_kind == '1st':
                ele = df.FiniteElement("N1curl", self.mesh.ufl_cell(), p)
            if ned_kind == '2nd':
                ele = df.FiniteElement("N2curl", self.mesh.ufl_cell(), p)
            if ned_kind == 'div':
                ele = df.FiniteElement("N1div", self.mesh.ufl_cell(), p)

            self.W = df.FunctionSpace(self.mesh, df.MixedElement([ele, ele]))
            if 'E' in self.approach:
                self.M = self.W
            elif 'H' in self.approach:
                self.M = self.W
            elif 'Am' in self.approach or 'Fm' in self.approach:
                ele = df.FiniteElement("N1curl", self.mesh.ufl_cell(), p)
                sce = df.FiniteElement("CG", self.mesh.ufl_cell(), p)
                self.M = df.FunctionSpace(
                        self.mesh, df.MixedElement([ele, ele, sce, sce]))
            elif 'An' in self.approach or 'Fn' in self.approach:
                sce = df.FiniteElement("CG", self.mesh.ufl_cell(), p)
                self.M = df.FunctionSpace(
                        self.mesh, df.MixedElement([sce, sce, sce, sce,
                                                    sce, sce, sce, sce]))
            if 'dc' in self.approach.lower():
                self.M = self.S
            self.U = df.Function(self.M)

    def add_primary_fields(self, MP, calc=True, **kwargs):

        """
        add PrimaryField instance to FunctionSpace class, called by
        *FE* instance if the 'Secondary' field formulation is used.

        Required arguments:
        -------------------

        - MP, type class
            ModelParameters instance

        Keyword arguments:
        ------------------

        see full list in the description of the *PrimaryField* class

        """

        self.PF = pf.PrimaryField(self, MP, **kwargs)
        if calc:
            self.PF.load_primary_fields()

    def add_dirichlet_bc(self, MP=None, dof_re=None, dof_im=None, bc_H=False):

        """
        Initalizes Zero Dichichlet BoundaryConditions for the solution space
        """

        """
        Warning! Inhomoegenous Dirichlet conditions currently not working
        Need to be reimplemented agai.
        Using Zero-Dirichlet conditions insted! Continuing
        """

        class B(df.SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary

        if MP is not None and dof_re is not None and dof_im is not None:
            self.dof_re = dof_re
            self.dof_im = dof_im

        if MP is not None:
            self.PF.load_primary_fields(boundary_fields=True)
            if bc_H:
                pass
                # currently not using IF conditions for H field conversion
                # real_B = df.interpolate(self.PF.H_0_r_cg_B[0], self.V)
                # imag_B = df.interpolate(self.PF.H_0_i_cg_B[0], self.V)
            else:
                real_B = df.interpolate(self.PF.E_0_r_cg_B[0], self.V)
                imag_B = df.interpolate(self.PF.E_0_i_cg_B[0], self.V)
            non_zero = df.Function(self.M)
            non_zero.vector()[self.dof_re] = real_B.vector().get_local()
            non_zero.vector()[self.dof_im] = imag_B.vector().get_local()

            if 'A' in self.approach:
                non_zero.vector()[self.dof_re] = (1./MP.omega) *\
                    imag_B.vector()[:]
                non_zero.vector()[self.dof_im] = (-1./MP.omega) *\
                    real_B.vector()[:]

            self.bc = df.DirichletBC(self.M, non_zero, B())
            self.bc_type = 'ID'
        else:
            if 'E' in self.approach or 'H' in self.approach:
                zero = df.Constant(("0.0", "0.0", "0.0", "0.0", "0.0", "0.0"))
            elif 'dc' in self.approach.lower():
                zero = df.Constant(("0.0"))
            else:
                zero = df.Constant(("0.0", "0.0", "0.0", "0.0",
                                    "0.0", "0.0", "0.0", "0.0"))
            self.bc = df.DirichletBC(self.M, zero, B())
            self.bc_type = 'ZD'


class ModelParameters:

    """
    ModelParameter class called internally from MOD instance.

    class internal functions:
    -------------------------

    - update_model_parameters()
        update model parameters such as resitivities or frequency

    - load_mesh_parameteres()
        update mesh-related model parameters from 'mesh-parameters' file

    Notice for 'Fullspace' models:
    ------------------------------

    If *Fullspace* models should be computed, set sigma_air to
    the same value as sigma_ground!

    Model parameters update:
    ------------------------

    All model parameters can be updated via the related function
    **update_model_parameters()** as follows; a list of available values is
    given below:

    >>> M.MP.update_model_parameteres(sigma_ground=1e-3,
                                      sigma_anom=[1e-2, 1e-1],
                                      omega=1e1)


    Description of model parameters:
    --------------------------------

    - approach and p: see MOD class decumentation, stored for later usage

    - mu = 4 * pi * 1e-7, type float
        magentic permeability (mu_r * mu_0)

    - eps = 8.85 * 1e-12, type float
        electric permittivity

    - omega = 1e1, type float
        angular frequency, note omega = 2 * pi * f (f - frequency)

    - J = 1, type float
        electric source current density for total field formulations

    - sigma_ground = 1e-2, type float or list of float
        subsurface resistivity or subsurface layer resistivities

    - sigma_air = 1e-8, type float or list
        homogeneous air-space conductivity

    - sigma_anom = [], type list of float
        condcutivities for anomaly-domains. Note that conductivity values for
        the subsurafce (**sigma_ground**) and airspace (**sigma_air**) MUST not
        appear in this list!

    - anomaly_layer_markers = [], type list of int
        define the reference subsurface layer (from top to depth) for each
        anomaly to calculate **delta_sigma**. For instance, if two anomalies
        are located in the first and third layer of a three-layered earth,
        the correct choice would be **[1, 3]**. This will be updated with an
        automated mapping soon.

    - sigma = [1e-2, 1e-6], type list of float
        list of all conductivities, the first two values in sigma are always
        **sigma_ground** and **sigma_air**.
        If the latter are updated later on, sigma will be updated as well.
        Further values are related to subsurface layers or anomaly-domains
        in the model (see **sigma_anom**). Note: updated internally

    - delta_sigma = [0., 0.], type list of float
        conductivity differences, needed for 'Secondary' field formulations.
        Note: updated internally

    - n_layers = 1, type int
        number of subsurface layers, automatically imported from
        mesh-parameter file

    - depth = [], type list of float
        depths of subsurface-layer interfaces, automatically imported from
        mesh-parameter file

    - thick = [0.], type list of float
        subsurface layer thicknesses, calculated automatically from **depths**

    - topo = None, type str
        definition of DEM- or synthetic topography, imported automatically from
        mesh-parameter file.

    - eps_for_coord_search = 1e-6, type float
        Needed for evaluation of correct source dofs for total field formu-
        lations. Might be changed but the default choice usually works fine.

    - procs_per_proc = 1, type int
        change, if more processes should be used to calculate primary fields
        with 'pyhed' than specified via the mpirun command (e.g., -n 12) for
        sloving the main FEm problem. For instance, a value of **3** would lead
        to 36 processes used for the primary field calculation.

    - all other parameters are descibed in the MOD class and just stored in
      the model paramter instance for piping them to other submodules.
    """

    def __init__(self, str_args, test_mode):

        """
        Initializes physical and modeling parameters.

        Required arguments:
        -------------------

        - str_args, type list of str
            list of strings containing mainly name conventions

        - test_mode, type bool
            specify if test_mode should be applied (default = **False**)

        """

        self.mod_name = str_args[0]
        self.mesh_name = str_args[1]
        self.approach = str_args[2]
        self.file_format = str_args[3]
        self.m_dir = str_args[4]
        self.r_dir = str_args[5]
        self.para_dir = str_args[6]
        self.out_dir = str_args[7]
        self.out_name = str_args[8]
        self.mute = str_args[9]
        self.mpi_cw = str_args[10]
        self.mpi_cs = str_args[11]

        self.mu = 4.0 * df.pi * 1e-7
        self.eps_0 = 8.85 * 1e-12
        self.eps_r = [1.]
        self.omega = 1e1
        self.J = 1e0
        self.sigma_ground = [1e-2]
        self.sigma_air = [1e-8]
        self.sigma = [self.sigma_air[0], self.sigma_ground[0]]
        self.sigma_anom = []
        self.anomaly_layer_markers = []
        self.delta_sigma = [0., 0.]
        self.n_layers = 1
        self.depth = []
        self.thick = [0.]
        self.a_tol = 1e-6
        self.procs_per_proc = 1
        self.topo = None
        self.tensor_flag = False

        if not test_mode:
            self.load_mesh_parameters(self.para_dir, self.mesh_name)
            self.thick = np.abs(np.diff(np.append(np.array(self.thick),
                                        np.array(self.depth)))).tolist()

    def update_model_parameters(self, **mod_kwargs):

        """
        Updates all non-default physical parameters if called. A list of
        available arguments is given in the description of ModelingParameters.
        """

        for key in mod_kwargs:
            if key not in self.__dict__:
                mpp('Warning! Unknown MODEL parameter set:', key)

        self.__dict__.update(mod_kwargs)

        if type(self.sigma_ground) is not list:
            self.sigma_ground = [self.sigma_ground]
        if type(self.sigma_air) is not list:
            self.sigma_air = [self.sigma_air]

        if self.n_layers != len(self.depth) + 1 or \
           self.n_layers != len(self.sigma_ground):
            mpp('Fatal Error! Number of given layer thicknesses or '
                'resistivities does not match the number of layers !!!\n'
                'n_layers          :    ' + str(self.n_layers) + '\n'
                'len(depths) + 1   :    ' + str(len(self.depth) + 1) + '\n'
                'len(sigma_ground) :    ' + str(len(self.sigma_ground)) + '\n')
            raise SystemExit

        self.eps = (np.array(self.eps_r) * self.eps_0).tolist()
        self.sigma[0] = self.sigma_air[0]
        self.sigma[1] = self.sigma_ground[0]
        self.sigma.extend(self.sigma_ground[1:])
        self.sigma.extend(self.sigma_anom)

        if not all([type(sig) is float for sig in self.sigma]):
            self.tensor_flag = True

        if self.n_layers == 1:
            self.anomaly_layer_markers = np.ones(len(self.sigma_anom),
                                                 dtype=int).tolist()

        elif len(self.sigma_anom) != len(self.anomaly_layer_markers):
            mpp('Fatal Error! Number of given anomaly resistivities ({}) '
                'does not match the number of anomaly_layer_markers ({}) !!!'
                .format(len(self.sigma_anom), len(self.anomaly_layer_markers)))
            raise SystemExit

        for j in range(self.n_layers - 1):
            self.delta_sigma.extend([0.])

        for j in range(len(self.sigma_anom)):

            tmp = self.sigma_ground[self.anomaly_layer_markers[j] - 1]
            if not isinstance(self.sigma_anom[j], float) or \
               not isinstance(tmp, float):
                self.tensor_flag = True
                if isinstance(self.sigma_anom[j], float):
                    self.sigma_anom[j] = df.as_matrix(
                            ((self.sigma_anom[j], 0., 0.),
                             (0., self.sigma_anom[j], 0.),
                             (0., 0., self.sigma_anom[j])))

                if isinstance(tmp, float):
                    tmp = df.as_matrix(((tmp, 0., 0.),
                                        (0., tmp, 0.),
                                        (0., 0., tmp)))
            self.delta_sigma.extend([self.sigma_anom[j] - tmp])
            self.sigma_ground.extend([tmp])

        if bool(mod_kwargs) is False:
            mpp('Warning! No parameters or wrong parameters updated!')

        mpp('MODEL parameters update:')
        for k, v in sorted(self.__dict__.items()):
            if isinstance(v, (tuple, list)) and not self.tensor_flag:
                try:
                    to_print = np.asanyarray(v)
                except ValueError:
                    to_print = v
            else:
                to_print = v
            mpp('-->  {:<22}'.format(str(k)) + '  =  ', to_print,
                pre_dash=False)

    def load_mesh_parameters(self, para_dir, mesh_name):

        """
        import mesh parameters such as the number of domains or topography
        information
        """

        self.centering = False
        self.topo = None
        self.easting_shift = None
        self.northing_shift = None
        self.rotation = None
        if os.path.isfile(para_dir + '/' + mesh_name + '_parameters.txt'):

            params = np.genfromtxt(para_dir + '/' + mesh_name +
                                   '_parameters.txt', dtype=None,
                                   encoding=None)
            self.topo = params[0]

            if 'True' in params[1]:
                self.centering = True
            if 'None' not in params[2]:
                self.easting_shift = float(params[2])
            if 'None' not in params[3]:
                self.northing_shift = float(params[3])
            if 'None' not in params[4]:
                self.rotation = float(params[4])
            self.n_layers = int(float(params[5]))
            for j in range(6, len(params)):
                self.depth.extend([float(params[j])])
        else:
            mpp('Warning! Mesh parameter file could not be found !\n'
                '...  continuing  ...')
            self.n_layers = 1


class Domains:

    """
    Domain class called internally from FunctionSpace instance.

    class internal functions:
    -------------------------

    - add_anomaly()
        deprecated, add an anomaly domain in the 'FEniCS-way'

    Add anomalies if not predefined in the mesh:
    --------------------------------------------

    With the **add_anomaly()** function custom anomalies, e.g. predefined
    anomaly instances from the file misc/anomaly_expressions.py or user-
    defined dolfin expressions can be added to the model. Each anomaly
    gets a new marker. Example usage:

    >>> from custEM.misc import anomaly_expressions as ae
    >>> MOD.FS.DOM.add_anomaly(ae.Plate(origin=[0.0, -100.0, -400.0])
    """

    def __init__(self, mesh, n_dom, domain_func=None, domain_vals=None):

        """
        Defines Domains as marked by the MeshGenerator (here TetGen) in the
        imported mesh. If no domain markers are available, the default domains
        are *ground* (z < 0 + eps) and *air* (z > 0 - eps).

        Required arguments:
        -------------------

        - mesh, type dolfin Mesh
            imported mesh

        - n_dom = Number of identified domains, type int
            if domain markers are available, they will be identified in the
            function *init_mesh*

        - domain_func, type dolfin MeshFunction
            initialized by function *init_mesh* if mesh contains domain markers

        - domain_vals, type list of int
            list of domain markers identified by function *init_mesh*
        """

        uniform_domain_func = df.MeshFunction("size_t", mesh,
                                              mesh.topology().dim())
        uniform_domain_func.set_all(0)
        self.dx_0 = df.Measure('dx', subdomain_data=uniform_domain_func)

        if n_dom == 1:
            try:
                from custEM.misc import Ground, Air
            except ModuleNotFoundError or ImportError:
                mpp('Warning! Definitions for "Ground" and "Air" -space could'
                    ' not be imported! Continuing  ...')
            self.domain_func = df.MeshFunction("size_t", mesh,
                                               mesh.topology().dim())
            self.domain_func.set_all(0)
            self.ground = Ground()
            self.air = Air()
            self.ground.mark(self.domain_func, 0)
            self.air.mark(self.domain_func, 1)
            self.dx = df.Measure('dx', subdomain_data=self.domain_func)
            self.n_adomains = 0
            self.n_domains = self.n_adomains + 2
        else:
            self.domain_func = domain_func
            self.domain_vals = domain_vals
            self.dx = df.Measure('dx', subdomain_data=self.domain_func)
            self.n_domains = n_dom

    def add_anomaly(self, Instance):

        anom = Instance
        anom.mark(self.domain_func, self.n_domains)
        self.n_adomains += 1
        self.n_domains += 1


def init_mesh(test_mode, MP, self_mode):

    """
    Imports the requested mesh and identifies domain markers, if available.
    If the mesh could not be found, the test_mode will be activated. The
    input parameteres are described in the documentation of the MOD class!
    """

    path = os.getcwd()
    if not MP.file_format == 'h5':
        if not MP.file_format == 'xml':
            mpp('Fatal Error, specify correct file format, either "h5" '
                '(default) or "xml"!')
            raise SystemExit

    if os.path.isfile(MP.m_dir + '/_' + MP.file_format + '/' + MP.mesh_name +
                      '.' + MP.file_format) is False and \
       os.path.isfile(MP.m_dir + '/_mesh/' + MP.mesh_name + '.mesh') is True:

        mpp('...  mesh not available in "' + MP.file_format + '" format yet, '
            'conversion started  ...')
        if df.MPI.rank(MP.mpi_cw) == 0:
            mesh2xml2h5(MP.mesh_name, MP.m_dir)
        else:
            pass
        mpp('...  conversion finished.', pre_dash=False)
    os.chdir(MP.m_dir + '/_' + MP.file_format + '/')
    comm = MPI.COMM_WORLD

    if test_mode is not False:

        mpp('Alert: Test mode activated!!! \n'
            'Using 48000 cell +- 10 km dimensions UnitCubeMesh ...')
        mesh = df.UnitCubeMesh(10, 10, 10)
        mesh.coordinates()[:] = (mesh.coordinates()[:] - 0.5) * 200.
        n_domains = 1
        DOM = Domains(mesh, n_domains)

        if MP.file_format == 'h5':
            f = df.HDF5File(MP.mpi_cw, 'UnitCubeMesh.h5', "w")
            f.write(mesh, "/mesh")
            domains = df.MeshFunction("size_t", mesh,
                                      mesh.topology().dim())
            f.write(domains, "/domains")
            f.close()
        elif MP.file_format == 'xml':
            mpp('Warning! TEST_MODE and "xml" file format buggy for '
                'interpolation!')
        os.chdir(path)
    else:

        if self_mode:
            if os.path.isfile(MP.mesh_name + '.' + MP.file_format) is \
               False and MP.mesh_name is not 'UnitCubeMesh':
                print('Fatal error! The specified mesh does not exist!')
                raise SystemExit

            elif MP.file_format == 'h5':
                mesh = df.Mesh(MP.mpi_cs)
                hdf5 = df.HDF5File(MP.mpi_cs,
                                   MP.mesh_name + '.' + MP.file_format, "r")
                hdf5.read(mesh, '/mesh', False)
                domain_func = df.MeshFunction("size_t", mesh,
                                              mesh.topology().dim())
                hdf5.read(domain_func, "/domains")
                domain_vals = np.unique(domain_func.array())
                if not MP.mute:
                    mpp('...  detected ' + str(len(domain_vals)) + ' domains.')
                DOM = Domains(mesh, len(domain_vals), domain_func, domain_vals)
                os.chdir(path)

            elif MP.file_format == 'xml':
                mesh = df.Mesh(MP.mpi_cs,
                               MP.mesh_name + '.' + MP.file_format)
                domain_func = df.MeshFunction("size_t", mesh, MP.mesh_name +
                                              '_domains.' + MP.file_format)
                domain_vals = np.unique(domain_func.array())
                if not MP.mute:
                    mpp('...  detected ' + str(len(domain_vals)) + ' domains.')
                DOM = Domains(mesh, len(domain_vals), domain_func, domain_vals)
                os.chdir(path)
            else:
                mpp('Typing Error! Specify correct mesh format "h5"/"xml"')
                raise SystemExit

        else:
            if os.path.isfile(MP.mesh_name + '.' + MP.file_format) is False:

                mpp('Fatal error! Specified mesh: "' + MP.mesh_name + '" '
                    'could not be found! Aborting ...')
                raise SystemExit

            if MP.file_format == 'h5':
                mesh = df.Mesh()
                hdf5 = df.HDF5File(MP.mpi_cw,
                                   MP.mesh_name + '.' + MP.file_format, "r")
                hdf5.read(mesh, '/mesh', False)
                domain_func = df.MeshFunction("size_t", mesh,
                                              mesh.topology().dim())
                hdf5.read(domain_func, "/domains")
                os.chdir(path)
                dom_markers = comm.gather(domain_func.array()[:], root=0)

                if comm.Get_rank() == 0:
                    dm = []
                    for j in range(comm.Get_size()):
                        dm.extend(dom_markers[j])
                    doms = np.unique(np.array(dm))
                else:
                    doms = None

                domain_vals = comm.bcast(doms, root=0)
                if not MP.mute:
                    mpp('...  detected ' + str(len(domain_vals)) + ' domains.',
                        pre_dash=False)
                DOM = Domains(mesh, len(domain_vals), domain_func, domain_vals)

            elif MP.file_format == 'xml':
                mesh = df.Mesh(MP.mesh_name + '.' + MP.file_format)
                domain_func = df.MeshFunction("size_t", mesh, MP.mesh_name +
                                              '_domains.' + MP.file_format)
                mesh2 = df.Mesh(MP.mpi_cs,
                                MP.mesh_name + '.' + MP.file_format)
                domain_func2 = df.MeshFunction("size_t", mesh2,
                                               mesh2.topology().dim(),
                                               mesh2.domains())
                domain_vals = np.unique(domain_func2.array())
                if not MP.mute:
                    mpp('...  detected ' + str(len(domain_vals)) + ' domains.')
                DOM = Domains(mesh, len(domain_vals), domain_func, domain_vals)
                os.chdir(path)

            else:
                mpp('Typing Error! Specify correct mesh format "h5"/"xml"')
                raise SystemExit

    return(mesh, DOM)
