# -*- coding: utf-8 -*-
"""
meshgen
=======

Submodules:

- **meshgen_tools** for tetrahedral mesh generation
- **meshgen_utils** providing utility functions for mesh generation
- **mesh_convert** for automated conversion of *.mesh* files to *.xml* or *.h5*
- **dem_interpolator** for interpolating real digital elevation data

################################################################################
"""

from . meshgen_tools import *
from . meshgen_utils import *
from . mesh_convert import mesh2xml2h5
from . dem_interpolator import DEM
from . bathy_tools import Bathy

# THE END


