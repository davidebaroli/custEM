#!/bin/bash

echo "Running basic custEM tests ..."
export OMP_NUM_THREADS=1
log_file=/home61/rochlitz/conda_build/test.log
echo $log_file
python clean_up.py
echo "Log file initialized" > $log_file
mpirun -n 2 python -u test_basics.py >> $log_file & PID1=$!
mpirun -n 12 python -u test_fd_approaches.py >> $log_file & PID2=$!
mpirun -n 32 python -u test_topo_model.py >> $log_file & PID3=$!

wait $PID1
wait $PID2
wait $PID3

python clean_up.py