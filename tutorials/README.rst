
The four tutorial files contain most of the commands and parameter options
which are currently available in custEM. The user can comment or uncomment
all arguments and keyword arguments to explore the different functionalities
of custEM.
	
NOTICE
======

* Secondary field computations and interpolation are not possible if the
  *run_tutorial* is started as *jupyter notebook*. For running this
  tutorial, please use the provided **run_tutorial.py** python script and call
  it from the command line with the *mpirun* syntax, e.g.:

    --> mpirun -n x python run_tutorial.py

  with x, the number of cores.

Without modification, all python scripts should run without errors to illustrate
the usage of custEM. The provided "run" example requires about 5 GB of RAM
and can be conducted in serial or parallel (please remember that even in serial,
the "mpirun -n 1 python run_tutorial.py" syntax is currently required.

The *html* docs are also accessible from the main documentation of **custEM** 
The *run_tutorial* is independed from the **meshgen_tutorial**, but the
**plot_tutorial** depends on the **run_tutorial**. Therefore, for using the 
**plot_tutorial** *jupyter notebook* or python script, it is necessary to run
the FEM computations as explained above.
   
################################################################################
