
The results of the four examples presented in Rochlitz et. al. (2018) can be
reproduced by executing the provided python files in the corresponding
sub-directories. For further detailed instructions, we refer to the provided 
*readme.txt* files for each example.

################################################################################
