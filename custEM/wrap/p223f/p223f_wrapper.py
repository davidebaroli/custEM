# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 15:14:20 2016

@author: Anonymous
"""

from __future__ import print_function
import numpy as np
import os


"""
Wrapper for the CSIRO P223f modeling tools (Raiche, 2007).

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
# # # # #        development of this tool was stopped for now         # # # # #
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
# # # # #              it might be reconsidered in future.            # # # # #
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
"""


class Leroi:

    def __init__(self, receiver_coordinates, transmitter_coordinates,
                 resistivities, thicknesses, conductances, plates):

        self.f_fact = 1.

        self.r_coords = receiver_coordinates
        self.t_coords = transmitter_coordinates
        self.zero = 0
        self.one = 1
        self.mod_name = 'My_titel_was_not_changed'
        self.FD = True
        self.B = True
        self.dod3 = 3
        self.n_wave = 4
        self.krxw = 1
        self.rt = 0.
        self.offtime = 500.
        self.tx_amp = 1.
        self.on_ramp = 1.5
        self.off_ramp = 0.086
        self.survey_type = 1
        self.survey_azimuth = 90.
        self.gates_file = 'default_gates.txt'
        self.frequency_file = 'default_frequencies.txt'
        self.lines = [1001]
        self.source = 'Loop'
        self.receiver = 'Magentic'
        self.components = 'Z'
        self.n_transmitters = 1
        self.n_transmitter_turns = 1
        self.idtx = 1
        self.rx_moment = 0.
        self.cell_size = 25.
        self.mod_dir = '../../results/Leroi/'

        self.res = resistivities
        self.thick = thicknesses
        self.cond = conductances
        self.plates = plates
        self.n_layers = len(self.res)
        self.n_lithologies = len(self.cond) + len(self.res)
        self.n_plates = len(self.plates)
        self.print_model_parameters = True

        self.set_input_dependent_parameteres()

    def update_modeling_parameters(self, **kwargs):

        for key in kwargs:
            if key not in self.__dict__:
                print('!!!                   Warning                   !!! \n '
                      'Unknown keyword argument set for Leroi Model:', key)
                print("... Let's stop before something unexpected happens ...")
                raise SystemExit
        self.__dict__.update(kwargs)

        self.set_input_dependent_parameteres()
        self.check_if_setup_is_currently_supported()

    def set_input_dependent_parameteres(self):

        # general stuff

        if not self.FD:
            self.fdtd = 1
        else:
            self.fdtd = 2

        if not self.B:
            self.step = 0
            self.units = 11
        else:
            self.step = 1
            self.units = 21

        if self.receiver == 'Magentic':
            self.r_type = 1

        if os.path.isdir(self.mod_dir + self.mod_name):
            pass
        else:
            os.makedirs(self.mod_dir + self.mod_name)

        # transmitter stuff

        if self.fdtd is 1:

            self.tx_waveform = np.array(([0., 0.], [self.on_ramp, self.tx_amp],
                                         [self.offtime - self.off_ramp,
                                          self.tx_amp], [self.offtime, 0.]))

            self.gates = np.loadtxt(self.gates_file)
            if len(self.gates) > 30:
                self.reduce_gates()

        else:
            self.frequencies = np.loadtxt(self.frequency_file) * self.f_fact
            self.n_frequencies = len(self.frequencies)

        # receiver stuff

        self.n_recs = len(self.r_coords)
        if self.source == 'Loop':
            self.source_type = 1
        elif self.source == 'Line':
            self.source_type = 2
        self.n_transmitter_verts = len(self.t_coords)
        print(self.source_type)

        if self.components is 'Z':
            self.comp = 3
        elif self.components is 'Y':
            self.comp = 2
        elif self.components is 'X':
            self.comp = 1
        elif self.components is 'XY':
            self.comp = 12
        elif self.components is 'YZ':
            self.comp = 23
        elif self.components is 'XZ':
            self.comp = 13
        elif self.components is 'XYZ':
            self.comp = 123
        else:
            print('Fatal error! - Choose either "X", "Y, "Z" or a combination '
                  'of more than one component, e.g "XYZ" !')
            raise SystemExit

        # lithology stuff

        self.resistivities = np.zeros((self.n_lithologies, 7))
        self.lithologies = np.zeros((self.n_layers - 1, 2))

        for j in range(self.n_layers):
            self.resistivities[j, :] = [self.res[j], -1, 1, 1, 0, 0, 1]
        for j in range(self.n_layers, self.n_lithologies):
            self.resistivities[j, :] = [-1, self.cond[j - self.n_layers],
                                        1, 1, 0, 0, 1]
        for j in range(self.n_layers - 1):
            self.lithologies[j, :] = [j + 1, self.thick[j]]

        if self.print_model_parameters is True:

            print('Relevant modeling parameters:')
            for k, v in self.__dict__.items():
                if k is not 't_or_s':
                    print('    ' + str(k) + '   -->  ', v)

    def write_leroi_input_file(self):

        file = open('Leroi.cfl', 'w')

        # write record 1, Title

        file.writelines("%s\n" % self.mod_name)

        # write record 2, TDFD, DO3D, ISYS, PRFL, ISTOP

        file.writelines("%i %i %i %i %i\n" % (self.fdtd, self.dod3, self.zero,
                                              self.one, self.zero))

        # # # time-domain # # #

        if self.fdtd is 1:

            # write record 3, STEP, NSX, NCHNL, KRXW, REFTYM, OFFTIME

            file.writelines("%i %i %i %i %8.3f %8.3f\n" % (self.step,
                                                           self.n_wave,
                                                           len(self.gates),
                                                           self.krxw,
                                                           self.rt,
                                                           self.offtime))

            # write record 4, Tx waveform and start and stop of gates

            for j in range(self.n_wave):
                file.writelines('{:7.3f} {:7.3f}\n'.format(
                                *self.tx_waveform[j]))

            for j in range(len(self.gates)):
                file.writelines('{:7.3f} {:7.3f}\n'.format(*self.gates[j]))

        # # # frequency-domain # # #

        else:

            # write record 3: NFRQ

            file.writelines("%i\n" % (self.n_frequencies))

            # write record 4, Frequencies

            for j in range(self.n_frequencies):
                file.writelines('{:7.2f} {:5.2f}\n'.format(
                                *self.frequencies[j]))

        # write record 5, Survey type

        file.writelines("%d\n" % (self.survey_type))

        # write record 6, NLINES, MRXL, NTX, SOURCE_TYPE, MVRTX, NTRN

        file.writelines("%i %i %i %i %i %i\n" % (len(self.lines),
                                                 len(self.r_coords),
                                                 self.n_transmitters,
                                                 self.source_type,
                                                 self.n_transmitter_verts,
                                                 self.n_transmitter_turns))

        # write record 7, NVRTX, Transmitter-Z-valus

        # !!! Loop needs to be implemented for more than 1 Transmitter !!!

        file.writelines("%d %d\n" % (self.n_transmitter_verts, self.zero))
        for j in range(self.n_transmitter_verts):
            file.writelines('{:10.3f} {:10.3f}\n'.format(
                            *self.t_coords[j][:2]))

        # write record 8, LINE IDTX, RX_TYPE, NRX, UNITS
        #  and next line: CMP SV_AZM, KNORM, IPLT, IDH, RXMNT

        # Note RX_TYPE and NRX are flipped in the Leroi manual :-)
        # !!! Loop needs to be implemented for more than 1 Transmitter !!!

        file.writelines("%i %i %i %i %i\n" % (self.lines[0], self.idtx,
                                              self.r_type, len(self.r_coords),
                                              self.units))

        file.writelines("%i %8.3f %i %i %i %8.2f\n" % (self.comp,
                                                       self.survey_azimuth,
                                                       self.zero, self.one,
                                                       self.zero,
                                                       self.rx_moment))
        # write record 9, Receiver coordinates

        for j in range(len(self.r_coords)):
            file.writelines('{:10.3f} {:10.3f} {:10.3f}\n'.format(
                            *self.r_coords[j]))

        # write record 10, N_Layers, N_Plates, N_Liths

        file.writelines("%i %i %i\n" % (self.n_layers, self.n_plates,
                                        self.n_lithologies))

        # write record 11 + 12, Lithology description

        for j in range(len(self.resistivities)):
            file.writelines('{:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} '
                            '{:8.3f}\n'.format(*self.resistivities[j]))
        for j in range(len(self.lithologies)):
            file.writelines('{:3.0f} {:8.3f}\n'.format(*self.lithologies[j]))

        # write last line of record 12 and record 13, CELL_W

        file.writelines("%3.0f\n%6.2f\n" % (self.n_layers, self.cell_size))

        # write records 14 + 15, Plate descriptiom

        for j in range(self.n_plates):

            file.writelines('{:8.3f} {:8.3f} {:8.3f} {:8.3f}\n'.format(
                            *self.plates[j][:4]))
            file.writelines('{:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f}\n'.format(
                            *self.plates[j][4:]))

            # finished record writing

        file.close()

    def read_leroi_output_file(self):

        file = open('Leroi.out')

        raw_result = []

        if self.fdtd is 1:

            self.transients = np.zeros((self.n_recs, self.n_gates, 3))

        else:

            self.inphases = np.zeros((self.n_recs, self.n_frequencies, 3))
            self.quadratures = np.zeros((self.n_recs, self.n_frequencies, 3))

        for line in file:
            if len(line.strip()) > 0:
                raw_result.append(line.strip())

        for i, line in enumerate(raw_result):

            if line.split()[0] == 'X' and raw_result[i+2][0] == 'R':

                self.fill_arrays(raw_result, i, 0, line.split()[1][0])

            if line.split()[0] == 'Y' and raw_result[i+2][0] == 'R':

                self.fill_arrays(raw_result, i, 1, line.split()[1][0])

            if line.split()[0] == 'Z' and raw_result[i+2][0] == 'R':

                self.fill_arrays(raw_result, i, 2, line.split()[1][0])

    def fill_arrays(self, raw_result, n, comp, inphase_or_quad):

        if self.fdtd is 1:

            for j in range(0, self.n_recs):
                temp_decay = []
                for elem in raw_result[n + j + 4].split():
                    temp_decay.append(float(elem))
                temp_decay = temp_decay[4:]
                self.transients[j, :, comp] = temp_decay

        else:

            for j in range(0, self.n_recs):
                temp_decay = []
                for elem in raw_result[n + j + 4].split():
                    temp_decay.append(float(elem))
                temp_decay = temp_decay[4:]

                if inphase_or_quad is 'I':

                    self.inphases[j, :, comp] = temp_decay

                elif inphase_or_quad is 'Q':

                    self.quadratures[j, :, comp] = temp_decay

    def check_if_setup_is_currently_supported(self):

        if self.survey_type is not 1 and self.surveytype is not 2:
            print('Fatal error! - Choose either "Loop" or "Line" source!')
            raise SystemExit
        if self.receiver is not 'Magentic':
            print('So far, only magentometer- and coil-receiver are supported,'
                  ' exiting ... ')
            raise SystemExit
        if len(self.lines) is not 1:
            print('So far, only 1 receiver line is supported, exiting ... ')
            raise SystemExit
        if self.n_transmitters is not 1:
            print('So far, only 1 transmitter position is supported, '
                  'exiting ... ')
            raise SystemExit

    def reduce_gates(self):
        print('##########################################################')
        print('Leroi is not able to handle more than 34 '
              'gates, assumed are issues with fortran i/o routines!')
        print('Since most user might be interested in the late-time '
              'response, the last 34 gates were taken.')
        print('##########################################################')
        self.gates = self.gates[-34:, :]
