# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import sample_topo_func
from custEM.misc import block_print
from custEM.misc import enable_print
from mpi4py import MPI


"""
Create halfspace-like test mesh with synthetic sample topography.
"""


def create_topo_mesh():

    M = BlankWorld(name='sample_topo_mesh',
                   m_dir='meshes',
                   topo=sample_topo_func,
                   preserve_edges=True)

    M.build_surface(insert_loops=[mu.loop_r(start=[-500., -500.],
                                            stop=[500., 500.], n_segs=100)],
                    insert_lines=[mu.line_x(start=-5e3, stop=5e3, n_segs=100),
                                  mu.line_y(start=-5e2, stop=5e2, n_segs=10)])
    M.build_layered_earth_mesh(n_layers=3, layer_depths=[-500., -1200.])

    M.add_paths([mu.line_x(start=-5e3, stop=5e3, y=0., z=50., n_segs=200,
                           topo=M.topo, t_dir=M.t_dir)])

    M.add_plate(dx=500., dy=500., dz=100.,
                origin=[0.0, 0.0, -200.],
                dip=25.,
                dip_azimuth=17.,
                cell_size=1e3)

    M.there_is_always_a_bigger_world(1e1, 1e1, 1e1)

    M.call_tetgen(tet_param='-pq1.4aAQ', export_vtk=True)


if __name__ == "__main__":

    block_print()
    parent = MPI.Comm.Get_parent()
    create_topo_mesh()
    parent.Barrier()
    parent.Disconnect()
    enable_print()
