#####################
Maintenance of custEM
#####################

This file will be updated from time to time with more experience in providing
software packages. Curerntly used as memory aid for releaseing new versions.

################################################################################

**Steps to consider before building new conda version**

1. check version string and release date in 'custEM/VERSION.rst' file 

2. check if changes affect functionality of the *examples* 

3. check if changes affect functionality of the *tutorials*

4. check if nomenclature changes require new sphinx docs:

    * change to *custEM/docs* directory
    * update submodules by calling 'sphinx-apidoc --force -o . ../custEM'
    * build docs loaclly by calling 'make html', if desired

5. unset PYTHONPATH and conda deactivate before building new conda packages!

6. build custem conda package with --python 3.7 instead of specifying in meta 