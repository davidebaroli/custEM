# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
from custEM.misc import mpi_print as mpp
from custEM.core import Solver
import time
from mpi4py import MPI


class RHS_assembler:

    """
    Right-hand side "assembly". Setting the current density on corresponsing
    dofs on the right-hand side vector for total field formulations.

    class internal functions:
    -------------------------

    - assemble()
        controls the complete assembly of right- and left-hand side

    - find_dof_coords()
        utility function searching for all Nedelec dofs and whose
        coordinates which belong to the source defined on edges

    - finf_cells()
        for each dof which belongs to the source, search for an element
        which contains the former

    - find_directions()
        evaluate the local direction of the Nedelec dof in the element
        which contains it and switch the signto match the global direction

    - find_and_fill_start_stop_dofs()
        required for the A_Phi approach on mixed elements to set divergence
        term on the start and stop point of grounded wire sources

    - find_for_HED(), find_for_VMD()
        utility functions called by **find_dof_coords()**
    - find_for_Line(), find_for_Loop_R(), find_for_Path()
        utility functions called by **find_dof_coords()**

    - append_lists(), eval_target_dir(),
        utility functions called by the other class internal functions
    """

    def __init__(self, FE, bc):

        """
        Assembly of right-hand side vector

        Required arguments:
        -------------------

        - FE, type class
            FE-approach (e.g., **E_vector**) instance

        - bc, None or type dolfin DirichletBC
            see **MOD** class function: **solve_main_problem()**
        """

        if FE.path is not None and len(FE.path) != FE.n_tx and \
           FE.n_tx != 1:
            mpp('Fatal error! "len(path):" ' + str(len(FE.path)) + ' must be '
                'equal to "n_tx:" ' + str(FE.n_tx) + '. Aborting  ...')
            raise SystemExit
        self.FE = FE
        self.bc = bc

        ownership_offset = self.FE.FS.M.dofmap().ownership_range()
        if 'A' in self.FE.MP.approach:
            self.xyz = self.FE.FS.M.tabulate_dof_coordinates().reshape(-1, 3)
        elif 'DC' in self.FE.MP.approach:
            self.xyz = self.FE.FS.S.tabulate_dof_coordinates().reshape(-1, 3)
            self.nodal_dofs_re = (np.array(self.FE.FS.S.dofmap(
                    ).dofs()) - ownership_offset[0])
        else:
            self.xyz = self.FE.FS.V.tabulate_dof_coordinates().reshape(-1, 3)

        if self.FE.MP.approach in ['E_IE', 'E_RA']:
            self.xyz = self.FE.FS.V.tabulate_dof_coordinates().reshape(-1, 3)
            ownership_offset = self.FE.FS.V.dofmap().ownership_range()
            self.ned_dof_im = (np.array(self.FE.FS.V.dofmap().dofs()) -
                               ownership_offset[0])
        elif 'E' in self.FE.MP.approach or 'H' in self.FE.MP.approach:
            self.ned_dof_re = (np.array(self.FE.FS.M.sub(0).dofmap(
                    ).dofs()) - ownership_offset[0])
            self.ned_dof_im = (np.array(self.FE.FS.M.sub(1).dofmap(
                    ).dofs()) - ownership_offset[0])
        elif 'Am' in self.FE.MP.approach:
            self.all_dofs = (np.array(self.FE.FS.M.dofmap(
                    ).dofs()) - ownership_offset[0])
            self.ned_dof_re = (np.array(self.FE.FS.M.sub(0).dofmap(
                    ).dofs()) - ownership_offset[0])
            self.nodal_dofs_re = (np.array(self.FE.FS.M.sub(2).dofmap(
                    ).dofs()) - ownership_offset[0])
        elif 'An' in self.FE.MP.approach:
            self.x_dofs = (np.array(self.FE.FS.M.sub(0).dofmap().dofs()) -
                           ownership_offset[0])
            self.y_dofs = (np.array(self.FE.FS.M.sub(1).dofmap().dofs()) -
                           ownership_offset[0])
            self.nodal_dofs_re = (np.array(self.FE.FS.M.sub(3).dofmap(
                    ).dofs()) - ownership_offset[0])

        self.mesh_coords = self.FE.FS.mesh.coordinates()
        self.source_dofs, self.tmp_sd = [], []
        self.source_dof_coords, self.tmp_sdc = [], []
        self.target_directions, self.tmp_td = [], []
        self.component_switches, self.tmp_cs = [], []
        self.a_tol = self.FE.MP.a_tol
        self.b = []

        self.Solver = Solver(self.FE.FS, self.FE)

    def assemble(self):

        """
        function which controls the manual right-hand side assembly step by
        step and lets dolfing assemble the left-hand side
        """

        if 'An' in self.FE.MP.approach:
            self.test_An_total()
            return

        mpp('RHS vector and system matrix assembly:')
        t0 = time.time()

        if self.FE.MP.approach != 'DC':
            self.find_dof_coords()
            self.find_cells()
            self.find_directions()

        if self.FE.MP.approach in ['E_IE', 'E_RA']:
            b = df.PETScVector()
            bb = df.inner(df.Constant(("0.0", "0.0", "0.0")), self.FE.v) *\
                self.FE.dx_0
            self.b = df.assemble(bb, tensor=b)
            self.boundary = df.Function(self.FE.FS.V)
            self.b[self.source_dofs[0]] = (self.sign[0] *
                                           self.FE.MP.J /
                                           np.float(self.FE.FS.p))
            self.b.apply('insert')
            return

        A, b = df.PETScMatrix(), df.PETScVector()
        mpp('...  assembling system matrix  ...', pre_dash=False)
        if self.bc is None:
            self.A, bb = df.assemble_system(self.FE.L, self.FE.R,
                                            A_tensor=A, b_tensor=b)
        else:
            if self.bc in ['ZD', 'ZeroDirichlet']:
                self.FE.FS.add_dirichlet_bc()
                mpp('...  applying zero Dirichlet boundary '
                    'conditions  ...', pre_dash=False)
            elif self.bc in ['ID', 'InhomogeneousDirichlet']:
                self.FE.FS.add_dirichlet_bc(self.FE.MP, self.ned_dof_re,
                                            self.ned_dof_im)
                mpp('...  applying inhomogeneous Dirichlet boundary '
                    'conditions  ...', pre_dash=False)
                if self.FE.MP.approach not in ['E_t', 'Am_t']:
                    mpp('Inhomogeneous Dirichlet conditions only supported '
                        'for total electric field or mixed-space potential '
                        'approach (E_t, Am_t) right now. Aborting  ...')
                    raise SystemExit
            else:
                mpp('Fatal error! Choose either zero or inhomogeneous '
                    'Dirichlet boundary conditions (bc="ZD" or "ID", '
                    'if not Neumann conditions by default. Aborting  ...')
                raise SystemExit
            self.A, bb = df.assemble_system(self.FE.L, self.FE.R,
                                            self.FE.FS.bc,
                                            A_tensor=A, b_tensor=b)

        if type(self.FE.MP.J) is not list:
            self.FE.MP.J = [self.FE.MP.J for ji in range(self.FE.n_tx)]

        if 'Am' in self.FE.MP.approach:
            if self.FE.n_tx > 1:
                mpp('Fatal Error! Multiple right-hand sides only for '
                    'total E-field supported for now, not A-Phi!')
                raise SystemExit
            for ti in range(self.FE.n_tx):
                bb[self.source_dofs[0]] = (self.sign[ti] *
                                           (1./self.FE.MP.omega) *
                                           self.FE.MP.J[ti] /
                                           np.float(self.FE.FS.p))
                bb = self.find_and_fill_start_stop_dofs(bb)
                bb.apply('insert')
                self.b.append(bb)

        elif 'E' in self.FE.MP.approach:
            for ti in range(self.FE.n_tx):
                bb[self.source_dofs[ti]] = (
                        -self.sign[ti] * self.FE.MP.omega *
                        self.FE.MP.J[ti] / np.float(self.FE.FS.p))
                bb.apply('insert')
                self.b.append(bb)

        elif 'H' in self.FE.MP.approach:

            for ti in range(self.FE.n_tx):
                vec = df.Function(self.FE.FS.M)
                vec.vector()[self.source_dofs[ti]] = (
                        self.sign[ti] * (1./self.lengths[ti]) *
                        self.FE.MP.J[ti] / np.float(-self.FE.FS.p))
                phi_r, phi_i = df.TestFunctions(self.FE.FS.M)

                bb = df.assemble(
                        df.inner(vec.sub(1),
                                 df.curl(self.FE.inv_s_func * phi_r)) *
                        self.FE.FS.DOM.dx_0 +
                        df.inner(df.Constant(("0.0", "0.0", "0.0")),
                                 phi_i) * self.FE.FS.DOM.dx_0, tensor=b)
                self.b.append(bb)
        elif 'DC' in self.FE.MP.approach:
            for ti in range(self.FE.n_tx):
                bb = df.Function(self.FE.FS.S)
                bb = self.find_and_fill_start_stop_dofs(bb)
                bb.vector().apply('insert')
                self.b.append(bb)

        self.FE.assembly_time = time.time() - t0
        mpp('  -  time elapsed [s]:  -->  ', self.FE.assembly_time,
            pre_dash=False)
        mpp('  -  system matrix size:  -->  ' + str(self.A.mat().getSize()[
                0]), pre_dash=False)

    def find_dof_coords(self):

        """
        utility function searching for all Nedelec dofs and whose
        coordinates which belong to the source defined on edges
        """

        if self.FE.s_type == 'hed':
            self.find_for_HED()

        if self.FE.s_type == 'vmd':
            if 'H' not in self.FE.MP.approach:
                mpp('Fatal error! VMD source only supported for H_vector '
                    'approach right now, but even not working for the latter!')
                raise SystemExit
            self.find_for_VMD()

        elif self.FE.s_type == 'line':
            self.find_for_Line()

        elif self.FE.s_type == 'loop_r':
            self.find_for_Loop_R()

        elif self.FE.s_type == 'loop_c':
            mpp('Fatal error! RHS assembly needs to be implemented for loop_c')
            raise SystemExit

        elif self.FE.s_type == 'path':
            self.find_for_Path()

    def find_cells(self):

        """
        for each dof which belongs to the source, search for an element
        which contains the former
        """

        all_cells = [cell for cell in df.cells(self.FE.FS.mesh)]
        midpoints = np.array([cell.midpoint().array() for cell in all_cells])
        self.cell_list = []
        for ti in range(self.FE.n_tx):
            cell_list, tmp = [], []
            for cc in self.source_dof_coords[ti]:
                tmp.append(np.argmin(np.linalg.norm(
                        cc.reshape(1, 3) - midpoints, axis=1)))
            if len(tmp) > 0:
                cell_list.extend([all_cells[idx] for idx in tmp])
            self.cell_list.append(cell_list)
        self.mp = midpoints
        self.ac = all_cells

    def find_directions(self):

        """
        evaluate the local direction of the Nedelec dof in the element
        which contains it and switch the signto match the global direction
        """

        self.sign = []
        if 'H' in self.FE.MP.approach:
            self.lengths = []
        for ti in range(self.FE.n_tx):
            if 'H' in self.FE.MP.approach:
                lengths = []
            nn, sign = [], []
            for jj, cell in enumerate(self.cell_list[ti]):
                coord = self.source_dof_coords[ti][jj].reshape(1, 3)
                eids = np.array([edge.entities(0) for edge in df.edges(cell)])
                dists = np.linalg.norm(self.mesh_coords[eids[:, 0]] -
                                       coord, axis=1) + \
                    np.linalg.norm(self.mesh_coords[eids[:, 1]] -
                                   coord, axis=1)
                idx = np.argwhere(dists - np.linalg.norm(
                    self.mesh_coords[eids[:, 0]] -
                    self.mesh_coords[eids[:, 1]],
                    axis=1) < self.a_tol)
                if len(idx) > 0:
                    idxx = idx[0][0]
                else:
                    dummy = True
                    counter = 0
                    cc = self.source_dof_coords[ti][jj]
                    arr = np.linalg.norm(cc.reshape(1, 3) - self.mp, axis=1)
                    arr_idx = arr.argsort()
                    arr = arr[arr_idx]
                    while dummy:
                        new_cell = [self.ac[idx] for idx in arr_idx][counter]
                        eids = np.array([edge.entities(0) for edge in
                                         df.edges(new_cell)])
                        dists = np.linalg.norm(self.mesh_coords[eids[:, 0]] -
                                               coord, axis=1) + \
                            np.linalg.norm(self.mesh_coords[eids[:, 1]] -
                                           coord, axis=1)
                        idx = np.argwhere(dists - np.linalg.norm(
                            self.mesh_coords[eids[:, 0]] -
                            self.mesh_coords[eids[:, 1]],
                            axis=1) < self.a_tol)
                        if len(idx) > 0:
                            idxx = idx[0][0]
                            dummy = False
                        else:
                            counter += 1
                            if counter == 100:
                                print('Warning! Assuming wrong source dof '
                                      'identification as no matching cell was '
                                      'found. Please check if the source field'
                                      ' is correct. If not, try changing the '
                                      'model parameter "eps_for_coord_search"'
                                      'or re-meshing your model with other '
                                      'options. Continuing  ...')
                                break
                n = cell.entities(0)
                nn.append([n[0], n[1], n[2], n[3],
                           eids[idxx][0], eids[idxx][1]],)

            for j in range(len(nn)):
                if 'H' in self.FE.MP.approach:
                    lengths.append(np.linalg.norm(self.mesh_coords[nn[j][4]] -
                                                  self.mesh_coords[nn[j][5]]))
                if self.mesh_coords[nn[j][5]][
                        self.component_switches[ti][j]] - \
                   self.mesh_coords[nn[j][4]][
                           self.component_switches[ti][j]] < \
                   0.0 and self.target_directions[ti][j] < 0.0:
                    sign.append(-1.)
                elif (self.mesh_coords[nn[j][5]][
                        self.component_switches[ti][j]] -
                      self.mesh_coords[nn[j][4]][
                              self.component_switches[ti][j]] >
                      0.0 and self.target_directions[ti][j] > 0.0):
                    sign.append(-1.)
                else:
                    sign.append(1.)

                for local_node in range(4):
                    if nn[j][4] == nn[j][local_node]:
                        tmp1 = local_node
                    elif nn[j][5] == nn[j][local_node]:
                        tmp2 = local_node
                if tmp2 < tmp1:
                    sign[j] *= -1.

            n_local_source_dofs = MPI.COMM_WORLD.gather(len(sign),
                                                        root=0)
            n_global_sd = MPI.COMM_WORLD.bcast(np.sum(n_local_source_dofs),
                                               root=0)
            if n_global_sd != 0:
                mpp('...  ' + str(n_global_sd) + ' dofs located. '
                    'Continuing  ...', pre_dash=False)
            else:
                print('Fatal Error! Not a single source dof could be found '
                      'for Tx no."' + str(ti) + '". '
                      'This is most probably caused by a wrong *s_type* or '
                      'source geometry (*start, stop* coodinates) definition!')
                raise SystemExit
            self.sign.append(np.array(sign))
            if 'H' in self.FE.MP.approach:
                self.lengths.append(np.array(lengths))

    def find_and_fill_start_stop_dofs(self, bb, dc=False):

        """
        required for the A_Phi approach on mixed elements to set divergence
        term on the start and stop point of grounded wire sources
        """

        self.start_dof = []
        self.stop_dof = []

        if self.FE.s_type == 'hed':
            print('Error, start and stop dofs evaluation for total potential'
                  ' approaches not implemented for HED sources yet. '
                  'Use a "Line" source with start and stop values instead!')
            raise SystemExit

        if self.FE.s_type in ['line', 'hed']:
            for j in range(len(self.xyz)):
                if df.near(self.xyz[j, 0], self.FE.start[0], self.a_tol) and\
                   df.near(self.xyz[j, 1], self.FE.start[1], self.a_tol) and\
                   df.near(self.xyz[j, 2], self.FE.start[2], self.a_tol):
                    if j in self.nodal_dofs_re:
                        self.start_dof.append(j)

                elif (df.near(self.xyz[j, 0], self.FE.stop[0], self.a_tol)and
                      df.near(self.xyz[j, 1], self.FE.stop[1], self.a_tol)and
                      df.near(self.xyz[j, 2], self.FE.stop[2], self.a_tol)):
                    if j in self.nodal_dofs_re:
                        self.stop_dof.append(j)

        elif self.FE.s_type == 'path' and self.FE.closed_path[0] is False:
            for j in range(len(self.xyz)):
                if df.near(self.xyz[j, 0], self.FE.path[0][0, 0],
                           self.a_tol) and \
                   df.near(self.xyz[j, 1], self.FE.path[0][0, 1],
                           self.a_tol) and \
                   df.near(self.xyz[j, 2], self.FE.path[0][0, 2],
                           self.a_tol):

                    if j in self.nodal_dofs_re:
                        self.start_dof.append(j)

                elif (df.near(self.xyz[j, 0], self.FE.path[0][-1, 0],
                              self.a_tol) and
                      df.near(self.xyz[j, 1], self.FE.path[0][-1, 1],
                              self.a_tol) and
                      df.near(self.xyz[j, 2], self.FE.path[0][-1, 2],
                              self.a_tol)):
                    if j in self.nodal_dofs_re:
                        self.stop_dof.append(j)

        s1 = np.array(self.start_dof).astype('int')
        s2 = np.array(self.stop_dof).astype('int')

        if 'A' in self.FE.MP.approach:
            bb[s1] = self.FE.MP.J[0] * (1./self.FE.MP.omega)
            bb[s2] = -self.FE.MP.J[0] * (1./self.FE.MP.omega)
        elif 'DC' in self.FE.MP.approach:
            bb.vector()[s1] = self.FE.MP.J[0]
            bb.vector()[s2] = -self.FE.MP.J[0]
        return(bb)

    def find_for_HED(self):

        """
        find source dofs and coordinates for HED source
        """

        mpp('  -  using "hed" source  -', pre_dash=False)
        if 'Am' in self.FE.MP.approach:
            mpp('Error! HED source for Am approach on mixed elements not '
                'implemented yet! You may choose a very short line source '
                'instead. Note in that case, the line must start and end '
                'at nodes!')
            raise SystemExit

        self.FE.start[:] = self.FE.origin[:]
        self.FE.stop[:] = self.FE.origin[:]
        if df.near(self.FE.azimuth, 0.):
            self.FE.start[0] = self.FE.origin[0] - self.FE.length/2.
            self.FE.stop[0] = self.FE.origin[0] + self.FE.length/2.
        elif df.near(self.FE.azimuth, 180.):
            self.FE.start[0] = self.FE.origin[0] + self.FE.length/2.
            self.FE.stop[0] = self.FE.origin[0] - self.FE.length/2.
        elif df.near(self.FE.azimuth, 90.):
            self.FE.start[1] = self.FE.origin[1] - self.FE.length/2.
            self.FE.stop[1] = self.FE.origin[1] + self.FE.length/2.
        elif df.near(self.FE.azimuth, 270.):
            self.FE.start[1] = self.FE.origin[0] + self.FE.length/2.
            self.FE.stop[1] = self.FE.origin[0] - self.FE.length/2.
        else:
            mpp('Alert! arbitrary directected HED for total field '
                'approach not implemented yet. Please choose azimuth '
                'values of "0, 90, 180 or 270".')
        self.find_for_Line()

    def find_for_VMD(self):

        """
        find source dofs and coordinates for VMD source
        """

        V = df.VectorFunctionSpace(self.FE.FS.mesh, 'CG', 1)
        xyz = V.tabulate_dof_coordinates().reshape(-1, 3)
        self.Vdiv = df.Function(V)

        mpp('  -  using "vmd" source  -', pre_dash=False)
        ll = []

        for j in range(len(xyz)):

            #    if xyz[j, 0] < 1. + self.a_tol and \
            #       xyz[j, 0] > -1. - self.a_tol and \
            #       xyz[j, 1] < 1. + self.a_tol and \
            #       xyz[j, 1] > -1. - self.a_tol and \
            #       df.near(xyz[j, 2], 0., self.a_tol):
            #        ll.extend([j])
            #        print('here')
            #
            #    if np.sqrt(xyz[j, 0]**2 + xyz[j, 1]**2) < 1.5 +self.a_tol and\
            #       df.near(xyz[j, 2], 0., self.a_tol):
            #        ll.extend([j])
            #        print('here')

            if df.near(xyz[j, 0], 0., 1e-3) and \
               df.near(xyz[j, 1], 0., 1e-3) and \
               df.near(xyz[j, 2], -1., self.a_tol):
                ll.extend([j])
                print('here')

        self.Vdiv.vector()[ll[2::3]] = np.ones(len(ll[2::3])) * self.FE.MP.J
        mpp('VMD source bug! Only enabled for testing')
        # raise SystemExit

    def find_for_Line(self):

        """
        find source dofs and coordinates for "line" source
        """

        if self.FE.start[0] < self.FE.stop[0]:
            x0, x1 = self.FE.start[0], self.FE.stop[0]
        else:
            x1, x0 = self.FE.start[0], self.FE.stop[0]

        if self.FE.start[1] < self.FE.stop[1]:
            y0, y1 = self.FE.start[1], self.FE.stop[1]
        else:
            y1, y0 = self.FE.start[1], self.FE.stop[1]

        if self.FE.start[1] == self.FE.stop[1] and \
           self.FE.start[2] == self.FE.stop[2] and \
           str(self.FE.MP.topo) == 'None':

            mpp('  -  using straight x-directed "line" source  -',
                pre_dash=False)

            for j in range(len(self.xyz)):

                if df.near(self.xyz[j, 1],
                           self.FE.start[1], self.a_tol) and \
                   df.near(self.xyz[j, 2],
                           self.FE.start[2], self.a_tol) and \
                   self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1:
                    self.append_lists(j)

        elif (self.FE.start[0] == self.FE.stop[0] and
              self.FE.start[2] == self.FE.stop[2] and
              str(self.FE.MP.topo) == 'None'):

            mpp('  -  using straight y-directed "line" source  -',
                pre_dash=False)

            for j in range(len(self.xyz)):

                if df.near(self.xyz[j, 0],
                           self.FE.start[0], self.a_tol) and \
                   df.near(self.xyz[j, 2],
                           self.FE.start[2], self.a_tol) and \
                   self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1:
                    self.append_lists(j, comp=1)

        elif self.FE.start[1] == self.FE.stop[1]:

            mpp('  -  using x-directed "line" source with topography  -',
                pre_dash=False)

            for j in range(len(self.xyz)):

                if df.near(self.xyz[j, 1],
                           self.FE.start[1], self.a_tol) and \
                   self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1:
                    self.append_lists(j)

        elif self.FE.start[0] == self.FE.stop[0]:

            mpp('  -  using y-directed "line" source with topography  -',
                pre_dash=False)

            for j in range(len(self.xyz)):

                if df.near(self.xyz[j, 0],
                           self.FE.start[0], self.a_tol) and \
                   self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1:
                    self.append_lists(j, comp=1)

        else:
            mpp('  -  using "line" source with arbitrary direction  -',
                pre_dash=False)
            mpp('Fatal error! This feature is not implemented yet! '
                'Aborting  ...')
            raise SystemExit

        self.source_dofs.append(np.array(self.tmp_sd))
        self.source_dof_coords.append(np.array(self.tmp_sdc))
        self.target_directions.append(np.array(self.tmp_td))
        self.component_switches.append(np.array(self.tmp_cs))

    def find_for_Loop_R(self):

        """
        find source dofs and coordinates for "loop_r" source
        """

        if self.FE.start[0] < self.FE.stop[0]:
            x0, x1 = self.FE.start[0], self.FE.stop[0]
            y0, y1 = self.FE.start[1], self.FE.stop[1]
        else:
            x1, x0 = self.FE.start[0], self.FE.stop[0]
            y1, y0 = self.FE.start[1], self.FE.stop[1]
        z = self.FE.start[2]

        if str(self.FE.MP.topo) == 'None':

            mpp('  -  using rectangular "loop" source with constant height  -',
                pre_dash=False)

            if df.near(self.FE.start[0], self.FE.stop[0], self.a_tol) or \
               df.near(self.FE.start[1], self.FE.stop[1], self.a_tol):

                mpp('Fatal error: Given start and stop coordinates do not '
                    'match to a rectangular loop source: x or y start or '
                    'stop coordinates are identical')
                raise SystemExit

            for j in range(len(self.xyz)):
                if df.near(self.xyz[j, 1], y0, self.a_tol) and \
                   df.near(self.xyz[j, 2], z, self.a_tol) and \
                   self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1:
                    self.append_lists(j)
                elif (df.near(self.xyz[j, 0], x1, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1):
                    self.append_lists(j, comp=1)
                elif (df.near(self.xyz[j, 1], y1, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1):
                    self.append_lists(j, tdir=-1.)
                elif (df.near(self.xyz[j, 0], x0, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1):
                    self.append_lists(j, tdir=-1., comp=1)

        else:

            mpp('  -  using rectangular "loop" source with topography  -',
                pre_dash=False)

            for j in range(len(self.xyz)):

                if df.near(self.xyz[j, 1], y0, self.a_tol) and \
                   self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1:
                    self.append_lists(j)
                elif (df.near(self.xyz[j, 0], x1, self.a_tol) and
                      self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1):
                    self.append_lists(j, comp=1)
                elif (df.near(self.xyz[j, 1], y1, self.a_tol) and
                      self.xyz[j, 0] > x0 and self.xyz[j, 0] < x1):
                    self.append_lists(j, tdir=-1.)
                elif (df.near(self.xyz[j, 0], x0, self.a_tol) and
                      self.xyz[j, 1] > y0 and self.xyz[j, 1] < y1):
                    self.append_lists(j, tdir=-1., comp=1)

        self.source_dofs.append(np.array(self.tmp_sd))
        self.source_dof_coords.append(np.array(self.tmp_sdc))
        self.target_directions.append(np.array(self.tmp_td))
        self.component_switches.append(np.array(self.tmp_cs))

    def find_for_Path(self):

        """
        find source dofs and coordinates for grounded or ungrounded Path source
        grounding (dipole source) or not (loop source) can be controlled with
        the **closed_path** flag in the **FE instance**
        """

        if len(self.FE.path[0][:]) == 3:
            self.FE.path = [self.FE.path]
        if type(self.FE.closed_path) is not list:
            self.FE.closed_path = [self.FE.closed_path]

        for ti, tx in enumerate(self.FE.path):
            if self.FE.closed_path[ti]:
                mpp('  -  building crooked "loop" source from arbitrary path  '
                    '-', pre_dash=False)
                tx = np.vstack((tx, tx[0, :]))
            else:
                mpp('  -  building crooked "line" source from arbitrary path  '
                    '-', pre_dash=False)

            if str(self.FE.MP.topo) != 'None':
                tx = self.add_topo(tx)

            for j in range(len(tx) - 1):
                dists = np.linalg.norm(tx[j].reshape(1, 3) -
                                       self.xyz, axis=1) + \
                        np.linalg.norm(tx[j+1].reshape(1, 3) -
                                       self.xyz, axis=1)
                tmp = np.argwhere(dists - np.linalg.norm(
                        tx[j] - tx[j + 1]) < self.a_tol)
                tdir, comp = self.eval_target_dir(tx[j], tx[j + 1])
                if len(tmp) > 0:
                    for idx in tmp.flatten():
                        self.append_lists(idx, tdir=tdir, comp=comp)
            self.source_dofs.append(np.array(self.tmp_sd))
            self.source_dof_coords.append(np.array(self.tmp_sdc))
            self.target_directions.append(np.array(self.tmp_td))
            self.component_switches.append(np.array(self.tmp_cs))
            self.tmp_sd = []
            self.tmp_sdc = []
            self.tmp_td = []
            self.tmp_cs = []

    def append_lists(self, idx, tdir=1., comp=0):

        """
        utility function appending dofs, coordinates, directions and component
        switches to list during the dof-coordinate search procedure
        """

        if 'E' in self.FE.MP.approach:
            self.tmp_sd.append(self.ned_dof_im[idx])
            self.tmp_sdc.append(self.xyz[idx, :])
            self.tmp_td.append(tdir)
            self.tmp_cs.append(comp)
        elif 'Am' in self.FE.MP.approach:
            if idx in self.ned_dof_re:
                self.tmp_sd.append(self.all_dofs[idx])
                self.tmp_sdc.append(self.xyz[idx, :])
                self.tmp_td.append(tdir)
                self.tmp_cs.append(comp)

    def eval_target_dir(self, a, b):

        """
        evaluate final global direction for each Nedelec dof which has a
        contribution of the source current density
        """

        if np.isclose(np.abs(a[0] - b[0]), np.abs(a[1] - b[1]),
                      atol=self.a_tol):
            if a[2] < b[2]:
                return(1., 2)
            else:
                return(-1., 2)

        elif np.abs(a[0] - b[0]) > np.abs(a[1] - b[1]):
            if a[0] < b[0]:
                return(1., 0)
            else:
                return(-1., 0)
        elif np.abs(a[0] - b[0]) < np.abs(a[1] - b[1]):
            if a[1] < b[1]:
                return(1., 1)
            else:
                return(-1., 1)

    def test_An_total(self):

        """
        !!!  experimental total A-Phi nodal approach,
             use with caution or not at all  !!!
        """

        A, b = df.PETScMatrix(), df.PETScVector()
        self.FE.FS.add_dirichlet_bc()
        mpp('...  applying zero Dirichlet boundary '
            'conditions  ...', pre_dash=False)
        self.A, bb = df.assemble_system(
                self.FE.L, self.FE.R, self.FE.FS.bc,
                A_tensor=A, b_tensor=b)
        if type(self.FE.MP.J) is not list:
            self.FE.MP.J = [self.FE.MP.J]
        self.FE.assembly_time = 999.

        if 'hed' in self.FE.s_type or 'line' in self.FE.s_type:

            dofs = []
            if self.FE.start[1] == self.FE.stop[1] and \
               self.FE.start[2] == self.FE.stop[2] and \
               str(self.FE.MP.topo) == 'None':

                mpp('  -  using straight x-directed "line" source  -',
                    pre_dash=False)

                for j in range(len(self.xyz)):
                    if df.near(self.xyz[j, 1],
                               self.FE.start[1], self.a_tol) and \
                       df.near(self.xyz[j, 2],
                               self.FE.start[2], self.a_tol) and \
                       self.xyz[j, 0] > self.FE.start[0] - self.a_tol and \
                       self.xyz[j, 0] < self.FE.stop[0] + self.a_tol:
                        if j in self.x_dofs:
                            dofs.append(j)
                tx_len = np.abs(self.FE.start[0] - self.FE.stop[0])

            elif (self.FE.start[0] == self.FE.stop[0] and
                  self.FE.start[2] == self.FE.stop[2] and
                  str(self.FE.MP.topo) == 'None'):

                mpp('  -  using straight y-directed "line" source  -',
                    pre_dash=False)
                for j in range(len(self.xyz)):
                    if df.near(self.xyz[j, 0],
                               self.FE.start[0], self.a_tol) and \
                       df.near(self.xyz[j, 2],
                               self.FE.start[2], self.a_tol) and \
                       self.xyz[j, 1] > self.FE.start[1] - self.a_tol and \
                       self.xyz[j, 1] < self.FE.stop[1] + self.a_tol:
                        if j in self.y_dofs:
                            dofs.append(j)
                tx_len = np.abs(self.FE.start[1] - self.FE.stop[1])
            else:
                mpp('Start: ', self.FE.start)
                mpp('Stop: ', self.FE.stop)
                mpp('Topo: ', self.FE.MP.topo)
                mpp('Currently, straight lines in x- or y- direction '
                    'required!  Aborting  ...')
                raise SystemExit

            n_local_source_dofs = MPI.COMM_WORLD.gather(len(dofs), root=0)
            n_global_sd = MPI.COMM_WORLD.bcast(np.sum(n_local_source_dofs),
                                               root=0)
            if n_global_sd != 0:
                mpp('...  ' + str(n_global_sd) + ' dofs located. '
                    'Continuing  ...', pre_dash=False)
            else:
                print('Fatal Error! Not a single source dof could be found '
                      'for Tx no."' + str(0) + '". '
                      'This is most probably caused by a wrong *s_type* or '
                      'source geometry (*start, stop* coodinates) definition!')
                raise SystemExit

            if self.FE.n_tx > 1:
                mpp('Fatal Error! Multiple right-hand sides only for '
                    'total E-field supported for now, not A-Phi!')
                raise SystemExit

            for ti in range(self.FE.n_tx):
                bb[dofs] = -tx_len * (1./(n_global_sd)) *\
                         (1./self.FE.MP.omega) * self.FE.MP.J[ti]
                bb = self.find_and_fill_start_stop_dofs(bb)
                bb.apply('insert')
                self.b.append(bb)

        elif 'loop_r' in self.FE.s_type:

            dofs = [[], [], [], []]
            if self.FE.start[0] < self.FE.stop[0]:
                x0, x1 = self.FE.start[0], self.FE.stop[0]
                y0, y1 = self.FE.start[1], self.FE.stop[1]
            else:
                x1, x0 = self.FE.start[0], self.FE.stop[0]
                y1, y0 = self.FE.start[1], self.FE.stop[1]
            z = self.FE.start[2]

            for j in range(len(self.xyz)):
                if df.near(self.xyz[j, 1], y0, self.a_tol) and \
                   df.near(self.xyz[j, 2], z, self.a_tol) and \
                   self.xyz[j, 0] > x0 - self.a_tol and \
                   self.xyz[j, 0] < x1 + self.a_tol:
                    if j in self.x_dofs:
                        dofs[0].append(j)
                elif (df.near(self.xyz[j, 0], x1, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 1] > y0 - self.a_tol and
                      self.xyz[j, 1] < y1 + self.a_tol):
                    if j in self.y_dofs:
                        dofs[1].append(j)
                elif (df.near(self.xyz[j, 1], y1, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 0] > x0 - self.a_tol and
                      self.xyz[j, 0] < x1 + self.a_tol):
                    if j in self.x_dofs:
                        dofs[2].append(j)
                elif (df.near(self.xyz[j, 0], x0, self.a_tol) and
                      df.near(self.xyz[j, 2], z, self.a_tol) and
                      self.xyz[j, 1] > y0 - self.a_tol and
                      self.xyz[j, 1] < y1 + self.a_tol):
                    if j in self.y_dofs:
                        dofs[3].append(j)

            tx_len = (2. * (np.abs(self.FE.start[1] - self.FE.stop[1])) +
                      2. * (np.abs(self.FE.start[0] - self.FE.stop[0])))

            all_dof = []
            for side in range(4):
                n_local_source_dofs = MPI.COMM_WORLD.gather(len(dofs[side]),
                                                            root=0)
                all_dof.append(MPI.COMM_WORLD.bcast(
                        np.sum(n_local_source_dofs), root=0))

            n_global_sd = sum(all_dof)
            if n_global_sd != 0:
                mpp('...  ' + str(n_global_sd) + ' dofs located. '
                    'Continuing  ...', pre_dash=False)
            else:
                print('Fatal Error! Not a single source dof could be found '
                      'for Tx no."' + str(0) + '". '
                      'This is most probably caused by a wrong *s_type* or '
                      'source geometry (*start, stop* coodinates) definition!')
                raise SystemExit

            for ti in range(self.FE.n_tx):
                for side in range(2):
                    bb[dofs[side]] = -tx_len * (1./(n_global_sd)) *\
                             (1./self.FE.MP.omega) * self.FE.MP.J[ti]
                for side in range(2, 4):
                    bb[dofs[side]] = tx_len * (1./(n_global_sd)) *\
                             (1./self.FE.MP.omega) * self.FE.MP.J[ti]
                    bb.apply('insert')
                    self.b.append(bb)

        else:
            mpp('Total A-phi nodal approach only test-wise implemented for '
                '*hed/line* or *loop_r* sources for now!  Aborting  ...')
            raise SystemExit

    def add_topo(self, tx):

        from custEM.meshgen.dem_interpolator import DEM
        dem = DEM(self.FE.MP.para_dir + '/' +
                  self.FE.MP.mesh_name + '_surf.xyz',
                  centering=False,
                  easting_shift=None, northing_shift=None)
        tx[:, 2] = dem(tx[:, 0], tx[:, 1], rotation=None)
        return(tx)
