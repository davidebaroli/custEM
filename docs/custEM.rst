custEM package
==============

Subpackages
-----------

.. toctree::

   custEM.core
   custEM.fem
   custEM.meshgen
   custEM.misc
   custEM.post
   custEM.tests
   custEM.wrap

Module contents
---------------

.. automodule:: custEM
   :members:
   :undoc-members:
   :show-inheritance:
