# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 1                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.meshgen_utils import line_x, line_y

# create world
M = BlankWorld(name='example_1_mesh', m_dir='./meshes',
               preserve_edges=True)

# add observation line: 10 km x-directed observation line
M.build_surface(insert_lines=[line_y(start=-0.5, stop=0.5, n_segs=1),
                              line_x(start=-5e3, stop=-2e1, n_segs=498),
                              line_x(start=2e1, stop=5e3, n_segs=498)])

# build halfspace mesh and extend 2D surface mesh to 3D world
M.build_halfspace_mesh()

# call TetGen
M.call_tetgen(tet_param='-pq1.3aA', export_vtk=True)
