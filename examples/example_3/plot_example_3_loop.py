# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                     visualization script                    # # # # #
# ########################################################################### #

import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import Plot_FD as Plot
from matplotlib import colors as cl

# define matplotlib parameters and model, mesh and key names
plt.ioff()
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 7, 9
clrs = cl.CSS4_COLORS

# ####################  specify which data to be plotted  ################### #

EH = 'H'
pf_EH = 'E'
mod1 = 'example_3_p1_aniso' + pf_EH
mod2 = 'example_3_p2_aniso' + pf_EH
mesh1 = 'example_3_mesh_p1_loop'
mesh2 = 'example_3_mesh_p2_loop'

linE1 = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_200_line_x'
linE2 = 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_200_line_x'
linE = linE1

t1, t2, t3 = 'T1', 'T2', 'T3'
s1, s2, s3, s4, s5 = 'S1', 'S2', 'S3', 'S4', 'S5'
t11, t22, t33 = 'T11', 'T22', 'T33'
s11, s22, s33, s44, s55 = 'S11', 'S22', 'S33', 'S44', 'S55'

# ############################## import data ################################ #

P = Plot(mod=mod1, mesh=mesh1, approach='E_t',
         r_dir='./results', s_dir='./plots', label_color='#002C72')

# # # # # # # # # # # # # #           p1          # # # # # # # # # # # # # # #

P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='E_t', key=t11)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='Am_t', key=t22)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='An_t', key=t33)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='E_s', key=s11)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='H_s', key=s22)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='Am_s', key=s33)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='An_s', key=s44)
P.import_line_data(linE, mesh=mesh1, mod=mod1, approach='Tm_s', key=s55)

# # # # # # # # # # # # # #           p2          # # # # # # # # # # # # # # #

#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='E_t', key=t1)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='Am_t', key=t2)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='An_t', key=t3)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='E_s', key=s1)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='H_s', key=s2)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='Am_s', key=s3)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='An_s', key=s4)
#P.import_line_data(linE, mesh=mesh2, mod=mod2, approach='Tm_s', key=s5)

# ########################### plot total fields ############################# #

# # # # # # # # # # # # # #           p1          # # # # # # # # # # # # # # #

P.plot_line_data(key=t11, EH=EH, label='E_t_p1', lw=1., ls='-',
                 color='k', ylim=[1e-10, 1e-2])
P.plot_line_data(key=t22, EH=EH, new=False, label='Am_t_p1',
                 lw=0.5, ls='-', color=clrs['orange'])
P.plot_line_data(key=t33, EH=EH, new=False, label='An_t_p1',
                 lw=1., ls=':', color='m')
P.plot_line_data(key=s11, EH=EH, new=False, label='E_s_p1',
                 lw=0.5, ls='-', color='c')
P.plot_line_data(key=s22, EH=EH, new=False, label='H_s_p1',
                 lw=0.5, ls='-', color='m')
P.plot_line_data(key=s33, EH=EH, new=False, label='Am_s_p1',
                 lw=0.5, ls='-', color='k')
P.plot_line_data(key=s44, EH=EH, new=False, label='An_s_p1',
                 lw=1., ls=':', color='r')
P.plot_line_data(key=s55, EH=EH, new=False, label='Tm_s_p1',
                 lw=1., ls=':', color='c')
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
                    hspace=0.14)

#plt.savefig('./plots/tf_p1.pdf', bbox_inches='tight', pad_inches=0)
#
## # # # # # # # # # # # # #           p2          # # # # # # # # # # # # # # #

#P.plot_line_data(key=t1, EH=EH, label='E_t_p2', lw=1., ls='-',
#                 color='b', ylim=[1e-10, 1e-2])
#P.plot_line_data(key=t2, EH=EH, new=False, label='Am_t_p2',
#                 lw=0.5, ls='-', color=clrs['orange'])
#P.plot_line_data(key=t3, EH=EH, new=False, label='An_t_p2',
#                 lw=1., ls=':', color='m')
#P.plot_line_data(key=s1, EH=EH, new=False, label='E_s_p2',
#                 lw=1., ls=':', color='y')
#P.plot_line_data(key=s2, EH=EH, new=False, label='H_s_p2',
#                 lw=1., ls=':', color='g')
#P.plot_line_data(key=s3, EH=EH, new=False, label='Am_s_p2',
#                 lw=0.5, ls='-', color='k')
#P.plot_line_data(key=s4, EH=EH, new=False, label='An_s_p2',
#                 lw=1., ls=':', color='r')
#P.plot_line_data(key=s5, EH=EH, new=False, label='Tm_s_p2',
#                 lw=1., ls=':', color='c')
#plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
#                    hspace=0.14)
#plt.savefig('./plots/tf_p2.pdf', bbox_inches='tight', pad_inches=0)

# ######################### plot secondary fields ########################### #

# # # # # # # # # # # # # #           p1          # # # # # # # # # # # # # # #

#P.plot_line_data(key=s11, EH=EH, label='E_s_p1', ylim=[1e-10, 1e-2],
#                 lw=0.5, ls='-', color='c', sf=True)
#P.plot_line_data(key=s22, EH=EH, new=False, label='H_s_p1',
#                 lw=0.5, ls=':', color='b', sf=True)
#P.plot_line_data(key=s33, EH=EH, new=False, label='Am_s_p1',
#                 lw=1., ls=':', color='k', sf=True)
#P.plot_line_data(key=s44, EH=EH, new=False, label='An_s_p1',
#                 lw=0.5, ls=':', color='m', sf=True)
#P.plot_line_data(key=s55, EH=EH, new=False, label='Tm_s_p1',
#                 lw=1., ls=':', color='r', sf=True)
#plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
#                    hspace=0.14)
#plt.savefig('./plots/sf_p1.pdf', transparent=True, bbox_inches='tight')
#
## # # # # # # # # # # # # #           p2          # # # # # # # # # # # # # # #
#
#P.plot_line_data(key=s1, EH=EH, label='E_SF_p2', ylim=[1e-10, 1e-2], lw=0.5,
#                 ls='-', color='m', sf=True)
#P.plot_line_data(key=s2, EH=EH, new=False, label='H_SF_p2', lw=0.5,
#                 ls=':', color='r', sf=True)
#P.plot_line_data(key=s3, EH=EH, new=False, label='Am_SF_p2', lw=0.5,
#                 ls='-', color='y', sf=True)
#P.plot_line_data(key=s4, EH=EH, new=False, label='An_SF_p2', lw=0.5,
#                 ls=':', color='c', sf=True)
#P.plot_line_data(key=s5, EH=EH, new=False, label='Tm_SF_p2', lw=0.5,
#                 ls='-', color='r', sf=True)
#plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
#                    hspace=0.14)
#plt.savefig('./plots/sf_p2.pdf', transparent=True, bbox_inches='tight')

plt.ion()   # uncomment to enable showing the plot
plt.show()  # uncomment to enable showing the plot
