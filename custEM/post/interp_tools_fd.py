# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import numpy as np
import custEM as ce
from custEM.misc import mpi_print as mpp
from custEM.misc import run_serial, run_multiple_serial as rs_multiple
from custEM.core import MOD
from mpi4py import MPI


class FrequencyDomainInterpoator:

    """
    Interpolator class for interpolating 3D modeling results on arbitrary
    meshes, which can be generated using this class as well.

    Notice:
    -------

    At least up to version 2018.1, FEniCS does not support interpolation
    between different meshes in parallel! Therefore, interpolation is always
    conducted in serial and might take some time. Multi-threading can be used
    to run several interpolation tasks simultaneously!

    Class internal functions:
    -------------------------

    - update_interpolation_parameters()
        update interpolation parameters such as line or slice information for
        corresponding mesh generation, if necessary, and interpolation.

    - create_line_meshes()
        create straight line meshes in serial. in addtion, horizontal lines
        following topography are supported as well. Arbitrary paths to inter-
        polate on can be implemented straigthforward, but not done yet.

    - create_slice_meshes()
        create straight slice meshes in serial. In addtion, horizontal slices
        following topography are supported as well.

    - interpolate()
        this function is a wrapper to call the *interpolate_in_serial.py* file,
        which calls the FEniCS interpolation routines.

    - interpolate_default()
        calls the **interpolate()** function several times for interpolation
        on a default set of lines or slices using multi-threading.

    - init_default_interpolation_meshes()
        initialize a default set of interpolation meshes and tag the latter.

    - find_quantity_in_fenics_instance()
        utility function to access field data via strings.

    - write_serial_calculation_parameters()
        write a config file with parameters to be read for serial interpolation
        from the *interpolate_in_serial.py* file.

    Example:
    --------

    >>> M = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
    >>>         owerwrite=False, load_existing=False)
    >>> M.IB.init_default_interpolation_meshes()
    >>> M.IB.interpolate_default(target_type='lines', interp_meshes='small')
    >>> M.IB.interpolate_default(target_type='lines', interp_meshes='large')
    >>> M.IB.interpolate_default(target_type='slices', interp_meshes='small')
    >>> M.IB.interpolate_default(target_type='slices', interp_meshes='large')
    >>>
    >>> M.IB.create_line_meshes('x', x0=-5e3, x1=5e3, z=50.)
    >>> M.IB.interpolate(
    >>>     'E_t', 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_ne_400_on_topo_line_x')
    """

    def __init__(self, PP, dg_interpolation):

        """
        The Interpolator class is initialized via the **MOD** class.

        Required Arguments:
        -------------------

        - PP; type class
            **Pre**- or **PostProcessing** instance.

        - MP, type class.
            **ModelParamters** instance.

        """

        self.PP = PP
        self.MP = PP.MP
        self.x0, self.x1 = -1e4, 1e4
        self.y0, self.y1 = -1e4, 1e4
        self.z0, self.z1 = -1e4, 1e4
        self.x, self.y, self.z = 0., 0., 0.
        self.n_segs = 100
        self.dim = 1e4
        self.tol = 1e-2
        self.update_print_flag = False
        self.rank = 0
        self.icomms = []
        self.max_procs = df.MPI.size(self.MP.mpi_cw)
        self.max_new_procs_spawn = 0
        self.on_topo = True
        self.on_water_surface = False
        self.dg_interpolation = dg_interpolation
        self.interp_p = 1
        if str(self.MP.topo) == 'None':
            self.on_topo = False

        self.t_dir = self.MP.para_dir
        self.interp_dir = (self.MP.r_dir + '/' + self.MP.approach + '/' +
                           self.MP.mesh_name +
                           '_results/' + self.MP.mod_name + '_interpolated')
        if os.path.isfile(self.MP.para_dir + '/interp_dict.json'):
            os.remove(self.MP.para_dir + '/interp_dict.json')

    def update_interpolation_parameters(self, **interp_kwargs):

        """
        update and check given keyword arguments for the Interpolator class.

        Notice:
        -------

        - All interpolation parameters can be specified individually when
        calling the line - or  slice-mesh generation functions. These values
        are just specified as class attributes to have reasonable default
        parameters available.

        Keyword arguments:
        ------------------

        - x0, x1, y0, y1, z0, z1 = +-1e4, type float
            limits ('0', '1') on the axis for generating line interpolation
            meshes which follow either the 'x', 'y' or 'z' axis.

        - x, y, z = 0., type float
            either offset for line interpolation meshes in not-the-axis
            direction or offset of slice interpolation meshes in
            slice-normal direction.

        - n_segs = 400, type int
            number of elements ('discretization') for interpolation meshes.
            Note: For slice meshes the final number of elements would be
            **ne x ne x 2**! (**ne** in x-dir, **ne** in y-dir, triangles *2).

        - dim = 1e4, type float
            Defines the dimension of slices. So far, only quadrangular slice
            meshes with the area 2*dim x 2*dim are supported.

        - tol = 0.01, type float
            numerical tolerance for cutting the line- or slice interpolation
            meshes. Does not need to be changed in general.

        - update_print_flag = False, type bool
            define if updated interpolation keyword arguments should be printed
            or not. By defualt, the latter are not printed when initialzing the
            class for the first time but afterwards, this flag is set **True**.

        - self.on_topo = True, type bool
            if the mesh has topography in z-direction, horizontal line or slice
            interpolation meshes at the surface are automatically adjusted to
            match the crooked surface or follow the surface with a parallel
            offset.

        - max_procs = "MPI_SIZE", type df.MPI.size(df.mpi_comm_world())
            number of parallel processes used during the *mpirun* call
        """

        for key in interp_kwargs:
            if key not in self.__dict__:
                mpp('Warning! Unknown interpolation parameter set:', key)

        self.__dict__.update(interp_kwargs)

        if self.update_print_flag is False:
            mpp('Interpolation parameters update:')
            for k, v in sorted(self.__dict__.items()):
                mpp('-->  {:<22}'.format(str(k)) + '  =  ', v, pre_dash=False)
        self.update_print_flag = True

    def create_line_meshes(self, axes, line_name=None, **interp_kwargs):

        """
        Create line interpolation meshes. So far, only lines parallel
        to the coordinate axes are supported. Topography for horizontal lines
        is also supported.

        Required arguments:
        -------------------

        - axes, type str
            a string which specifies parallel to which axis a line mesh should
            be generated. The string must be a combination or permutation of
            'xyz', so a triple of line meshes along each coordinate axis can be
            generated at once.

        Keyword arguments:
        ------------------

        - line_name = None, type str
            specify if a custom name for the interpolation mesh if desired.

        - **interp_kwargs,
            see **update_interpolation_parameters** docs.

        Example:
        --------

        >>> M = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
        >>>         owerwrite=False, load_existing=False)
        >>> M.IB.create_line_meshes('xy', x0=-5e3, x1=5e3, z=50., y0=0.0,
        >>>                         y1=1e4)
        >>> # The command above creates two line meshes in 50m height in x- and
        >>> # y-direction, with the x-limits [-5e3,5e3] and y-limits [0., 1e4].
        """

        dummy = None
        if 'on_topo' in interp_kwargs.keys():
            if interp_kwargs['on_topo'] != self.on_topo and self.on_topo:
                dummy = True
            elif interp_kwargs['on_topo'] != self.on_topo and not self.on_topo:
                dummy = False

        self.update_interpolation_parameters(**interp_kwargs)
        self.write_serial_calculation_parameters()
        script = os.path.dirname(ce.__file__) + '/post/build_line_mesh.py'
        if self.max_procs != 1:
            run_serial(script, ['-d'], [self.MP.para_dir],
                               ['-a'], [axes],
                               ['-o'], [str(line_name)])
        else:
            try:
                run_serial(script, ['-d'], [self.MP.para_dir],
                                   ['-a'], [axes],
                                   ['-o'], [str(line_name)])
            except:
                mpp('Interpolation mesh generation currently not available '
                    'using *jupyter notebooks*!')
        if dummy is not None:
            self.on_topo = dummy

    def create_slice_meshes(self, axes, slice_name=None, **interp_kwargs):

        """
        Create slice interpolation meshes. So far, only staight slices parallel
        to the coordinate axes are supported. Topography for z-normal slices is
        supported as well.

        Required arguments:
        -------------------

        - axes, type str
            a string which specifies orthogonal to which axis a slice mesh
            should be generated. The string must be a combination or
            permutation of 'xyz', so a triple of slice meshes along each
            coordinate axis can be generated at once.

        Keyword arguments:
        ------------------

        - slice_name = None, type str
            specify if a custom name for the interpolation mesh if desired.

        - **interp_kwargs,
            see **update_interpolation_parameters** docs.

        Example:
        --------

        >>> M = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
        >>>         owerwrite=False, load_existing=False)
        >>> M.IB.create_slice_meshes('xyz', ne=50, dim=1e3)
        >>> # The command above creates slice meshes orthogonal to the coord.
        >>> # axes with a grid of 51 x 51 nodes. The slice has dimensions of
        >>> # 2 x 2 km, from [-1e3, -1e3] to [1e3, 1e3]
        """

        dummy = None
        if 'on_topo' in interp_kwargs.keys():
            if interp_kwargs['on_topo'] != self.on_topo and self.on_topo:
                dummy = True
            elif interp_kwargs['on_topo'] != self.on_topo and not self.on_topo:
                dummy = False

        self.update_interpolation_parameters(**interp_kwargs)
        self.write_serial_calculation_parameters()
        script = os.path.dirname(ce.__file__) + '/post/build_slice_mesh.py'
        if self.max_procs != 1:
            run_serial(script, ['-d'], [self.MP.para_dir],
                               ['-a'], [axes],
                               ['-o'], [str(slice_name)])
        else:
            try:
                run_serial(script, ['-d'], [self.MP.para_dir],
                                   ['-a'], [axes],
                                   ['-o'], [str(slice_name)])
            except:
                mpp('Interpolation mesh generation currently not available '
                    'using *jupyter notebooks*!')
        mpp('Done')
        if dummy is not None:
            self.on_topo = dummy

    def create_path_mesh(self, points, path_name=None, **interp_kwargs):

        """
        Create path interpolation mesh to interpolate on arbitraily
        distributed points.

        Required arguments:
        -------------------

        - points, type array of shape (n, 3)
            array containing the 3D points to build the path mesh.

        Keyword arguments:
        ------------------

        - path_name = None, type str
            specify a path name for the interpolation mesh, recommended.

        - **interp_kwargs,
            see **update_interpolation_parameters** docs.

        Example:
        --------

        >>> M = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
        >>>         owerwrite=False, load_existing=False)
        >>> M.IB.create_path_mesh('xyz', ne=50, dim=1e3)
        >>> # The command above creates slice meshes orthogonal to the coord.
        >>> # axes with a grid of 51 x 51 nodes. The slice has dimensions of
        >>> # 2 x 2 km, from [-1e3, -1e3] to [1e3, 1e3]
        """

        dummy = None
        if 'on_topo' in interp_kwargs.keys():
            if interp_kwargs['on_topo'] != self.on_topo and self.on_topo:
                dummy = True
            elif interp_kwargs['on_topo'] != self.on_topo and not self.on_topo:
                dummy = False

        self.update_interpolation_parameters(**interp_kwargs)
        self.write_serial_calculation_parameters()
        if df.MPI.rank(self.MP.mpi_cw) == 0:
            np.save(self.MP.para_dir + '/points.npy', points)
        else:
            pass
        script = os.path.dirname(ce.__file__) + '/post/build_path_mesh.py'
        if self.max_procs != 1:
            run_serial(script, ['-d'], [self.MP.para_dir],
                               ['-o'], [str(path_name)])
        else:
            try:
                run_serial(script, ['-d'], [self.MP.para_dir],
                                   ['-o'], [str(path_name)])
            except:
                mpp('Interpolation mesh generation currently not available '
                    'using *jupyter notebooks*!')
        mpp('Done')
        if dummy is not None:
            self.on_topo = dummy

    def interpolate(self, quantity, interp_mesh, mute=True, interp_p=None,
                    max_new_procs_spawn=None, use_root=False, fs_type=None,
                    dg_interpolation=None, export_name=None, interp_dir=None):

        """
        Customized interpolation function.

        Required arguments:
        -------------------

        - quantitiy, type str
            Either **E_t, H_t, E_s** or **H_s**.

        - interp_mesh, type str
            name of the mesh to interpolate on.

        Keyword arguments:
        ------------------

        - mute = False, type bool
            set True, if info-interpolation messages should not be printed.

        - interp_p = 2, type int
            polyonimal order of interpolation mesh *FunctionSpaces*

        - max_new_procs_spawn = None, type int
            specify how many additional processes should be spawned in
            addition to the number of procs already used in the mpirun call,
            usually it is not necessary to spawn additional processes for
            significant acceleration of all interpolation tasks.
            Notice! This might lead to random synchronization issues.

        - use_root = False, type bool
            Use also the root process for serial interpolation tasks. Setting
            this flag to **True** has no effect if **max_new_procs_spawn**
            is not **0**!

        - export_name = None, type str
            specify a custom export name of the interpolated data if desired.

        - interp_dir, type str
            specify, if a custom interpolation directory is desired, otherwise,
            the defualt interpolation directory is located in the corresponding
            results directory

        - dg_interpolation = None, type bool
            set True or False to overwrite *self.dg_interpolation* for
            manually enabling discontinuous or continuous interpolation

        Example:
        --------

        >>> # The following code uses multi-threading to interpolate E- and
        >>> # H-fields (each one in serial) simultaneously if an 'mpirun' call
        >>> # was chosen. Note that multithreading is 'True' by default.
        >>> M = MOD('Test_1', 'halfspace_mesh', 'E_t', p=1,
        >>>         owerwrite=False, load_existing=False)
        >>> i_mesh = 'default_small_slice_z'
        >>> M.IB.interpolate('E_t', i_mesh, export_name='E_t_slice_z')
        >>> M.IB.interpolate('H_t', i_mesh, export_name='H_t_slice_z')
        >>> M.IB.synchronize()
        """

        if export_name is None:
            export_name = quantity + '_on_' + interp_mesh
        if interp_dir is not None:
            self.interp_dir = interp_dir

        if interp_p is None:
            interp_p = self.interp_p
        if max_new_procs_spawn is None:
            max_new_procs_spawn = self.max_new_procs_spawn

        if dg_interpolation is None:
            dg_interp = self.dg_interpolation
        else:
            dg_interp = dg_interpolation

        if fs_type is None:
            fs_type = self.PP.fs_type

        if max_new_procs_spawn != 0:
            if len(self.icomms) >= max_new_procs_spawn:
                mpp('Waiting for simultaneously running interpolation '
                    'tasks to be finished! If might be considered to increase '
                    'the value of "max_new_procs_spawn".')
                self.synchronize()
                self.rank = 0

        if not mute:
            mpp('Interpolation export directory is: "' + self.interp_dir[:-1] +
                ', "files named as: "' + export_name + '"', post_dash=True,
                barrier=False)

        if df.MPI.rank(self.MP.mpi_cw) == 0:
            if not os.path.isdir(self.interp_dir):
                os.mkdir(self.interp_dir)
        else:
            pass

        if interp_mesh.find('line') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/lines'
        elif interp_mesh.find('slice') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/slices'
        elif interp_mesh.find('path') is not -1:
            self.interp_mesh_dir = self.MP.m_dir + '/paths'
        else:
            mpp('MeshName Error: Interpolation target mesh name must '
                'end either with "line", "slice" or "path" so far!',
                post_dash=True)
            raise SystemExit

        if os.path.isfile(self.interp_mesh_dir + '/' +
                          interp_mesh + '.xml') is False:
            mpp('Warning! Interpolation mesh "' + str(interp_mesh) + '.xml" '
                'could not be found. Continuing ...')
            return

        mpp('...  interpolating ' + quantity + ' on ' + '"' + interp_mesh +
            '"  ...', pre_dash=False, barrier=False)

        self.rank += 1
        if self.max_procs == 1 and max_new_procs_spawn == 0:
            use_root = True
        if use_root and max_new_procs_spawn == 0:
            self.rank -= 1

        if df.MPI.rank(self.MP.mpi_cw) == self.rank:
            for jj in range(self.MP.n_tx):
                if self.PP.FS.test_mode:
                    tm = True
                else:
                    tm = False
                M = MOD(self.MP.mod_name, self.MP.mesh_name,
                        self.MP.approach, overwrite=False, p=self.PP.FS.p,
                        file_format=self.MP.file_format, self_mode=True,
                        load_existing=str(jj), r_dir=self.MP.r_dir,
                        para_dir=self.MP.para_dir, mute=True, test_mode=tm,
                        m_dir=self.MP.m_dir, field_selection=quantity,
                        dg_interpolation=self.dg_interpolation,
                        fs_type=fs_type)

                M.IB.find_quantity_in_fenics_instance(quantity, fs_type)
                if M.IB.q1 is None or M.IB.q2 is None:
                    print('Warning! "' + quantity + '"could not be imported. '
                          'Skipping this interpolation and continuing  ...')
                    break
                target_mesh = df.Mesh(self.MP.mpi_cs, self.interp_mesh_dir +
                                      '/' + interp_mesh + '.xml')

                if dg_interp:
                    V_target = df.VectorFunctionSpace(
                            target_mesh, "DG", interp_p)
                else:
                    V_target = df.VectorFunctionSpace(
                            target_mesh, "CG", interp_p)

                data_interp1 = df.interpolate(M.IB.q1, V_target)
                q1_data = np.hstack((data_interp1.vector().get_local(
                        ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                        ).reshape(-1, 3)[0::3, :]))
                data_interp2 = df.interpolate(M.IB.q2, V_target)
                q2_data = np.hstack((data_interp2.vector().get_local(
                        ).reshape(-1, 3), V_target.tabulate_dof_coordinates(
                        ).reshape(-1, 3)[0::3, :]))
                complex_data = q1_data + 1j * q2_data
                if self.MP.n_tx != 1:
                    exp_name = 'tx_' + str(jj) + '_' + export_name
                else:
                    exp_name = export_name
                df.File(self.MP.mpi_cs, str(self.interp_dir) + '/' +
                        exp_name + '_real.pvd') << data_interp1
                df.File(self.MP.mpi_cs, str(self.interp_dir) + '/' +
                        exp_name + '_imag.pvd') << data_interp2
                np.save(str(self.interp_dir) + '/' + exp_name + '.npy',
                        complex_data)
        else:
            pass
        if self.rank == self.max_procs - 1 and max_new_procs_spawn == 0:
            self.rank = 0
            mpp('Waiting for all mpi processes to finish serial interpolation '
                'tasks...\nNotice: For a greater amount of '
                'interpolation tasks it might be considered to change the '
                '"max_new_procs_spawn" flag.')
        elif self.rank >= self.max_procs - 1 and max_new_procs_spawn != 0:
            spawn_rank = self.rank - self.max_procs + 1
            self.write_serial_calculation_parameters(rank=spawn_rank)
            if df.MPI.rank(self.MP.mpi_cw) == 0:
                print('Spawning additional process to accelerate all '
                      'interpolation tasks in multithreading mode ...')
            self.icomms.append(
                    rs_multiple(os.path.dirname(ce.__file__) +
                                '/post/serial_interpolation.py',
                                ['-d'], [self.MP.para_dir],
                                ['-q'], [quantity],
                                ['-i'], [interp_mesh],
                                ['-j'], [self.interp_mesh_dir],
                                ['-o'], [export_name],
                                ['-r'], [str(spawn_rank)]))

    def interpolate_default(self, target_type='lines', interp_meshes='small',
                            var='EH', sync=False):

        """
        Calling default interpolation tasks.

        Keyword arguments:
        ------------------

        - target_type = 'lines', type str
            Either **lines** or **slices**. See also the docs of the
            **init_default_interpolation_meshes()** function.

        - interp_meshes = 'small' type str
            name of a set of default line or slice interpolation meshes. See
            also the docs of **init_default_interpolation_meshes()**.

        - var = 'EH', type str
            a combination of **E** and **H** to specify if both types of fields
            or solely one type should be interpolated with default names on the
            specified default set of interpolation meshes.

        - sync = True, type bool
            set to **False**, if you want to add another set of default
            interpolation tasks after the current ones to the multi-threading
            process afterwards (e.g. run interpolation on default 'small' and
            'large' interpolation meshes simultaneously).
        """

        if target_type == 'lines':
            if interp_meshes is 'small':
                mpp('Interpolating on "small" default "line" meshes: ...',
                    barrier=False)
                x_mesh = 'default_small_line_x'
                y_mesh = 'default_small_line_y'
                z_mesh = 'default_small_line_z'
            elif interp_meshes is 'large':
                mpp('Interpolating on "large" default "line" meshes: ...',
                    barrier=False)
                x_mesh = 'default_large_line_x'
                y_mesh = 'default_large_line_y'
                z_mesh = 'default_large_line_z'
            else:
                mpp('Interpolating on custom meshes: ...\n' +
                    'x: -->  ' + interp_meshes[0] + '\n' +
                    'y: -->  ' + interp_meshes[1] + '\n' +
                    'z: -->  ' + interp_meshes[2], barrier=False)

                x_mesh = interp_meshes[0]
                y_mesh = interp_meshes[1]
                z_mesh = interp_meshes[2]

        elif target_type == 'slices':
            if interp_meshes is 'small':
                mpp('Interpolating on "small" default "slice" meshes: ...',
                    barrier=False)
                x_mesh = 'default_small_slice_x'
                y_mesh = 'default_small_slice_y'
                z_mesh = 'default_small_slice_z'
            elif interp_meshes is 'large':
                mpp('Interpolating on "large" default "slice" meshes: ...',
                    barrier=False)
                x_mesh = 'default_large_slice_x'
                y_mesh = 'default_large_slice_y'
                z_mesh = 'default_large_slice_z'
            else:
                mpp('Interpolating on custom meshes: ...\n' +
                    'x: -->  ' + interp_meshes[0] + '\n' +
                    'y: -->  ' + interp_meshes[1] + '\n' +
                    'z: -->  ' + interp_meshes[2], barrier=False)

                x_mesh = interp_meshes[0]
                y_mesh = interp_meshes[1]
                z_mesh = interp_meshes[2]

        else:
            mpp('Typing error! Choose valid target mesh type, either '
                '"lines" or "slices".')
            raise SystemExit

        if self.PP.FS.p == 2:
            mpp("### interpolation for polynomils of 2nd order may take "
                "several minutes, don't worry ... ###")

        if var is 'E' or var is 'EH':
            if hasattr(self.PP, 'E_t_r_cg') or hasattr(self.PP, 'E_t_r'):
                self.interpolate('E_t', x_mesh, export_name='E_t_on_' + x_mesh)
                self.interpolate('E_t', y_mesh, export_name='E_t_on_' + y_mesh)
                self.interpolate('E_t', z_mesh, export_name='E_t_on_' + z_mesh)
            else:
                mpp('Warning!, "E_t" does not exist, this should be the '
                    'case if the *H-field* approach was used without '
                    'conversion to *E*-fields ! continuing ...', barrier=False)

            if '_s' in self.MP.approach:
                if hasattr(self.PP, 'E_s_r_cg') or hasattr(self.PP, 'E_s_r'):
                    self.interpolate('E_s', x_mesh,
                                     export_name='E_s_on_' + x_mesh)
                    self.interpolate('E_s', y_mesh,
                                     export_name='E_s_on_' + y_mesh)
                    self.interpolate('E_s', z_mesh,
                                     export_name='E_s_on_' + z_mesh)
                else:
                    mpp('Warning! "E_s" does not exist,'
                        ' this should be the case if no anomaly is set for'
                        ' "Secondary" field formulations! continuing ...',
                        barrier=False)
        if var is 'H' or var is 'EH':
            if hasattr(self.PP, 'H_t_r_cg') or hasattr(self.PP, 'H_t_r'):
                self.interpolate('H_t', x_mesh,
                                 export_name='H_t_on_' + x_mesh)
                self.interpolate('H_t', y_mesh,
                                 export_name='H_t_on_' + y_mesh)
                self.interpolate('H_t', z_mesh,
                                 export_name='H_t_on_' + z_mesh)
            else:
                mpp('Warning! "H_t" does not exist, this should be the '
                    'case if no *H*-fields were exported! continuing ...',
                    barrier=False)
            if '_s' in self.MP.approach:
                if hasattr(self.PP, 'H_s_r_cg') or hasattr(self.PP, 'H_s_r'):
                    self.interpolate('H_s', x_mesh,
                                     export_name='H_s_on_' + x_mesh)
                    self.interpolate('H_s', y_mesh,
                                     export_name='H_s_on_' + y_mesh)
                    self.interpolate('H_s', z_mesh,
                                     export_name='H_s_on_' + z_mesh)
                else:
                    mpp('Warning! "H_s" does not exist, this should be the '
                        'case if no anomaly is set for "Secondary" field '
                        'formulations! continuing ...', barrier=False)
        if sync:
            self.synchronize()
            mpp('... "default" interpolations finished, continuing ...')

    def init_default_interpolation_meshes(self):

        """
        Initialize a default set of 'small' and 'large' line and slice
        interpolation meshes.
        """

        # # # # hacked
        dummy = False
        if self.on_topo:
            dummy = True
            self.on_topo = False

        self.create_line_meshes('xyz', line_name='default_large')
        self.create_line_meshes('xyz', x0=-2000., x1=2000.,
                                y0=-2000., y1=2000., z0=-2000., z1=2000.,
                                line_name='default_small')
        self.create_slice_meshes('xyz', slice_name='default_large')
        self.create_slice_meshes('xyz', dim=2000.0, slice_name='default_small')
        if dummy:
            self.on_topo = True
        # # # # hacked

    def find_quantity_in_fenics_instance(self, string, fs_type):

        """
        Utility function to access *dolfin* solution functions via a string.

        Required arguments:
        -------------------

        - string, type str
            Either **'E_t, 'E_s', 'H_t'** or  **'H_s'**.
        """

        if fs_type is 'None' or 'ned' in self.PP.fs_type.lower():
            if string == 'E_t':
                self.q1 = self.PP.E_t_r
                self.q2 = self.PP.E_t_i
            elif string == 'E_s':
                self.q1 = self.PP.E_s_r
                self.q2 = self.PP.E_s_i
            elif string == 'H_t':
                self.q1 = self.PP.H_t_r
                self.q2 = self.PP.H_t_i
            elif string == 'H_s':
                self.q1 = self.PP.H_s_r
                self.q2 = self.PP.H_s_i
            else:
                print('Wrong variable name chosen, use either: "E_t", '
                      '"E_s", "H_t", "H_s"')
                raise SystemExit
            return

        if fs_type is 'None' or 'cg' in self.PP.fs_type.lower():
            if string == 'E_t':
                self.q1 = self.PP.E_t_r_cg
                self.q2 = self.PP.E_t_i_cg
            elif string == 'E_s':
                self.q1 = self.PP.E_s_r_cg
                self.q2 = self.PP.E_s_i_cg
            elif string == 'H_t':
                self.q1 = self.PP.H_t_r_cg
                self.q2 = self.PP.H_t_i_cg
            elif string == 'H_s':
                self.q1 = self.PP.H_s_r_cg
                self.q2 = self.PP.H_s_i_cg
            else:
                print('Wrong variable name chosen, use either: "E_t", '
                      '"E_s", "H_t", "H_s"')
                raise SystemExit

    def write_serial_calculation_parameters(self, rank=0):

        """
        Utility function to store interpolation parameters for line or slice
        mesh generation of interpolation in serial in a temporary file.
        """

        if df.MPI.rank(self.MP.mpi_cw) == 0:
            import json
            A = self.__dict__.copy()
            del A['PP']
            del A['MP']
            if 'q1' in A.keys():
                del A['q1']
            if 'q2' in A.keys():
                del A['q2']
            if 'MOD' in A.keys():
                del A['MOD']
            A['icomms'] = None
            A.update(self.MP.__dict__)
            A['topo'] = str(A['topo'])
            A['p'] = self.PP.FS.p
            del A['sigma']
            del A['sigma_air']
            del A['sigma_ground']
            del A['sigma_anom']
            del A['delta_sigma']
            del A['mpi_cw']
            del A['mpi_cs']
            with open(self.MP.para_dir + "/interp_dict_" + str(rank) +
                      ".json", "w") as outfile:
                json.dump(A, outfile, indent=0)
        else:
            A = []
        mpp('...  interpolation parameters dumped to file.', pre_dash=False)

    def synchronize(self):

        """
        Synchronize multiple processes via the intercommunicator object
        (see MPI docs), which were called during multithreading tasks.
        """

        mpp('  -  interpolation may take a while, in particular with p2 on '
            'slices  -', pre_dash=False)
        if MPI.COMM_WORLD.Get_rank() == 0:
            for ic in self.icomms:
                ic.Barrier()
                ic.Disconnect()
        else:
            pass
        self.icomms = []
        self.rank = 0
        mpp('...  synchronization finished.', pre_dash=False, post_dash=True)
