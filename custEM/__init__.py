# -*- coding: utf-8 -*-

"""
@author: Rochlitz.R
"""

import sys
import os

sys.path.append(os.path.dirname(__file__))

from . import fem
from . import core
from . import post
from . import misc
from . import wrap
from . import meshgen
from . import tests
import sys
import os

def run_tests():

    """
    Starting test functions for conda builds.
    """

    import custEM
    
    cwd = os.getcwd()
    os.chdir(os.path.dirname(custEM.__file__))
    os.chdir('tests')
    mode = os.stat('run_all_tests.sh').st_mode
    mode |= (mode & 0o444) >> 2
    os.chmod('run_all_tests.sh', mode)
    os.system('bash run_all_tests.sh')
    os.chdir(cwd)


# define custEM version
with open(os.path.dirname(__file__) + '/VERSION.rst', 'r') as v_file:
    version = v_file.readline()[:7]
    release_date = v_file.readline()[:10]
    
__version__ = version 
# __release__ = release_date